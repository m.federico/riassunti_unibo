# Riassunti_unibo

Questi documenti **non** sono riassunti ufficiale, ma raccolgono il contenuto delle slide e degli appunti presi a lezione. Inoltre non hanno la pretesa di essere una sostituzione alle lezioni, in quanto non è possibile raccogliere in tempo utile tutti gli esempi e le precisazioni fatte a lezione, pertanto sono stati riportati in questo documento solamente i principali. Infine non è detto che tutto ciò che è racchiuso all'interno di questi riassunti sia completamente corretto.

Buono studio e che la forza sia con te!