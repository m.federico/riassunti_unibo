\chapter{Modelli e linguaggi di modellazione}

\section{Modelli}
\begin{definizione}{Modello}{}
    Per modello si intende genericamente una rappresentazione di un oggetto o di un fenomeno reale che riproduce caratteristiche o comportamenti ritenuti fondamentali per il tipo di ricerca che si sta svolgendo
\end{definizione}

È un insieme di concetti e proprietà volti a catturare aspetti essenziali di un sistema, collocandosi in uno spazio concettuale preciso. Si tratta di una visione semplificata di un sistema che rende il sistema stesso più accessibile alla comprensione e valutazione, e che facilita il trasferimento dell'informazione e la collaborazione tra persone.

Nel processo di produzione del sofware il lavoro di diversi attori si basa su un insieme di conoscenze implicite all'interno del processo. Lo scopo di un processo basato su modelli è esplicitare tali conoscenze tramite diagrammi formali, ovvero con un linguaggio preciso.

I diagrammi descrivono in modo coinciso e preciso conoscenze del problema, servono per individuare rischi e scelte progettuali. Inoltre, la mente umana trova difficoltà a impostare ragionamenti efficaci con dettagli minuti\footnote{I linguaggi per descrivere i modelli rappresentano il culmine del livello di astrazione raggiunto rispetto alle macchine.}.

I modelli che descrivono un sistema devono fornire una descrizione completa, coerente e non troppo ridondante.

La transizione tra modelli deve essere continua per aiutare la navigazione tra di essi. Le caratteristiche delle connessioni devono essere predicibili e sistematiche. In qualsiasi direzione si percorra la sequenza di modelli generati, deve essere possibile mappare uno o più elementi in un modello in uno o più elementi in un altro. Questo garantisce la coerenza tra i modelli, e crea un percorso logico che va dai requisiti al codice. La tracciabilità permette inoltre di tenere sotto controllo le modifiche fatte. Si tratta tipicamente di un compito difficile.

\section{Linguaggi di modellazione}
\begin{definizione}{Linguaggio di modellazione}{}
    Un linguaggio di modellazione è un linguaggio (semi-) formale che può essere utilizzato per descrivere (modellare) un sistema di qualche natura
\end{definizione}

Nell'ingegneria del software i modelli (software) vengono rappresentati tramite diagrammi realizzati per mezzo di un linguaggio di modellazione\footnote{Lo stesso modello può essere rappresentato mediante linguaggi diversi.}.

Anche il codice costituisce una rappresentazione del modello, tuttavia si tratta di una rappresentazione piatta e fin troppo dettagliata, che pertanto non consente di cogliere a colpo d'occhio i tratti salienti di un modello.

Durante la progettazione occorre usare sia modelli di alto livello per la comunicazione tra progettisti o con il cliente, sia codice per poter effettivamente implementare il modello. Questo crea la necessità di mantenere allineate le diverse rappresentazioni del modello. Una tecnica efficace consiste nel modificare le rappresentazioni di alto livello e solo successivamente apportare le modifiche necessarie al codice.

\section{Modelli di processo}
\begin{definizione}{Processo di sviluppo}{}
    Un processo di sviluppo è un insieme ordinato di passi che  coinvolge tutte quelle attività, vincoli e risorse necessari per produrre il desiderato output a partire dall'insieme dei requisiti in ingresso.

    Tipicamente un processo è composto da differenti fasi messe in relazione una con l'altra Ogni fase identifica una porzione di lavoro che deve essere svolto nel contesto del processo, le risorse che devono essere utilizzate e vincoli a cui si deve ``obbedire''. Ogni fase inoltre può essere composta di più attività che devono essere messe in relazione tra loro
\end{definizione}

Un modello di processo software è una rappresentazione semplificata di un processo presentato da una specifica prospettiva. Un modello di processo prescrive sia l'elenco ordinato di fasi attorno alle quale il processo dovrebbe essere organizzato sia l'interazione e la coordinazione del lavoro tra le diverse fasi.
Un modello di processo definisce dunque un template attorno al quale organizzare e dettagliare un vero processo di sviluppo.

In generale nei vari modelli si incontrano le seguenti fasi:
\begin{itemize}
    \item Specifica: cosa il sistema dovrebbe fare e vincoli di sviluppo
    \item Sviluppo: produzione del sistema software
    \item Validazione: testare che il sistema sviluppato sia quello che il committente voleva
    \item Evoluzione: cambiamenti nel prodotto in accordo a modifiche dei requisiti o incremento delle funzionalità del sistema
\end{itemize}

\subsection{Modello a cascata}
Il modello si fonda sul presupposto che introdurre cambiamenti sostanziali nel software in fasi avanzate dello sviluppo ha costi troppo elevati pertanto, ogni fase deve essere svolta in maniera esaustiva prima di passare alla successiva, in modo da non generare retroazioni. Pertanto le fasi sono distinte e rigorosamente sequenziali.

Le uscite che una fase produce come ingresso per la fase successiva sono i cosiddetti semilavorati del processo di sviluppo.

I limiti del modello sono dati dalla sua rigidità, in particolare da due assunti di fondo molto discutibili:
\begin{itemize}
    \item Immutabilità dell'analisi, secondo cui i clienti sono in grado di esprimere esattamente le loro esigenze e, di conseguenza, in fase di analisi iniziale è possibile definire tutte le funzionalità che il software deve realizzare. In realtà la visione che i clienti hanno del sistema evolve man mano che il sistema prende forma e quindi le specifiche cambiano in continuazione
    \item Immutabilità del progetto, secondo cui è possibile progettare l'intero sistema prima di aver scritto una sola riga di codice. Tuttavia nel campo della progettazione spesso ``le idee migliori vengono in mente per ultime'', quando si comincia
          a vedere qualcosa di concreto. Inoltre, problemi di prestazioni costringono spesso a rivedere le scelte di progetto
\end{itemize}

Per limitare l'insorgenza dei problemi è possibile introdurre limitate retroazioni del modello tra le varie fasi, oppure realizzare un prototipo su cui ragionare con i clienti.

\begin{definizione}{Prototipo}{}
    Modello approssimato dell'applicazione, con l'obiettivo di essere mostrato al cliente al fine di ottenere un'indicazione su quanto il prototipo colga i reali fabbisogni.

    Deve essere sviluppabile in tempi brevi e con costi minimi
\end{definizione}

\subsection{Modelli evolutivi}

I modelli evolutivi consentono di partire da specifiche molto astratte, sviluppando prima un prototipo da sottoporre al cliente che viene poi raffinato iterativamente (sottoponendo sempre i risultati al cliente, raffinamento dell'analisi e del design).

Ci sono diversi modelli evolutivi, ma tutti vertono sul fatto che il prototipo evolva in prodotto finito un po' alla volta.

I modelli iterativi soffrono dei seguenti problemi:
\begin{itemize}
    \item Il processo di sviluppo non p visibile
    \item Il sistema è poco strutturato
    \item È richiesta una particolare abilità nella programmazione
\end{itemize}

Questi modelli vanno bene per sistemi di piccole dimensioni, di breve durata o parti di sistemi più grandi.

Nel caso in cui le iterazioni sono brevissime si parla di extreme programming. Questo tipo di programmazione richiede comunicazione costanti tra sviluppatori e clienti, test più che accurati e sempliceità dell'implementazione.

\subsection{Sviluppo incrementale}

Con questo modello si costruisce il sistema sviluppandone sistematicamente e in sequenza parti ben definite. Una volta costruita una parte, essa non viene più modificata. È di fondamentale importanza essere in grado di specificare perfettamente i requisiti della parte da costruire, prima della sua implementazione.

\subsection{Sviluppo iterativo}

Con questo modello si effettuano molti passi dell'intero ciclo di sviluppo del software, per costruire iterativamente tutto il sistema,
aumentandone ogni volta il livello di dettaglio. Lo sviluppo iterativo non funziona bene per progetti significativi.

\subsection{Sviluppo incrementale-iterativo}

È dato dall'unione dello sviluppo incrmentale con quello iterativo. Sostanzialmente si applica lo sviluppo iterativo alle varie parti identificate tramite lo sviluppo incrmentale.

\subsection{Modelli ibridi}

QUesti modelli vedono i sistemi come composti di sotto-sistemi. Per ogni sotto-sistema è possibile adottare un diverso modello di sviluppo a seconda delle caratteristiche del sotto-sistema stesso. Ad esempio è possibile utilizzare il modello evolutivo per sotto-sistemi con specifiche ad alto rischio, ed uteilizzare il modello a cascata per sotto-sistemi con specifiche ben definite.

Di norma conviene creare e raffinare prototipi funzionanti dell'intero sistema o di sue parti, secondo l'approccio incrementale-iterativo.

\paragraph{Rational Unified Process}
Si tratta di un framework adattabile che può essere usato in progetti di diversi contesti ed è pensato per progetti di grandi dimensioni.

\acrshort{rup_model} presenta tre visioni del processo di svilippo:
\begin{itemize}
    \item Dinamica, mostra le fasi del modello nel tempo
          \begin{itemize}
              \item Inception (Avvio), consiste in una generalizzazione dell'analisi di fattibilità
              \item Elaboration (Elaborazione), consiste nella definizione della struttura complessiva del sistema
              \item Construction (Costruzione), consiste nella progettazione, programmazione e verifica del sistema
              \item Transition (Transizione), durante questa fase il sistema passa dall'ambiente dello sviluppo a quello del cliente finale
          \end{itemize}
    \item Statica, mostra attività del processo coinvolte. Prevede i seguenti workflow:
          \begin{itemize}
              \item Modellazione delle attività aziendali
              \item Requisiti, vengono identificati gli attori che interagiscono con il sistema e sviluppati i casi d'uso per modellare i requisiti
              \item Analisi e Progetto, viene creato e documentato un modello di progetto utilizzando modelli architetturali, dei componenti, degli oggetti e sequenziali
              \item Implementazione: i componenti del sistema sono implementati e strutturati nell'implementazione dei sottosistemi
              \item Test
              \item Rilascio
          \end{itemize}
    \item Pratica, suggerisce buone pratiche da seguire durante il processo. Le pratiche fondamentali sono:
          \begin{itemize}
              \item Sviluppare il software ciclicamente
              \item Gestire i requisiti
              \item Usare architetture basate sui componenti
              \item Creare modelli visivi del software
              \item Verificare la qualità del software
              \item Controllare le modifiche al software
          \end{itemize}
\end{itemize}

\section{Processi di sviluppo}

La definizione di un processo non può prescindere dalle caratteristiche dell'organizzazione in cui esso avviene e dalla competenza ed esperienza degli attori coinvolti.

L'uso di un processo inappropriato riduce la qualità e l'utilità del prodotto software che deve essere sviluppato o migliorato.