\chapter{Principi di design}

Definire la qualità di una progettazione è difficile, in quanto si tratta di un concetto vago che varia da caso a caso e dalle specifiche priorità di una organizzazione. In generale un buon progetto potrebbe essere il più affidabile, il più efficiente o anche ilpiù economico.
Al contrario risulta più semplice definire cosa renda cattiva una progettazione. Alcuni esempi sono:
\begin{itemize}
    \item Mancato soddisfacimento dei requisiti
    \item \textbf{Rigidità del software}, ovvero la tendenza per il software a essere difficile da modificare. Un software si dice rigido quando ogni modifica provoca una cascata di modifiche successive in moduli dipendenti. Come effetto spesso si ha che i manager hanno timore ad accettare che gli sviluppatori risolvano problemi non critici, o che non sanno se/quando gli sviluppatori termineranno le modifiche
    \item \textbf{Fragilità del software}, ovvero la tendenza del software a ``rompersi'' in molti punti ogni volta che viene modificato. In questo caso i cambiamenti tendono a causare comportamenti inaspettati in altre parti del sistema (spesso in aree che non hanno alcuna relazione concettuale con l'area che è stata modificata), di conseguenza ogni correzione peggiora le cose, introducendo più problemi di quelli risolti, rendendo quindi il software impossibile da mantenere. Come effetto ogni volta che i manager autorizzano una correzione, temono che il software si ``rompa'' in modo inaspettato
    \item \textbf{Immobilità del software}, ovvero l'impossibilità di riutilizzare il software di altri progetti o di parti dello stesso progetto. Come effetto tipicamente si ha che il software viene semplicemente riscritto anziché riutilizzato
    \item \textbf{Viscosità del software}, ovvero durante lo sviluppo risulta semplice fare la cosa sbagliata e difficile fare la cosa giusta. Questo viene spesso causato dall'accumularsi di ``pezze'', scorciatoie e correzioni temporanee, facendo sì che nel tempo la manutenibilità del software venga sempre meno
\end{itemize} 

Una progettazione può essere scadente per vari motivi, alcuni ovvi come la presenza di vincoli di tempo/risorse e la mancanza di capacità del progettista, altri meno ovvi come il cambiamento dei requisiti nel tempo, la mancata gestione delle dipendenze o la ``putrefazione naturale'' del software. 

\section{Principi di Design}

\subsection{Rasoio di Occam}

Il principio zero dell'ingegneria del software si può ritrovare nel rasoio di Occam, il quale afferma che tra due soluzioni equivalenti occorre preferire quella che introduce il minor numero di ipotesi e fa uso del minor numero di concetti.

\begin{definizione}{Rasoio di Occam}{}
    Non bisogna introdurre concetti che non siano strettamente necessari
\end{definizione}

Spesso un software deve aver a che fare con un notevole quantitativo di complessità derivante dal contesto in cui deve essere utilizzato, di conseguenza occorre non aggiungere ulteriore complessità mantenendolo \textbf{semplice} (semplice, non semplicistico).

\subsection{Divide et Impera}

La decomposizione è una tecnica fondamentale per il controllo e la gestione della complessità. Non esiste un solo modo per decomporre il sistema e la qualità della progettazione dipende direttamente
dalla qualità delle scelte di decomposizione adottate.
In questo contesto il principio fondamentale risulta minimizzare il grado di accoppiamento tra i moduli del sistema.

\subsection{Single Responsibility Principle}

\begin{principio}{\acrfull{srp_design}}{}
    Ogni classe deve avere una ed una sola responsabilità, interamente incapsulata al suo interno
\end{principio}

Il \acrshort{srp_design} parte dall'osservazione che se una classe ha più di una responsabilità (intesa come motivo per cambiare) queste diventano accoppiate con il tempo, rendendo eventuali modifiche o evoluzione del software molto più complesse in quanto una modifica ad una sola responsabilità ha impatto anche sull'altra (eventualmente compromettendola). Questo tipo di accoppiamento porta a design fragili.

La soluzione spesso consiste nel separare in due o più classi le varie responsabilità, facendo in modo che ogni classe ne abbia solamente una.

\subsection{Dependency Inversion Principle}

\begin{principio}{\acrfull{dip_design}}{}
    Una classe dovrebbe dipendere dalle astrazioni, non da classi concrete
\end{principio}

Questa è una tecnica di disaccoppiamento dei moduli software, che consiste nel rovesciare la pratica tradizionale secondo cui i moduli di alto livello dipendono da quelli di basso livello. Questa scelta è motivata dal fatto che le astrazioni non devono dipendere dai dettagli, ma sono i dettagli che dovrebbero dipendere dalle astrazioni.

Nel caso in cui questo principio non venisse osservato il software ottenuto soffrirebbe di:
\begin{itemize}
    \item Rigidità: bisogna intervenire su un numero elevato di moduli
    \item Fragilità: si introducono errori in altre parti del sistema
    \item Immobilità: i moduli di alto livello non si possono riutilizzare perché non si riescono a separare da quelli di basso livello 
\end{itemize}

Questo principio va osservato anche nel caso in cui siano presenti dipendenze transitive (rimuovendo la dipendenza diretta e sostituendola con l'uso di interfacce) o dipendenze cicliche (rompendo i cicli ed inserendo delle interfacce).

\subsection{Interface Segregation Principle}

\begin{principio}{\acrfull{isp_design}}{}
    Un client non dovrebbe dipendere da metodi che non usa
\end{principio}

Tale principio afferma che è preferibile che le interfacce\footnote{Tali interfacce sono anche chiamate \textit{role interfaces}} siano molte, specifiche e piccole (che contengono pochi metodi collegati tra di loro) piuttosto che poche, generali e grandi.
Questo consente a ciascun client di dipendere da un insieme minimo di metodi, ovvero quelli appartenenti alle interfacce che effettivamente usa. 
In ossequio a questo principio, un oggetto dovrebbe tipicamente implementare numerose interfacce, una per ciascun ruolo che l'oggetto stesso gioca in diversi contesti o diverse interazioni con altri oggetti.

Questo pirncipio risulta utile anche nello sviluppo di sistemi distribuiti in generale e di sistemi a microservizi in particolare.

\subsection{Open/Closed Principle}

\begin{principio}{\acrfull{ocp_design}}{}
    Le entità software dovrebbero essere aperte all'estensione, ma chiuse alle modifiche
\end{principio}

Ciò è particolarmente importante in un ambiente di produzione, in cui i cambiamenti al codice sorgente possono richiedere la revisione del codice, test di unità, e altre procedure tali da garantire la qualità di un prodotto software: il codice che rispetta il principio non cambia quando viene esteso, e quindi non ha bisogno di tale sforzo. 

Esistono principalmente due modi per soddisfare tale principio attraverso l'ereditarietà:
\begin{itemize}
    \item Tramite ereditarietà dell'implementazione. In questo caso vengono create nuove classi che estendono la classe originale. Le nuove classi possono avere o meno la medesima interfaccia della classe base. Questo approccio riduce la ridondanza del codice nelle classi derivate, spostando il codice sorgente comune nella classe base
    \item Tramite ereditarietà dell'interfaccia. In questo caso le classi derivate ereditano da una classe base astratta con funzioni virtuali pure. L'interfaccia è chiusa alle modifiche ed il suo comportamento può essere modificato implementando nuove classi derivate. Questo approccio consente la gestione di un insieme di oggetti differenti in modo uniforme. 
\end{itemize}

\subsection{Liskov Substitution Principle}

\begin{principio}{\acrfull{lsp_design}}{}
    Ogni sottoclasse deve poter sostituire la propria classe base. 
    
    Più formalmente, se $q(x)$ è una proprietà che si può dimostrare essere valida per oggetti $x$ di tipo $T$, allora $q(y)$ deve essere valida per oggetti $y$ di tipo $S$ dove $S$ è un sottotipo di $T$
\end{principio}

Se \acrshort{ocp_design} sostiene la necessità di utilizzare classi concrete che derivano da un'astrazione (da un'interfaccia o da una classe astratta), \acrshort{lsp_design} costituisce una guida per la creazione di queste classi mediante l'ereditarietà.

Si noti che \acrshort{lsp_design} non riguarda il comportamento privato di una classe ma il comportamento pubblico, ovvero quello da cui dipende chi usa la classe.

La principale causa di violazione di questo principio è data dalla ridefinizione dei metodi virtuali nelle classi derivate. La chiave per evitare tali violazioni sta nel \textit{Design by contract}. 

\paragraph{Design by contract}
Nel Design by Contract ogni metodo ha un insieme di pre-condizioni, ovvero di requisiti minimi che devono essere soddisfatti dal chiamante perché il metodo possa essere eseguito correttamente, ed un insieme di post-condizioni, ovvero di requisiti che devono essere soddisfatti dal metodo nel caso di esecuzione corretta. 
Questi due insiemi di condizioni costituiscono un contratto tra chi invoca il metodo (cliente) e il metodo stesso (e quindi la classe a cui appartiene). Le pre-condizioni vincolano il chiamante, mentre le post-condizioni vincolano il metodo. Se il chiamante garantisce il verificarsi delle pre-condizioni, il metodo garantisce il verificarsi delle post-condizioni. 

Quando un metodo viene ridefinito in una sottoclasse le pre-condizioni devono essere identiche o meno stringenti, mentre le post-condizioni devono essere identiche o più stringenti. Questo perché un cliente che invoca il metodo conosce il contratto definito a livello della classe base, quindi non è in grado di soddisfare pre-condizioni più tringenti o di accettare post-condizioni meno stringenti. In caso contrario, il cliente dovrebbe conoscere informazioni sulla classe derivata e questo porterebbe a una violazione del principio di Liskov.



\section{Principi di Architettura dei Package}

\subsection{Reuse/Release Equivalency Principle}

\begin{principio}{\acrfull{rep_package}}{}
    L'unità di riutilizzo corrisponde all'unità di rilascio
\end{principio}

Un elemento riutilizzabile, sia esso un componente, una classe o un insieme di classi, non può essere riutilizzato a meno che non sia gestito da un sistema di rilascio di qualche tipo. Questo meccanismo è necessario per garantire a chi riusa un componente di essere notificato (in qualche modo) della correzione di un qualche errore o di un qualche aggiornamento. 

Non si dovrebbe riutilizzare un elemento a meno che l'autore non prometta di tenere traccia delle versioni, delle modifiche eseguite, delle correzioni apportate e di mantenere le vecchie versioni per qualche tempo.

Un criterio per raggruppare le classi in pacchetti è il riutilizzo, pertanto le classi riutilizzabili assieme dovrebbero essere raccolte nello stesso pacchetto.


\subsection{Common Closure Principle}

\begin{principio}{\acrfull{ccp_package}}{}
    Classi che mutano insieme devono essere distribuite insieme
\end{principio}

Il lavoro per gestire, testare e rilasciare un pacchetto in un sistema di grandi dimensioni non è banale. Più pacchetti cambiano in un dato rilascio, maggiore è il lavoro per ricostruire, testare e distribuire il rilascio, di conseguenza è desiderabile ridurre al minimo il numero di pacchetti che vengono modificati in un dato ciclo di rilascio
del prodotto. Per raggiungere questo obiettivo, è possibile  reggruppare assieme classi che pensiamo cambieranno insieme.

\subsection{Common Reuse Principle}

\begin{principio}{\acrfull{ccp_package}}{}
    Classi che non sono riutilizzate assieme non dovrebbero essere raggruppate assieme
\end{principio}

Una dipendenza da un pacchetto è una dipendenza da tutto ciò che è contenuto nel pacchetto. Quando un pacchetto cambia e il suo numero di rilascio viene aggiornato, tutti i client di quel pacchetto devono verificare di funzionare con il nuovo pacchetto anche se nulla di ciò che hanno usato all'interno del pacchetto è effettivamente cambiato. 
Pertanto, le classi che non vengono riutilizzate insieme non dovrebbero essere raggruppate insieme.

\subsection{Discussione}

I tre principi sopra descritti non possono essere soddisfatti assieme in quanto:
\begin{itemize}
    \item \acrshort{rep_package} e \acrshort{crp_package} semplificano la vita ai riutilizzatori, mentre \acrshort{ccp_package} semplifica la vita ai manutentori
    \item \acrshort{ccp_package} cerca di rendere i package più grandi possibile (dopotutto, se tutte le classi stanno in un solo package,
    allora solo quel package cambierà). \acrshort{crp_package}, tuttavia, cerca di creare package molto piccoli
\end{itemize}

Una soluzione consiste nel soddisfare diversi principi in momenti diversi dello sviluppo. All'inizio di un progetto, gli architetti possono impostare la struttura dei package in modo tale che \acrshort{ccp_package} domini per facilità di sviluppo e manutenzione. Successivamente, quando l'architettura si stabilizza, gli architetti possono ri-fattorizzare la struttura dei package per massimizzare \acrshort{rep_package} e \acrshort{crp_package} per i riutilizzatori esterni

\section{Relazioni tra i Package}

\subsection{Acyclic Dependencies Principle}

\begin{principio}{\acrfull{adp_package}}{}
    Le dipendenze tra pacchetti non devono formare cicli
\end{principio}

Per rompere un ciclo occorre:
\begin{itemize}
    \item Inframezzare un nuovo package
    \item Aggiungere una nuova interfaccia
\end{itemize}

\subsection{Stable Dependencies Principle}

\begin{principio}{\acrfull{sdp_package}}{}
    Le dipendenze tra pacchetti dovrebbero tener conto anche della stabilità dei pacchetti stessi. Un pacchetto dovrebbe dipendere solo da pacchetti più stabili, mai da pacchetti meno stabili
\end{principio}

\subsection{Stable Abstractions Principle}

\begin{principio}{\acrfull{sap_package}}{}
    I pacchetti più stabili dovrebbero essere pacchetti di astrazioni
\end{principio}
