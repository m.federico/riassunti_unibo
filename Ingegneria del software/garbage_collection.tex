\chapter{Garbage Collection}

\section{Ciclo di vita di un oggetto}
In un ambiente \acrshort{oo} la vita di ogni oggetto viene scandita dalle seguenti fasi:
\begin{itemize}
    \item Allocazione della memoria. Ogni oggetto ha bisogno di un'area di memoria dove memorizzare il proprio stato. Tale memoria, a seconda dei casi va allocata sullo stack o sull'heap, nel secondo caso occorre richiederla al sistema operativo (o al runtime). All'interno di questo capitolo ci si concentra prevelentemente sul secondo caso, che è quello che riguarda linguaggi con un modello di memoria gestita con ad esempio \texttt{c\#}
    \item Inizializzazione della memoria per rendere utilizzabile l'oggetto. Tipicamente questo avviene per mezzo di istruzioni particolari che vengono raccolte all'interno del costruttore del tipo
    \item Utilizzo dell'oggetto
    \item Finalizzazione. Quando l'oggetto non è più necessario occorre eseguire un'operazione di pulizia dello stato dell'oggetto (esempio chiudere file aperti o connessioni di rete attive)
    \item Liberare la memoria precedentemente allocata. Questa operazione in generale può essere eseguita manualmente (come in \texttt{C++}) o in maniera automatica (come in \texttt{C\#} e \texttt{Java}) tramite Garbage collection
\end{itemize} 


\section{Garbage collection}
È una modalità automatica di rilascio delle risorse che consente di evitare una grossa fetta degli errori dovuti alla gestione della memoria (es. doppia deallocazione, memory leak, \ldots). Con questa modalità i puntatori alle aree di memoria non sono più gestiti manualmente dal programmatore, ma sono gestiti automaticamente da un sistema che tiene traccia di ogni singola allocazione e determina autonomamente quando eseguire la deallocazione. L'utilizzo di questa tecnica porta con sè alcuni svantaggi però, come il rilascio della memoria non deterministico e l'aumento delle risorse di calcolo necessarie.

Sono possibili varie strategie per la garbage collection:
\begin{itemize}
    \item Tracing
    \item Reference counting
    \item Escape analysis
\end{itemize}

\subsection{Tracing}
Consiste nel determinare a runtime quali oggetti non sono più raggiungibili (potenzialmente) ed eliminarli. 

\begin{definizione}{Raggiungibilità}{raggiungibilita}
    Siano $p$ e $q$ due oggetti, sia $q$ un oggetto raggiungibile. Diremo che $p$ è raggiungibile in maniera ricorsiva se e solo se esiste un riferimento a $p$ tramite $q$, ovvero $p$ è raggiungibile attraverso un oggetto, a sua volta raggiungibile.
    
\end{definizione}

\begin{lemma}{}{}
    Un oggetto è raggiungibile in due soli casi:
    \begin{itemize}
        \item è un oggetto radice creato all'avvio del programma (oggetto globale) creato da una sub-routine (oggetto scope, riferito da variabile sullo stack)
        \item è referenziato da un oggetto raggiungibile
    \end{itemize}
\end{lemma}

Nonostante la definizione \ref{def:raggiungibilita} sia precisa, non risulta ottimale dal punto di vista pratico in quanto può accadere che un programma utilizzi un oggetto per l'ultima volta molto prima che diventi irraggiungibile.

A questo punto occorre distinguere \textit{garbage sintattico} (ovvero oggetti che il programma non \textbf{può} raggiungere) da \textit{garbage semantico} (ovvero oggetti che il programma non \textbf{vuole} più usare).

\subsection{Refence counting}

Ogni oggetto contiene un contatore che indica quanti riferimenti puntano ad esso. Il contatore viene incrementato (potenzialmente) ad ogni nuovo riferimento, e decrementato ogni volta che un riferimento viene rimosso. Nel momento in cui il contatore raggiunge il valore 0 la memoria può essere liberata, in quanto nessun oggetto mantiene un riferimento forte (\textit{string reference}) all'oggetto.

Questa soluzione risulta nel complesso piuttosto semplice da comprendere, tuttavia presenta una serie di svantaggi notevoli:
\begin{itemize}
    \item Nel caso di cicli di riferimenti gli oggetti potrebbero non essere mai deallocati
    \item Aumento dell'occupazione della memoria, in quanto per ogni oggetto occorre memoriazzare anche un contatore intero
    \item Riduzione della velocità delle operazioni sui riferimenti, in quanto per ogni operazione occorre manipolare il contatore. Inoltre ogni operazione su un riferimento può potenzialmente richiedere la deallocazione di diversi oggetti
    \item Comportamento in ambiente concorrente. In ambiente concorrente le operazioni sui riferimenti devono necessariamente essere thread-safe, andando quindi a rendere tali operazioni più costose (tenendo anche conto di quanto spesso vengono eseguite)
\end{itemize}

\subsection{Escape analysis}

L'escape analysis è un metodo per determinare lo scope dinamico dei puntatori (ovvero l'area semantica in cui nel programma è possibile accedere a un puntatore). 

Quando una variabile (o un oggetto) viene allocata in una funzione, un puntatore alla variabile può essere traferito (\textit{escape}) ad altri thread o alla funzione chiamante. L'escape analysis consente di determinare tutte le posizioni in cui è possibile memorizzare un puntatore e se è possibile dimostrare che la durata del puntatore è limitata solo alla procedura e/o al thread corrente.

Si possono di conseguenza prevedere alcune ottimizzazioni, fra cui nello specifico la conversione di un'allocazione sull'heap ad una sullo stack. Se un'oggetto è allocato sull'heap all'interno di una funzione e il puntatore all'oggetto non viene mai traferito ad altri thread o alla funzione chiamante allora l'allocazione sull'heap può essere trasformata in un'allocazione sullo stack.
