\chapter{Analisi del problema}

L'obiettivo dell'analisi del problema è esprimere fatti oggettivi sul problema focalizzandosi su sottosistemi, ruoli e responsabilità degli scenari durante l'analisi dei requisiti senza descriverne la soluzione. Questa fase ha come risultato:
\begin{itemize}
    \item \textbf{Architettura logica}. Si tratta di un insieme di modelli UML costruiti per definire una struttura concettuale del problema robusta e modificabile. Tramite l'architettura logica l'analista descrive la struttura, il comportamento atteso e le interazioni. Queste informazioni vengono estrapolate dai requisiti, dalle caratteristiche del problema e del dominio senza però far alcun riferimento a tecnologie realizzative.
    \item \textbf{Piano di lavoro}. Esprime l'articolazione delle attività in termini di risorse umane, temporali e di elaborazione necessarie allo sviluppo del prodotto dall'analisi del problema
    \item \textbf{Piano del collaudo}. Definisce l'insieme dei risultati attesi da ogni entità definita nell'Architettura Logica in relazione a specifiche sollecitazioni stimolo-risposta prevista. Un modo per impostare il Piano del Collaudo è quello di fornire i test delle classi definite nell'Architettura Logica, così da pianificare già in questa fase i test di integrazione dei vari sottosistemi. Ciò agevola il lavoro della successiva fase di progetto, riducendo il rischio di brutte sorprese in fase di integrazione delle sotto-parti
\end{itemize}

L'analisi del problema è strutturata in un insieme di fasi descritte nei paragrafi successivi:
\begin{enumerate}
    \item Analisi del Documento dei Requisiti. Ha come obiettivo concentrarsi sull'analisi delle funzionalità e dei rischi evidenziati nel documento dei requisiti
    \item Analisi dei Ruoli e delle Responsabilità. Ha come obiettivo analizzare bene i ruoli emersi nei casi d'uso e porre particolare attenzione nell'attribuzione delle responsabilità a tali ruoli tenendo conto dell'analisi del rischio
    \item Scomposizione del problema. Ha come obiettivo, se possibile, suddividere il problema in sotto-problemi più piccoli
    \item Creazione del Modello del Dominio. Ha come obiettivo costruire, partendo dal vocabolario, il diagramma delle classi del dominio
    \item  Architettura Logica (Struttura). Ha come obiettivo la creazione dei diagrammi strutturali (package e classi) dell'Architettura Logica
    \item  Architettura Logica (Interazione). Ha come obiettivo lacreazione dei diagrammi di interazione (diagrammi di sequenza) dell'Architettura Logica
    \item  Architettura Logica (Comportamento). Ha come obiettivo la creazione dei diagrammi di comportamento (stato e attività) dell'Architettura Logica
    \item Definizione del Piano di Lavoro. Ha come obiettivo assegnare le responsabilità a ciascun membro del team di progetto, stabilire i vincoli temporali, impostare eventuali milestone, \ldots
    \item  Definizione del Piano del Collaudo. Ha come obiettivo definire i risultati attesi da ogni entità (classe, sottosistema) che compare nell'Architettura Logica
\end{enumerate}

\section{Analisi del documento dei requisiti}
Il Documento dei Requisiti evidenzia le funzionalità ed i servizi che dovranno essere sviluppati sotto forma di requisiti funzionali, i vincoli che devono essere tenuti in considerazionesotto forma di requisiti non funzionali, le informazioni relative al ``mondo esterno'' con cui il prodotto finale dovrà interagire ed infine i ``rischi'' legati a possibili attacchi alla protezione, integrità e privacy dei dati espressi sotto forma di requisiti di sicurezza. Partendo da tale documento occorre applicare un'attenta analisi delle singole funzionalità, dei vincoli, delle interazioni e dei rischi.

\paragraph{Analisi delle funzionalità}
Per ognuna delle funzionalità espresse all'interno dei casi d'uso occorre analizzare:
\begin{itemize}
    \item l'obiettivo della funzionalità. Sono esempi di obiettivi la memorizzazione dei dati, l'interazione con l'esterno e la gestione/manipolazione dei dati. Ogni funzionalità va etichettata in modo da rendere evidente durante la creazione dell'Architettura Logica dove ``posizionarla''. Se la funzionalità è molto complessa potrebbe appartenere a più tipi differenti, in questo caso è possibile marcarla come ``complessa''\footnote{Queste funzionalità verranno eventualmente scomposte successivamente, vedi \ref{sec_analisi_problema}}
    \item le informazioni coinvolte. Occorre un'analisi sistematica di tutte le informazioni sulle quali la funzionalità deve ``operare'', in particolare occorre individuare il tipo di dato (se composto o semplice) su cui viene opera la funzionalità e il grado di riservatezza richiesto
    \item il ``flusso delle informazioni''. Occorre individuare quali sono le informazioni che si ricevono in ingresso: e quali sono le informazioni in uscita. Le prime permettono di dare indicazioni sulla validazione dell'input e se sono presenti vincoli sui valori ammessi, le invece permettono di valutare dall'esterno se la funzionalità si comporta come ci si aspetta tramite l'esecuzione di test
\end{itemize}

\paragraph{Analisi dei vincoli}
Per ognuno dei requisiti non funzionale identificati precedentemente occorre analizzare:
\begin{itemize}
    \item la categoria di appartenenza. Sono esempi di categorie i vincoli sulle performance, sui tempi di risposta, sulla scalabilità, sull'usabilità e sulla protezione dei dati.Per tener traccia dell'analisi ogni requisito va etichettato con la corrispondente categoria
    \item le funzionalità coinvolte. Alcuni requisiti potrebbero influenzare diversi aspetti del sistema o alctre, di conseguenza occorre anche indicare il tipo di impatto che tale vincolo ha in generale nel complesso
\end{itemize}

\paragraph{Analisi delle interazioni}
In questa fase vanno distinte le interazioni con gli umani da quelle con sistemi esterni. Nel caso delle interazioni con gli umani occorre analizzare le eventuali interfacce identificate con il cliente nella fase di analisi dei requisiti\footnote{Se non sono già state individuate occorre delinearle}, individuare le maschere di inserimento/output dati, individuare le sole informazioni necessarie da mostrare
in ogni maschera e creare un legame tra maschere, informazioni e funzionalità. Nel caso delle interazioni con sistemi esterni occorre analizzare ``ai morsetti'' i sistemi esterni con cui si dovrà interagire e di conseguenza individuare i protocolli di interazione con tali sistemi.

\section{Analisi dei ruoli e delle responsabilità}
Per ogni attore individuato all'interno dei casi d'uso occorre specificare
\begin{itemize}
    \item le responsabilità, ovvero cosa debba deve fare relativamente ad ogni funzionalità definita
    \item le informazioni a cui possa accedere nel contesto di ogni funzionalità in cui è coinvolto indicando contestualmente anche il tipo di accesso
    \item le maschere che può visualizzare ed a cui può accedere
    \item il suo livello di riservatezza, ovvero quale livello di riservatezza è necessario avere per poter ricoprire quel ruolo
    \item la numerosità attesa, ovvero il numero di persone fisiche che possono giocare quel ruolo
\end{itemize}

\section{Scomposizione del problema}\label{sec_analisi_problema}
Come anticipato, il punto di partenza per questa fase è l'analisi delle funzionalità: per ogni funzionalità marcata come ``complessa'' all'interno della tabella delle funzionalità occorre valutare se sia possibile operare una scomposizione. Per eseguire questa operazione occorre anzitutto capire se sia possibile scomporla in sotto-funzionalità e se soprattutto abbia senso farlo. Nel caso si scelga di eseguire tale operazione occorre anche individuare quale legame sussista tra le sotto-funzionalità e come debba configurarsi il flusso delle informazioni tra le nuove funzionalità individuate.

\section{Creazione modello del dominio}
A partire dal glossario e dalle tabelle precedentemente definite viene creato il diagramma delle classi, che va a rappresentare il modello del dominio. Tale modello verrà riutilizzato nel paragrafi \ref{sec_arc_log_struttura}, \ref{sec_arc_log_interazioni} e \ref{sec_arc_log_comportamento}. Non tutti i sostantivi presenti nel glossario devono diventare classi, bisogna infatti evitare:
\begin{itemize}
    \item ridondanze: classi uguali ma con diverso nome
    \item costruzione di classi a partire da termini ambigui nel glossario
    \item nomi che si riferiscono a eventi o operazioni
    \item nomi appartenenti al meta-linguaggio del processo come, per esempio, requisiti e sistema
    \item nomi al di fuori dell'ambito del sistema
    \item nomi che possono essere attributi
\end{itemize}

\paragraph{Individuazione delle classi}
Per individuare le classi occorre tenere in considerazione solo quei sostantivi che effettivamente fanno riferimento ad esse, ad esempio ignorando quelli che indicano attributi o operazioni oppure ignorando i sinonimi. Va posta particolare attenzione per quanto riguarda gli aggettivi o le specificazioni che accompagnano e denotano tali sostantivi, in quanto potrebbero non essere rilevanti oppure potrebbero essere sintomo di una struttura gerarchica tra classi.

\paragraph{Individuazione delle relazioni}
La maggior parte delle classi (degli oggetti) interagisce con altre classi (altri oggetti) in vari modi. In ogni tipo di relazione, esiste un cliente $C$ che dipende
da un fornitore di servizi $F$: $C$ ha bisogno di $F$ per lo svolgimento di alcune funzionalità che $C$ non è in grado di effettuare autonomamente. Di conseguenza per il corretto funzionamento di $C$ è indispensabile il corretto funzionamento di $F$.
\begin{itemize}
    \item \textbf{Ereditarietà}. Durante questa fase l'ereditarietà deve rispecchiare una tassonomia effettivamente presente nel dominio del problema, non deve rispecchiare caratteristiche o suluzioni implementative. Inoltre L'ereditarietà non va usata per riunire caratteristiche comuni di classi altrimenti distinte.
    \item \textbf{Associazione}. Un'associazione rappresenta una relazione strutturale tra due istanze di classi diverse (o della stessa classe). Casi specifici di associazione sono l'\gls{aggregazione} e la \gls{composizione}. Occorre porre attenzione alle associazioni molti a molti in quanto possono nascondere una classe(classe di associazione) del tipo ``evento da ricordare''
    \item \textbf{Collaborazione}. Rappresenta una relazione generica in cui $C$ utilizza $F$ per lo svolgimento di alcune funzionalità. Questo tipo di relazione evidentemente non è simmetrica
\end{itemize}

\paragraph{Individuazione degli attributi}
Ogni attributo modella una proprietà atomica di una classe, sia esso un valore singolo (come un importo) o un gruppo di valori singoli strettamente legati tra di loro (come una data). Il dettaglio importante in questa fase è che l'attributo rappresenta un valore ``atomico'', in quanto valori non ``atomici'' devono essere rappresentati come associazioni. 

Per ogni attributo è importante specificare: il tipo, l'opzionalità, eventuali valori ammessi, vincoli di crezione, eventuale unità di misura e precisione.

Nel caso di attributi calcolabili occorre specificare sempre e solo l'operazione di calcolo e mai l'attributo. La scelta relativa alla memorizzazione del valore del calcolo è una scelta progettuale che riguarda uno specifico compromesso tra tempo di calcolo e spazio di archiviazione.

\paragraph{Individuazione delle operazioni}
Per ogni operazione occorre specificare il nome (possibilimente usando un verbo dal glossario), la visibilità, i dati che utilizza, e soprattuto tutti i vincoli applicabili (pre e post condizioni comprese).

\section{Architettura Logica: Struttura}\label{sec_arc_log_struttura}
La parte strutturale dell'Architettura Logica dovrebbe essere composta di due tipi differenti di diagrammi UML, ovvero il diagramma dei package che fornisce una visione di alto livello dell'architettura ed il diagramma delle classi (uno o più diagrammi in base alla complessità) che fornisce una visione più dettagliata del contenuto dei singoli package.

È opportuno organizzare sin da subito l'Architettura Logica usando un pattern architetturale chiamato \acrfull{bce_architecture}, il quale suggerisce di basare l'architettura di un sistema sulla partizione sistematica degli use case in oggetti di tre categorie: informazione (\textit{entity}), presentazione (\textit{boundary}) e controllo (\textit{control}). Gli elementi \textit{entity} rappresentano la dimensione delle classi che contengono le informazione che caratterizzano il problema, si tratta in generale delle classi che sono state individuate all'interno del modello del dominio. Gli elementi \textit{boundary} rappresentano la dimensione relativa a tutte quelle funzionalità che dipendono dall'ambiente esterno, e che quindi costituiscono l'interfaccia del sistema verso il mondo esterno. Gli elementi \textit{control} rappresentano infine gli enti che incapsulano il controllo e che fanno da collante tra interfacce ed entità.

Utilizzando \acrshort{bce_architecture} l'architettura del sistema tende ad organizzarsi fisiologicamente a livelli. Nello specifico il livello di presentazione comprende le parti che realizzano l'interfaccia utente, il livello di applicazione comprende le parti che provvedono a elaborare l'informazione di ingresso, a produrre i risultati attesi ed a presentare le informazioni in uscita, infine il livello delle entità forma il dominio applicativo.

Dopo aver stabilito la struttura di alto livello dell'architettura logica occorre andare a riempire i vari package con le classi che vi appartengono, facendo attenzione ad introdurre solamente quelle classi la cui presenza può essere inferita dal dominio applicativo e dagli use case, evitando dunque di introdurre scelte progettuali o possibili strategie implementative. Per quanto riguarda tutti i casi in cui una funzionalità è stata scomposta in sotto-funzionalità durante la fase precedente è possibile indicare tutte le sotto-funzionalità come classi separate.

È possibile creare uno o più diagramma dei package, questa scelta non ha alcun carattere tecnico, ma è dettata solamente dalla comodità di visualizzazione dei dati e dal grado di ``visione di insieme'' che si vuole fornire a chi legge i diagrammi\footnote{Quindi spesso anche chi li crea}.

\section{Architettura Logica: Interazione}\label{sec_arc_log_interazioni}
Utilizzando diagrammi di sequenza occorre evidenziare le interazioni tra gli oggetti e l'ordine in cui le operazioni vengono eseguite. Questo risulta utile soprattutto in quei casi in cui l'interazione tra gli oggetti è particolamente complessa o richiede un elevato numero di oggetti. 

Durante questa fase occorre fare attenzione a non introdurre troppi dettagli nella decrizione delle interazioni, usando i diagrammi solamente per descrivere ad alto livello il comportamento del sistema. 

\section{Architettura Logica: Comportamento}\label{sec_arc_log_comportamento}
Durante questa fase è possibile descrivere i comportamenti delle entità tramite opportuni diagrammi di stato o diagrammi delle attività. Questo tipo di diagrammai risulta utile in tutti quei casi in cui il comportamento dell'entità è sufficientemente complesso o dipende dal proprio stato interno (ad esempio quando l'entità modella un dispositivo che può comportarsi diversamente a seconda del fatto che sia acceso, spento, \ldots). Anche in questo caso i diagrammi non devono essere eccessivamente dettagliati.

\section{Definizione del Piano di Lavoro}
Dopo la creazione dell'architettura logica è possibile iniziare a suddividere il lavoro. In particolare è necessario:
\begin{itemize}
    \item suddividere le responsabilità ai diversi membri del team di progetto e sviluppo
    \item stabilire le tempistiche per la progettazione di ciascuna parte
    \item stabilire i tempi di sviluppo di ciascun sottosistema
    \item programmare i test di integrazione tra le parti
    \item identificare i tempi e le modalità di rilascio delle diverse versioni del prototipo
    \item identificare un piano per gli sviluppi futuri
\end{itemize}

\section{Definizione del Piano del Collaudo}
Al termine dell'analisi del problema, i modelli che definiscono il dominio e l'architettura logica dovrebbero dare sufficienti informazioni su cosa le varie parti del sistema debbano fare senza specificare ancora molti dettagli del loro comportamento. Il ``cosa fare'' di una parte dovrà comprendere anche le forme di interazione con le altre parti. 

Lo scopo del piano del collaudo è cercare di precisare il comportamento atteso da parte di una entità prima ancora di iniziarne il progetto e la realizzazione. Focalizzando l'attenzione sulle interfacce delle entità e sulle interazioni è possibile impostare scenari in cui specificare in modo già piuttosto dettagliato la ``risposta'' di una parte a uno ``stimolo'' di un'altra parte. Lo sforzo di definire nel modo più preciso possibile un piano del collaudo di un sistema prima ancora di averne iniziato la fase di progettazione viene ricompensato da una miglior comprensione dei requisiti, un approfondimento nella comprensione dei problemi e da una più precisa definizione dell'insieme delle funzionalità (operazioni) che ciascuna parte deve fornire alle altre per una effettiva integrazione nel ``tutto'' che costituirà il sistema da costruire.

Un piano del collaudo va concepito e impostato da un punto di vista logico, cercando di individuare categorie di comportamenti e punti critici. In molti casi tuttavia può anche risultare possibile definire in modo precoce piani di collaudo concretamente eseguibili, avvalendosi di strumenti di test che sono ormai diffusi in tutti gli ambienti di programmazione.