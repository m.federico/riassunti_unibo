\chapter{Progettazione}
La fase di progettazione ha come obiettivo quello di arrivare ad ottenere l'architettura del sistema a partire dall'architettura logica. Durante questa fase occorre considerare anche tutti gli aspetti vincolanti che erano stati ignorati durante le fasi precedenti, descrivendo quindi la soluzione ai problemi ma soprattutto la motivazione che ha portato a determinarla.

Possiamo vedere la fase di progettazione come suddivisa in cinque sottofasi:
\begin{itemize}
    \item Progettazione architetturale, che ha come obiettivo quello di definire l'architettura del sistema tenendo conto di tutti i vincoli e delle forze in gioco
    \item Progettazione di dettaglio, che ha come obiettivo quello di progettare nel dettaglio ogni aspetto del sistema
    \item Progettazione della Persistenza, che ha come obiettivo quello di progettare i meccanismi per la persistenza dei dati
    \item Progettazione del Collaudo, che ha come obiettivo quello di definire in modo chiaro e preciso come il sistema dovrà essere collaudato una volta terminata l'implementazione
    \item Progettazione per il Deployment, che ha come obiettivo quello di progettare il sistema in modo da rendere semplice il deployment sulle macchine e per garantire la sicurezza
\end{itemize}

\section{Progettazione architetturale}
Durante questa fase occorre decidere quale sia l'architettura complessiva del sistema. Per prima cosa occorre valutare attentamente i requisiti non funzionali al fine di indentifcare un compromesso che consenta di soddisfare tutti i requisiti. Fatto ciò occorre scegliere l'architettura di riferimento, tenendo conto sia dei requisiti non funzionali che della tipologia di applicazione. Solo alla fine del processo è opportuno eseguire la scelta della tecnologia (o delle tecnologie, vedi anche diversità \ref{par_diversita_sec}) che verranno impiegate per lo sviluppo vero e proprio del sistema.

\subsection{Analisi dei requisiti non funzionali}\label{subsec_req_non_funz}
Diversi requisiti non funzionali portano a diverse scelte architetturali. Alcuni requisiti non funzionali notevoli sono:
\begin{itemize}
    \item \textbf{Prestazioni}. Se le prestazioni sono un requisito critico l'architettura dovrebbe essere progettata localizzando le operazioni critiche all'interno di un piccolo numero di componenti e minimizzando le comunicazioni possibile tra essi. Queste decisioni portano a dover definire componenti ``grandi'' per ridurre la comunicazione
    \item \textbf{Protezione dei dati}. Se la protezione dei dati (security) è un requisito critico l'architettura dovrebbe essere progettata con una struttura ``stratificata'' collocando le risorse più critiche nello strato più interno e protetto. Questa decisione porta a dover definire una struttura con un alto livello di convalida di protezione a ogni strato
    \item \textbf{Sicurezza}. Se la sicurezza (safety) è un requisito critico     l'architettura dovrebbe essere progettata in modo tale che le operazioni relative siano tutte collocate in un singolo componente o in un piccolo insieme di componenti. Ciò comporta una riduzione dei costi e dei problemi di convalida della sicurezza, oltre alla possibilità di poter fornire sistemi di protezione correlati. Questa scelta porta a dover definire componenti ``grandi'' per localizzare le operazioni
    \item \textbf{Disponibilità}. Se la disponibilità è un requisito critico l'architettura dovrebbe essere progettata per comprendere componenti ridondanti ed in modo che sia possibile sostituirli e aggiornarli senza fermare il sistema Queste scelte portano a dover sviluppare un numero maggiore di componenti rispetto a quelli strettamente necessari
    \item \textbf{Manutenibilità}. Se la manutenibilità è un requisito critico l'architettura dovrebbe essere progettata usando componenti piccoli, atomici ed autonomi che possano essere modificati velocemente. I componenti che producono informazioni dovrebbero essere separati dai componenti che le consumano e le strutture dati condivise dovrebbero essere evitate. Questa scelta porta a dover sviluppare componenti di ``piccole'' dimensioni
\end{itemize}

Evidentemente ci sono dei conflitti potenziali tra alcune di queste architetture così come abbiamo visto sussistono conflitti tra i requisiti non funzionali. Ad esempio l'utilizzo di componenti di ``grandi dimensioni'' migliora le prestazioni ma peggiora la manutenibilità e viceversa. Se sono entrambi requisiti critici occorre trovare un compromesso.

\subsection{Scelta dell'architettura del sistema}
La scelta dell'Architettura del Sistema deve basarsi su vari fattori, in primis dal tipo di applicazione che deve essere sviluppata. Occorre poi tener conto dell'architettura logica definita in \ref{sec_arc_log_struttura}, \ref{sec_arc_log_interazioni} e \ref{sec_arc_log_comportamento} e dei requisiti non funzionali secondo le considerazioni fatte nel paragrafo \ref{subsec_req_non_funz}. A partire da queste considerazioni è possibile scegliere uno tra i tanti pattern architetturali o usare un approccio ibrido (se necessario).
\begin{itemize}
    \item \textbf{Blackboard}. Il pattern Blackboard aiuta a strutturare quelle applicazioni in cui vengono applicate strategie di soluzione non deterministiche (tipici problemi di intelligenza artificiale). I diversi sotto-sistemi condividono la stesse conoscenze attraverso la Blackboard al fine di costruire una soluzione approssimata o parziale
    \item \textbf{MVC/BCE}. Il pattern MVC divide le applicazioni in tre distinte parti: il model che gestisce i dati, il controller che manipola i dati ed infine la view che mostra i dati
    \item \textbf{Layers}. Il pattern Layer aiuta a strutturare quelle applicazioni che possono essere scomposte in gruppi di sotto-attività in cui ciascun gruppo si trova a un ben definito livello di astrazione
    \item \textbf{Client/Server}. Il pattern client/server aiuta a strutturare un'applicazione come un insieme di servizi forniti da uno o più server e un insieme di client che utilizza tali servizi
    \item \textbf{Broker}. Il pattern Broker può essere usato per strutturare sistemi distribuiti con un disaccoppiamento tra i diversi sotto-sistemi che comunicano     tra loro attraverso remote server invocation. Il Broker è responsabile della coordinazione delle comunicazioni, come inoltro richieste, invio risposte ed eccezioni
    \item \textbf{Pipe \& Filters}. Il pattern Pipe \& Filters aiuta a strutturare quelle applicazioni che processano flussi di dati. Ogni passo del processo è incapsulato in un apposito filtro e i dati attraversano una pipe di filtri. Variando l'ordine dei filtri si possono ottenere diversi tipi di sistemi
\end{itemize}
Così come non esiste un processo di sviluppo ideale, non esiste un'Architettura ideale sempre utilizzabile. Talvolta infatti è necessario usare stili architetturali diversi per parti diverse del sistema al fine di soddisfare tutti i vincoli imposti dai requisiti.

\subsection{Scelta delle tecnologie}
L'uso di una specifica tecnologia (intesa anche come linguaggio di programmazione, piattaforma o strumento) non è sempre neutro. In taluni casi potrebbe risultare vantaggioso scegliere le tecnologie già in fase di progettazione legando così il progetto alla specifica tecnologia. In tal caso occorre specificarlo chiaramente e fare un'analisi costi/benefici.

\section{Progettazione di dettaglio}
La Progettazione di dettaglio definisce il dettaglio dell'architettura del sistema nelle sue tre viste:
\begin{itemize}
    \item Struttura
    \item Interazione
    \item Comportamento
\end{itemize}

Durante questa fase vengono modificati i modelli ottenuti dall'analisi al fine di definire in dettaglio le classi e le loro interazioni, migliorare le prestazioni, supportare caratteristiche specifiche per le comunicazioni e la protezione dei dati.

Possiamo vedere questa fase divisa in tre sottofasi:
\begin{itemize}
    \item Progettazione del dettaglio strutturale, che ha come obiettivo la presa in considerazione dei dettagli relativi a tutte le classi, eventualmente introducendone di nuove al fine di disaccoppiare i livelli
    \item Progettazione del dettaglio delle interazioni, che ha come obiettivo la presa in considerazione di tutti i protocolli di interazione, eventualmente progettandone di nuovi
    \item Progettazione del dettaglio del comportamento, che ha come obiettivo la presa in considerazione di tutti i diagrammi di stato e delle attività, eventualmente aggiungendone di nuovi
\end{itemize}

\subsection{Progettazione del dettaglio strutturale}
Durante questa fase occorre definire i tipi di dato che non sono stati definiti in precedenza e la navigabilità delle associazioni che li legano, inoltre occorre definire le strutture dati necessarie per l'effettiva implementazione del sistema.

Durante questa fase risulta particolamente utile il pattern Adapter (vedi paragrafo \ref{subsec_pattern_adapter}) per incapsulare tutti quei dettagli che non rispecchiano i requisiti all'interno di strutture che invece li soddisfano. Ad esempio questo risulta utile nel caso di sistemi esterni con interfacce non conformi o che non soddisfano i requisiti di sicurezza.

\subsection{Progettazione del dettaglio delle interazioni}
Durante la Progettazione di Dettaglio della parte di interazione è necessario:
\begin{itemize}
    \item ridefinire i protocolli di interazione emersi in fase di analisi dettagliandoli tenendo conto delle nuove entità emerse durante la fase di progettazione
    \item progettare accuratamente i protocolli di interazione verso i sistemi esterni
    \item definire nuovi protocolli di interazione tra le classi che sono state introdotte nella progettazione
\end{itemize}

\subsection{Progettazione del dettaglio del comportamento}
Durante la progettazione di dettaglio della parte di comportamento è necessario:
\begin{itemize}
    \item definire gli algoritmi che implementano le operazioni complesse/complicate in modo chiaro e preciso avvalendosi eventualmente di diagrammi delle attività
    \item dettagliare i diagrammi di stato/attività già definiti nella fase precedente
    \item eventualmente aggiungere diagrammi di stato/attività per le nuove entità emerse in questa fase
\end{itemize}

\section{Progettazione della Persistenza}
La persistenza dei dati è un fattore cruciale nello sviluppo di un sistema. Il progettista, dopo un'attenta valutazione di vincoli imposti dai requisiti funzionali, tipologia di accesso accesso ai dati e frequenza di accesso ai dati (per citarne alcune) deve scegliere la tecnica migliore di persistenza. Per ogni sistema va valutato attentamente quale strategia dà il miglior bilanciamento tra i vincoli e le forze in gioco nel sistema, ad esempio per le memorizzazione dei log un file di testo rappresenta la miglior soluzione.

L'output di questa fase può essere rappresentato dal formato dei dati salvati, rappresentato dallo schema ER nel caso di un DB relazionale o dal formato dei file che dovranno essere scritti/letti dall'applicazione. 

\section{Progettazione del Collaudo}
La base di partenza di questa attività è il piano del collaudo sviluppato nell’analisi corrispondente. Dopo la progettazione di dettaglio è possibile scrivere i test unitari di ciascuna classe. Successivamente vanno progettati con cura anche i test di integrazione del sistema.

L’output di questa attività è rappresentato dalla Suite completa dei test unitari e di integrazione.

\section{Progettazione per il Deployment}
Durante questa fare occorre finalizzare la procedura usata per il deployment, facendo anche e soprattutto riferimento alle informazioni trovate durante le analisi di sicurezza (vedi \ref{par_prog_deployment}).