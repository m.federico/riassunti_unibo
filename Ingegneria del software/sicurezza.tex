\chapter{Security Engineering}

\section{Progettazione per la sicurezza}
La sicurezza non è qualcosa che possa essere aggiunto al sistema, ma deve necessariamente essere progettata insieme al sistema prima dell'implementazione. Allo stesso tempo però si tratta anche di un problema implementativo, in quanto spesso le vulnerabilità vengono introdotte durante la fase di implementazione\footnote{È possibile ottenere un'implementazione non sicura da una progettazione sicura, ma non è possibile ottenere un'implementazione sicura partendo da una progettazione non sicura}. Ad esempio possono essere considerati i seguenti aspetti relativi alla gestione della sicurezza:
\begin{itemize}
    \item Gestione degli utenti e dei permessi. In questo caso si tratta di controllare e gestire l'inserimento e la rimozione di utenti dal sistema, la gestione dell'identificazione e dell'autenticazione degli utenti, la creazione di appropriati permessi per gli utenti
    \item Deployment e mantenimento del sistema. In questo caso si tratta dell'installazione e della configurazione dei software e del middleware oltre che del loro aggiornamento con tutte le patch disponibili
    \item Controllo degli attacchi, rilevazione e ripristino. Si tratta di controllare il sistema alla ricerca di accessi non autorizzati, della messa in opera di strategie contro gli attacchi, e del backup per ripristinare il normale utilizzo dopo un attacco
\end{itemize}
Essi possono essere analizzati su più fronti: architetturale, applicativo ed infrastrutturale.

\paragraph{Fronte architetturale}
La scelta dell'architettura influenza profondamente la sicurezza. Tale scelta va effettuata tenendo conto di due aspetti fondamentali:
\begin{itemize}
    \item \textbf{Protezione}, ovvero come dovrebbe essere organizzato il sistema in modo che i beni critici possano essere protetti dagli attacchi esterni
    \item \textbf{Distribuzione}, ovvero come dovrebbero essere distribuiti i beni in modo da minimizzare gli effetti di un attacco andato a buon fine
\end{itemize}
Potenzialmente questi due aspetti sono in conflitto tra di loro o con gli altri requisiti dell'applicazione, in generale tuttavia è comunque sempre necessario trovare il miglior compromesso tra i vari requisiti, cercando di non ignorarne nessuno ma allo stesso tempo rispettando le priorità che sussitono tra di essi. 

\paragraph{Fronte applicativo e infrastrutturale}
La \textit{sicurezza dell'applicazione} è un problema di ingegnerizzazione del software dove gli ingegneri devono garantire che il sistema sia progettato per resistere agli attacchi, mentre la \textit{sicurezza dell'infrastruttura} è invece un problema manageriale nel quale gli amministratori dovrebbero garantire che l'infrastruttura sia configurata per resistere agli attacchi. Gli amministratori dei sistemi devono inizializzare l'infrastruttura in modo tale che tutti i servizi di sicurezza siano disponibili ed allo stesso tempo monitorare e riparare eventuali falle di sicurezza che emergono durante l'uso del software.



\subsection{Linee guida per la progettazione sicura}
È possibile tuttavia affidarsi ad alcune linee guida di carattere ampio:
\begin{itemize}
    \item Basare le decisioni della sicurezza su una politica esplicita
    \item Evitare un singolo punto si fallimento
    \item Fallire in modo certo
    \item Bilanciare sicurezza e usabilità
    \item Essere consapevoli dell'esistenza dell'ingegneria sociale
    \item Usare ridondanza e diversità riduce i rischi
    \item Validare tutti gli input
    \item Dividere in compartimenti i beni
    \item Progettare per il deployment
    \item Progettare per il ripristino
\end{itemize}

\paragraph{Basare la sicurezza su policy}
La ``Security Policy'' è un documento di alto livello che definisce ``cosa'' è la sicurezza ma non ``come'' ottenerla. Questo documento infatti non definisce i meccanismi usati per fornire e far rispettare la sicurezza, in quanto gli aspetti della security policy dovrebbero originare dei requisiti di sistema. In pratica ciò accade raramente, specie se viene adottato un processo di sviluppo rapido. Di conseguenza i progettisti dovrebbero consultare la policy sia nelle decisioni di progettazione che nella loro valutazione, incorporando le politiche di sicurezza all'interno della progettazione con lo scopo di:
\begin{itemize}
    \item specificare come le informazioni possano essere accedute
    \item definire quali precondizioni debbano essere testate per l'accesso all sistema
    \item definire a chi vada concesso l'accesso al sistema
\end{itemize}

Tipicamente le politiche vengono rappresentate come un insieme di regole e condizioni, le quali devono essere incorporate in uno specifico componente del sistema chiamato ``Security Authority'', il quale ha il compito di far rispettare le politiche all'interno dell'applicazione.

A livello progettuale le politiche di sicurezza sono suddivise in sei specifiche categorie:
\begin{itemize}
    \item Identity policies: definiscono le regole per la verificadelle credenziali degli utenti
    \item Access control policies: definiscono le regole da applicare sia alle richieste di accesso alle risorse sia all'esecuzione di specifiche operazioni messe a disposizione dall'applicazione
    \item Content-specific policies: definiscono le regole da applicare a specifiche informazioni durante la memorizzazione e la comunicazione
    \item Network and infrastructure policies: definiscono le regole per controllare il flusso dei dati e il deployment sia delle reti che dei servizi infrastrutturali di hosting pubblici e privati
    \item Regulatory policies: definiscono le regole a cui l'applicazione deve sottostare per soddisfare i requisiti legali e di regolamentazione delle leggi in vigore nel Paese/Stato in cui il sistema opera
    \item Advisor and information policies: queste regole non sono imposte, ma sono caldamente consigliate in riferimento alle regole dell'organizzazione e al ruolo delle attività di business. Per esempio queste regole possono essere applicate per informare il personale sull'accesso ai dati sensibili o per stabilire comunicazioni commerciali con partner esterni
\end{itemize}

\paragraph{Evitare un punto singolo di fallimento}
Nei sistemi critici è buona norma di progettazione quella di cercare di evitare un singolo punto di fallimento. Questo perché un singolo fallimento in una parte del sistema non si trasformi nel fallimento di tutto il sistema. Per quanto riguarda la sicurezza questo significa che non ci si dovrebbe affidare a un singolo meccanismo per assicurarla, ma si dovrebbero impiegare differenti tecniche (\textbf{defence in depth}).

\paragraph{Fallire in modo certo}
Qualche tipo di fallimento è inevitabile in tutti i sistemi, ma i sistemi critici per la sicurezza dovrebbero \textbf{sempre} fallire in modo sicuro e deterministico. Ad esempio non si dovrebbero avere procedure di fallback meno sicure del sistema stesso, poiché, anche se il sistema dovesse fallire, non deve essere consentito alcun accesso ad un attaccante.

\paragraph{Bilanciare sicurezza e usabilità}
Sicurezza e usabilità sono spesso in contrasto. Questo accade poiché per avere sicurezza occorre introdurre un numero di controlli che garantiscano che gli utenti siano autorizzati a usare il sistema e che nello stesso tempo agiscano in accordo alle politiche di sicurezza. Questo fatto inevitabilmente ricade sull'utente finale che deve spendere più tempo per imparare ad utilizzare il sistema.

Ogni volta che si aggiunge una caratteristica di sicurezza al sistema questo inevitabilmente diventa meno usabile. A tal proposito alle volte può diventare controproduttivo introdurre nuove caratteristiche di sicurezza a spese dell'usabilità, dato che superata una certa soglia l'utente comincerà attivamente a comportarsi in modo scorretto per poter risparmiare tempo (vedi \ref{es:pass_postit}).

\begin{esempio}{}{pass_postit}
    Se i vincoli sulle passowrd fossero troppo rigidi si otterrebbe l'effetto che certi utenti comincerebbero a tener traccia delle password su dei postit
\end{esempio}

\paragraph{Essere consapevoli dell'\gls{ing_soc}}
Questi approcci si avvantaggiano della ``volontà di aiutare'' delle persone e della loro fiducia nell'organizzazione. Dal punto di vista della progettazione contrastare l'ingegneria sociale è quasi impossibile. Se la sicurezza è molto critica non si dovrebbe far affidamento solo a meccanismi di autenticazione basati su password, ma bisognerebbe utilizzare meccanismi di \textbf{autenticazione forte}.

Per quantomeno accorgersi della presenza di un attacco possono essere utili sia meccanismi di log che tracciano sia la locazione che l'identità dell'utente sia programmi di analisi del log potrebbero essere utili ad identificare brecce nella sicurezza.

\paragraph{Usare ridondanza e diversità}\label{par_ridondanza_sec}\label{par_diversita_sec}
La ridondanza risulta utile per ridurre gli effetti di un attacco in quanto consente di fare in modo che un attacco eseguito con successo su una copia dei dati non ha impatto (potenzialmente) su una seconda copia dei dati. L'utilizzo della ridondanza però ha anche effetti secondari poco desiderabili come un aumento dei costi e dei problemi gestionali per fare in modo che tutte le copie siano allineate.

Similmente la diversità riveste un ruolo importante nella robustezza di un sistema. Spesso gli attacchi sfruttano vulnerabilità specifiche di una certa tecnologia, vulnerabilità che dunque non sono condivise da altre tecnologie. L'utilizzo di tecnologie differenti consente quindi di mitigare un attacco, in quanto per ogni tecnologia occorre usare un vettore d'attacco differente. Come per la ridondanza anche l'utilizzo di diverse tecnologie può porre problemi di tipo gestionale ed organizzativo.

\paragraph{Validare tutti gli input}
Un comune attacco consiste nel fornire input inaspettati che causano un comportamento imprevisto (crash, perdita della disponibilità del servizio, esecuzione di codice malevolo). Tipici esempi sono \textbf{buffer overflow} e \textbf{SQL injection}. Molti di questi problemi possono essere evitati progettando e testando la validazione dell'input in tutto il sistema. Nei requisiti dovrebbero essere definiti tutti i controlli che devono essere applicati.

\paragraph{Dividere in compartimenti i beni}
Una buona pratica è quella di organizzare le informazioni nel sistema in modo che gli utenti abbiano accesso solo alle informazioni necessarie, piuttosto che a tutte le informazioni del sistema. Gli effetti di un attacco in questo modo sono più contenuti: qualche informazione sarà persa o danneggiata, ma è meno probabile che tutte le informazioni del sistema siano coinvolte.

\paragraph{Progettare per il deployment}\label{par_prog_deployment}
Molti problemi di sicurezza sorgono perché il sistema non viene configurato correttamente al momento del deployment, il quale coinvolge la configurazione del sistema per operare in un determinato ambiente, l'installazione vera e propria del software ed infine la configurazione del sistema installato.
È sempre necessario progettare il sistema in modo che possa essere configurato in maniera automatica tramite strumenti appositi, i quali servono a ridurre le possibilità che si verifichi un errore di configurazione ed anche a verificare che l'installazione e la configurazione siano avvenute nel modo corretto.

Esistono delle linee guida generali:
\begin{itemize}
    \item Supportare la visione e l'analisi le configurazioni. Questo può essere fatto, ad esempio, sotto forma di un programma di utilità in grado di mostrare in un unico posto tutta la configurazione del sistema
    \item Minimizzare i privilegi di default. Al termine dell'installazione occorre fare attenzione al fatto che la configurazione di default fornisca solamente i minimi privilegi necessari
    \item Localizzare le impostazioni di configurazione. Occorre raggruppare tutte le risorse per la configurazione per categoria, in modo che visualizzando una categoria di impostazioni sia possibile osservare \textit{tutte} le impostazioni associate e non solo una parte. Questo riduce la probabilità che un'impostazione non venga usata per il solo fatto che si trova in un posto diverso
    \item Fornire modi per rimediare a vulnerabilità di sicurezza. Ogni sistema dovrebbe includere meccanismi diretti per l'aggiornamento e per la riparazione delle vulnerabilità, ad esempio controllo automatico degli aggiornamenti di sicurezza
\end{itemize}

\paragraph{Progettare per il ripristino}
È buona norma progettare il sistema con l'assunzione che gli errori di sicurezza possano accadere. Si deve quindi pensare a come ripristinare il sistema dopo possibili errori e riportarlo a uno stato operazionale sicuro.

\subsection{Sistemi critici}
La definizione di una politica di sicurezza deve tenere conto di vincoli tecnici, logistici, amministrativi, politici ed economici, imposti dalla struttura organizzativa in cui il sistema opera. Per questo serve introdurre la sicurezza sin dalle prime fasi di analisi dei requisiti di un nuovo sistema. Le vigenti leggi, le politiche e i vincoli aziendali sono la base di partenza per la definizione di un piano per la sicurezza.

Un'applicazione o un servizio possono consistere di uno o più componenti funzionali allocati localmente o distribuiti sulla rete. La sicurezza viene vista come un processo complesso, come una catena di caratteristiche. La sfida maggiore lanciata ai progettisti è quella di progettare applicazioni sicure e di qualità che tengano conto in modo strutturato di tutti gli aspetti della sicurezza sin dalle prime fasi di analisi del sistema.

Esistono tre principali tipi di \glspl{sistema_critico}:
\begin{itemize}
    \item Sistemi safety-critical: i fallimenti possono provocare incidenti, perdita di vite umane o seri danni ambientali
    \item Sistemi mission-critical: i malfunzionamenti possono causare il fallimento di alcune attività e obiettivi diretti
    \item Sistemi business-critical: i fallimenti possono portare a costi molto alti per le aziende che li usano
\end{itemize}

Per sistemi di questo tipo la proprietà più importante è la fidatezza, data dalla combinazione di \textit{disponibilità}, \textit{affidabilità}, \textit{sicurezza e protezione}. L'importanza è data dal fatto che i costi dovuti ad un fallimento possono essere enormi e dal fatto che sistemi inaffidabili possono causare la perdita di informazioni.

\subsection{Minacce e controlli}
Esistono vari tipi di \glspl{minaccia}:
\begin{itemize}
    \item Minacce alla riservatezza del sistema o dei suoi dati. In questo caso le informazioni posso essere svelate a persone o programmi non autorizzati
    \item Minacce all'integrità del sistema o dei suoi dati. In questo caso i dati o il software possono essere danneggiati o corrotti
    \item Minacce alla disponibilità del sistema o dei suoi dati. In questo caso può essere negato l'accesso agli utenti autorizzati al software o ai dati
\end{itemize}
In generale queste minacce sono interdipendenti, ovvero l'una può causare l'altra. Ad esempio se un attacco rende il sistema non disponibile, la modifica sulle informazioni potrebbe non avvenire, rendendo così il sistema non integro.

Al fine di prevenire tali minacce è possibile avvalersi di uno o più \glspl{controllo}:
\begin{itemize}
    \item Controlli per garantire che gli attacchi non abbiano successo. In questo caso la strategia è quella di progettare il sistema in modo da evitare i problemi di sicurezza. Ad esempio i sistemi militari sensibili non sono connessi alla rete pubblica
    \item Controlli per identificare e respingere attacchi. In questo caso la strategia è quella di monitorare le operazioni del sistema e identificare pattern di attività atipici, nel caso agire di conseguenza, ad esempio spegnendo parti del sistema, restringere l'accesso agli utenti, \ldots
    \item Controlli per il ripristino come backup, replicazione, polizze assicurative
\end{itemize}

\subsection{Analisi del rischio}

L'analisi del rischio si occupa di valutare le possibili perdite che un attacco
può causare ai beni di un sistema ed allo stesso tempo di bilanciare queste perdite con i costi richiesti per la protezione dei beni stessi stando attenti a fare in modo che il costo della protezione sia sempre minore\footnote{Si spera molto minore} del costo della perdita. Si tratta di una problematica più manageriale che tecnica. All'interno di questo panorama il ruolo degli ingegneri della sicurezza è quindi quello di fornire una guida tecnica e giuridica sui problemi di sicurezza del sistema. Sarà poi compito dei manager decidere se accettare i costi della sicurezza o i rischi che derivano dalla mancanza di procedure di sicurezza.

L'analisi del rischio inizia dalla valutazione delle politiche di sicurezza organizzazionali che spiegano cosa dovrebbe e cosa non dovrebbe essere consentito fare. Le politiche di sicurezza propongono le condizioni che dovrebbero sempre essere mantenute dal sistema di sicurezza, quindi aiutano ad identificare le minacce
che potrebbero sorgere. La valutazione del rischio è un processo in più fasi.

\subsubsection{Valutazione preliminare del rischio}
Questa prima fase consiste nel determinare i requisiti di sicurezza dell'intero sistema.

\paragraph{Identificazione del \gls{bene}}
Analizzando le risorse fisiche e logiche, nonché le relazioni che intercorrono fra di esse lo scopo è quello di elencare tutte le risorse di valore presenti all'interno del sistema.

\paragraph{Identificazione delle \glspl{minaccia}}
Considerando come eventi indesiderati qualsiasi tipo di evento di accesso non esplicitamente permesso, lo scopo è quello di definire tutte le casistiche che non devono presentarsi all'interno del sistema, siano essi attacchi intenzionali che eventi accidentali. Ad ogni minaccia individuata occorre associare un \gls{rischio} che tiene conto sia della facilità con l'evento possa presentarsi sia del potenziale danno che il verificarsi di tale evento possa procurare\footnote{Occorre tenere anche presente che i rischi raramente sono idnipendenti l'uno dall'altro, in quanto l'attuarsi di una situazione di rischio può facilitare o consentire l'avverarsi di un'altra situazione}.

\paragraph{Individuazione e valutazione del \gls{controllo}}
Occorre scegliere i \glspl{controllo} da adottare per neutralizzare gli attacchi individuati, valutando per ognuno di essi rapporto costo/efficacia\footnote{Tenendo conto anche di costi nascosti come decadimento delle prestazioni, peggioramento dell'ergonomia di un sistema e costi burocratici} e l'aderenza a standard e modelli di riferimento. Sono possibili sia controlli di carattere organizzativo che di carattere tecnico.

Un insieme di controlli non deve presentarsi come una ``collezione di espedienti'' non correlati tra loro. È importante quindi integrare i vari controlli in una politica di sicurezza organica, tenendo conto dei seguenti vincoli:
\begin{itemize}
    \item Completezza: il sottoinsieme deve fare fronte a tutti gli eventi indesiderati
    \item Omogeneità: le contromisure devono essere compatibili tra loro e deve essere possibile integrarle tra loro
    \item Ridondanza controllata: la ridondanza delle contromisure ha un costo e deve essere rilevata e vagliata accuratamente
    \item Effettiva attuabilità: l'insieme delle contromisure deve rispettare tutti i vincoli imposti dall'organizzazione nella quale andrà ad operare
\end{itemize}

\subsubsection{Ciclo di vita della valutazione del rischio}
La valutazione del rischio dovrebbe essere parte di tutto il ciclo di vita del software: dall'ingegnerizzazione dei requisiti al deployment del sistema. Mano a mano che il processo di progettazione e di sviluppo procede si guadagna sempre maggior consapevolezza dell'architettura del sistema ed una sempre maggior comprensione sui dettagli.

Il processo seguito è simile a quello della valutazione preliminare dei rischi, con l'aggiunta di attività riguardanti l'identificazione e la valutazione delle \glspl{vulnerabilità}. La valutazione delle vulnerabilità identifica i beni che hanno più probabilità di essere colpiti da tali vulnerabilità. Vengono inoltre messe in relazione le vulnerabilità con i possibili attacchi al sistema. Il risultato della valutazione del rischio è un insieme di decisioni ingegneristiche che influenzano la progettazione o l'implementazione del sistema o limitano il modo in cui esso è usato.

\subsection{Security use case e misuse case}
Similmente agli use case usati per la modellazione del progetto, security use case e security misuse modellano i requisiti di sicurezza del sistema. I misuse case si concentrano sulle interazioni tra l'applicazione e gli attaccanti che cercano di violarla. La condizione di successo di un misuse case è un attacco andato a buon fine. Questo li rende particolarmente adatti per analizzare le minacce, ma non molto utili per la determinazione dei requisiti di sicurezza. È invece compito dei security use case specificare i requisiti tramite i quali l'applicazione dovrebbe essere in grado di proteggersi dalle minacce. Essi non dovrebbero mai specificare meccanismi di sicurezza, in quanto tali decisioni vanno lasciate alla fase di progettazione.

\subsection{Specifica dei requisiti di sicurezza}
Non è sempre possibile specificare i requisiti associati alla sicurezza in modo quantitativo, tant'è che quasi sempre questa tipologia dei requisiti è espressa nella forma ``non deve'', definendo comportamenti inaccettabili per il sistema. L'approccio convenzionale della specifica dei requisiti è basato sul contesto, sui beni da proteggere e sul loro valore per l'organizzazione. Si individuano varie categorie di requisiti di sicurezza:
\begin{itemize}
    \item Requisiti di identificazione: specificano se un sistema debba identificare gli utenti prima di interagire con loro
    \item Requisiti di autenticazione: specificano come autenticare gli utenti
    \item Requisiti di autorizzazione: specificano i privilegi e i permessi di accesso degli utenti identificati
    \item Requisiti di immunità: specificano come il sistema deve proteggersi da virus, worm e minacce simili
    \item Requisiti di integrità: specificano come evitare la corruzione dei dati
    \item Requisiti di scoperta delle intrusioni: specificano quali meccanismi utilizzare per scoprire gli attacchi al Sistema
    \item Requisiti di non-ripudiazione: specificano che una parte interessata in una transazione non può negare il proprio coinvolgimento
    \item Requisiti di riservatezza: specificano come deve essere mantenuta la riservatezza delle informazioni
    \item Requisiti di controllo della protezione: specificano come può essere controllato e verificato l'uso del sistema
    \item Requisiti di protezione della manutenzione del sistema: specificano come una applicazione può evitare modifiche autorizzate da un accidentale annullamento dei meccanismi di protezione
\end{itemize}


\section{Testare la sicurezza}
Il test di un sistema gioca un ruolo chiave nel processo di sviluppo software e dovrebbe essere eseguito con molta attenzione. Spesso però accade che quest'area sia quella più trascurata durante lo sviluppo del sistema. Ciò può essere attribuito a diversi fattori tra cui la mancanza di comprensione dell'importanza dei test relativi alla sicurezza, la mancanza di tempo, la mancanza di conoscenza su come svolgere un test di sicurezza o anche la mancanza di tool integrati per compiere test.

Il test della sicurezza è un lavoro molto lungo e tedioso, spesso molto più complesso dei test funzionali che vengono svolti normalmente. Inoltre esso coinvolge diverse discipline. Ad esempio è possibile usare i tradizionali test per accertare la sicurezza dei requisiti applicativi che possono essere svolti normalmente dal team di testing, ma esistono dei test non funzionali di ``rottura'' del sistema che devono essere condotti da esperti di sicurezza.

\paragraph{Black box testing}
Questa metodologia di esecuzione dei test ha come assunto di base la non conoscenza dell'applicazione. I tester affrontano l'applicazione come farebbe un attaccante indagando sulle informazioni riguardanti la struttura interna e successivamente applicando un insieme di tentativi di violazione del sistema basati sulle informazioni ottenute.

\paragraph{White box testing}
Questa metodologia di esecuzione dei test ha come assunto di base la completa conoscenza dell'applicazione. I tester hanno accesso a tutte le informazioni di configurazione e anche al codice sorgente ed operano una revisione del codice cercando possibili debolezze. Tipicamente questi tester sono ex-sviluppatori o persone che conoscono molto bene l'ambiente di sviluppo

\section{Capacità di sopravvivenza del sistema}
Con il termine capacità di sopravvivenza si intende la capacità del sistema di continuarea fornire i servizi essenziali agli utenti legittimi mentre è sotto attacco o anche dopo che parti del sistema sono state danneggiatecome conseguenza di un attacco o di un fallimento. Si tratta di una proprietà dell'intero sistema, non dei singoli componenti di questo, tuttavia occorre partire da una progettazione attenta dei componenti per far sì che il sistema nel complesso risulti robusto.

L'analisi e la progettazione della capacità di sopravvivenza dovrebbero essere parte del processo di ingegnerizzazione dei sistemi sicuri in quanto la disponibilità dei servizi critici è l'essenza della sopravvivenza. Questo implica conoscere quali sono i servizi maggiormente critici, come questi servizi possono essere compromessi, qual è la qualità minima dei servizi che deve essere mantenuta, come proteggere questi servizi e  come ripristinare velocemente il sistema se i servizi diventano non disponibili.

Il \textbf{Survivable Analysis Systems} è un metodo di analisi ideato agli inizi del 2000 per valutare le vulnerabilità nel sistema e supportare la progettazione di architetture e caratteristiche che promuovono la sopravvivenza del sistema. Si tratta di un processo a quattro fasi (capire il sistema, identificare i servizi critici, simulare gli attacchi ed analizzare la sopravvivenza) secondo cui la sopravvivenza di un sistema dipende da tre strategie complementari:
\begin{itemize}
    \item Resistenza: consiste nell'evitare problemi costruendo all'interno del sistema le capacità di respingere attacchi
    \item Identificazione: consiste nell'individuare problemi costruendo all'interno del sistema le capacità di riconoscere attacchi e fallimenti e valutare il danno risultante
    \item Ripristino: consiste sia nel tollerare problemi costruendo all'interno del sistema le capacità di fornire servizi essenziali durante un attacco sia nel ripristinare le complete funzionalità dopo l'attacco
\end{itemize}

L'aggiunnta di tecniche di sopravvivenza è una pratica che può comportare spese anche ingenti, tant'è che spesso le aziende sono molto riluttanti ad investire sulla sopravvivenza, specie se non hanno mai subito attacchi e perdite. In realtà sarebbe buona norma investire nella sopravvivenza prima piuttosto che dopo aver già subito un attacco.


\section{\acrlong{gdpr_norm}}
Dal 2018 è in vigore la nuova normativa europea relativa al trattamento dei dati personali e della privacy, la \acrfull{gdpr_norm}. Occorre aderire a questa norma nel caso in cui i dati vengano trattati all'interno dell'\acrlong{ue_europe} o nel caso in cui siano relativi a cittadini dell'\acrshort{ue_europe}. In generale la norma impone i concetti di:
\begin{itemize}
    \item Privacy by design
    \item Privacy by default
    \item Anonimizzazione e/o \gls{pseudonimizzazione} dei dati
    \item Adeguatezza delle misure di sicurezza
\end{itemize}

La gestione dei dati personali non è qualcosa che possa essere aggiunto in seguito, ma è qualcosa che deve essere considerato fin da subito durante la progettazione. I dati personali devono essere gestiti secondo i seguenti principi:
\begin{itemize}
    \item Devono trattati in modo lecito, equo e trasparente nei confronti dell'interessato (``liceità, equità e trasparenza'')
    \item Devono essere raccolti per finalità determinate, esplicite e legittime, e successivamente trattati in modo non incompatibile con tali finalità
    \item Devono essere adeguati, pertinenti e limitati a quanto necessario rispetto alle finalità per le quali sono trattati (``minimizzazione dei dati'')
    \item Devono essere esatti e, se necessario, aggiornati. Di conseguenza devono essere prese tutte le misure ragionevoli per cancellare o rettificare tempestivamente i dati inesatti rispetto alle finalità per le quali sono trattati (``esattezza'')
    \item Devono essere conservati in una forma che consenta l'identificazione degli interessati per un arco di tempo non superiore al conseguimento delle finalità per le quali sono trattati. I dati personali possono essere conservati per periodi più lunghi a condizione che siano trattati esclusivamente per finalità di archiviazione nel pubblico interesse o per finalità di ricerca scientifica e storica o per finalità statistiche
    \item Devono essere trattati in modo da garantire un'adeguata sicurezza dei dati personali, compresa la protezione, mediante misure tecniche e organizzative adeguate, da trattamenti non autorizzati o illeciti e dalla perdita, dalla distruzione o dal danno accidentali (``integrità e riservatezza'')
\end{itemize}
