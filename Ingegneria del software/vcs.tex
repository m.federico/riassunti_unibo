\chapter{Version Control System}

Durante lo sviluppo di ogni programma si presenta le necessità di gestire versioni multiple. Sono versioni i diversi rilasci, le eventuali variazioni specifiche per le diverse piattaforme, o più semplicemente quelle che si ottengono dopo la modifica del codice.

Occorre un sistema per tener traccia delle versioni di un progetto in modo da poter gestire al meglio i cambiamenti. A questo proposito esiste una classe di programmi chiamata \acrfull{vcs}.

È possibile usare soluzioni casalinghe, ad esempio tenendo traccia delle differenze mediante l'uso di diverse copie del progetto. Questo può funzionare all'inizio di piccolissimi progetti a cui lavora una singola persona. Tuttavia al crescere del progetto, del numero di modifiche, o del numero di partecipanti diventa impossibile tenere traccia dei cambiamenti in modo preciso e comodo. 

In generale quindi ci si appoggia a soluzioni standard (Git, SVN, \ldots) in grado di fornire varie funzionalità come:
\begin{itemize}
    \item Memorizzazione efficiente del codice sorgente
    \item Supporto alla cronologia dei cambiamenti
    \item Supporto alla collaborazione di più sviluppatori
\end{itemize}

\section{Concetti di un \acrshort{vcs}}

I seguenti concetti sono comuni a vari \acrshort{vcs}.

\begin{definizione}{Progetto}{progetto_vcs}
    Un progetto è un insieme di file nel version control, non importa di che tipo
\end{definizione}

\begin{definizione}{Repository}{repo_vcs}
    È dove sono memorizzati i file correnti ed i dati storici. È tipicamente un server remoto aﬃdabile. Tutti gli utenti condividono lo stesso repository, ma possono avere diverse cartelle di lavoro (\ref{def:workdir_vcs}).
\end{definizione}

\begin{definizione}{Cartella di lavoro}{workdir_vcs}
    È la copia locale di un repository in un dato istante
\end{definizione}

Ogni cartella di lavoro è un ambiente isolato e separato da quello principale contenuto all'interno del repository, quindi le modifiche fatte sulla copia locale non si ripercuotono automaticamente sul repository principale. L'operazione fatta per ottenere una copia locale a partire dal repository si dice \textit{check out}, mentre l'operazione inversa è detta \textit{check in}.

\begin{definizione}{Revisioni}{revision_vcs}
    Nuovo stato del repository a seguito di un'operazione di \textit{check in}
\end{definizione}

Consideriamo le seguenti operazioni:
\begin{itemize}
    \item Eseguire il \textit{check out} di un repository
    \item Eseguire una serie di modifiche
    \item Eseguire il \textit{check in}
\end{itemize}

Questa serie di operazioni crea una nuova revisione. Per efficienza, generalmente i \acrshort{vcs} non memorizzano per interno il nuovo stato del repository, ma solamente le differenze rispetto allo stato precedente\footnote{Questa soluzione risulta efficiente dal punto di vista dello spazio occupato, tuttavia potrebbe rendere le operazioni di \textit{check in} e \textit{check out} più lente}.

\begin{definizione}{Branch}{}
    Rappresenta un ramo di sviluppo indipendente. È possibile creare, distruggere ed unire branch
\end{definizione}

\section{Modelli di utilizzo di un \acrshort{vcs}}

Quando un \acrshort{vcs} viene utilizzato da più utenti in contemporanea bisogna evitare che le modifiche fatte da un utente vengano sovrascritte dal \textit{check in} fatto da un secondo utente. 

\subsection{Lock-Modify-Unlock}
Questo modello permette di modificare un dato file ad un solo utente alla volta. Ogni volta che un utente vuole modificare un file (o un insieme di file) occorre eseguire le seguenti operazioni:
\begin{enumerate}
    \item Bloccare il/i file
    \item Eseguire le modifiche
    \item Eseguire il \textit{check in}
    \item Sbloccare il/i file\footnote{L'utente \textbf{deve} ricordarsi di sbloccare tutti i file interessati, altrimenti altri utenti non possono apportarvi modifiche}
\end{enumerate}

Questa soluzione pone vari problemi tra cui:
\begin{itemize}
    \item Serializzazione del carico di lavoro. Spesso due utenti non devono apportare modifiche alla stessa sezione di uno stesso file, tuttavia utilizzando dei lock questo tipo di collaborazione diventa impossibile
    \item Crea un falso senso di sicurezza. Non è detto che rilasciato il lock il repository nel complesso si trovi in uno stato valido (ad esempio due utenti potrebbero modificare due file diversi introducendo modifiche non compatibili tra loro)
\end{itemize}

\subsection{Copy-Modify-Merge}

In questo caso non vengono utilizzati lock. Ogni operazione di \textit{check in} viene eseguita sul repository direttamente. Se il \acrshort{vcs} si accorge che la base della copia di lavoro locale non è allineata al repository (ovvero tra il momento in cui è stato eseguito il \textit{check out} ed il \textit{check in} qualcuno ha eseguito il \textit{check in} di alcune modifiche) possono nascere diversi scenari:
\begin{itemize}
    \item Se le modifiche interessano file diversi, il \textit{check in} viene accettato
    \item Se le modifiche interessano gli stessi file, ma in sezioni diverse (non sovrapposte), il \textit{check in} viene accettato
    \item Se le modifiche interessano gli stessi file in sezioni sovrapposte, il \textit{check in} viene rifiutato in quanto sono presenti dei conflitti (vedi \ref{def:conflitto_vcs})
\end{itemize}

\begin{definizione}{Conflitto}{conflitto_vcs}
    Si hanno quando due programmatori modificano la stessa parte di codice. In questa situazione il sistema non può prendere alcuna azione in automatico, pertanto richiede la risoluzione manuale.
    
    I conflitti sono sintattici, basati sulla vicinanza delle modifiche, ovvero vanno in conflitto i cambiamenti sulla stessa riga
\end{definizione}