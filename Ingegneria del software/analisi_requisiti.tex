\chapter{Analisi dei requisiti}

\section{Requisiti}

I requisiti rappresentano la descrizione dei servizi forniti e dei vincoli operativi. Si hanno più tipi di requisiti. I \textbf{requisiti utente} dichiarano quali servizi il sistema dovrebbe fornire, i vincoli sotto cui deve operare. Questi requisiti sono spesso molto astratti e di alto livello. Tipicamente sono espressi in linguaggio naturale, o con l'ausilio di qualche diagramma. I \textbf{requisiti di sistema} definiscono le funzioni, i servizi ed i vincoli operativi del sistema in modo dettagliato. Forniscono una descrizione dettagliata di quello che il sistema deve fare e possono essere divisi in requisiti funzionali, non funzionali e di dominio.

L'insieme dei requisiti viene contenuto all'interno del ``documento dei requisiti'', il quale fa parte del contratto tra committente e progettista e quindi deve essere preciso e definire esattamente cosa debba essere sviluppato.

\paragraph{Requisiti funzionali}
I requisiti funzionali quello che il sistema dovrebbe fare. Si tratta di elenchi di servizi che il sistema deve offrire, per ognuno dei quali occorre indicare come reagire a particolari input, come comportarsi in situazioni particolari e soprattutto cosa il sistema non dovrebbe fare.

Le specifiche devono essere complete, cioè tutti i servizi devono essere definiti, e coerenti, cioè i requisiti non devono avere informazioni contraddittorie.

\paragraph{Requisiti non funzionali}
I requisiti non funzionali non riguardano direttamente le funzionalità del prodotto (come suggerisce il nome), ma riguardano varie condizioni al contorno. Possono essere di tre tipi:
\begin{itemize}
    \item Requisiti del prodotto, i quali specificano o limitano le proprietà complessive del sistema (es. affidabilità, prestazioni, \ldots)
    \item Requisiti Organizzativi, i quali possono vincolare anche il processo di sviluppo adottato (es. politiche e procedure dell'organizzazione cliente e sviluppatrice)
    \item Requisiti esterni: si identificano tutti i requisiti che derivano da fattori non provenienti dal sistema e dal suo processo di sviluppo (es. necessità di interoperabilità con altri sistemi, requisiti legislativi, requisiti etici, \ldots)
\end{itemize}

I requisiti di questo tipo possono essere difficili da verificare perché spesso sono vaghi e sono mescolati ai requisiti funzionali veri e propri. Spesso infine essi contrastano con i requisiti funzionali (es. requisiti di privacy confliggono con la facilità d'suo di un sitema). Per questo motivo questa classe di requisiti va studiata con molta cura, esplicitando quanto più possibile ogni requisito ed i collegamenti che ha o può avere con gli altri requisiti.

\paragraph{Requisiti di dominio}
I requisiti di dominio derivano dal dominio\footnote{Chi l'avrebbe mai detto?} di applicazione del sistema. Tipicamente sono formulati utilizzando la terminologia specifca del dominio si rifanno ai suoi concetti.

Spesso, soprattuto se il dominio è nettamente differente da quelli conosciuti dai progettisti, risulta complesso capire come questo elenco di requisiti si possa integrare con il resto dei requisiti. Per questo l'analisi di tali requisiti richiede il coinvolgimenti con degli esperti del settore.

\section{Analisi dei requisiti}
L'analisi dei requisiti ha come obiettivo quello di definire le proprietà che il sistema dovrà avere senza descriverne la realizzazione. Come risultato l'analisi produce una serie di documenti con la descrizione dettagliata dei requisiti ed una base di partenza per l'analisi del problema.

Per determinare al meglio i requisiti occorre interagire molto con l'utente e
conoscere l'area applicativa (eventualmente con l'aiuto di uno o più esperti del sistema).

È normale che i requisiti cambino ed evolvano nel tempo. Queste modifiche vanno gestite, tenendo conto del fatto che più lo sviluppo si trova in fase avanzata più il cambiamento è costoso. Di conseguenza quando si cambia bisogna valutare la fattibilità, l'impatto e il costo.

\paragraph{Raccolta e validazione dei requisiti}
Durante questa fase occorre raccogliere tutte le informazioni su cosa il sistema debba fare secondo le intenzioni del cliente. Non ci sono passi formali predefiniti, in quanto l'esecuzione della fase dipende dal problema.

Il completamento della fase produce un documento testuale approvato dal cliente e scritto da un'analista, contenente un accenno di glossario contenente tutti i termini
senza ambiguità.

Questa fase coinvolge analisti, clienti e a volte esperti del dominio tramite varie modalità come interviste\footnote{La gestione delle interviste è complessa in quanto i clienti possono avere una vaga idea dei requisiti, non essere in grado di
    spiegarli chiaramente, oppure avere attese irreali. I clienti inoltre possono essere
    poco disponibili a collaborare}, studio di documentazione, osservazioni passive e/o attive e prototipazione.

Ogni requisito va validato e negoziato con i clienti. Si svolge in parallelo alla raccolta. Ogni requisito deve essere valido (inerente al problema da risolvere), coerente (non in conflitto o sovrapposto ad altri), realizzabile e completo.

Il documento dei requisiti prodotto al termine della fase deve specificare in modo chiaro ed univoco cosa il sistema dovrà fare. All'interno del documento i requisiti devono essere chiari, precisi, corretti, coerenti, completi, tracciabili, concisi, modificabili, non ambigui, verificabili.

\paragraph{Analisi del dominio}
Questa fase ha come obiettivo la definizione della porzione di mondo reale rilevante per il sistema. In questa fase un principio importante è l'astrazione, la quale permette di gestire la complessità intrinseca del mondo reale ignorando gli aspetti che non sono importanti o rilevanti per gli scopi attuali.
Come risultato viene prodotto una prima versione di vocabolario basata sui sostantivi dei requisiti.

Per eseguire l'analisi è possibile anche fare rifermento a sistemi afferenti la area applicativa, identificando entità e comportamenti comuni ai sistemi e realizzando schemi e componenti riusabili.

\paragraph{Analisi dei requisiti}
All'interno di questa fase occorre definire il comportamento del sistema da realizzare. Il risultato è un modello comportamentale o dinamico che descriva in modo chiaro e coinciso le funzionalità del sistema, cosa debba fare per soddisfare il cliente. A tal proposito ci sono due strategia:
\begin{itemize}
    \item Scomposizione funzionale, che consiste nell'identificare le singole funzionalità previste dal sistema
    \item Astrazione procedurale, che consiste nel considerare ogni operazione come un'entità, anche se realizzata da più operazioni di basso livello
\end{itemize}

Durante l'analisi occorre porre particolare attenzione ai sostantivi ed ai verbi che compaiono nel testo di specifica dei requisiti. Dall'analisi dei sostantivi è possibile formalizzare la conoscenza sul dominio applicativo (costruzione di un primo modello del dominio), mentre dall'analisi dei verbi è possibili individuare l'insieme delle azioni che il sistema dovrà compiere (modello dei casi d'uso)

\paragraph{Analisi e gestione rischi}
Questa fase consiste in un'analisi sistematica e completa di tutti i possibili rischi che possono far fallire o intralciare la realizzazione del sistema in qualsiasi momento. Ogni rischio ha una probabilità che avvenga\footnote{Se la probabilità è al 100\% si tratta di un vincolo} e un costo.
Di seguito un piccolo elenco di rischi:
\begin{itemize}
    \item Rischi relativi ai requisiti (es. il sistema non soddisfa le esigenze del cliente)
    \item Rischi relativi alle risorse umane (es. non hanno la giusta esperienza per il progetto
    \item Rischi relativi alla protezione dei dati e della privacy (es. quali dati trattare e come proteggerli da attacchi informatici)
    \item Rischi tecnologici (es. se la tecnologia è corretta e quali saranno i suoi futuri sviluppi)
    \item Rischi politici (es. se ci sono forze politiche che intralciano il progetto)
\end{itemize}

In generale si possono adottare due strategie. Una prima opzione è di tipo reattivo, trovando una soluzione ai problemi mano a mano che si presentano. Una seconda opzione è di tipo preventivo, individuando, classificando e studiando i rischi prima dell'inizio del lavoro tecnico.

\section{Casi d'uso e scenari}
I caso d'uso permettono di formalizzare i requisiti funzionali, aiutando a comprendere il funzionamento del sistema, e permettondo di comunicare meglio con il cliente.
L'insieme dei casi d'uso sono l'immagine del sistema verso l'esterno.

Occorre seguire alcuni passi:
\begin{enumerate}
    \item Individuare il confine del sistema
    \item Individuare gli attori presenti all'interno del sistema o che interagiscono con esso. Ogni attore modella il ruolo interpretato da un utente nei confronti del sistema
    \item Individuare i casi d'uso per gli attori individuati, ogni caso d'uso modella un servizio richiesto dal sistema da un attore o da un altro caso d'uso
    \item Disegnare i diagrammi
    \item Descrivere i dettagli di ogni caso d'uso mediante uno o più scenari, i quali descrivono sia l'interazione tra l'attore ed il sistema sia le elaborazioni necessarie per soddisfare la richiesta
    \item Ricontrollare e validare i casi d'uso con il cliente
\end{enumerate}

Un caso d'uso viene sempre avviato dall'intervento diretto o indiretto di un
attore, e si conclude con successo se l'obiettivo viene raggiunto, con fallimento altrimenti. Viene inoltre sempre descritto da:
\begin{itemize}
    \item $0+$ precondizioni, ovvero condizioni verificate prima dell'esecuzione del caso d'uso
    \item $1+$ scenario, ovvero sequenze di passi che descrivono le interazioni tra l'attore e il sistema per raggiungere l'obiettivo, con eventuali ramificazioni.  Ogni scenario deve essere espresso in forma narrativa usando il vocabolario realizzato durante le analisi. In tal modo il committente potrà comprendere facilmente la descrizione dei casi d'uso e di conseguenza non solo sarà in grado di validare i casi d'uso ma sarà anche incoraggiato a partecipare attivamente alla loro definizione. Ogni caso d'uso possiede uno scenario principale, ed eventuali scenari alternativi (es. per rappresentare situazioni anomale)
    \item $0+$ postcondizioni, ovvero condizioni vere se il caso d'uso viene eseguito con successo
\end{itemize}

I casi d'uso posono essere messi in relazione l'uno con l'altro tramite:
\begin{itemize}
    \item Generalizzazione/specializzazione quando un caso d'uso è simile a un altro ma fa qualcosa di più\footnote{Anche un attore può essere la specializzazione di un altro attore}
    \item Inclusione, quando un caso d'uso usa almeno una volta un altro caso d'uso
    \item Estensione, quando è necessario aggiungere un comportamento opzionale a un
          caso esistente
\end{itemize}
