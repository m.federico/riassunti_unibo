\chapter{Design Pattern}

Come nell'ingegneria tradizionale anche nell'ingegneria del software vengono usati dei pattern per risolvere velocemente specifici problemi noti con soluzioni note. Ognuno dei pattern cattura l'esperienza acquisita nell'affrontare un problema, consentendo di riutilizzare tale esperienza in altri casi simili.

Ogni pattern ha quattro elementi essenziali:
\begin{itemize}
    \item Nome. Identifica il pattern ed aiuta la comunicazione tra progettisti. Inoltre l'attribuzione di un nome aiuta la memorizzazione del pattern
    \item Problema. Descrive le situazioni in cui è possibile applicare il pattern
    \item Soluzione. Descrive il pattern in termini degli elementi che lo compongono, delle relazioni che intercorrono tra di essi delle responsabilità e delle collaborazioni
    \item Conseguenze. Descrivono pro e contro dell'applicazione del pattern, consentendo di valutare eventuali alternative progettuali
\end{itemize}

Si possono identificare tre gruppi di pattern:
\begin{itemize}
    \item Pattern di creazione (creational pattern). Risolvono problemi inerenti il processo di creazione di oggetti
    \item Pattern strutturali (structural pattern). Risolvono problemi inerenti la composizione di classi o di oggetti
    \item Pattern comportamentali (behavioral pattern). Risolvono problemi inerenti le modalità di interazione e di distribuzione delle responsabilità tra classi o tra oggetti
\end{itemize}

\section{Pattern di creazione}

\subsection{Pattern singleton}

\begin{pattern}{Singleton}{}
    Assicura che una classe abbia una sola istanza e fornisce un punto di accesso globale a tale istanza
\end{pattern}

Questo pattern porta ad alcuni vantaggi come:
\begin{itemize}
    \item Consente di essere certi che esista una sola istanza della classe
    \item Controllo dell'accesso forte. Poiché esiste una sola istanza della classe è possibile controllarne l'accesso in maniera puntuale
    \item Consente il raffinamento di operazioni. Poiché è generalmente possibile creare una sottoclasse del singleton è possibile anche eseguire l'override dei suoi metodi
\end{itemize}

L'utilizzo di un signleton è un'alternativa all'utilizzo di una classe statica con soli membri statici.

\subsection{Pattern Builder}

\begin{pattern}{Builder}{}
    Consente di separare la costruzione di un oggetto complesso dalla sua rappresentazione in modo che lo stesso processo di costruzione possa creare rappresentazioni diverse
\end{pattern}

Il pattern Builder risulta utile quando:
\begin{itemize}
    \item il processo di creazione di un oggetto deve essere indipendente dalle parti che compongono l'oggetto e da come vengono composte
    \item il processo di costruzione deve consentire diverse configurazioni dell'oggetto costruito
\end{itemize}

L'utilizzo di questo pattern ha vari vantaggi:
\begin{itemize}
    \item Consente di variare la rappresentazione interna dell'oggetto e la modalità di costruzione
    \item Fornisce controllo granulare durante la costruzione di un oggetto senza imporre l'utilizzo di costruttori aventi molti parametri e senza richiedere l'uso di vari metodi per impostare ognuna delle possibilità di configurazione dell'oggetto
\end{itemize}

\section{Pattern strutturali}

\subsection{Pattern flyweight}

\begin{pattern}{Flyweight}{}
    Descrive come condividere oggetti ``leggeri'' (cioè a granularità molto fine) in modo tale che il loro uso non sia troppo costoso
\end{pattern}

In certi contesti infatti è necessario usare e condividere svariati oggetti, se venisse creata una nuova istanza per ognuno degli utilizzi l'impatto sulla memoria sarebbe non trascurabile. Questo pattern descrive come condividere questi oggetti ``leggeri'' (cioè a granularità molto fine) in modo tale che il loro uso non sia troppo costoso.

In generale un flyweight è un oggetto condiviso che può essere utilizzato simultaneamente ed efficientemente da più clienti (del tutto indipendenti tra loro). Benché condiviso, non deve essere distinguibile da un oggetto non condiviso ed allo stesso tempo non deve fare ipotesi sul contesto nel quale opera. Per assicurare una corretta condivisione i clienti non devono istanziare direttamente i flyweight, ma devono ottenerli esclusivamente tramite una factory.

Nel caso dei flyweight occorre porre particolare attenzione alla gestione dello stato. Si individuano due tipi di stato:
\begin{itemize}
    \item Stato intrinseco. Non dipende dal contesto di utilizzo e può essere condiviso da tutti i clienti. Di conseguenza può essere menorizzato all'interno del flyweight
    \item Stato estrinseco. Dipende dal contesto di utilizzo  e di conseguenza non può essere condiviso dai clienti. Di conseguenza non può essere salvato all'interno del flyweight, ma viene passato come argomento quando viene invocata un'operazione
\end{itemize}

\subsection{Pattern adapter (wrapper)}\label{subsec_pattern_adapter}

\begin{pattern}{Adapter}{}
    Converte l'interfaccia originale di una classe nell'interfaccia (diversa) che si aspetta il cliente. In questo modo consente a classi che hanno interfacce incompatibili di lavorare insieme
\end{pattern}

È utile far riferimento al pattern adapter quando occorre utilizzare una classe esistente e la sua interfaccia non corrisponde a quella necessaria.

\subsection{Pattern decorator}

\begin{pattern}{Decorator}{}
    Permette di aggiungere responsabilità ad un oggetto dinamicamente. Fornisce un'alternativa flessibile alla specializzazione
\end{pattern}

A volte vogliamo aggiungere responsabilità a singoli oggetti, non ad un'intera classe. Un modo per aggiungere responsabilità è con l'eredità. Questa opzione però non è flessibile, tuttavia, perché la scelta viene effettuata staticamente. Un approccio più flessibile consiste nel racchiudere il componente in un altro oggetto che aggiunge la funzionalità desidaerata. L'oggetto che racchiude è chiamato decoratore (\textit{decorator}). Il decoratore è conforme a l'interfaccia del componente che decora in modo che la sua presenza sia trasparente al client del componente. Il decoratore inoltra le richieste al componente e può eseguire azioni aggiuntive prima o dopo l'inoltro. La trasparenza consente di nidificare i decoratori in modo ricorsivo, consentendo così un numero di responsabilità aggiuntive.

Il pattern Decorator risulta utile quando:
\begin{itemize}
    \item occorre aggiungere responsabilità ai singoli oggetti in modo dinamico e trasparente, cioè, senza influenzare altri oggetti
    \item occorre aggiungere o revocare dinamicamente responsabilità
    \item l'estensione per ereditarietà non è praticabile. A volte un gran numero di estensioni indipendenti sono possibili e produrrebbero un'esplosione di sottoclassi per supportare ogni combinazione
\end{itemize}

\subsection{Pattern Composite}

\begin{pattern}{Composite}{}
    Permette di comporre oggetti in una struttura ad albero, al fine di rappresentare una gerarchia di \textit{oggetti contenitore}-\textit{oggetti contenuto}. Permette ai clienti di trattare in modo uniforme oggetti singoli e oggetti composti
\end{pattern}

Il pattern Composite risulta utile quando:
\begin{itemize}
    \item si desidera rappresentare gerarchie di oggetti in relazione \textit{parte-intero}
    \item si desirera che i clienti siano in grado di ignorare la differenza tra le composizioni di oggetti e singoli oggetti. I clienti tratteranno tutti gli oggetti nella composizione in maniera uniforme
\end{itemize}

L'utilizzo di questo pattern ha vari vantaggi:
\begin{itemize}
    \item I clienti possono trattare in maniera uniforme oggetti semplici ed oggetti composti. Questo in generale tende a semplificare la gestione dei client
    \item Rende semplice aggiungere nuovi componenti all'interno della gerarchia, in quanto le nuove classi funzionano automaticamente con quelle già esistenti
\end{itemize}

L'utilizzo del pattern ha anche alcuni svantaggi:
\begin{itemize}
    \item L'architettura complessiva può risultare oltremodo generica, rendendo più complessa la gestione in certi contesti in cui la distinzione tra oggetti contenitore e oggetti contenuto è rilevante
    \item Nel caso in cui gli oggetti della gerarchia mantengano un riferimento all'oggetto contenitore occorre prestare particolare attenzione al fatto che questa invariante venga rispettata
    \item Per rendere contenuto e contenitore il più simile possibile spesso si tende a massimizzare l'interfaccia comune, facendo in modo che molte delle operazioni siano condivise tra i due casi. Questo in generale può andare in conflitto con \acrfull{srp_design}
\end{itemize}

\section{Pattern comportamentali}

\subsection{Pattern observer (Pub-Sub)}

\begin{pattern}{Observer}{}
    Consente di definire una relazione uno-a-molti tra oggetti, in modo che lo stato di un oggetto cambia (soggetto), tutti gli oggetti che dipendono da lui (osservatori) siano aggiornati e notificati automaticamente
\end{pattern}

Questo pattern risulta utile quando:
\begin{itemize}
    \item un'astrazione è composta da vari aspetti, dipendenti l'uno dall'altro. L'incapsulamento di tali aspetti in oggetti separati consente di riutilizzarli indipendentemente
    \item la modifica ad un oggetto richiede la modifica di altri, e non si sa quanti oggetti debbano essere modificati
    \item un oggetto dev'essere in grado di notificare altri oggetti senza fare assunzioni su chi sono questi oggetti. In altre parole, non vuoi questi oggetti strettamente accoppiati
\end{itemize}

Questo pattern porta ad alcuni vantaggi come:
\begin{itemize}
    \item Un incremento del disaccoppiamento tra i soggetti e gli osservatori, i quali possono trovarsi anche a diversi livelli dell'architettura del sistema oppure avere diversi gradi di astrazione
    \item Il supporto nativo per le notifiche broadcast. Questo accade poiché ogni notifica non specifica né quali né quanti oggetti debbano essere notificati, offrendo la possibilità di aggiungere e rimuovere destinatari liberamente
\end{itemize}

Alcune conseguenze negative sono invece:
\begin{itemize}
    \item È possibile che avvengano aggiornamenti inaspettati di grosse dimensioni. Poiché ogni oggetto notificante non ha conoscenza né di quali né di quanti altri oggetti riceveranno l'aggiornamento e poiché ognuno di questi, a sua volta, potrebbe scatenare altri aggiornamenti si pone il problema di eventuali grosse dimensioni scatenati da singole piccole modifiche, fatto che potrebbe rallentare il sistema
    \item Nel caso in cui le dipendenze non siano tracciate correttamente può diventare difficile tracciare le catene di aggiornamenti fino a risalire alla causa di un eventuale problema
    \item Possibili eventuali sottoscrizioni fatte da oggetti eliminati. Poiché il ciclo di vita di ogni oggetto è di base indipendente da quello delle sottoscrizioni associate al pattern observer può accadere inavvertitamente che un oggetto non pulisca le proprie sottoscrizioni prima della propria eliminazione. Non è detto che questo generi problemi seri, in quanto è possibile a seconda del linguaggio che tali sottoscrizioni vengano direttamente ignorate. Tuttavia sicuramente è una fonte di overhead
\end{itemize}

\subsection{Pattern strategy}

\begin{pattern}{Strategy}{}
    Consente di definire una famiglia di algoritmi, incapsulare ciascuno di essi e renderli intercambiabili. Questo pattern consente agli algoritmi di variare indipendentemente dai client che li utilizzano
\end{pattern}

È utile far riferimento al pattern strategy quando:
\begin{itemize}
    \item Molte classi correlate differiscono solo nel loro comportamento. Le varie ``strategy'' forniscono un modo per configurare una classe con uno dei tanti comportamenti
    \item Sono necessarie diverse varianti di un algoritmo, ad esempio aventi diversi compromessi in termini di utilizzo delle risorse
    \item Un algoritmo utilizza dei dati che i clienti non dovrebbero conoscere
\end{itemize}

\subsection{Pattern state}

\begin{pattern}{State}{}
    Consente a un oggetto di modificare il proprio comportamento quando lo stato interno cambia. L'oggetto si comporta come se appartenesse ad una diversa classe
\end{pattern}

Il pattern State risulta utile nei seguenti casi:
\begin{itemize}
    \item Il comportamento di un oggetto dipende dal suo stato e deve cambiare il suo comportamento a runtime a seconda di tale stato
    \item Le operazioni contengono blocchi condizionali di grandi dimensioni che dipendono dal stato dell'oggetto. Questo stato è in genere rappresentato da uno o più enumerati (o costanti). Spesso, diverse operazioni contengono la medesima separazione e struttura condizionale. Questo pattern sposta ogni ramo in una classe separata. In questo modo è possibile considerare lo stato dell'oggetto come un oggetto a sé stante che può variare indipendentemente da altri oggetti
\end{itemize}

\subsection{Pattern Visitor}

\begin{pattern}{Visitor}{}
    Rappresenta un'operazione da eseguire sugli elementi di una struttura di oggetti. Tale pattern consente di definire una nuova operazione senza modificare le classi degli elementi su cui opera
\end{pattern}

Il pattern visitor risulta utile quando:
\begin{itemize}
    \item devono essere eseguite molte operazioni distinte e non correlate su oggetti in un struttura e si vuole evitare di ``sporcare'' le classi con l'implementazione delle operazioni. Questo pattern consente di tenere insieme le operazioni correlate definendole all'interno di una classe
    \item le classi che definiscono la struttura dell'oggetto cambiano raramente, ma si desidera spesso definire nuove operazioni sulla struttura o modificare le operazioni esistenti
\end{itemize}
