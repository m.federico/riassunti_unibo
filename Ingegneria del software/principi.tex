\chapter{Principi programmazione Object Oriented}

\section{Tipi di dato}

I primi software erano tendenzialmente caotici rispetto agli standard odierni. Era fatto uso indiscriminato dell'istruzione \texttt{GOTO}, che portava ad effetti deleteri sulla qualità del software scritto (discusso da Dijkstra nel 1968).

\begin{definizione}{Programmazion strutturata}{}
    Qualsiasi programma facente uso dell'istruzione \texttt{GOTO} può essere riscritto senza \texttt{GOTO} a patto che il linguaggio contenga tre tipi di strutture di controllo:
    \begin{itemize}
        \item sequenza
        \item ripetizione
        \item alternativa
    \end{itemize}
\end{definizione}

Per domare la complessità si è cominciato a far uso di tipi di dato astratti (\acrshort{adt}).

\subsection{Tipi di dato astratti}

\begin{definizione}{Tipo di dato astratto}{}
    Un tipo di dato astratto è ciò che si ottiene dall'unione dei dati e del codice che opera su di essi. In questo modo ogni tipo ha un'interfaccia visibile esternamente, mentre l'implementazione è nascosta.
\end{definizione}

Così facendo lo stato dell'oggetto è accessibile solo tramite l'interfaccia dell'\acrshort{adt}.

\begin{definizione}{Information hiding}{}
    L'information hiding è il concetto teorico secondo cui alcuni aspetti di un componente software non debbano essere accessibli all'esterno del componente stesso.
\end{definizione}

\begin{definizione}{Incapsulamento}{}
    L'incapsulamento è una tecnica software usata per realizzare l'information hiding.
\end{definizione}

Per definire un ADT occorre definirne l'interfaccia, ovvero le operazioni pubbliche applicabili al singolo oggetto. Per implementarlo, invece, occorre definire una classe che implementi l'interfaccia dell'\acrshort{adt}. Tale classe può essere dotata di attributi privati che vanno a definirne lo stato interno. Inoltre la classe può essere dotata di metodi pubblici (che vanno ad implementare l'interfaccia o che disciplinano l'accesso a certi dati) e privati.

L'utilizzo dell'inclapsulamento ha come scopo quello di nascondere le scelte progettuali fatte per implementare un determinato componente software, minimizzando così le eventuali modifiche da fare duarnte la fase di sviluppo e manutenzione dei componenti che lo utilizzano. Come secondo effetto, l'incapsulamento promuove la riutilizzabilità di un componente.

\subsection{Oggetti e classi}

\begin{definizione}{Classe}{}
    Una classe è un template per la creazione di \textbf{oggetti}. Ogni classe definisce l'implementazione di un \acrshort{adt}.
\end{definizione}

\begin{definizione}{Oggetto}{oggetto-def}
    Un oggetto è l'istanza di una classe.
\end{definizione}

Di conseguenza oggetto e classe, sebbene relazionati differiscono principalmente per il loro ruolo. Una classe definisce il comportamento e l'interfaccia generale di un oggetto, la forma che l'oggetto avrà una volta creato. Mentre un ogetto esiste solamente a runtime.

\section{Programmazione Orientata agli oggetti}

La programmazione orientata agli oggetti (\acrshort{oop}) estende quella basata sugli oggetti (\acrshort{obp}) aggiungendo di due importanti caratteristiche:
\begin{itemize}
    \item Supoprto per l'ereditarietà
    \item Supporto per il polimorfismo
\end{itemize}

Nella programmazione orientata agli oggetti le classi possono essere organizzate in gerarchie, ponendole in relazioni di ereditarietà. In qquesto caso gli oggetti di una sottoclasse devono esibire tutti i comportamenti e le proprietà degli oggetti appartenenti alla superclasse in modo tale da poter essere sostituiti da quest'ultima.

\begin{definizione}{Principio di sostituibilià di Liskov}{}
    Se $q(x)$ è una proprietà che si può dimostrare essere valida per oggetti $x$ di tipo $T$, allora $q(y)$ deve essere valida per oggetti $y$ di tipo $S$ dove $S$ è un sottotipo di $T$
\end{definizione}

La sottoclasse può in generale esibire caratteristiche aggiuntive rispetto alla classe base, e può anche eseguire alcune funzionalità in maniera differente a patto che tale differenza non sia osservabile dall'esterno.

\subsection{Ereditarietà}

\begin{definizione}{Ereditarietà}{}
    Con ereditarietà si intende la possibilità condividere attirbuti ed operazioni comuni tra più classi facendo derivare una classe $A$ da una classe $B$.
\end{definizione}

Esistono varie tipologie di ereditarietà tra cui:
\begin{itemize}
    \item Ereditarietà di interfaccia
    \item Ereditarietà di estensione
    \item Ereditarietà di implementazione
\end{itemize}

\subsubsection{Ereditarietà di interfaccia e di estensione}
L'ereditarietà di interfaccia e l'ereditarietà di estensione definiscono dei meccanismi di compatibilità tra tipi tramite una relazione del tipo \textit{isA}.

\begin{esempio}{}{}
    Ad esempio uno \texttt{Studente} \textit{è} una \texttt{Persona}, e un \texttt{Docente} \textit{è} una \texttt{Persona}. Non è tuttavia detto che una persona sia uno \texttt{Studente} o un \texttt{Docente}.
\end{esempio}

\subsubsection{Ereditarietà di implementazione}
L'ereditarietà di implementazione definisce invece dei meccanismi di riuso del codice definito nelle superclassi. Questo si verifica tipicamente quando una classe ha bisogno dei servizi di un'altra classe.

Questo tipo di ereditarietà è concesso in C++ (usando la keyword \texttt{private} durante la dichiarazione della relazione di ereditarietà) ma non in altri linguaggi \acrfull{oo}, in quanto questo tipo di ereditarietà tende a rompere l'incapsulamento legando a doppio filo due o più classi rendendone difficile la modifica o il riuso.

Un'alternativa più efficace all'ereditarietà di implementazione è il meccanismo detto di \textbf{composizione e delega}.

\subsubsection{Composizione e delega}

Questo meccanismo prevede di delegare certe operazioni ad un altro oggetto. Di conseguenza vi è una sola implementazione e gli oggetti clienti possono utilizzarla tramite delle API pubbliche che sono meno soggette a cambiamenti.

\subsubsection{Ereditarietà semplice e multipla}

Ortogonalmente è possibilie definire l'ereditarietà semplice o multipla nel caso in cui una classe possa estendere una sola superclasse o più superclassi.

In generale l'ereditarietà multipla ha vari problemi, in particolare quello di determinare in presenza di conflitti di nome quale vada utilizzato. Di conseguenza al posto dell'ereditarietà multipla conviene utilizzare altri meccanismi come le interfacce o "composizione e delega".

\subsection{Polimorfismo}

Con polimorfismo si fa riferimento alla capacità di una stessa cosa di apparire in forme diverse a seconda dei contesti o di cose diverse di apparire allo stesso modo a seconda del contesto. Esistono dievrsi tipi di polimorfismo:
\begin{itemize}
    \item Per inclusione
    \item Parametrico
    \item Overloading
    \item Coercion (o casting)
\end{itemize}

\subsubsection{Polimorfismo per inclusione}
Si tratta della possibilità di usare a runtime una sottoclasse tramite un puntatore (o un riferimento) ad una classe base. In questo caso a runtime viene usata una \acrfull{vmt} per determinare quale, tra i metodi di cui è stato eseguito l'\textbf{override}, vada invocato. Questo tipo di polimorfismo consente il \textbf{late binding}.

\subsubsection{Polimorfismo parametrico}
Questo tipo di polimorfismo consente di utilizzare lo stesso codice per tipi differenti.

\subsubsection{Overloading}
Questo tipo di polimorfismo consente ad una funzione di comportarsi diversamente a seconda dei tipi utilizzati per invocarla.

\subsubsection{Coercion o casting}
Questo tipo di polimorfismo consente ad un dato di un determinato tipo di essere convertito un dato di un altro tipo. La conversione può essere implicita (in questo caso ad opera del compilatore) o esplicita (per mezzo di sintassi apposita).

\section{Programmazione generica}

La programmazione generica è uno stile di programmazione in cui i componenti sono scritti in termini di tipi di dati generici (da specificare in seguito). Quando diventa necessario utilizzare un componente occorre specificare tali tipi passandoli come parametri. Questo approccio consente di scrivere funzioni o tipi comuni che differiscono solo nell'insieme di tipi su cui operano quando vengono utilizzati, riducendo così il codice duplicato.

\begin{definizione}{Classe generica}{}
    Una classe in cui uno o più tipi sono parametrici
\end{definizione}

\section{Relazioni}
La maggior parte delle classi (degli oggetti) interagisce
con altre classi (altri oggetti) in vari modi. L'interazione tra entità diverse è possibile solo se tra loro esiste un qualche tipo di relazione:
\begin{itemize}
    \item Generalizzazione o ereditarietà. Modella una relazione del tipo \textit{isA}
    \item Realizzazione. Modella una relazione di implementazione tra un componente che definisce un'interfaccia ed un altro componente che la implementa
    \item Associazione. In questo caso vi sono più possibilità:
          \begin{itemize}
              \item Associazione generica
              \item Aggregazione. Modella il caso in cui un componente è dato dall'insieme di altri componenti
              \item Composizione. Modella il caso in cui un componente contiene dei sottocomponenti
          \end{itemize}
    \item Dipendenza. Anche in questo caso vi sono più possibilità:
          \begin{itemize}
              \item Collaborazione. Modella una relazione d'uso tra due componenti
              \item Relazione Istanza-Classe
              \item Relazione Classe-Metaclasse
          \end{itemize}
\end{itemize}

\section{Processo di sviluppo} 

L'analisi \acrshort{oo} ha come obiettivo modellare la porzione di mondo reale tramite oggetti che forniscono una rappresentazione della porzione di mondo reale di interesse. In questa fase ogni classe descrive una categoria di oggetti. 

La progettazione \acrshort{oo} modella la soluzione. In questo caso si possono trasformare gli oggetti definiti durante l'analisi. In questa fase ogni classe descrive un tipo di dato. 

La programmazione \acrshort{oo} realizza la soluzione tramite linguaggi di programmazione \acrshort{oo} per definire le classi di oggetti e sistemi runtime per creare, usare ed eliminare le istanze di queste classi. In questa fase ogni classe descrive l'implementazione di un dato.