\begin{definizione}{algoritmo}{}
  Si dice \textbf{algoritmo} una sequenza \textbf{finita} di istruzioni
  con lo scopo di risolvere una \emph{classe} di problemi in un tempo
  finito. Ogni algoritmo deve essere poi descritto mediante un insieme
  ordinato di frasi appartenenti ad un linguaggio (di programmazione) che
  specificano quali siano le azioni da svolgere
\end{definizione}

\begin{definizione}{programma}{}
  Si dice programma un testo scritto in accordo alla sintassi e alla semantica 
  di un linguaggio di programmazione
\end{definizione}

Non è detto che un \textbf{programma} sia un algoritmo, in quanto è 
sufficiente che il programma non termini (tempo di esecuzione infinito, basta 
pensare ad un server) o che non dia un risultato specifico per violare la 
definizione di algoritmo. Sebbene siano differenti, un programma è utile nei 
momenti in cui è necessario agire in un determinato modo (magari sulla base di 
uno o più algoritmi). \\
In questo senso un algoritmo esprime la soluzione ad un problema a partire da 
un insieme di dati in ingresso, mentre un programma ne è la formulazione 
scritta in un linguaggio di programmazione. 

L'esecuzione delle azioni descritte dall'algoritmo
presuppone l'esistenza di una macchina astratta in grado di eseguirlo
(\textbf{automa esecutore}), ma poiché le istruzioni facenti parte 
dell'algoritmo provengono dall'esterno e poiché esse sono scritte in un 
particolare linguaggio l'automa deve essere un \textbf{interprete di 
linguaggio} (chiamato \textbf{linguaggio macchina}). Inoltre, poiché l'automa 
deve essere realizzabile fisicamente, se è composto da parti esse devono 
essere in \emph{numero finito} e i dati in ingresso ed in uscita devono essere
esprimibili tramite un numero \emph{finito} di simboli.

Formalizzando il concetto di automa si giunge a definire il concetto stesso di 
\textbf{computabilità}. Esistono vari approcci per fornirne una definizione:

\begin{itemize}
  \item
    Approccio a \emph{gerarchia di macchine astratte}: si costruiscono via
    via macchine sempre più potenti determinando alla fine il concetto di
    computabilità sulla base delle capacità computazionali di queste
    macchine
  \item
    Approccio \emph{funzionale}: fondato sul concetto di funzione
    matematica, ha come scopo quello di capire quali funzioni siano
    computabili
  \item
    Sistemi di \emph{riscrittura}: l'automa è descritto come un insieme di
    regole di riscrittura (successive) che trasformano frasi in altre
    frasi equivalenti tramite cui è possibile giungere ad una soluzione
    del problema
\end{itemize}

\section{Gerarchia di macchine astratte}\label{sec:gerarchia_macchine_astratte}

È possibile definire una gerarchia di macchine in quanto è possibile che 
macchine diverse abbiano potenzialità diverse e che siano in grado di 
risolvere problemi diversi, generalmente via via più complessi salendo verso 
la sommità della gerarchia. Di conseguenza è utile saper scegliere quale sia 
la macchina più adatta a risolvere il problema corrente.

Dato un problema non è detto che esso sia risolvibile (in generale o da
una macchina specifica), se nemmeno la macchina più potente è in grado
di risolverlo allora potrebbe essere che questo problema non possa
essere risolto in assoluto (da una macchina almeno). A questo punto
diventa importante conoscere quali siano i problemi risolvibili e quali
non lo siano (più nello specifico quale sia la macchina migliore per
poter risolvere un problema).

  \begin{itemize}
  \item
    Macchina base (combinatoria): essa è formalmente definita dalla tripla
    $$\langle I, O, mfn\rangle$$ dove:

    \begin{itemize}
      \item
        $I$ è l'insieme finito dei simboli di ingresso
      \item
        $O$ è l'insieme finito dei simboli di uscita
      \item
        $mfn: I \rightarrow O$ è la funzione di macchina 
    \end{itemize}

    Questo tipo di macchina non è dotato del concetto di \emph{storia passata},
    pertanto è in grado solamente di risolvere problemi in tempo reale.
    Inoltre è necessario enumerare esplicitamente tutte le possibile
    configurazioni delle uscite sulla base degli ingressi
  \item
    Automa a stati finiti: è formalmente definito da
    $$\langle I, O, S, mfn, sfn\rangle$$ dove:

    \begin{itemize}
      \item
        $I$ è l'insieme finito dei simboli di ingresso
      \item
        $O$ è l'insieme dei finito simboli di uscita
      \item
        $S$ è l'insieme finito degli stati
      \item
        $mfn: I \times S \rightarrow O$ è la funzione di macchina 
      \item
        $sfn: I \times S \rightarrow S$ è la funzione di stato
    \end{itemize}

    L'automa consente di tener traccia della storia passata mediante un
    numero finito di stati interni, di conseguenza le operazioni future
    sono influenzate da quelle precedenti. Inoltre il concetto di stato
    consente di tener traccia dell'ordine con cui vengono eseguite le
    azioni. 
    
    È possibile avere rappresentazioni differenti di un automa a
    stati (Mealy/Moore, sincroni/asincroni, \dots), che tuttavia possono anche
    essere equivalenti tra di loro.
    
    Il principale limite di questi automi sta nella finitezza della loro
    memoria: poiché essa non è illimitata sorgono problemi ogni qual volta
    non sia possibile determinarne a priori la quantità necessaria (es.
    bilanciamento delle parentesi)
  \item
    Push Down Automaton
  \item
    Macchina di Turing
\end{itemize}

\section{Macchina di Turing}

Al fine di superare il limite della memoria finita si introduce un
nastro esterno come supporto di memorizzazione. Questo nastro non è
infinito ma ha la possibilità di essere esteso indefinitamente secondo
la necessità. Di conseguenza la MDT è una macchina a stati la cui
memoria è esterna; essa rappresenta un potente modello matematico per la
comprensione di quali problemi possano essere risolti e quali no. 

Questa macchina è dotata di una testina in grado di leggere simboli dal 
nastro, scrivere il risultato dato dalla $mfn$ sul nastro, transitare in un 
nuovo stato sulla base della $sfn$ e spostarsi lungo il nastro sulla base 
del risultato della $dfn$.\\
Una MDT è definita univocamente dalla quintupla
$$\langle A, S, mfn, sfn, dfn\rangle$$ dove:

\begin{itemize}
  \item
    $A$ è l'insieme finito dei simboli di ingresso ed uscita (l'alfabeto). Si
    utilizza un unico insieme in quanto i risultati prodotti dalla MDT
    devono poter essere nuovamente letti dalla stessa macchina per
    continuare ad operare
  \item
    $S$ è l'insieme finito degli stati caratterizzanti la macchina, tra i quali
    vi è necessariamente lo stato $HALT$ (indica che la macchina ha
    finito di operare e che quindi è possibile leggere il risultato delle
    operazioni)
  \item
    $mfn: A \times S \rightarrow A$ è la funzione di macchina
  \item
    $sfn: A \times S \rightarrow S$ è la funzione di stato
  \item
    $dfn: A \times S \rightarrow D = \{Left, Right, None\}$ è la
    funzione di direzione
\end{itemize}

Risolvere un problema con una MDT si riduce quindi alla definizione di
una rappresentazione di partenza dei dati (definizione dell'insieme
$A$) ed alla definizione delle funzioni che ne determinano il
comportamento.

\begin{esempio}{riconoscimento di un palindromo}{}
  Un semplice algoritmo per la macchina di Turing consiste nei seguenti passi:
  \begin{enumerate}
    \item 
      Leggere il primo simbolo a sinistra
    \item 
      Ricordare se tale simbolo sia uno $0$ oppure un $1$ e marcare la 
      casella come già visitata
    \item 
      Spostarsi sull'ultimo simbolo a destra
    \item 
      Se tale simbolo non coincide con quello ricordato scrivere un 
      simbolo d'errore e terminare, altrimenti marcare la casella come 
      visitata e tornare al primo simbolo a sinistra non ancora visitato
    \item 
      Ripetere dal primo punto
  \end{enumerate}
  Se, giunti al passo 4 sono stati consumati tutti i simboli allora la frase è 
  palindroma, in caso contrario non lo è.
  
  Questo algoritmo può essere espresso tramite il seguente alfabeto ed i  
  seguenti stati
  $$A=\{0,1,\bullet, \CheckedBox ,\XBox  \}$$
  $$S=\{HALT, s0,s1,s2,s3,s4,s5\}$$
  La funzione di macchina può essere espressa tramite la seguente tabella:
  \begin{center}
    \begin{tabular}{|c|c|c|c|c|c|c|} 
      \hline
                    & $s0$          & $s1$          & $s2$          & $s3$          & $s4$          & $s5$           \\ 
      \hline
      $0$           & $0$           & $\bullet$ & $0$           & $0$           & $\bullet$ & $\XBox$        \\ 
      \hline
      $1$           & $1$           & $\bullet$ & $1$           & $1$           & $\XBox$       & $\bullet$  \\ 
      \hline
      $\bullet$ & $\bullet$ & $\CheckedBox$ & $\bullet$ & $\bullet$ & $\CheckedBox$ & $\CheckedBox$  \\
      \hline
    \end{tabular}
  \end{center}
  La funzione di stato può essere espressa tramite la seguente tabella:
  \begin{center}
    \begin{tabular}{|c|c|c|c|c|c|c|} 
      \hline
                    & $s0$ & $s1$   & $s2$ & $s3$ & $s4$   & $s5$    \\ 
      \hline
      $0$           & $s0$ & $s2$   & $s2$ & $s3$ & $s0$   & $HALT$  \\ 
      \hline
      $1$           & $s0$ & $s3$   & $s2$ & $s3$ & $HALT$ & $s0$    \\ 
      \hline
      $\bullet$ & $s1$ & $HALT$ & $s4$ & $s5$ & $HALT$ & $HALT$  \\
      \hline
    \end{tabular}
  \end{center}
  La funzione di direzione può essere espressa tramite la seguente tabella:
  \begin{center}
    \begin{tabular}{|c|c|c|c|c|c|c|} 
      \hline
                    & $s0$ & $s1$ & $s2$ & $s3$ & $s4$ & $s5$  \\ 
      \hline
      $0$           & $L$  & $R$  & $R$  & $R$  & $L$  & $N$   \\ 
      \hline
      $1$           & $L$  & $R$  & $R$  & $R$  & $L$  & $L$   \\ 
      \hline
      $\bullet$ & $R$  & $N$  & $L$  & $L$  & $N$  & $N$   \\
      \hline
      \end{tabular}
  \end{center}
  
  \medskip Ad ogni passo la macchina può spostare la testina di una posizione, 
  dunque eseguendo un numero arbitrario di passi è possibile spostare la 
  testina di $N$ passi. Questa capacità di movimento non ha vincoli, dunque la 
  macchina può leggere e scrivere qualsiasi cella
\end{esempio}

A questo punto ci si può chiedere se effettivamente la
macchina si fermi dando un risultato corretto ogni volta per ogni
problema. Ipotizzando di non aver commesso errori durante la definizione
delle funzioni e dei simboli è possibile che la macchina entri in un
loop infinito non dando mai la risposta al problema che le è stato
sottoposto. Sebbene sembri una limitazione, la possibilità di entrare in
loop infiniti è positiva in quanto deriva dalla potenza della macchina
stessa, infatti solo se la macchina fosse meno potente, e quindi fosse
in grado di risolvere meno problemi, si eviterebbe questa evenienza.

\begin{tesi}{di Church-Turing}{}
Non esiste alcun formalismo capace di risolvere una classe di problemi
più ampia di quella risolta dalla Macchina di Turing
\end{tesi}

Sebbene sia una tesi, quindi mai dimostrata o confutata, essa significa
che la MDT è la macchina più potente che si possa immaginare. Ciò è
importante in quanto permette di capire quali siano i limiti della
computazione.

\section{Push down automaton}

Non sempre è necessario utilizzare una macchina di Turing per la
risoluzione dei problemi. Per alcune categorie di problemi infatti può
essere sufficiente l'utilizzo di altre categorie di macchine più
semplici con un modello di memoria più limitato. Infatti sebbene la MDT
sia molto potente ha alcuni problemi di efficienza dovuti alla
possibilità di accedere ad una qualsiasi cella del nastro.
Un'alternativa più efficiente è il \emph{push down automaton} (PDA o
macchina a stack) che, sfruttando il principio di località, ha la
possibilità di accedere solamente alla cella in cima alla pila.

\section{Macchine specifiche e macchine universali}

Una volta definita la parte di controllo, la MDT è in grado di risolvere
uno specifico problema, ma così facendo la MDT stessa diventa specifica per
quel dato problema. Una macchina specifica ha vari vantaggi, ad
esempio può essere prodotta facilmente per il mercato di massa ad un
basso costo. Tuttavia nel caso volessimo ottenere una macchina in grado
di risolvere \emph{qualsiasi} (o almeno il maggior numero possibile) problema
occorrerebbe progettare una \textbf{MDT universale}.

Infatti se la macchina non avesse lo specifico algoritmo cablato al suo interno 
ma fosse in grado di cercarlo sul nastro e poi di eseguirlo otterremmo proprio
una MDT universale. Tale macchina sarebbe dunque in grado di eseguire un
compito differente a seconda del contenuto del nastro di memoria
permettendone una maggior versatilità. L'unico algoritmo cablato nella
macchina sarebbe quello per la lettura dal nastro dell'algoritmo da
eseguire. Una macchina di questo tipo dovrebbe essere inoltre in grado
di leggere ed interpretare il linguaggio in cui viene scritto
l'algoritmo sul nastro, rendendola di fatto un \emph{interprete di
linguaggio}, e rappresenta dunque l'automa esecutore descritto nel
capitolo precedente.

\subsection{Macchina di Turing universale ed architettura di Von Neuman}

Una MDT universale modella dunque il concetto di elaboratore di uso
generale, confrontabile con la macchina di Von Neuman, dotato delle fasi
di:

\begin{enumerate}
  \item
    \textbf{fetch}: ricerca delle istruzioni da svolgere
  \item
    \textbf{decode}: interpretazione delle istruzioni
  \item
    \textbf{execute}: esecuzione delle istruzioni
\end{enumerate}

La scrittura di un simbolo sul nastro si traduce nella scrittura di un dato 
sulla RAM (equivalentemente per la lettura). La transizione in un nuovo stato 
della MDT universale si traduce in una nuova configurazione dei registri della 
CPU, mentre il movimento lungo il nastro si traduce nella possibilità di 
accedere ad una qualsiasi delle celle di memoria.

Nonostante le similitudini la MDT universale non è in grado di catturare tutti 
gli aspetti dell'architettura di Von Neumann in quanto per la MDT universale 
non esiste il concetto di "mondo esterno" né di istruzioni di I/O. In altre 
parole la MDT universale modella unicamente l'aspetto computazionale 
dei problema ignorando l'aspetto interazionale.

\subsection{Computazione ed interazione}
Computazione ed interazione sono due aspetti distinti ed ortogonali, espressi 
da due linguaggi diversi:

\begin{itemize}
  \item
    Linguaggio di computazione: esprime le primitive necessarie
    all'elaborazione delle informazioni
  \item
    Linguaggio di interazione: esprime le primitive per l'interazione con
    il mondo esterno

    \begin{itemize}
      \item
        Linguaggio di coordinazione: fornisce le primitive di comunicazione
        (come dire le cose)
      \item
        Linguaggio di comunicazione: definisce le informazioni che verranno
        trasmesse (cosa dire)
    \end{itemize}
\end{itemize}
