Storicamente, i paradigmi di programmazione e i linguaggi funzionali hanno 
dato un notevole contributo di idee forti, ma sono stati penalizzati dalla 
sintassi poco user-friendly per il grande pubblico facendo sì che questi 
linguaggi rimanessero confinati all'interno dell'ambiente accademico. 
Nell'ultimo decennio invece tali linguaggi hanno guadagnato popolarità, spesso 
anche grazie alla nascita di linguaggi blended, che uniscono idee del mondo 
funzionale ad idee del mondo imperativo. 

Alcune idee chiave del paradigma funzionale sono:

\begin{itemize}
  \item
    La distinzione tra variabili e valori: se da un lato i linguaggi
    imperativi si basano sul cambio di valore dei simboli, il paradigma
    funzionale si basa sul fatto che i simboli mantengano il loro
    significato. I linguaggi blended uniscono i due mondi dando la
    possibilità di fare entrambe le cose con parole chiave diverse ad esempio 
    \verb|val| e \verb|var|, perciò supportando uno stile di programmazione 
    imperativo offrono (ed in certi casi impongono\footnote{Ad esempio le proprietà introdotte da \texttt{val} possono avere solamente metodi getter e non metodi setter in quanto il loro valore non può cambiare nel tempo, rafforzando a livello sintattico quello che altrimenti sarebbe stato espresso al più con un commento}) l'uso di uno stile più funzionale
  \item
    Costrutti considerati come espressioni (\emph{everything is an expression}): i linguaggi imperativi spesso
    fanno distinzione tra istruzioni (le quali non ritornano alcun valore)
    ed espressioni. Questo induce ad esprimere sequenze di operazioni
    facendo uso di variabili di appoggio, le quali riducono la leggibilità dei 
    programmi. I linguaggi blended invece, tipicamente, posseggono solamente 
    espressioni permettendo di porle in cascata senza il bisogno di variabili 
    di appoggio
  \item
    Collezioni di oggetti immodificabili (\emph{compute by synthesis}): le collezioni modificabili
    presentano problemi in caso di concorrenza e promuovono uno stile
    imperativo basato sulla modifica delle stesse. I linguaggi funzionali
    invece suggeriscono di computare per sintesi di nuovi oggetti,
    aiutando la gestione delle situazioni di concorrenza. Tipicamente i
    linguaggi blended forniscono sia collezioni modificabili che
    collezioni immodificabili, facilitando l'uso di queste ultime
  \item
    Funzioni come "first class entities"
  \item
    Concisione ed operatori come funzioni
\end{itemize}

Queste caratteristiche, inserite all'interno di un linguaggio blended,
permettono di rafforzarne gli aspetti ad oggetti abolendo:

\begin{itemize}
  \item
    I tipi primitivi (\emph{everything is an object}): molti linguaggi ad oggetti 
    tradizionali si basano su tipi primitivi. Questo causa una mancanza di 
    uniformità e comporta la necessità di un trattamento specifico per queste 
    entità, ad esempio i tipi primitivi sono spesso passati per valore mentre 
    gli oggetti sono passati per riferimento. I linguaggi blended risolvono 
    queste differenze rendendo tutto un oggetto, spesso distinguendo però tra 
    classi-valore (immodificabili e non estendibili) e classi-riferimento (le 
    classiche classi)
  \item
    Le parti statiche (\emph{don't be static}): i linguaggi ad oggetti tradizionali si basano sul
    concetto di classe, che differisce enormemente da quello di oggetto.
    Questo porta alla necessità dei design pattern. I linguaggi blended
    introducono il concetto di \textbf{companion object} che contiene le
    parti "statiche" di un altro oggetto sebbene non sia statico
\end{itemize}

\section{Funzioni come first-class entities}

Tutti i linguaggi di programmazione supportano il concetto di funzione ed i 
relativi costrutti sintattici, ma di rado esse sono supportate come entità 
aventi "pieno diritto di cittadinanza". Una funzione che gode di queste 
caratteristiche può essere manipolata come un qualsiasi altro tipo di dato, 
quindi:

\begin{itemize}
  \item
    poter essere assegnata a variabili (di tipo funzione)
  \item
    poter essere passata come argomento ad un'altra funzione
  \item
    poter essere restituita come risultato da un'altra funzione
  \item
    poter essere definita al volo mediante una sintassi literal come altri
    tipi di dato
  \item
    poter essere definita eventualmente senza un nome
\end{itemize}

Nei linguaggi imperativi invece le funzioni sono dei puri contenitori di
codice e dunque non possono essere manipolate come gli altri tipi di
dato. I linguaggi ad oggetti moderni stanno incorporando questi
concetti, sebbene sotto varie forme. Ad esempio javascript ha da sempre il 
costruttore \verb|Function| e la parola chiave \verb|function| che consente di 
specificare funzioni anonime, C\# possiede delegati e costruttori 
\verb|Func<>|, Java possiede le lambda expression (abbastanza limitate).\\
I linguaggi blended come Scala e Kotlin, d'altro canto, propongono nativamente 
questi concetti.

La possibilità di poter passare una funzione come argomento ad un'altra
funzione o di ritornare funzioni permette la generazione di funzioni di
ordine superiore, ovvero \emph{funzioni che manipolano funzioni}. Per poter 
eseguire queste operazioni però è necessario possedere un tipo funzione che 
consente alle istanze di essere valori, ma al contempo di essere eseguibili.

\begin{esempio}{}{}
  \begin{lstlisting}[language=javascript]
  //definire una funzione anonima al volo
  var f = function (z) { return z*z; }
    
  //creare al volo una funzione che ne riceve un'altra come parametro e che computa il risultato sulla base di essa
  var ff = function (f, x) { return f(x); }
  
  //una funzione puo' restituire una nuova funzione come risultato
  function ff(){return function(r){return r+10}}
  \end{lstlisting}
\end{esempio}


\section{Variabili libere e chiusure}

Se un linguaggio ammette variabili libere, ovvero non definite localmente, tali 
funzioni dipendono dal contesto circostante. Di conseguenza sono necessari dei 
criteri con cui darvi significato, ovvero \textbf{chiusura lessicale} o 
\textbf{chiusura dinamica}.

Nei linguaggi tradizionali l'esistenza di variabili libere non crea particolari 
problemi. In C, ad esempio, le uniche variabili libere possibili sono quelle 
globali, mentre in Pascal (che offre la possibilità di creare funzioni 
innestate) c'è la necessità di distinguere le catene di chiamate.

Nel caso in cui il linguaggio supporti le funzioni come first-class entities la 
presenza di variabili libere comporta la nascita del concetto di 
\textbf{chiusura}, ovvero di un oggetto funzione ottenuto chiudendo rispetto ad 
un certo contesto una definizione di funzione che aveva variabili libere.

\begin{esempio}{}{}
  \begin{lstlisting}[language=javascript]
  function ff(f,x) {
    return function(r) { return f(x) + r; }
  }
  
  var f1 = ff( Math.sin, 0.8)
  var f2 = ff( function(q){return q*q}, 0.8)

  var r1 = f1(3) // 3.717356091
  var r2 = f2(0.36) // 1.000000000 [0.64 + 0.36]
  \end{lstlisting}

   Invocando \verb|ff| si ottiene dunque un oggetto funzione che al suo interno 
   incorpora i riferimenti alle variabili \verb|f| e di \verb|x| e dunque tale 
   funzione \emph{anonima} deve mantenere traccia dei loro valori. Una seconda 
   esecuzione di \verb|ff| con parametri differenti, infatti, comporta la 
   creazione di una seconda funzione anonima \emph{differente} dalla prima. I 
   parametri \verb|f| e \verb|x| sono variabili libere per la funzione anonima, 
   poiché non definite localmente ad essa
\end{esempio}

Questo concetto è da sempre disponibile nei linguaggi che contengono le 
funzioni come first-class entities, ma recentemente è stato aggiunto anche nei 
linguaggi OO nella forma delle lambda expression (spesso però depotenziate) con 
forti conseguenze sul modello computazionale.

In presenza di chiusure il tempo di vita delle variabili di una funzione non 
coincide più necessariamente con quello della funzione che le contiene, dunque 
alcune variabili (non tutte, solo quelle usate nelle chiusure) devono essere 
allocate sullo heap e non più semplicemente sullo stack. Infatti una variabile 
locale ad una funzione esterna può essere indispensabile al funzionamento della 
funzione interna, dunque il tempo di vita di tale variabile non può coincidere 
con quello della funzione esterna, ma deve necessariamente coincidere con 
quello della chiusura, anche se la funzione che la definisce termina prima.\\
Nell'esempio precedente i parametri della funzione \verb|ff| devono essere 
allocati sullo heap in quanto devono sopravvivere alla terminazione della 
funzione in modo da poter essere usati dalla funzione anonima generata in 
qualsiasi momento. 

Poiché tuttavia queste variabili possono essere condivise possono presentarsi 
problemi in ambienti concorrenti a memoria comune. Per questo motivo in linguaggi come Java le variabili presenti all'interno della chiusura possono essere accedute solamente in lettura e mai in scrittura (devono essere dunque \verb|final| o \verb|effectively final|).

In generale le chiusure possono essere utilizzate per:

\begin{itemize}
  \item
    Rappresentare uno stato interno privato\footnote{Tecnica utilizzata in 
    linguaggi come Javascript} (in assenza di atri strumenti) perché le 
    variabili contenute nella chiusura non sono visibili da fuori
  \item
    Creare un canale di comunicazione nascosto, in quanto definendo più 
    funzioni nella stessa chiusura esse condividono uno stato interno, che può 
    essere usato anche per comunicare privatamente
  \item
    Definire nuove strutture di controllo, in quanto la funzione esterna
    esprime il controllo mentre quella ottenuta come parametro esprime il
    corpo da eseguire
  \item
    Riprogettare e semplificare le API ed i framework di largo uso permettendo 
    di esprimere funzioni parametriche che ricevano il comportamento desiderato 
    come parametro
\end{itemize}

\subsection{Chiusure nei linguaggi mainstream}
Le chiusure sono presenti da anni in linguaggi come Javascript, Scala e Ruby. Ora sono presenti anche in Java (>1.8), C\#, C++ (>11).

\paragraph{Chiusure in Java 8 e C\#}

Questi due linguaggi offrono le chiusure sotto forma di lambda expression con 
una notazione leggermente differente.
In C\# vengono introdotte tramite:
\begin{lstlisting}[language=java]
{ params => body }
\end{lstlisting}
mentre in Java
\begin{lstlisting}[language=java]
(params) -> {body}
\end{lstlisting}
In entrambi i casi \verb|params| è una lista di parametri tipati separati da virgole, mentre \verb|body| è una lista di istruzioni. Il valore di ritorno, se presente, è dato dal valore dell'ultima espressione.

\begin{esempio}{chiusure in Java 8}{}
  \begin{lstlisting}[language=java]
  (int x) -> {x + 1}
  (String s, int y) -> System.out.println(s + " " + y) 
  \end{lstlisting}
  
  In entrambi i casi le variabili di chiusura sono solamente in lettura (effectively final), perciò 
  un tentativo di modifica risulta in un errore di compilazione. Lo scopo è cercare di evitare effetti collaterali difficilmente prevedibili in presenza di multi-threading
\end{esempio}

\paragraph{Chiusure in C++}

Sono state introdotte le labda expression con la sintassi
\begin{lstlisting}[language=c++]
  auto f = [ext_var_ref] (args)
\end{lstlisting}
Dove la keyword \verb|auto| lascia dedurre al compilatore il tipo dell'oggetto 
funzione. Questo viene tipicamente fatto poiché i tipi per le lambda expression 
possono diventare estremamente complessi molto rapidamente. La sezione tra 
parentesi quadre consente di specificare quali siano (e come accedervi) le 
variabili libere, ovvero le variabili di chiusura.

\begin{esempio}{chiusura in C++}{}
  \begin{lstlisting}[language=c++]
  #include <iostream>
  
  int main() {
    int start = 20;
    //definizione di una lambda che considera tutte le variabili locali come variabili di chiusura in lettura e scrittura
    auto myfunc = [&] (int x) {
      std::cout << x << std::endl;
      int y = x * start;
      std::cout << y << std::endl;
      start = y;
    };
    
    myfunc(10);
    myfunc(5);
  }
  \end{lstlisting}
  La prima chiamata a \verb|myfunc| stampa il valore \verb|10|, modifica il 
  valore \verb|start| assegnandogli il nuovo valore \verb|200|. La seconda 
  chiamata invece stampa il valore \verb|5| ed assegna a \verb|start| il nuovo 
  valore \verb|1000|
\end{esempio}

\paragraph{Chiusure in Scala, Kotlin e Javascript}

Questi tre linguaggi offrono le funzioni come first class entities e rendono 
possibile definire dei literal di tipo funzione. Nel caso in cui la definizione 
di un literal includa delle variabili libere nasce una chiusura.\\
La sintassi Scala:
\begin{lstlisting}[language=scala]
(params) => body
\end{lstlisting}
permette di definire un oggetto di tipo \verb|(A,B,C,...) => R|.\\
La sintassi Kotlin:
\begin{lstlisting}[language=kotlin]
(params) -> body
\end{lstlisting}
permette di definire un oggetto di tipo \verb|(A,B,C,...) -> R|.\\
Mentre la sintassi javascript:
\begin{lstlisting}[language=javascript]
function() { body }
\end{lstlisting}
permette di definire un oggetto di tipo \verb|function|.

\subsection{Criteri di chiusura}
A questo punto occorre definire un criterio riguardante il come ed il dove 
cercare le variabili libere da chiudere. In particolare, in presenza di 
funzioni innestate ci si pone il problema se debba prevalere la catena di 
ambienti di definizione innestati secondo il codice, ovvero se debba prevalere 
la definizione più prossima della variabile (\textbf{catena lessicale}), oppure se debba 
prevalere la catena degli ambienti attivi, ovvero se debba prevalere la 
definizione più vicina secondo l'ordine delle chiamate (\textbf{catena dinamica}). 
Queste due catene sono generalmente differenti.

\begin{esempio}{criteri di chiusura}{}
  \begin{lstlisting}[language=javascript]
  var x = 20;
  
  function provaEnv(z) {
    return z + x; 
  }

  function testEnv() {
    var x = -1;
    return provaEnv(18);
  }
  \end{lstlisting}

  Nel caso prevalga la catena lessicale il risultato ottenuto alla fine
  sarebbe $20+18=38$, tuttavia nel caso prevalga la catena dinamica il
  risultato sarebbe $18-1=17$ in quanto la funzione \verb|testEnv|,
  che chiama \verb|provaEnv| ridefinisce il valore della variabile
  \verb|x|
\end{esempio}
Nel caso venga scelta la catena lessicale il modello computazionale segue la 
\textbf{chiusura lessicale}. Questa da un lato vincola a priori il valore di 
una variabile, ma proprio per questo motivo permette di eseguire un debug 
efficace e di leggere il testo del programma stesso (sarebbe una follia 
debuggare un programma che si comporta in maniera differente a seconda 
dell'ordine di sequenza delle chiamate).\\
Se invece prevale la catena dinamica si parla di \textbf{chiusura dinamica}. 
Essa è molto più potente della chiusura lessicale permettendo una grande 
dinamicità del codice in quanto i simboli vengono legati ai valori sul momento, 
tuttavia questa potenza riduce le possibilità di debug. 

Poiché tipicamente la comprensione del comportamento del codice è cruciale 
praticamente tutti i linguaggi di programmazione adottano la \textbf{chiusura 
lessicale}.

\subsection{Modelli per la valutazione delle funzioni}

Ogni linguaggio che introduca funzioni deve prevedere un modello computazionale 
per la loro valutazione, il quale deve stabilire \textbf{quando} vengano 
valutati i parametri, \textbf{cosa} venga passato alla funzione e \textbf{come} 
si attivi la funzione. 

Tradizionalmente il modello più usato è il cosiddetto 
\textbf{modello applicativo}. Esso prevede che tutti i parametri vengano 
valutati all'atto della chiamata, che alla funzione vengano passati i valori 
dei parametri (o l'indirizzo in memoria\footnote{Anche gli indirizzi 
costituiscono dei valori}) e che la chiamata a funzione segua il modello 
sincrono.\\
Questo modello è semplice, pratico ed efficiente in quanto valuta i parametri 
una sola volta, rende facile seguire il flusso di esecuzione e passando dei 
valori viene ridotto l'impatto per il trasferimento dei dati. Tuttavia ha 
alcuni svantaggi, infatti valutando tutti i parametri sempre e comunque 
potrebbe venir fatto del lavoro inutile (si pensi ai casi in cui sulla base di 
una condizione interna servano i primi due parametri e non gli altri oppure 
viceversa), ma soprattutto nei casi in cui tale valutazione produca un errore 
questo modello porta ad un fallimento che si sarebbe potuto evitare.

\begin{esempio}{modello applicativo in Java}{}
  \begin{lstlisting}[language=java]
  class Esempio {
    static void printOne(boolean cond, double a, double b) {
      if (cond) {
        System.out.println("result = " + a);
      } else {
        System.out.println("result = " + b);
      }
    }
    
    public static void main(String[] args) {
      int x=5, y=4;
      printOne(x>y, 3+1, 3/0); //ArithmeticException: div by zero
    }
  }
  \end{lstlisting}

  In questo caso la valutazione immediata porta ad una eccezione che si sarebbe 
  potuta evitare
\end{esempio}

\begin{esempio}{modello applicativo in Javascript}{}
  \begin{lstlisting}[language=javascript]
  var f = function(flag, x) {
    return (flag<10) ? 5 : x;
  }
  
  var b = f(3, abort() ); // Errore!!
  document.write("result =" + b);
  \end{lstlisting}

  Anche in questo caso il modello computazionale applicativo causa un 
  fallimento non necessario valutando tutti i parametri a prescindere dalla 
  loro reale utilità all'interno della chiamata corrente. In questa specifica 
  chiamata poiché \verb|flag<10 == true|  la funzione non avrebbe mai 
  utilizzato il parametro \verb|x|
\end{esempio}

Esistono vari altri modelli per la chiamata a funzione, tra cui il modello 
\textbf{call by name}. Questo modello prevede che i parametri vengano valutati 
al momento del loro utilizzo e non all'atto della chiamata. A tal fine i 
parametri passati non sono dei valori ma degli oggetti computazionali 
(eseguibili a richiesta), di conseguenza, a seconda del flusso di esecuzione 
della funzione, un parametro potrebbe non essere mai valutato. Utilizzando 
queste modalità l'insieme delle funzioni che terminano con successo aumenta 
sensibilmente. Riprendendo gli esempi precedenti, se Java o Javascript 
utilizzassero questo modello, l'esecuzione non avrebbe dato problemi, in quanto 
i parametri problematici non sarebbero mai stati valutati.

Sebbene sia utile e potente questo modello è raramente utilizzato all'interno 
dei linguaggi di largo uso perché:

\begin{itemize}
  \item
    La valutazione di uno stesso parametro potrebbe avvenire più volte,
    riducendo l'efficienza. Questo problema sarebbe risolvibile mediante
    una cache (modello \textbf{call by need})
  \item
    Sono richieste più risorse a runtime in quanto è necessario gestire
    degli oggetti computazionali e non dei semplici valori
  \item
    È richiesta una macchina virtuale capace di effettuare una valutazione
    pigra dei parametri (\emph{lazy evaluation}). In questa maniera non si valuta tutto a priori, ma volta per volta. Chiaramente il tempo di esecuzione aumenta perché non si hanno i valori "già pronti".
  \item
    Permette di catturare pochi casi che spesso sono dei veri e propri
    errori di programmazione da risolvere (quindi meglio che esploda tutto  subito piuttosto che in faccia all'utente). Inoltre molti casi di
    fallimento aritmetici sarebbero risolvibili arricchendo l'aritmetica
    mediante i concetti di \verb|NaN| ed infinito (come avviene in
    Javascript)
\end{itemize}

Se un linguaggio supporta questa estensione della matematica anche il
modello applicativo risulta particolarmente potente e sicuro.

\begin{esempio}{modello applicativo in Javascript (matematica arricchita)}{}
  \begin{lstlisting}[language=javascript]
  printOne = function (cond, a, b) {
    if (cond) {
      println("result = " + a);
    } else {
      println("result = " + b);
    }
  }
  
  var x=5, y=4;
  printOne(x>y, 3+1, 3/0);
  \end{lstlisting}

  Questo esempio non fallisce in quanto l'espressione \verb|3/0| restituisce il 
  valore \verb|Infinity|
\end{esempio}

Sebbene il modello call by name non sia molto diffuso esso può essere \emph{simulato}. 
\begin{esempio}{call by name simulata in Javascript}{}
  \begin{lstlisting}[language=javascript]
  var f = function(flag, x) { //i parametri sono due funzioni
  	return (flag()<10) ? 5 : x(); //invoca le due funzioni
  }
  
  var b = f( function(){ return 3 }, function(){ abort() } ); //ok!
  document.write("result =" +b); 
  \end{lstlisting}
\end{esempio}


In C possono essere utilizzate delle macro per ritardare la valutazione dei 
parametri\footnote{Anche se poi non ci si capisce più niente}.
\begin{esempio}{call by name simulata in C}{}
  \begin{lstlisting}[language=c]
  #define f(FLAG, X) (((FLAG)<10) ? 5 : (X))
  
  main()
  {
    float b = f( 3, abort()); // non da' errore!
  } 
  \end{lstlisting}
  Questo esempio funziona grazie alla valutazione in corto circuito 
  dell'operatore ternario
\end{esempio}

Più interessante è il caso dei linguaggi che possiedono le funzioni come first-class entities. In questo caso è sufficiente passare alle funzioni altre funzioni come argomento e non dei semplici valori, in modo che la valutazione di queste funzioni possa avvenire solo al momento più opportuno.

\begin{esempio}{call by name simulata in Scala}{}
  \begin{lstlisting}[language=scala]
  object CallByName0 {
    def printTwo(cond: Boolean, a: Unit=>Int, b: Unit=>Int):Unit = {
      if (cond) {
        println("result = " + a() );
      } else {
        println("result = " + b() );
      }
    }

    def main(args: Array[String]) {
      val x=5;
      val y=4;
      printTwo(x>y, (Unit => 3+1), (Unit => 3/0) );
    }
  }
  \end{lstlisting}
  In Scala è possibile passare delle funzioni come parametro ed eseguirle solo  
  al momento
\end{esempio}

Fino a Java 7 questo risultava molto complesso, in quanto non erano presenti 
nemmeno le lambda expression, da Java 8 in poi la situazione è migliorata lato 
cliente ma è rimasta comunque complessa per chi implementa le funzioni. In 
particolare la nomenclatura delle lambda expression in Java segue un modello 
basato sul nome di interfacce\footnote{I nomi delle interfacce vanno saputi 
tutti a memoria} e non sulla firma della funzione stessa (che renderebbe le 
cose molto più semplici come in Scala o Go). In C\# invece la soluzione 
proposta è più completa ed elegante permettendo di definire delegati e di 
invocarli come vere funzioni e non come metodi di una interfaccia.

\begin{esempio}{call by name simulata in Java}{}
  \begin{lstlisting}[language=java]
  static void printTwo(boolean cond, IntSupplier a, IntSupplier b) {
    if (cond) {
      System.out.println("result = " + a.getAsInt() );
    } else {
      System.out.println("result = " + b.getAsInt() );
    }
  }
  
  public static void main(String[] args) {
    int x=5, y=4;
    printTwo(x>y, () -> 3+1, () -> 3/0 );
  }
  \end{lstlisting}
  Poiché non esiste un tipo funzione autonomo ogni lambda è vista in realtà 
  come un'istanza di un'opportuna interfaccia funzionale. In questo caso 
  \verb|IntSupplier| è una lambda che ritorna un \verb|int| ed è dotata di un 
  metodo \verb|getAsInt()|
\end{esempio}

In linguaggi come Scala è tuttavia data la possibilità di attivare la call by 
name su richiesta mediante l'operatore \verb|=>|. Sotto banco il runtime 
provvederà a gestire le lambda per la JVM, ma il tutto viene mascherato per 
l'utente rendendone più semplice l'utilizzo.

\begin{esempio}{call by name nativa in Scala}{}
  \begin{lstlisting}[language=scala]
  object CallByName0ByName {
    def printTwo(cond: Boolean, a: =>Int, b: =>Int):Unit = {
      if (cond) {
        println("result = " + a );
      } else {
        println("result = " + b );
      }
    }

    def main(args: Array[String]) {
      val x=5;
      val y=4;

      printTwo(x>y, 3+1, 3/0 );
    } 
  }
  \end{lstlisting}

  Scala supporta nativamente il modello call by name permettendo un salto
  espressivo. In questo caso l'operatore \verb|=>| non è legato ad una lambda 
  expression ma è l'espressione della keyword \emph{by name}
\end{esempio}
