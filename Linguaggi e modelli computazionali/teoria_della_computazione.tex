Poiché la tesi di Church-Turing postula l'impossibilità di creare un
automa più potente della macchina di Turing si può dedurre che se
nemmeno la MDT sia in grado di risolvere un problema (ovvero non è in
grado di fermarsi fornendo una risposta), allora quel problema è
irresolubile.

\begin{definizione}{problema risolubile}{}
  Si definisce problema risolubile un problema la cui soluzione può 
  essere espressa da una MDT (o da un formalismo equivalente)
\end{definizione}

Poiché però una MDT è in grado di computare solamente funzioni e non problemi
occorre creare un ponte tra i due mondi definendo una funzione (chiamata
\textbf{funzione caratteristica di un problema}) che associ ad ogni
dato in ingresso del problema la corrispondente risposta corretta:

\begin{definizione}{funzione caratteristica di un problema}{}
  Dato un problema $P$ e detti $X$ l'insieme dei suoi dati in ingresso
  ed $Y$ l'insieme delle risposte corrette, si dice funzione
  caratteristica del problema $P$:
  $$F_p : X \rightarrow Y$$
\end{definizione}

In questo modo un problema (ir-)resolubile diventa sinonimo di una
funzione caratteristica (non) computabile.

\begin{definizione}{funzione computabile}{}
  Si definisce \textbf{funzione computabile} una funzione $f:A\rightarrow B$ 
  per cui esiste una MDT in grado di produrre un risultato in un numero 
  finito di passi
\end{definizione}

Poiché la tesi di Church-Turing implica che i problemi che non possono
essere risolti con una MDT non sono risolvibili da nessun'altra macchina
occorre comprendere se tutte le funzioni siano computabili oppure se
esistano funzioni definibili ma non computabili.
Per semplicità ci limitiamo a considerare solo le funzioni sui numeri
naturali. Questo non è limitativo in quanto, poiché ogni informazione è
finita, essa può essere codificata tramite una collezione di numeri naturali,
la quale a sua volta può essere espressa tramite un unico numero
naturale tramite il procedimento di Gödel.

\begin{definizione}{procedimento di Gödel}{}
  Data una collezione di numeri naturali ($N_1, N_2,\ ...\ ,N_k$) e dati
  i primi $k$ numeri primi si ottiene il valore $R$ secondo il
  procedimento di Gödel:
  $$R = \prod_{i=1}^k P_{i}^{N_i}$$
  Dall'unicità della scomposizione in fattori primi di un valore naturale 
  deriva il fatto che $R$ identifichi univocamente la collezione originale 
\end{definizione}

Limitandoci alle sole funzioni sui naturali è noto dall'analisi
matematica che l'insieme rappresentante le funzioni definibili su $\mathbb{N}$
$$F = \{f:\mathbb{N}\rightarrow \mathbb{N}\}$$ non è numerabile.
Tuttavia l'insieme delle funzioni computabili è numerabile in quanto la
cardinalità dei simboli dell'alfabeto e degli stati è finita e associabile 
univocamente ad un intero tramite il procedimento di Gödel. Ciò significa 
che gran parte delle funzioni definibili non sono computabili. 

Sebbene ciò sia vero, ai fini pratici sono interessanti solo quelle 
funzioni definibili per mezzo di un linguaggio basato su un alfabeto 
finito di simboli, da cui deriva che anche l'insieme delle funzioni 
definibili (utili ai fini pratici) è numerabile. Purtroppo i due insiemi 
non coincidono, in quanto vi sono delle funzioni definibili ma non 
computabili, ovvero vi sono problemi irresolubili da qualsiasi MDT, 
come il "problema dell'halt".

\section{Problema dell'halt}

Il problema consiste nello stabilire se una data macchina di Turing con
un generico ingresso $X$ si ferma oppure no. Il problema è facilmente
definibile ma nel caso generale non è computabile.

\begin{dimostrazione}{non computabilità del problema dell'halt}{}
  Data una macchina di Turing $m\in M$ e un generico ingresso
  $x \in X$ definiamo la funzione caratteristica
  $$f_{halt}(m,x) = \begin{cases}1 & \text{se } m\text{ con ingresso }x\text{ si ferma}\\ 0 & \text{se }m\text{ con ingresso }x\text{ non si ferma}\end{cases}$$
  Questa funzione è definibile, ma calcolarla conduce ad un assurdo. 
  
  Se questa funzione fosse computabile allora dovrebbe per forza esistere una 
  qualche MDT in grado di calcolarla. Definiamo quindi la funzione $g_{halt}$ 
  avente come unico parametro una generica macchina di Turing $n$:
  $$g_{halt}(n) = \begin{cases}1 & \text{se }f_{halt}(n,n)=0\\\bot  & \text{se }f_{halt}(n,n) = 1\end{cases}$$
  In sostanza $g_{halt}$ si ferma solo quando la MDT $n$ con ingresso $n$ non si
  ferma, mentre non si ferma quando la MDT $n$ con ingresso $n$ si ferma.
  Prendiamo ora come ingresso $n_g$, ovvero proprio la MDT che calcola 
  $g_{halt}$. Da ciò deriva un \emph{assurdo} poiché sostituendo:
  $$g_{halt}(n_g) = \begin{cases}1 & \text{se }f_{halt}(n_g,n_g)=0\\\bot  & \text{se } f_{halt}(n_g,n_g) = 1\end{cases}$$
  Si ottiene che:
  \begin{itemize}
    \item 
      Se la MDT si ferma (poiché $g_{halt}$ dà come risultato 1), $f_{halt}$ 
      dice che \textbf{non} avrebbe dovuto fermarsi (poiché vale 0)
    \item 
      Se la MDT \textbf{non} si ferma (poiché $g_{halt}$ è indefinita), 
      $f_{halt}$ dice che avrebbe dovuto fermarsi (poiché vale 1)
  \end{itemize}
\end{dimostrazione}

Questo particolare problema è quindi \textbf{indecidibile}, ovvero non
computabile.

\section{Generabilità e decidibilità}

Poiché un linguaggio è un insieme di frasi, risulta importante indagare in
generale il problema di generabilità e decidibilità di un insieme.

L'analisi matematica introduce il concetto di \textbf{insieme
numerabile}, ovvero di insiemi a cui può essere associata una funzione $F$
in grado di metterne in corrispondenza gli elementi con i numeri
naturali. Tuttavia quest'unica definizione non è sufficiente, in quanto 
è necessario che tale funzione sia anche computabile affinché 
l'insieme sia effettivamente generabile da una macchina di Turing. 
Si introduce quindi il concetto di insieme \textbf{ricorsivamente enumerabile}. 

\begin{definizione}{insieme ricorsivamente enumerabile}{}
  Un insieme è ricorsivamente numerabile (o semidecidibile) se l'insieme è 
  numerabile e la funzione $F$ è anche computabile
\end{definizione}
In questo ultimo caso una MDT potrà inoltre generare l'insieme elemento per 
elemento, rendendo l'insieme \textbf{effettivamente generabile}.

\medskip Va notato, tuttavia, che il fatto che un insieme sia generabile 
non implica il fatto che sia possibile decidere se un generico elemento 
appartenga o meno all'insieme.

\begin{esempio}{}{}
  Supponiamo di costruire una MDT in grado di generare tutti i numeri pari
  e di chiederle se il numero $x$ sia pari. Questa comincerà a generare
  tutti i numeri pari e, uno ad uno, li confronterà con $x$ nella
  speranza che coincidano. Supponiamo ora che il numero $x$ sia dispari.
  L'algoritmo appena descritto rende impossibile il raggiungimento dello
  stato di $HALT$ da parte della macchina, in quanto la MDT continuerà a
  generare numeri pari all'infinito non trovandone mai uno uguale ad $x$
\end{esempio}

Il problema appena descritto è definito \textbf{problema semidecidibile}, 
infatti è possibile ricavare la soluzione solo per una restrizione dell'insieme 
dei possibili input (si riesce a decidere se "appartiene" in positivo, ma non 
se "non appartiene"). Occorre quindi un concetto più potente della semidecidibilità 
di un insieme, ovvero la \textbf{decidibilità}. 

\begin{definizione}{insieme decidibile}{}
  Un insieme $S$ è decidibile se la sua funzione caratteristica è computabile:
  $$f(x) = \begin{cases}1 & x\in S\\ 0 & x\notin S\end{cases}$$
  Ovvero se esiste una macchina di Turing in grado di comprendere, in un numero 
  finito di passi, se un elemento appartenga o meno all'insieme $S$
\end{definizione}
Riprendendo l'esempio precedente, per ottenere questo effetto è
sufficiente fare generare alla macchina di Turing anche l'insieme dei
numeri dispari.

\begin{teorema}{}{}
  Se un insieme è decidibile allora è anche semidecidibile ma non
  viceversa
\end{teorema}

\begin{teorema}{}{}
  Un insieme $S$ è decidibile se e solo se sia $S$ che il suo complemento 
  $U \setminus  S$ sono semidecidibili
\end{teorema}

Tutto questo è importante in quanto i linguaggi sono costruiti a partire
da un alfabeto finito e sono caratterizzati dall'insieme infinito delle
sue frasi lecite. Non basta che tale insieme di frasi possa essere
generato (semidecidibilità), ma è indispensabile poter decidere se una
frase sia lecita o meno senza entrare in un loop infinito (decidibilità).
