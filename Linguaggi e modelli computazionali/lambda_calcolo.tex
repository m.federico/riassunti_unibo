Il lambda calcolo è un formalismo sviluppato da Church attorno agli anni 
trenta in grado di descrivere qualunque algoritmo. Tale formalismo è Turing-
equivalente, e possiede come unico concetto quello di \textbf{funzione} e 
come unico paradigma quello di \textbf{applicazione di funzioni}. 

Il lambda calcolo ha fornito le basi per tutti i linguaggi funzionali ed 
orientati agli oggetti del XX secolo.

\medskip Per comprendere cosa sia il lambda calcolo possiamo rifarci su di un 
esempio.

\begin{esempio}{}{}
    Per definire matematicamente una funzione potremmo usare la seguente 
    notazione:
	$$cube: Int \rightarrow Int \text{ dove } cube(n)=n^3$$
	A questo punto possiamo porci alcune domande:
	\begin{itemize}
		\item
			Qual è il valore dell'identificatore $cube$?
		\item 
			Come possiamo rappresentare l'oggetto associato a $cube$?
		\item
			Può questa funzione essere definita senza un nome?
	\end{itemize}

	La stessa funzione può essere scritta usando la sintassi data da Church,
	tuttavia il lambda calcolo permette di definire solamente funzioni anonime:
	$$\lambda n.n^3$$
	Questa è una funzione  anonima che mappa ogni $n$ del dominio in $n^3$.
	
	Diciamo che l'espressione rappresentata da $\lambda n.n^3$ è il valore
	legato all'identificatore $cube$
\end{esempio}

\paragraph{Lambda calcolo e linguaggi di programmazione}
Si può notare che le funzioni utilizzate dal lambda calcolo sono simili alle 
funzioni anonime di Javascript. In questo linguaggio infatti abbiamo la 
sintassi:
\begin{center}
   \verb|f = function(x){return expr;}|\\
   \verb|f(arg)|
\end{center}
che costituiscono la sintassi per dichiarare una funzione anonima e per 
applicarla. Nel lambda calcolo il medesimo risultato è raggiunto tramite la 
sintassi:
\begin{align*}
  \lambda &x.<expr>\\
  (\lambda &x.<expr>)(arg)
\end{align*}

In linguaggi come Java (8+) e C\# la forma $\lambda$ è sostituita da 
operatori infissi come $\rightarrow$ (o $\Rightarrow$) ed i tipi degli 
argomenti possono essere sia espliciti che inferiti.

\medskip È inoltre possibile interpretare il lambda calcolo in chiave "object 
oriented".
\begin{itemize}
  \item 
    Definizione di metodi
    \begin{itemize}
    \item 
      $\lambda x.x$ può essere letta come \verb| Object m1 (Object x) {return x;}|
    \item
      $\lambda x.L$ può essere letta come \verb| Object m2 (Object x) {return expr;}| dove $L$ è il lambda termine corrispondente ad \verb|expr|
    \end{itemize}
    \item 
      Invocazione di metodi
      \begin{itemize}
        \item 
          $(\lambda x.x)y$ può essere letto come \verb|m1(y)|, dove \verb|m1| 
          è il nome del metodo che contiene il codice corrispondente a 
          $\lambda x.x$
      \end{itemize}
\end{itemize}

\section{Sintassi}
La potenza del lambda calcolo sta nell'avere una sintassi con pochi elementi e 
dotata di una "semplice" semantica che tuttavia ha sufficiente espressività da 
poter rappresentare tutte le funzioni computabili (vedi tesi di Church-
Turing).

Un lambda termine può essere:
\begin{itemize}
	\item
		$x$, ovvero una variabile. Tipicamente questi identificatori vengono 
		scritti con un singolo carattere minuscolo
	\item
		$\lambda x.L$, ovvero l'applicazione di una funzione anonima con un 
		unico parametro (la variabile $x$) ed un corpo $L$ costituito da un 
		lambda termine
	\item
		$LM$, ovvero l'applicazione di un termine $L$ ad un altro termine $M$
\end{itemize}

La sintassi del lambda calcolo può essere così rappresentata mediante sintassi 
BNF:

\begin{align*}
	<termine> ::=& <variabile>\\
	&|\, (<termine><termine>)\\
	&|\, \lambda <variabile>.<termine>\\
	&|\, (<termine>)
\end{align*}

La grammatica così definita è però ambigua. Prendiamo come esempio l'elemento 
$\lambda x.xy$. Occorre chiedersi come esso vada interpretato:
\begin{itemize}
  \item 
    come $(\lambda x.x)y$, ovvero come l'applicazione di $y$ alla funzione 
    $\lambda x.x$
  \item 
    come $\lambda x.(xy)$, ovvero come la definizione di una funzione con un 
    singolo parametro $x$ e corpo $xy$, a sua volta interpretabile come 
    applicazione di $y$ alla funzione $x$
\end{itemize}
Per evitare ambiguità è possibile utilizzare parentesi ed associatività degli 
operatori, tuttavia (esattamente come in matematica) vi sono delle regole per 
comprendere correttamente il significato di un'espressione.
\begin{itemize}
	\item
		L'applicazione delle funzioni è associativa a sinistra
	\item 
		I corpi delle funzioni sono estesi il più possibile, ovvero lo scope 
		di una variabile si estende a destra quanto più possibile
\end{itemize} 
Date queste regole si può vedere come l'applicazione di una funzione abbia 
precedenza sulle abstraction
\begin{esempio}{}{}
	$yLxM$ va letto come $((yL)x)M$ poiché l'applicazione delle funzioni è
	associativa a sinistra
\end{esempio}

\begin{esempio}{}{}
	$\lambda x.\lambda y.xyx$ va letto come $\lambda x.(\lambda y.xyx)$ 
	(ovvero come $\lambda x.(\lambda y.(xy)x)$ considerando l'associatività 
	dell'applicazione delle funzioni)
\end{esempio}

\begin{esempio}{}{}
	Per l'espressione $(\lambda x.LM)N$ sono strettamente necessarie le 
	parentesi nel caso in cui $N$ sia da intendere come argomento per la 
	funzione $\lambda x.LM$ e non come parte del corpo di tale funzione	
\end{esempio}

Data la semplicità della sintassi in alcuni casi può essere difficile 
interpretare correttamente un'espressione e inserire correttamente le 
parentesi.

\begin{esempio}{}{}
	Consideriamo il termine
	$$(\lambda n.\lambda f. \lambda x. f(nfx))(\lambda g.\lambda y.gy)$$
	Per prima cosa occorre identificare le lambda abstraction che lo 
	compongono:
	\begin{itemize}
		\item 
			$(\lambda x. f(nfx))$
		\item
			$(\lambda f. (\lambda x. f(nfx)))$
		\item 
			$(\lambda n.(\lambda f. (\lambda x. f(nfx))))$
		\item 
			$(\lambda y.gy)$
		\item 
			$(\lambda g.(\lambda y.gy))$
	\end{itemize}
	Infine occorre ricomporre i pezzi ricordando l'associatività sinistra
	$$((\lambda n.(\lambda f. (\lambda x. (f((nf)x)))))(\lambda g.(\lambda y.(gy))))$$
\end{esempio}

\section{Semantica}
La semantica è definita tramite delle regole di trasformazione del tipo
$$(\lambda x.L)M\rightarrow L[M/x]$$
Questa computazione (chiamata \textbf{riduzione}) può essere scomposta in 
alcuni passi fondamentali:
\begin{enumerate}
  \item 
    Identificazione di un pattern del tipo $(\lambda x.L)M$ (ovvero 
    l'applicazione al parametro $M$ della funzione $\lambda x.L$)
  \item
    Sua trasformazione nel lambda termine $L[M/x]$ sostituendo nel corpo 
    della funzione $L$ ogni occorrenza di $x$ con $M$
\end{enumerate}

\begin{esempio}{}{}
  L'espressione
  $$x$$
  non è chiaramente riducibile
\end{esempio}

\begin{esempio}{}{}
  L'espressione
  $$\lambda x.x$$
  non è riducibile e rappresenta la funzione identità
\end{esempio}

\begin{esempio}{}{}
  L'espressione
  $$(\lambda x.x)y$$
  si riduce ad $y$, infatti l'applicazione di questa funzione può essere 
  vista come la sostituzione di tutte le occorrenze della variabile $x$ 
  all'interno del corpo della funzione con il valore $y$
\end{esempio}

\begin{esempio}{}{}
  L'espressione
  $$(\lambda x.xx)(\lambda y.(\lambda z.yz))$$
  si riduce ad $(\lambda y.(\lambda z.yz))(\lambda y.(\lambda z.yz))$, 
  infatti la funzione $\lambda x.xx$ si occupa di duplicare l'argomento 
  passatogli ottenendo il risultato già descritto
\end{esempio}

A questo punto si può riformulare il concetto di applicazione di una funzione 
ad un argomento come "valutarne" il corpo dopo aver sostituito al parametro 
formale il parametro attuale

\section{Funzioni con più argomenti}
Il lambda calcolo, come già detto, possiede come unico concetto quello delle 
funzioni ad un argomento. Di conseguenza ci si pone il problema di 
determinare come rappresentare funzioni aventi più argomenti. Una soluzione 
al problema è data dal \emph{currying}, che permette di esprimere una 
funzione ad $n$ argomenti tramite $n$ funzioni ad un argomento innestate 
l'una dentro l'altra, in modo che ogni funzione ne restituisca un'altra 
parametrizzata come risultato. Mostriamo ora un caso con due argomenti (ma si 
possono fare osservazioni analoghe per il caso con tre o più argomenti).

\begin{esempio}{currying con due argomenti}{}
  La funzione
  $$\lambda x.\lambda y.xy$$ 
  si legge come 
  $$\lambda x.(\lambda y.xy)$$
  ovvero una funzione in $x$ ($\lambda x.U$) il cui corpo $U$ è una funzione 
  in $y$ ($\lambda y.xy$). Pertanto
  $$(\lambda x.\lambda y.xy)LM \rightarrow (\lambda y.xy)[L/x]M=(\lambda y.Ly)M$$
  A sua volta $(\lambda y.Ly)M$ è una funzione in $y$ in cui $y$ vale $M$. 
  Pertanto 
  $$(\lambda y.Ly)M\rightarrow (Ly)[M/y]=LM$$ 
  In definitiva 
  $$(\lambda x.\lambda y.xy)LM \rightarrow xy[L/x][M/y]$$
\end{esempio}

\section{Funzioni notevoli}
Alcune funzioni hanno un nome standard:
\begin{itemize}
  \item 
    $I::=\lambda x.x$ è la funzione identità
  \item 
    $K::=\lambda x.\lambda y.x$ è un operatore di proiezione che dati due 
    argomenti restituisce sempre il primo
  \item
    $K_{I}::=\lambda x.\lambda y.y$ è un operatore di proiezione che dati due 
    argomenti restituisce sempre il secondo
  \item 
    $succ::=\lambda n.\lambda z.\lambda s.s(nzs)$ permette di esprimere il 
    successore di un numero naturale
  \item 
    $\omega::=\lambda x.xx$ è la funzione duplicatrice
  \item 
    $\Omega::=\omega\omega=(\lambda x.xx)(\lambda x.xx)$ è la funzione di loop
\end{itemize}
Si può osservare che la funzione $\Omega$ riproduce sempre se stessa.

\section{Forme normalizzate}
\begin{definizione}{forma normalizzata}{}
  Un lambda termine si dice in forma normale se non si possono applicare 
  altre riduzioni
\end{definizione}
Si noti che questo fatto può dipendere dall'ordine con cui vengono applicate 
le riduzioni. A seconda delle sue caratteristiche un lambda termine si dice:
\begin{itemize}
  \item 
    \textbf{Fortemente normalizzabile} se qualunque sequenza di riduzioni 
    porta ad una forma normale
  \item
    \textbf{Debolmente normalizzabile} se esiste almeno una sequenza di 
    riduzioni che porta ad una forma normale
  \item
    \textbf{Non normalizzabile} se non si arriva mai ad una forma normale
\end{itemize}

\begin{esempio}{forma fortemente normalizzabile}{}
  Il termine $$K\ 0\ (K\ 0\ 1)$$ 
  è fortemente normalizzabile. Infatti nel caso si scegliesse di applicare 
  prima la funzione esterna si otterrebbe il termine $0$ non più riducibile. 
  Nel caso si scegliesse di applicare prima la seconda funzione si otterrebbe 
  prima la forma $K\ 0\ 0$ e poi applicando nuovamente la funzione $K$ si 
  otterrebbe nuovamente il termine non riducibile $0$
\end{esempio}

\begin{esempio}{forma debolmente normalizzabile}{}
  Il termine 
  $$K\ 0\ \Omega$$ 
  è debolmente normalizzabile. Infatti nel caso si scegliesse di applicare 
  prima la funzione $\Omega$ si entrerebbe in un loop infinito, mentre se si 
  scegliesse di applicare la funzione $K$ per prima si otterrebbe il termine 
  non riducibile $0$
\end{esempio}

\begin{esempio}{forma non normalizzabile}{}
  Il termine 
  $$\Omega$$ 
  non è normalizzabile. Infatti applicando la funzione $\Omega$ si otterrebbe 
  una nuova funzione $\Omega$ in un ciclo infinito.
  $$\Omega=\omega\omega=(\lambda x.xx)(\lambda x.xx)=(\lambda x.xx)(\lambda x.xx)=\omega\omega=\Omega$$
\end{esempio}

\begin{teorema}{di Church-Rosser}{}
  Ogni lambda ha al più una forma normale. Essa può essere dunque 
  interpretata come un risultato e dunque il lambda calcolo è 
  \textbf{deterministico}
\end{teorema}

Il teorema appena enunciato afferma semplicemente che se esiste una forma normale essa è unica. Pertanto
\begin{itemize}
  \item 
    Nel caso il termine sia fortemente normalizzabile essa verrà sempre 
    raggiunta. Si pensi a 
    \begin{center}
      \verb|int f(int x) { return x + 2; }|)\\
      \verb|f(2)|
    \end{center}
  \item 
    Nel caso sia debolmente normalizzabile il suo raggiungimento dipende 
    dall'ordine delle riduzioni (è importante dunque scegliere la giusta 
    strategia di valutazione)
    \begin{center}
      \verb|int f(int x){ return x == 0 ? 0 : f(x); }|\\
      \verb|f(2)|
    \end{center}
  \item
    Nel caso il termine non sia riducibile tale forma non esiste e dunque non 
    può essere raggiunta
    \begin{center}
      \verb|int f(int x){ return f(x); }|\\
      \verb|f(2)|
    \end{center}
\end{itemize}

\section{Computare con le lambda}\label{sec:computare_lambda}
Gli algoritmi descrivono funzioni che prendono in input una struttura dati e 
che, dopo un numero finito di passi, restituiscono un'altra struttura dati. 
Nel lambda-calcolo un primo lambda-termine $L$ descrive l'algoritmo, un altro 
lambda-termine $D$ descrive i dati di input. La computazione viene espressa 
mediante l'applicazione di $L$ a $D$ (ossia, scrivendo $LD$) e riducendo $LD$ 
"il più possibile". Il risultato è il nuovo lambda-termine $R$. 
I dati sono essi stessi dei termini-funzione opportunamente scelti (ossia, 
non riducibili).

\begin{esempio}{i booleani di Church}{}
	Supponiamo di voler rappresentare i valori booleani "true" e "false" e gli 
	operatori logici per mezzo del lambda calcolo. Poiché l'unico tipo di dato 
	è la funzione, sia i dati (i valori vero e falso) sia gli algoritmi (gli 
	operatori logici) saranno delle funzioni. A questo proposito dobbiamo 
	garantire che:
	\begin{itemize}
		\item 
			$NOT(true) = false$
		\item
			$NOT(false) = true$
		\item 
			$AND(true, x) = x$
		\item 
			$AND(false, x) = false$
		\item 
			$OR(true, x) = true$
		\item 
			$OR(false, x) = x$
	\end{itemize}

	Diamo dunque la rappresentazione dei dati e degli operatori logici:
	\begin{itemize}
		\item 
			Il valore "vero" può essere rappresentato mediante una funzione 
			(chiamiamola $T$) di proiezione:
			$$\lambda x.\lambda y.x$$
			essa restituisce il primo dei due argomenti
		\item 
			Il valore "falso" può essere rappresentato mediante una funzione 
			(chiamiamola $F$) di proiezione:
			$$\lambda x.\lambda y.y$$
			essa restituisce il secondo dei due argomenti
    \end{itemize}
    Si può notare come il funzionamento dei  valori $T$ ed $F$ ricordi molto 
    da vicino il funzionamento di un operatore ternario. Nel caso il booleano 
    sia vero viene restituito il primo valore, in caso contrario viene restituito il secondo.
    
    Su questi booleani possiamo definire i seguenti operatori: 
    \begin{itemize}
    \item 
      L'operatore $NOT$ può essere definito come:
      $$\lambda x.xFT$$
      ovvero una funzione che restituisce il primo valore (ossia $F$) nel 
      caso in cui l'ingresso sia $T$, mentre restituisce il secondo valore 
      ($T$) nel caso in cui l'ingresso sia $F$. Vediamo le sequenze di 
      riduzione
      $$NOT\ T = (\lambda x.xFT)T \rightarrow TFT \rightarrow (\lambda x.\lambda y.x)FT \rightarrow (\lambda y.F)T \rightarrow F$$
      $$NOT\ F = (\lambda x.xFT)F \rightarrow FFT \rightarrow (\lambda x.\lambda y.y)FT \rightarrow (\lambda y.y)T \rightarrow T$$
    \item 
      L'operatore $AND$ può essere definito come:
      $$\lambda x.\lambda y.xyF$$
      ovvero una funzione che nel caso in cui $x$ sia $T$ restituisce il 
      secondo valore (qualunque esso sia) e che restituisce $F$ nel caso 
      contrario (valutando l'operatore in corto circuito). Vediamo le 
      sequenze di riduzione
      $$AND\ T\ X = (\lambda x.\lambda y.xyF)TX \rightarrow  (\lambda y.TyF)X \rightarrow  TXF \rightarrow X$$
      $$AND\ F\ X = (\lambda x.\lambda y.xyF)FX \rightarrow  (\lambda y.FyF)X \rightarrow  FXF \rightarrow F$$
	\item
      L'operatore $OR$ può essere definito come:
      $$\lambda x.\lambda y.xTy$$
      ovvero una funzione che nel caso in cui $x$ sia $T$ restituisce $T$ 
      (valutando l'operatore in corto circuito) e che restituisce il secondo 
      valore (qualunque esso sia) nel caso contrario. Vediamo le sequenze di 
      riduzione
      $$OR\ T\ X = (\lambda x.\lambda y.xTy)TX \rightarrow  (\lambda y.TTy)X \rightarrow  TTX \rightarrow T$$
      $$OR\ F\ X = (\lambda x.\lambda y.xTy)FX \rightarrow  (\lambda y.FTy)X \rightarrow  FTX \rightarrow X$$
  \end{itemize}

  È importante notare che questa non è l'unica rappresentazione possibile, ma 
  che altre sono possibili. Inoltre occorre tenere a mente che questa è 
  solamente una rappresentazione dei dati e delle operazioni, sebbene possa 
  apparire strana nel caso in cui non si sia abituati ad essa (anche le 
  costanti sono delle funzioni). Per quanto strana possa apparire però è 
  sufficiente pensare che lo stesso tipo di operazione, ovvero la definizione 
  dei tipi di dato, viene effettuata in un qualsiasi linguaggio di 
  programmazione.
  
  Utilizzando una notazione javascript si può adottare la seguente 
  rappresentazione alternativa (forse più familiare):
  \begin{itemize}
    \item 
      True: $\lambda x.\lambda y.x$
      \begin{center}
        \verb|T = x => y => x|
      \end{center}
    \item
      False: $\lambda x.\lambda y.y$
      \begin{center}
        \verb|F = x => y => y|
      \end{center}
    \item 
      NOT: $\lambda x.xFT$
      \begin{center}
        \verb|NOT = x => x(F)(T)|
      \end{center}
    \item 
      AND: $\lambda x.\lambda y.xyF$
      \begin{center}
        \verb|AND = x => y =>  x(y)(F)|
      \end{center}  
    \item 
      OR: $\lambda x.\lambda y.xTy$
      \begin{center}
        \verb|OR = x => y =>  x(T)(y)|
      \end{center} 
  \end{itemize}
  
  Si può notare anche qui che le lambda seguono la filosofia "tutto è 
  funzione"
\end{esempio}

\subsection{Strategie di riduzione}

Come già detto (vedi \ref{sec:computare_lambda}) nel lambda calcolo alcuni 
termini ($L$) rappresentano gli algoritmi, altri ($D$) i dati di input. Il 
risultato $R$ viene ottenuto nel momento in cui non sono più possibili altre 
riduzioni dei vari lambda termini. Il concetto di "riduzione massima" 
introduce il concetto di \textbf{strategia di riduzione}, infatti a seconda 
della strategia scelta alcune computazioni potrebbero terminare (portando ad 
un risultato) o non terminare (non consegnando alcun risultato).

Vi sono due principi ispiratori alternativi:
\begin{itemize}
  \item 
    \textbf{Valutazione eager}: viene privilegiata la valutazione 
    dell'argomento rispetto all'applicazione dell'argomento alla funzione, 
    ovvero gli argomenti vengono valutati il prima possibile (\emph{strict 
    evaluation}). Questo tipo di valutazione è alla base di alcune strategie 
    trovate nei linguaggi di programmazione:
    \begin{itemize}
      \item
        \textbf{Applicative order}: \emph{rightmost innermost}, prima riduce 
        e valuta gli argomenti poi li applica alla funzione, pertanto 
        potrebbe non trovare la forma normale sebbene essa esista (non è una 
        strategia normalizzante). 
      \item 
        \textbf{Call by value}: è la famiglia di strategie più utilizzata nei 
        linguaggi di programmazione (i componenti della famiglia differiscono 
        per l'ordine con cui sono valutati gli argomenti della funzione, ad 
        esempio da destra a sinistra...). Questa soluzione è simile alla 
        precedente ma non riduce i termini dentro la funzione prima di 
        applicare gli argomenti e, poiché non passa funzioni ancora da 
        calcolare, considera come corpo \textbf{solo} lambda termini della 
        forma
        $$E::=x\ |\ xE\ |\ (\lambda x.L)$$
        escludendo quindi quelli del tipo $\lambda z.z$
      \item
        \textbf{Call by reference}: la funzione riceve un riferimento 
        implicito all'argomento del chiamante anziché una copia dello stesso. 
        È presente in vari linguaggi di programmazione, ma raramente è la 
        forma di default. Si noti che questa strategia non equivale al 
        passaggio di un puntatore in call by value, poiché in tal caso il 
        riferimento è passato esplicitamente
      \item
        \textbf{Call by copy-restore}: è un caso speciale della precedente 
        adatta al multi-threading. In questo caso ogni funzione riceve una 
        copia privata dell'argomento ed al termine dell'esecuzione tale 
        argomento viene copiato indietro. Questo sistema evita le 
        interferenze, tuttavia solo il valore computato dall'ultimo thread 
        sopravvive come risultato finale
    \end{itemize}
  \item
    \textbf{Valutazione lazy}: viene privilegiata l'applicazione 
    dell'argomento alla funzione rispetto alla valutazione dell'argomento 
    stesso, ovvero gli argomenti vengono valutati il più tardi possibile 
    (solamente quando serve, \emph{non strict evaluation}). Questo tipo di 
    valutazione è alla base di alcune strategie trovate nei linguaggi di 
    programmazione:
    \begin{itemize}
      \item
        \textbf{Normal order}: \emph{leftmost outermost}, applica gli 
        argomenti alle funzioni prima di ridurli, riducendo per prima la 
        lambda più a sinistra e considerando come corpo ciò che compare a 
        destra. Trova sempre la forma normale nel caso esista (da ciò deriva 
        il nome "normal"), tuttavia questo procedimento può causare la 
        creazione di copie multiple dello stesso termine causando 
        inefficienza (risolta dalla call by need)
      \item 
        \textbf{Call by name}: funziona similmente al "normal order", 
        tuttavia considera irriducibili le "lambda abstraction" (come 
        $\lambda x.x$). Ad esempio $\lambda x.(\lambda x.x)x$ è irriducibile 
        con questa strategia, mentre in normal order verrebbe ridotto a 
        $\lambda x.x$, di fatto dunque non porta le sostituzioni fin dentro 
        le funzioni interne
      \item
        \textbf{Call by need}: simile alla call by name, tuttavia evita 
        valutazioni multiple dello stesso termine memorizzandone il risultato 
        (è la forma più usata di valutazione lazy)
      \item 
        \textbf{Call by macro}: simile alla call by name ma basata su 
        sostituzioni testuali, per questo motivo richiede particolare 
        attenzione ai nomi delle macro in modo da evitare effetti spuri
      \item
        \textbf{Call by future}: variante della call by need in cui gli 
        argomenti sono valutati in parallelo alla funzione stessa (dunque non 
        solo quando vengono usati) ed i thread si sincronizzano solo nel 
        momento in cui l'argomento serve
      \item
        \textbf{Valutazione ottimistica}: variante della call by need in cui 
        gli argomenti rimangono parzialmente valutati per un determinato 
        periodo di tempo, allo scadere del quale scatta la call by need 
        normale. Con questa variante è possibile ridurre il costo a runtime
    \end{itemize}
\end{itemize}

\begin{esempio}{riduzione}{}
  Il termine 
  $$(\lambda x.\lambda y.x) p ((\lambda u.v)u)$$
  è fortemente normalizzabile. In normal order si ottiene la seguente 
  sequenza di riduzioni: 
  \begin{align*}
    (\lambda x.\lambda y.x) p ((\lambda u.v)u) &\rightarrow  (\lambda y.x)[p/x] ((\lambda u.v)u)\\
    &\rightarrow  (\lambda y.p) ((\lambda u.v)u) \rightarrow  p [((\lambda u.v)u)/y] \rightarrow  p
  \end{align*}
  In applicative order invece si ottiene la seguente sequenza di riduzioni:
  \begin{align*}
    (\lambda x.\lambda y.x) p ((\lambda u.v)u) &\rightarrow  (\lambda x.\lambda y.x) p v[u/u] \rightarrow  (\lambda x.\lambda y.x) p v\\
    &\rightarrow  (\lambda y.x)[p/x] v \rightarrow  (\lambda y.p) v \rightarrow  p[v/y] \rightarrow  p
  \end{align*}
\end{esempio}

\begin{esempio}{riduzione}{}
  Il termine 
  $$(\lambda x.a) ((\lambda x.xx) (\lambda y.yy)) = (\lambda x.a) \Omega $$
  è debolmente normalizzabile. In normal order si ottiene la seguente 
  sequenza di riduzioni: 
  \begin{align*}
    (\lambda x.a) \Omega  \rightarrow  a [\Omega /x] \rightarrow  a
  \end{align*}
  In applicative order invece si ottiene la seguente sequenza di riduzioni:
  \begin{align*}
    (\lambda x.a) ((\lambda x.xx) (\lambda y.yy)) &\rightarrow  (\lambda x.a) (xx)[(\lambda y.yy)/x]\\
    &\rightarrow  (\lambda x.a) ((\lambda y.yy)(\lambda y.yy)) = (\lambda x.a) \Omega 
  \end{align*}
  In questo caso si ha una riproduzione infinita
\end{esempio}

\begin{esempio}{riduzione}{}
  Il termine 
  $$\lambda x.(\lambda x.x) x $$ 
  è debolmente normalizzabile. In normal order e con la call by value si 
  riduce a: 
  $$\lambda x.x$$
  Con la call by name è invece irriducibile
\end{esempio}


\section{Turing-equivalenza}
Perché un formalismo sia Turing equivalente esso deve poter rappresentare:
\begin{itemize}
  \item 
    I numeri naturali
  \item
    Il successore di un numero naturale
  \item
    Il concetto di proiezione
  \item 
    Il concetto di composizione
  \item
    La ricorsione
\end{itemize}

Poiché il lambda calcolo è in grado di rappresentare tutti questi concetti 
esso \textbf{è} Turing equivalente. Alcune rappresentazioni risultano 
semplici, mentre altre risultano particolarmente complesse. In particolare, 
poiché le funzioni sono anonime, risulta particolarmente complesso 
rappresentare il concetto di ricorsione.

\paragraph{Numeri naturali e successore}
È possibile dare la seguente rappresentazione dei numeri naturali (numeri 
naturali di Church):
\begin{itemize}
  \item 
    $0=\lambda z.\lambda s.z$, ovvero una funzione a due argomenti che funge 
    da base
  \item
    $1 = \lambda z.\lambda s.sz$, ovvero una funzione a due argomenti che 
    aggiunge una $s$ in testa
  \item
    $2 = \lambda z.\lambda s.s(sz)$, ovvero una funzione a due argomenti che 
    aggiunge un'altra $s$ in testa
  \item
    $3 = \lambda z.\lambda s.s(s(sz))$, ovvero una funzione a due argomenti 
    che aggiunge una terza $s$ in testa
\end{itemize}

È importante notare che tali funzioni non vengono calcolate, ma sono esse 
stesse i risultati.

Mediante questa rappresentazione dei numeri naturali è possibile anche dare 
una definizione per quanto riguarda il concetto di successore di un numero 
naturale. Esso è la funzione 
$$Succ\ n = \lambda n.\lambda z.\lambda s.s(nzs)$$
\begin{esempio}{successore di $0$}{}
  \begin{align*}
    Succ\ 0 &\rightarrow \lambda z.\lambda s.s(0zs) \rightarrow \lambda z.\lambda s.s((\lambda z.\lambda s.z)zs) \rightarrow\\
    &\rightarrow \lambda z.\lambda s.s((\lambda s.z)s) \rightarrow \lambda z.\lambda s.s(z) \rightarrow 1
  \end{align*}
\end{esempio}

\begin{esempio}{successore di $1$}{}
  \begin{align*}
    Succ\ 1 &\rightarrow \lambda z.\lambda s.s(1zs) \rightarrow \lambda z.\lambda s.s((\lambda z.\lambda s.sz)zs) \rightarrow\\
    &\rightarrow \lambda z.\lambda s.s((\lambda s.sz)s) \rightarrow \lambda z.\lambda s.s(sz) \rightarrow 2
  \end{align*}
\end{esempio}

Inoltre si possono definire anche altre funzioni da utilizzare con i numeri 
naturali. Qui è riportato l'esempio della somma tra numeri naturali data 
dalla funzione $$Sum = \lambda m.\lambda n.\lambda z.\lambda s.n(m z s)s$$

\begin{esempio}{somma}{}
  \begin{align*}
    Sum\ 1\ 1 &\rightarrow \lambda z.\lambda s.1(1zs)s \rightarrow \lambda z.\lambda s.1(sz)s \rightarrow\\
    &\rightarrow \lambda z.\lambda s.(\lambda u.\lambda v.vu)(sz)s \rightarrow \lambda z.\lambda s.s(sz) \rightarrow 2
  \end{align*}
  
  Si noti come, anche in questo caso, la funzione "$2$" non venga utilizzata 
  per calcolare il risultato, ma essa stessa sia il risultato
\end{esempio}

\paragraph{Proiezione}
Come già anticipato la proiezione è facilmente ottenibile mediante 
un'apposita funzione $$K::=\lambda x.\lambda y.x$$

\paragraph{Composizione}
La composizione può essere ottenuta con molta semplicità tenendo conto 
dell'associatività dell'applicazione delle funzioni nel seguente modo:
$$\lambda f.\lambda g. \lambda  x. f(gx)$$

\paragraph{Ricorsione}
Per esprimere la ricorsione servono due ingredienti principali: la capacità 
di definire funzioni (il lambda calcolo ha solamente funzioni come tipo di 
dato) e la capacità di chiamare funzioni per nome. Questo secondo ingrediente 
presenta un grosso problema, infatti le funzioni lambda sono anonime, dunque 
serve un metodo alternativo per eseguire il corpo di una funzione e, al 
contempo, per rigenerare il corpo della funzione stessa. A questo provvede il 
combinatore di punto fisso $Y$.

\begin{definizione}{punto fisso}{}
  In matematica $x$ è un punto fisso per una funzione $f(x)$ se 
  $$x=f(x)$$
  ovvero se la funzione mappa il valore $x$ in se stesso
\end{definizione}

\begin{definizione}{combinatore di punto fisso Y}{}
  Il combinatore di punto fisso è definito come
  $$Y = \lambda f.(\lambda x.f(x x)) (\lambda x.f(x x))$$
\end{definizione}
Applicando questo operatore ad una funzione $F$ si ottiene il seguente 
effetto 
$$YF\rightarrow F(YF)$$
cioè si ottengono esattamente i due effetti richiesti precedentemente, ossia 
eseguire il corpo della funzione ($F(...)$) e rigenerare la funzione stessa 
($YF$). L'entità $YF$ è un punto fisso per la funzione $F$, più nello 
specifico si ottiene la seguente sequenza di riduzioni\footnote{il 
combinatore $Y$ presuppone la strategia call by name: se usato con strategia 
call by value diverge perché il lambda termine $(x x)$ presuppone di 
applicare la funzione $x$ a se stessa}
\begin{align*}
  Y F = &\lambda f.(\lambda x.f(x x)) (\lambda x.f(x x)) F\\
  &\rightarrow   (\lambda x.F(x x)) (\lambda x.F(x x))\\
  &\rightarrow   F((\lambda x.F(x x)) (\lambda x.F(x x))) \rightarrow F(YF)  
\end{align*}

Per comprenderne meglio il funzionamento conviene tradurne in codice 
Javascript il comportamento.
\begin{lstlisting}[language=javascript]
  function Y(f) {
    return (
      //la chiamata x(x) diverge se valutata con call by value in quanto per valutare la funzione occorrerebbe valutare prima il valore di x
      (function(x) {return f( x(x) ); })
      (function(x) {return f( x(x) ); })
    );
  }
\end{lstlisting}
Tuttavia per usare questo approccio in un modello computazionale basato su 
call by value occorre simulare la call by name nel solito modo, ovvero 
introducendo una funzione intermedia ausiliaria. 

Il combinatore $Y$ modificato per funzionare con la call by value viene anche 
chiamato combinatore $Z$.
\begin{definizione}{combinatore Z}{}
  Il combinatore $Z$ è definito come
  $$Z = \lambda f.(\lambda  x.f(\lambda  v.((x x) v))) (\lambda  x.f (\lambda  v.((x x) v)))$$
\end{definizione}
Utilizzando questa forma, esso può essere implementato in qualunque 
linguaggio che abbia gli oggetti come entità di prima classe (come Java, 
Javascript, C\#...). Tradotto in Javascript esso diverrebbe:
\begin{lstlisting}[language=javascript]
  function Z(f) {
    return (
      (function (x) { return f( function(v){ return x(x)(v); } ); })
      (function (x) { return f( function(v){ return x(x)(v); } ); })
    );
  }
\end{lstlisting}
Il combinatore $Y$ esploderebbe subito poiché avrebbe solo \verb|x(x)|.

A questo punto è possibile creare algoritmi ricorsivi mediante l'utilizzo di 
tali operatori. Per prima cosa occorre eliminare la ricorsione esplicita 
sostituendola con una implicita. Per farlo occorre:
\begin{enumerate}
  \item 
    Trovare una $F$ di ordine superiore il cui punto fisso sia la funzione 
    ricorsiva $f$ desiderata
  \item 
    Estrarre dalla funzione ricorsiva $f$ il passo-chiave, incapsulandolo in 
    una nuova funzione $step$ non ricorsiva
  \item
    Delegare al combinatore $Y$ la "composizione" dei passi di step,
    tramite la scrittura $Y\ step$
  \item 
    La "chiamata" di funzione sarà quindi $(Y\ step)(args)$
\end{enumerate}

Riportiamo qui l'esempio del calcolo del fattoriale di un numero mediante l'utilizzo dell'operatore di punto fisso\footnote{Per altri esempi consultare le slide 47-61 del pacco c20}

\begin{esempio}{calcolo del fattoriale}{}
  Se il lambda calcolo permettesse di dare un nome alle funzioni tale calcolo 
  sarebbe semplice, tuttavia le funzioni sono anonime, pertanto introduciamo 
  una funzione $F$ di ordine superiore che riceva $f$ come argomento
  $$F := \lambda f.\lambda x.((x\ ==\ 0)\ ?\ 1\ :\ x\ *\ f(\ x\ -\ 1\ ))$$
  Con questa funzione di ordine superiore la computazione effettiva del 
  fattoriale si esprime come 
  $$(YF)(args)$$
  ossia in questo caso 
  $$(YF)(n)$$

  \medskip Passiamo al calcolo di $3!$
  \begin{align*}
    3! &= (YF)(3) = F(YF)(3) =\\
    &\lambda  f.\lambda  x.((\ x==0\ )\ ?\ 1\ :\ x\ *\ f(x-1))(YF)(3) =\\
    &\lambda  x.((\ x==0\ )\ ?\ 1\ :\ x\ *\ (YF)(x-1))(3) =\\
    &(\ 3==0\ )\ ?\ 1\ :\ 3\ *\ (YF)(2) =\\
    &3\ *\ (2==0)\ ?\ 1\ :\ 2\ *\ (YF)(1) =\\
    &3\ *\ 2\ *\ (1==0)\ ?\ 1\ :\ 1\ *\ (YF)(0) =\\
    &3\ *\ 2\ *\ 1\ *\ (0==0)\ ?\ 1\ :\ ... =\\
    &3\ *\ 2\ *\ 1\ *\ 1 = 6
  \end{align*}
  
  \medskip Questo esempio può essere tradotto in codice Javascript come segue
  \begin{lstlisting}[language=javascript]
  FactGen = function(f){
    return function(x) {
      return (x==0) ? 1: x * f(x - 1); 
    }
  }
  
  //in questo caso utilizziamo il combinatore Z in quanto Javascript utilizza la strategia call by value
  var fact = Z(FactGen)
  
  3! = fact(3) = Z(FactGen)(3) = ... = 6
  \end{lstlisting}
\end{esempio}

\section{Utilizzi del lambda calcolo}
Il lambda calcolo è stato utilizzato per definire le proprietà dei linguaggi 
di programmazione, come sorgente di ispirazione per i linguaggi funzionali e 
come base per studiare i concetti dei sistemi di typing. Naturalmente però 
non è destinato direttamente agli utenti finali. Gli utenti godono dei 
vantaggi che ha comportato senza saperlo.
