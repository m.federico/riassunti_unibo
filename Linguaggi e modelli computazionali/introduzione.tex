Un linguaggio di alto livello è uno strumento per far eseguire ad un 
elaboratore le funzioni desiderate lavorando ad un livello concettuale più 
elevato di quello messo a disposizione (o indotto) dall'hardware con lo scopo 
di permettere una impostazione del problema più chiara. Tuttavia l'introduzione 
di un linguaggio porta con sé la necessità di opportuni traduttori che si 
occupino di colmare la distanza tra il linguaggio e quanto può essere 
direttamente compreso dalla macchina.\\
Oggigiorno i sistemi sono però caratterizzati da una sempre maggior 
interattività da cui consegue un legame più forte tra il \emph{linguaggio} ed i 
livelli sottostanti. Ogni livello costituisce una macchina virtuale a sé stante 
in grado di interpretare simboli. Per il corretto funzionamento di tale 
macchina tuttavia è necessario che essa sia dotata di opportuni sistemi per 
l'interpretazione dei simboli, i quali richiedono una certa \textbf{sintassi}, 
che fornisce gli strumenti per la verifica della correttezza strutturale,
ed una certa \textbf{semantica}, che fornisce il significato dei singoli 
simboli.

La capacità espressiva di un linguaggio ne fa un potente suggeritore di 
concetti e metodi di soluzione. Esiste infatti una molteplicità di linguaggi, 
in quanto ognuno di essi porta a sviluppare secondo determinati paradigmi e di 
conseguenza la risoluzione di un dato problema può risultare semplificata o 
meno a seconda del linguaggio scelto. Quindi non esiste un linguaggio 
"migliore" in assoluto, ma esiste un linguaggio migliore per l'ambito di 
applicazione.

\section{Risoluzione dei problemi}

Un modello relativamente semplice è costituito dal \textbf{modello a cascata}. 
Esso prevede una prima fase di enunciazione, analisi e progettazione seguita 
dall'implementazione (corredata da test estensivi). Per quanto riguarda 
l'analisi si può procedere sia con metodologia \emph{bottom-up} sia con 
metodologia \emph{top-down}. Queste due metodologie non sono mutuamente 
esclusive, ma possono essere usate entrambe nell'ambito dello stesso progetto 
tipicamente in fasi diverse della risoluzione di un problema. Solitamente in 
fase di analisi conviene scomporre il problema in più componenti base, mentre 
in fase di progettazione conviene procedere alla composizione di questi 
sottoparti al fine di ottenere il risultato finale.

La risoluzione di un problema implica sempre la creazione di un modello della 
realtà, spesso usando le metafore od i concetti tipici del linguaggio che verrà 
poi usato per l'implementazione. Questo accade poiché l'uso di un linguaggio 
spinge a ragionare in termini dei suoi costrutti. Ad esempio programmando in C 
difficilmente la soluzione farà uso di concetti estranei al C, come oggetti ed 
ereditarietà.\\ 
L'insieme delle metafore e dei concetti legati ad un linguaggio è chiamato 
\textbf{spazio concettuale}, e contiene sia i concetti legati all'ambiente 
supposto dal linguaggio (file, interfacce, processi, \dots) sia i concetti 
propri del linguaggio stesso (funzioni, oggetti, \dots). Poiché gli spazi 
concettuali possono essere differenti diversa sarà anche la visione del mondo 
durante l'utilizzo di questi linguaggi, portando a tecniche di progettazione 
differenti. Questo porta alla scelta di uno specifico linguaggio per la 
risoluzione di uno specifico problema, in quanto ogni linguaggio è in grado di 
esprimere meglio certi particolari aspetti e concetti.

\section{Linguaggi}

I linguaggi nascono da sorgenti di ispirazione in genere molto comuni.

\begin{itemize}
  \item
    I linguaggi imperativi nascono direttamente dall'architettura di Von
    Neumann in quanto rappresentano in astratto le singole istruzioni che il 
    processore dovrà eseguire per portare a termine un determinato compito, non
    lasciando alla macchina alcuna libertà di esecuzione. Questi linguaggi
    esprimono il \emph{controllo} sulla macchina, di conseguenza sono
    error prone e difficili da seguire o debuggare
  \item
    I linguaggi logici ed i linguaggi funzionali invece nascono dalla
    logica matematica, consentendo più ampi gradi di libertà. Questi
    linguaggi in genere hanno un approccio dichiarativo, ovvero il
    programmatore esprime il risultato desiderato senza preoccuparsi
    eccessivamente delle modalità con cui verrà ottenuto. Di conseguenza
    il programma diviene più semplice da leggere e la programmazione si
    basa sulla delegazione delle responsabilità, permettendo di concentrarsi
    sull'obiettivo e non sulla specifica modalità di risoluzione
\end{itemize}

\section{Stili di programmazione}

Un automa concepito secondo uno qualunque di questi formalismi può supportare 
linguaggi ispirati a un qualsiasi modello computazionale, tuttavia ogni modello 
computazionale promuove uno specifico stile di programmazione, caratterizzante 
per i linguaggi di tale famiglia. L'adozione di uno di questi stili ha 
conseguenze sulle proprietà dei programmi scritti, sulla metodologia di 
risoluzione dei problemi e sulle caratteristiche dei sistemi software 
(efficienza, modificabilità, manutenibilità \dots).\\
Poiché ogni modello computazionale ha diversi punti di forza ha senso usarne 
più d'uno, ad esempio sviluppando parti diverse della medesima applicazione con 
modelli diversi. Per far ciò è sempre più comune ricorrere a linguaggi 
"blended", ovvero che uniscono più modelli computazionali, con l'obiettivo di 
prendere il meglio da ognuno dei mondi (es. oggetti + funzionale).
