Un interprete è più di un puro riconoscitore in quanto è in grado di eseguire 
delle azioni (svolte immediatamente o in un secondo tempo) sulla base alla 
semantica della frase riconosciuta. In questi casi la sequenza di derivazione 
diventa importante poiché contribuisce ad attribuire significato alle frasi, 
dunque, ad esempio, non è sempre possibile sostituire le grammatiche ricorsive 
a sinistra con quelle ricorsive a destra.\\
Solitamente un interprete è strutturato in due componenti:

\begin{itemize}
  \item
    Un \textbf{analizzatore lessicale} (\emph{scanner}) con il compito di
    analizzare le parti \emph{regolari} del linguaggio fornendo al \emph{parser}
    le singole parole già aggregate, occupandosi dunque dei dettagli
    relativi ai singoli caratteri
  \item
    Un \textbf{analizzatore sintattico e semantico} (\emph{parser}) con il
    compito di valutare la correttezza della sequenza dei token ottenuti
    dallo scanner. Opera quindi sulle parti \emph{context-free} del linguaggio
\end{itemize}
Spesso queste due componenti sono organizzanti in un'architettura client/server.

\section{Analisi}
\subsection{Analisi lessicale}

Consiste nell'individuazione delle singole parole che compongono una frase 
raggruppando i caratteri di input secondo le produzioni regolari associate 
alle diverse categorie lessicali. Durante l'analisi lessicale i token possono 
essere categorizzati sulla base dello stato finale in cui si trova lo scanner, 
tuttavia questa non è sempre una strategia vincente. Spesso infatti un 
linguaggio consta di varie categorie di token, e cablarle tutte all'interno 
della struttura del RSF lo renderebbe eccessivamente complicato. 

Una soluzione alternativa consiste nell'utilizzare delle tabelle che contengano 
questo grado di conoscenza. In prima battuta lo scanner riconosce dunque degli 
identificatori che, in un successivo momento, possono venir riconsiderati sulla 
base del contenuto delle tabelle. Come risultato si ottiene uno scanner modulare 
ed estensibile.

\medskip In Java queste funzionalità possono essere ottenute mediante l'utilizzo 
delle classi \verb|Scanner| e \verb|Regex| (dal pacchetto \verb|java.util|).

\subsection{Analisi sintattica}

L'analisi top-down ricorsiva discendente (vedi Definizione 
\ref{def:analisi_ricorsiva_discendente}) offre, in presenza di grammatiche 
$LL(1)$, uno strumento semplice per costruire un riconoscitore, tuttavia nel 
caso si voglia passare ad un interprete occorre propagare qualcosa di più 
rispetto ad un valore booleano, ovvero un singolo valore (se l'obiettivo è la 
valutazione immediata) o un albero (se l'obiettivo è una valutazione 
differita).

\begin{esempio}{espressioni aritmentiche}{espressioni_aritmetiche_1}
  Consideriamo la creazione di una grammatica $LL(1)$ per l'analisi di 
  espressioni aritmetiche ed un interprete per tale grammatica (considerando 
  solamente le quattro operazioni e le parentesi). In prima battuta si 
  potrebbe pensare ad una grammatica come
  \begin{align*}
    EXP&::=EXP+EXP\\
    EXP&::=EXP-EXP\\
    EXP&::=EXP*EXP\\
    EXP&::=EXP/EXP\\
    EXP&::=num
  \end{align*}
  Questa grammatica è ambigua in quanto consente di avere due alberi 
  sintattici per la derivazione della medesima frase (vedi Esempio 
  \ref{es:grammatica_ambigua} e Definizione \ref{def:ambiguita}), in più non 
  consente di esprimere formalmente la priorità tra gli operatori
\end{esempio}

\begin{esempio}{espressioni aritmentiche (continua)}{espressioni_aritmetiche_2}
  Per esprimere associatività e priorità è possibile adottare una grammatica a 
  "strati" come
  \begin{align*}
    EXP&::= TERM\\
    EXP&::=EXP+TERM\\
    EXP&::=EXP-TERM\\
    TERM&::=FACTOR\\
    TERM&::=TERM*FACTOR\\
    TERM&::=TERM/FACTOR\\
    FACTOR&::=num\\
    FACTOR&::=(EXP)
  \end{align*}
  Il primo strato è il meno prioritario mentre il terzo è il più prioritario. 
  È importante notare che la grammatica è ricorsiva sinistra in modo da 
  rispettare l'associatività degli operatori, nel caso in cui venissero 
  aggiunti operatori associativi a destra (come l'esponenziazione) le 
  corrispondenti regole sarebbero ricorsive a destra. Poiché però la 
  grammatica è ricorsiva a sinistra non è possibile applicare l'analisi 
  ricorsiva discendente.
  
  A questo proposito, come anticipato, sarebbe possibile cambiare le 
  ricorsioni sinistre con delle ricorsioni destre cambiando però 
  l'associatività degli operatori. Oppure si potrebbe pensare di rimuovere 
  direttamente l'associatività obbligando l'utente ad utilizzare sempre le 
  parentesi per la scrittura delle espressioni
\end{esempio}

\subsubsection{Ricorsione sinistra}

Come osservato nell'esempio \ref{es:espressioni_aritmetiche_2}, a volte la 
grammatica presenta delle ricorsioni sinistre che non possono essere eliminate 
a meno di modificare la semantica del linguaggio. In questo casi è possibile 
utilizzare uno stratagemma atto a rendere possibile l'analisi ricorsiva 
discendente. È possibile creare una seconda grammatica (che non presenti 
ricorsioni sinistre) ed utilizzarla solamente per l'implementazione del parser. 
In questo modo la prima grammatica, ovvero quella reale, sarà descrittiva delle 
caratteristiche del linguaggio, mentre la seconda renderà possibile 
un'implementazione agevole del parser.

\begin{esempio}{espressioni aritmentiche (continua)}{espressioni_aritmetiche_3}
  Partendo dalla grammatica descritta dal precedente esempio il primo passo 
  consiste nel riconsiderare i sotto-linguaggi generati dai diversi strati. Si 
  può osservare che essi sono regolari e pertanto descrivibili tramite 
  espressioni regolari:
  \begin{align*}
    L(EXP)&=TERM\ ((+|-)\ TERM)^*\\
    L(TERM)&=FACTOR\ ((*|/)\ FACTOR)^*
  \end{align*}
  Successivamente possiamo utilizzare la notazione EBNF per denotare una 
  ripetizione senza dover far uso diretto della ricorsione.
  \begin{align*}
    L(EXP)&=TERM\ \{(+|-)\ TERM\}\\
    L(TERM)&=FACTOR\ \{(*|/)\ FACTOR\}
  \end{align*}
  Come si può notare queste regole non presentano più una ricorsione diretta ma 
  descrivono un processo computazionale iterativo, che può essere implementato 
  senza far uso della ricorsione.

  La grammatica così realizzata consente l'analisi ricorsiva discendente, in 
  quanto, prendendo come esempio un $EXP$, o successivamente vi è un segno 
  $(+|-)$ seguito da un $TERM$ oppure vi è la stringa vuota e quindi la fine 
  dell'input.
  
  Questa grammatica può essere dunque utilizzata per l'implementazione del 
  parser. Ad esempio la funzione \texttt{parseExp()}, deputata al 
  riconoscimento dei termini $EXP$ può così essere implementata:
  \begin{lstlisting}[language=java]
  public boolean parseExp() {
    boolean t1 = parseTerm();
    while (currentToken != null) {
      if (currentToken.equals("+")) {
        currentToken = scanner.getNextToken();
        boolean t2 = parseTerm();
        // Accumulo il risultato a sinistra
        t1 = t1 && t2; 
      } else if (currentToken.equals("-")) {
        currentToken = scanner.getNextToken();
        boolean t2 = parseTerm();
        t1 = t1 && t2;
      } else {
        return t1;
      }
    }
    return t1;
  }
  \end{lstlisting}
\end{esempio}

\subsection{Analisi semantica}

Una volta effettuata l'analisi lessicale e l'analisi sintattica, al fine di 
ottenere un interprete, occorre fornire una valutazione semantica delle frasi 
lette. Se il linguaggio è \emph{finito} è sufficiente un semplice elenco che 
metta in evidenza la relazione tra le frasi (corrette) ed il loro significato, 
mentre nel caso il linguaggio sia infinito occorre utilizzare una notazione 
finita in grado di racchiudere la nozione di infinito.

In prima battuta si potrebbe pensare di utilizzare una funzione in grado di 
mappare le frasi ed i token del linguaggio sui vari significati, tuttavia 
questo richiederebbe di enumerare vari casi esplicitamente.

Un approccio alternativo consiste nell'associare ad ogni regola sintattica una 
regola semantica, in modo da non lasciare nulla indietro durante 
l'elaborazione. In questo modo vengono realizzate funzioni, simili a quelle 
sintattiche che, secondo l'associatività richiesta dal contesto, sono in grado 
di attribuire un significato alle varie espressione mentre vengono lette.

\begin{esempio}{espressioni aritmetiche (continua)}{espressioni_aritmetiche_4}
  Riprendendo l'esempio delle espressioni aritmetiche, la funzione per la 
  valutazione semantica dei blocchi $EXP$ può essere così implementata:
  \begin{lstlisting}[language=java]
  public int parseExp() {
    int t1 = parseTerm();
    while (currentToken != null) {
      if (currentToken.equals("+")) {
        currentToken = scanner.getNextToken();
        int t2 = parseTerm();
        // Accumulo risultato a sinistra
        t1 = t1 + t2;
      } else if (currentToken.equals("-")) {
        currentToken = scanner.getNextToken();
        int t2 = parseTerm();
        t1 = t1 - t2;
      } else {
        // Il token letto non fa parte di L(Exp)
        return t1;
      }
    }
    // Fine dell'input
    return t1;
  }
  \end{lstlisting}
\end{esempio}

\section{Valutazione differita}

Le tecniche appena descritte possono andar bene nel caso in cui la semantica 
non cambi o nel caso in cui sia necessaria una valutazione in tempo reale del 
significato delle espressioni. Tuttavia nel caso si voglia ottenere una 
valutazione differita queste tecniche non sono più adatte e vanno modificate. 
Per prima cosa le varie funzioni atte a occuparsi della semantica non 
restituiranno più dei valori puntuali ma elaboreranno degli alberi (che 
racchiudono in sintesi il significato della frase).

Gli alberi ottenuti dalle sequenze delle chiamate a funzione contengono 
tuttavia varie informazioni non essenziali, derivanti da tutti i passi di 
derivazione. Ad esempio sapere che per ottenere un $num$ occorre passare prima 
per $EXP$ e poi per $TERM$ ed infine per $FACTOR$ risulta importante durante la 
costruzione dell'albero, tuttavia al momento dell'elaborazione queste 
informazioni non sono rilevanti. Tale albero, più compatto, viene chiamato 
\emph{Abstract Syntax Tree} (AST).

Si distingue quindi la \textbf{grammatica concreta} delle frasi, la quale 
contiene le regole ed i simboli utili a rendere chiaro il significato per chi 
legge, ma non strettamente necessari (ovvero che non aggiungono significato, ma 
guidano la lettura), dalla \textbf{grammatica astratta} degli alberi, la quale 
viene utilizzata durante l'elaborazione all'interno degli alberi e contiene le 
informazioni essenziali ad attribuire un significato alle frasi.

\begin{figure}[htp]
  \begin{center}
    \begin{subfigure}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=0.8\textwidth]{albero_concreto}
        \caption{Albero concreto}
        \label{fig:albero_concreto}
      \end{center}
    \end{subfigure}%
    \begin{subfigure}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=0.4\textwidth]{albero_astratto}
        \caption{Albero astratto}
        \label{fig:albero_astratto}
      \end{center}
    \end{subfigure}%
    \caption{Esempi di albero astratto e di albero concreto}
  \end{center}
\end{figure}
Di tutti i nodi solamente alcuni sono significativi, mentre altri non lo sono e 
dunque possono essere rimossi:

\begin{itemize}
  \item
    Alcuni nodi foglia contengono informazioni necessarie durante la
    lettura, ma che una volta generato l'albero hanno esaurito la loro
    funzione(ad esempio per la corretta interpretazione dell'ordine 
    degli operatori nelle espressioni aritmetiche le parentesi non 
    sono più necessarie ottenuto l'albero)
  \item
    I nodi aventi un unico figlio, ovvero che non generano biforcazioni,
    possono essere sostituiti con il figlio, in quanto il percorso per la
    lettura dell'albero diventa obbligato
  \item
    i nodi terminali non legati ad alcun significato sul piano semantico,
    tipicamente segni di punteggiatura o zucchero sintattico, possono essere 
    rimossi
\end{itemize}


Questi nodi possono essere rimossi in modo da ottenere l'\textbf{albero 
sintattico astratto} (Figura \ref{fig:albero_astratto}). Una volta ottenuto 
tale albero la rappresentazione dell'informazione diventa univoca e l'albero 
stesso fornisce l'ordine \emph{corretto} di valutazione. A partire dal 
medesimo albero astratto è poi possibile costruire diversi valutatori che, 
visitandolo nel modo opportuno, produrranno il risultato desiderato (es. 
sintesi vocale, compilazione del codice, elaborazione dei dati, \dots).

Risulta utile inoltre introdurre una \textbf{sintassi astratta} avente
l'obiettivo di descrivere come sia fatto l'AST. Questa sintassi \emph{non è
destinata all'utente}, ma viene utilizzata internamente per dare una
rappresentazione dell'albero in termini delle strutture dati che poi lo
andranno a costruire. Va notato che questa sintassi non è unica per un
dato problema, in quanto dipende fortemente dalla forma che si vuol
conferire alla soluzione.

\begin{esempio}{espressioni aritmetiche (continua)}{espressioni_aritmetiche_5}
  Riprendendo l'esempio delle espressioni aritmetiche, si potrebbe utilizzare la seguente sintassi astratta
  \begin{align*}
    EXP&::=EXP+EXP\\
    EXP&::=EXP-EXP\\
    EXP&::=EXP*EXP\\
    EXP&::=EXP/EXP\\
    EXP&::=num
  \end{align*}
  Da cui derivano le classi utilizzate all'interno del modello dei dati. 
  Utilizzando tali classi la funzione adibita al parsing delle espressioni 
  diventa:
  \begin{lstlisting}[language=java]
  public Exp parseExp() {
    Exp termSeq = parseTerm();
    while (currentToken != null) {
      if (currentToken.equals("+")) {
        currentToken = scanner.getNextToken();
        Exp nextTerm = parseTerm();
        if (nextTerm != null) {
          // costruzione APT a sinistra
          termSeq = new PlusExp(termSeq, nextTerm);
        } else {
          return null;
        }
      } else if (currentToken.equals("-")) {
        currentToken = scanner.getNextToken();
        Exp nextTerm = parseTerm();
        if (nextTerm != null) {
          // costruzione APT a sinistra
          termSeq = new MinusExp(termSeq, nextTerm);
        } else {
          return null;
        }
      } else {
        return termSeq;
      }
    } // end while
    return termSeq;
  }
  \end{lstlisting}
\end{esempio}

\subsection{Valutazione degli alberi}

Una volta ottenuto l'AST esso può essere valutato in più modi da più 
valutatori differenti. Ad esempio un primo valutatore potrebbe restituire la 
lettura del testo fornito (es. "Roberto" di Trenitalia), oppure fornire una 
valutazione del contenuto della frase inserita. \\
Esistono, inoltre, più modalità con cui valutare un albero, nel caso specifico 
un AST:

\begin{itemize}
  \item
    \textbf{Pre-order}: viene valutata prima la radice e poi i figli
    nell'ordine. Produce una notazione prefissa
  \item
    \textbf{Post-order}: vengono valutati i figli nell'ordine e solo
    successivamente la radice. Produce una notazione postfissa
  \item
    \textbf{In-order} (solo per gli alberi binari): viene valutato il
    figlio sinistro, poi la radice e successivamente il figlio destro.
    Produce una notazione infissa, la quale ha il difetto però di appiattire 
    l'albero facendo perdere la nozione di livello. Per questo motivo, nel 
    caso in cui venga prodotta una notazione infissa occorre utilizzare degli 
    stratagemmi grafici, quali parentesi o colori, atti a riportare 
    l'informazione circa i livelli che altrimenti verrebbe perduta
\end{itemize}

La notazione infissa è quella che, culturalmente, ci risulta più semplice 
nell'uso matematico, tuttavia questa rende necessaria l'introduzione della 
priorità degli operatori e delle parentesi per indicare quale sia l'ordine 
desiderato per l'esecuzione delle operazioni. Questi problemi sono assenti nel 
caso vengano utilizzate la notazione prefissa o la notazione infissa, esse 
infatti hanno il pregio di indicare quali siano gli operandi separatamente da 
quali siano gli operatori, in modo che risulti semplice l'esecuzione 
dell'operazione e che non sia necessario specificare esplicitamente quale sia 
la priorità degli operatori.

\begin{esempio}{confronto fra le notazioni}{}
  \begin{center}
    \begin{tabular}{ccc} 
      \toprule
      Notazione infissa & Notazione prefissa & Notazione postfissa  \\ 
      \hline
      $9-4-1$          & $- - 9\ 4\ 1$     & $9\ 4 - 1 -$        \\
      $9-(4-1)$        & $- 9 -4\ 1$       & $9\ 4\ 1 - -$       \\
      \bottomrule
    \end{tabular}
  \end{center}
\end{esempio}

Nel caso della notazione prefissa, ovvero la classica notazione funzionale, 
infatti viene specificata prima quale sia l'operazione da effettuare e solo 
successivamente quali siano i due operandi. Nella notazione postfissa invece 
vengono specificati prima gli operandi e solo successivamente l'operazione da 
svolgere. Questa ultima notazione ha il pregio di essere interpretabile in 
maniera estremamente semplice da una macchina.

Da quest'ultima idea si può pensare di introdurre una macchina a stack per 
l'esecuzione dei programmi: ogni volta che viene incontrato un simbolo 
(numerico in questo caso) esso viene aggiunto sullo stack, ogni volta che viene 
incontrato un operatore esso implica due pop dallo stack, l'esecuzione 
dell'operazione ed infine una push per inserire il risultato sullo stack.

\begin{esempio}{valutazione da parte di una macchina a stack}{}
  Data l'operazione $$13\ 4\ 5\ -\ -$$ si potrebbe ottenere il seguente codice 
  macchina:
  \begin{lstlisting}
    push 13
    push 4
    push 5
    sub \\include due pop per estrarre i dati ed una push per inserire il risultato
    sub
    pop \\pop finale per restituire il risultato
  \end{lstlisting}
\end{esempio}

\subsection{Implementazione del valutatore}

Il valutatore incorpora la \textbf{funzione di valutazione}, ovvero deve 
visitare l'albero applicando ad ogni nodo la semantica prevista per quel tipo 
di nodo (discriminando quale tipo di nodo stia visitando). 

Un primo approccio di implementazione prevede una funzione statica che esegua 
la giusta operazione in base al tipo del nodo. Di conseguenza la struttura sarà 
costituita da una lunga serie di blocchi condizionali necessari a discriminare 
i vari casi, rendendo la soluzione verbosa ed inefficiente. 

Un secondo approccio è quello di rompere la serie di blocchi condizionali 
incapsulando ognuno di essi in un metodo di un oggetto: questa metodologia è 
più efficiente, pratica e orientata ad oggetti. Se questo elenco di funzioni 
viene aggiunto al parser non si hanno sostanzialmente miglioramenti rispetto al 
caso precedente, tuttavia se esse vengono aggiunte all'AST stesso è possibile 
discriminare il tipo di nodo per mezzo del polimorfismo.

Mettiamo ora a confronto la due metodologie appena presentate: una metodologia 
funzionale:
\begin{itemize}
  \item
    Permette di separare nettamente la sintassi dall'interpretazione poiché si 
    basa su di una funzione di interpretazione esterna alle classi. 
  \item
    Facilita l'introduzione di nuove interpretazioni, perché basta scrivere 
    una nuova funzione senza toccare le classi già esistenti
  \item
    Rende però più oneroso introdurre una nuova produzione, perché occorre
    modificare il codice di tutte le funzioni di interpretazione per
    tenere conto della nuova struttura
\end{itemize}
Una metodologia basata su oggetti invece:
\begin{itemize}
\item
  Utilizzando un metodo di interpretazione specializzato all'interno di ogni 
  classe facente parte dell'AST, è sufficiente invocare il metodo perché venga 
  eseguita l'azione corretta
\item
  Facilita l'aggiunta di nuove produzioni, perché basta definire una nuova
  sottoclasse con il relativo metodo di valutazione senza la necessità di 
  modificare le classi già esistenti
\item
  Rende più oneroso introdurre nuove interpretazione, perché occorre
  definire il nuovo metodo in ogni classe
\end{itemize}
La scelta tra i due stili non è neutra, n quanto influenza modularità ed estensibilità del codice.

\subsubsection{Implementazione di un compilatore}

Una volta ottenuto l'interprete ottenere un compilatore è relativamente
semplice, in quanto se l'interprete restituisce una valutazione della
frase letta un compilatore restituisce una meta-valutazione della stessa
frase, ovvero una riscrittura in un codice intermedio.

\begin{wrapfigure}{R}{0.4\textwidth}
  \begin{center}
    \includegraphics[width=0.3\textwidth]{ast_visitor.png}
    \caption{Esempio di visitor per le espressioni numeriche}
    \label{fig:ast_visitor}
  \end{center}
\end{wrapfigure}

Nella realtà però le interpretazioni da effettuare su di uno stesso
codice sono molteplici (analisi semantica, type checking, generazione de
codice, \ldots{}). Ad una prima vista l'approccio funzionale sembra più
opportuno, sebbene vada contro i principi dell'ingegneria del software.
Perciò sovente si utilizza il \textbf{pattern visitor} per descrivere le
varie interpretazioni da effettuare sul codice. Questo pattern permette di 
ottenere i vantaggi descritti dalla seconda modalità sopra descritta separando 
però il codice applicativo relativo all'interpretazione da quello relativo al 
modello dei dati. 
Tutte le classi derivanti da \verb|Exp| implementeranno il metodo \verb|accept()| 
per accettare la visita da parte delle classi che implementano l'interfaccia 
\verb|IExpVisitor|.

\begin{esempio}{visitor per le stampe di un'espressione}{}
  Il visitor necessario a stampare le espressioni in forma prefissa
  \begin{lstlisting}[language=java]
  class ParExpVisitor implements IExpVisitor {
    String curs = "";
    
    public String getResult() { return curs; }
    
    protected void visitOpExp(OpExp e) {
      e.left().accept(this); 
      String sleft = getResult();
      e.right().accept(this); 
      String sright = getResult();
      curs = "(" + e.myOp() + " " + sleft + " " + sright + ")";
    }

    public void visit( PlusExp e ) { visitOpExp(e); }
    public void visit( MinusExp e ) { visitOpExp(e); }
    public void visit( TimesExp e ) { visitOpExp(e); }
    public void visit( DivExp e ) { visitOpExp(e); }
    public void visit( NumExp e ) { curs = "" + e.getValue(); }
  }
  \end{lstlisting}
  
  Il metodo \verb|getResult()| risulta utile per leggere il risultato alla fine di
  ogni valutazione in quanto il valore della variabile \verb|curs| viene 
  riscritto da ogni analisi
\end{esempio}

\subsection{Assegnamento e variabili}

Ogni linguaggio di programmazione prevede l'introduzione del concetto di 
operatore di assegnamento e di variabile. Sebbene spesso si utilizzi 
l'operatore "$=$" per gli assegnamenti, esso non è né riflessivo né 
simmetrico, al contrario dell'operatore matematico "$=$" rappresentante 
l'uguaglianza matematica. A questo proposito alcuni linguaggi come il Pascal 
utilizzano un diverso operatore per gestire l'assegnamento ("$:=$") che 
evidenzia la non riflessività dell'operatore stesso.

La peculiarità dell'assegnamento, soprattutto quello in stile C, è che il 
simbolo di variabile ha un significato diverso a seconda del fatto che si 
trovi a sinistra o a destra dell'uguale. Infatti una variabile che si trovi a 
sinistra dell'uguale rappresenta in realtà l'area di memoria in cui andare ad 
inserire i dati, mentre la variabile che si trovi a destra dell'uguale 
rappresenta il valore contenuto all'interno dell'area di memoria (si parla 
dunque di L-value, e di R-value), e dunque il simbolo di variabile è 
\emph{overloaded}.

Inoltre si possono effettuare altre scelte per quanto riguarda le 
caratteristiche dell'assegnamento. L'assegnamento può essere 
\emph{distruttivo} o \emph{non distruttivo} a seconda del fatto che una 
variabile possa cambiare valore durante l'esecuzione del programma o meno. 
Tipicamente i linguaggi imperativi sono dotati di assegnamento distruttivo in 
quanto partono dall'astrazione della macchina di Von Neumann e dal concetto di 
cella di memoria riscrivibile, mentre i linguaggi logici, che si basano sulla 
logica matematica, tipicamente sono dotati di assegnamento non distruttivo (si 
creano sempre nuove variabili).

\medskip Associato al concetto di variabile vi è quello di \emph{environment}, 
inteso come elenco di coppie simbolo valore. Nel caso la semantica 
dell'assegnamento sia quella dell'assegnamento distruttivo è consentita la 
modifica dei valori associati a variabili già esistenti all'interno 
dell'environment, in caso contrario tale modifica non è consentita, ma è 
consentito solamente l'inserimento di nuove variabili al suo interno.\\
Spesso risulta comodo dotarsi di environment multipli, soprattutto nel caso in 
cui si sia in presenza di un linguaggio di programmazione dotato di 
assegnamento distruttivo. In questo caso l'enviroment è suddiviso in vari 
sottoambienti separati tra loro e dotati di un ben specifico tempo di vita. Di 
conseguenza ogni modello computazionale deve specificare quale sia il campo di 
visibilità (\emph{scope}), ovvero quali environment siano visibili in un dato 
punto della struttura fisica del programma. L'environment globale contiene il 
valore delle variabili il cui tempo di vita è l'intero programma, mentre gli 
environment locali contengono le coppie il cui tempo di vita non coincide con 
quello del programma. Questi ultimi sono tipicamente legati all'attivazione di 
funzioni o ad altre strutture presenti a runtime.

\medskip Di base lo scopo dell'assegnamento è quello di produrre una modifica 
dell'environment, non quello di produrre un valore; di conseguenza 
l'assegnamento, almeno in linea teorica, sarebbe un'istruzione e non 
un'espressione. Tuttavia interpretando l'assegnamento come istruzione si 
rende impossibile la realizzazione dell'assegnamento multiplo come 
\verb|x=y=z=2| (che rende palese \textbf{l'associatività a destra}
dell'assegnamento).
\begin{esempio}{assegnamento multiplo}{}
  L'espressione \verb|x=y=z=2| può essere interpretata come 
  \verb|x=(y=(z=2))|. Si può notare che il valore 2 fluisce da destra a 
  sinistra
\end{esempio}

Ipotizzando di voler introdurre l'assegnamento all'interno di un ipotetico 
linguaggio di programmazione occorre anche decidere se diversificare o meno 
la sintassi di L-value e di R-value. Nel caso in cui si scelga di \emph{non} 
diversificare la sintassi per L-value e R-value la grammatica ottenuta, 
limitatamente agli assegnamenti, diviene però $LL(2)$, e dunque richiede una 
bufferizzazione maggiore da parte del parser\footnote{Infatti, se non si 
differenzia sintatticamente l'identificatore "sinistro" da quello "destro", 
per distinguerli bisogna per forza verificare se il token successivo è 
l'operatore $=$}. Nei linguaggi reali spesso si accetta questo compromesso, ma 
alcuni linguaggi che hanno necessità di essere particolarmente efficienti, 
come quelli interpretati in tempo reale, scelgono di diversificare le due 
sintassi (vedi Bash).

\begin{esempio}{espressioni aritmetiche (continua)}{espressioni_aritmetiche_6}
  Si supponga di riesaminare la grammatica delle espressioni aritmetiche 
  precedente e di voler aggiungere il concetto di assegnamento\footnote{Per 
  l'esempio completo si veda il pacco c08 di slide}. La grammatica assume la 
  forma:
  \begin{align*}
    EXP&::= TERM\\
    EXP&::= EXP+TERM\\
    EXP&::= EXP-TERM\\
    EXP&::= ASSIGN\\
    ASSIGN&::= IDENT=EXP\\
    TERM&::= FACTOR\\
    TERM&::= TERM*FACTOR\\
    TERM&::= TERM/FACTOR\\
    FACTOR&::= num\\
    FACTOR&::=\$IDENT\\
    FACTOR&::= (EXP)
  \end{align*}
  Si nota perciò che saranno necessarie tre nuove classi \verb|AssignExp|, 
  \verb|RIdentExp| (che rappresenta il nome di una chiave all'interno 
  dell'environment per ricercare una variabile) e \verb|LIdentExp| (che è 
  dotato di un nome e di un valore all'interno dell'environment). La 
  valutazione dunque di un token del tipo \verb|RIdentExp| ha lo scopo di 
  inserire (o modificare a seconda della semantica) all'interno 
  dell'environment un valore, mentre la valutazione di un token del tipo 
  \verb|LIdentExp| ha lo scopo di cerca e restituire un valore che deve 
  essere presente all'interno dell'environment
\end{esempio}

