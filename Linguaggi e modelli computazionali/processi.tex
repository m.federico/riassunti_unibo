\section{Paradigma imperativo e paradigma dichiarativo}

Solitamente siamo abituati ad utilizzare il \textbf{paradigma imperativo}, il 
quale prevede che un programma non sia altro che una \textbf{sequenza di 
ordini}. Ciò è intuitivo poiché comandare è facile ed è immediato mappare su 
una macchina mini-azioni che eseguono un determinato codice. Tuttavia un elenco 
di ordini è intrinsecamente non invertibile, ovvero per esprimere la situazione 
"inversa" occorre un elenco di ordini completamente diverso anche se in origine 
la relazione fra i dati era simmetrica. 

\begin{esempio}{}{}
	L'equazione $x=y-2$ in matematica è simmetrica poiché dato $y$ si ricava $x$ 
	e viceversa, mentre un programma imperativo pensato per ricavare $x$ non può 
	essere usato per ricavare $y$
\end{esempio}

\subsection{Il paradigma dichiarativo}
Il paradigma imperativo non è l'unico possibile. Esiste infatti il 
\textbf{paradigma dichiarativo} in cui non si esprimono ordini, ma 
\textbf{relazioni fra entità}. Esso è sicuramente meno intuitivo ma ha il 
vantaggio di essere invertibile poiché si limita ad affermare \emph{ciò che è 
vero}. Perciò in un programma dichiarativo si possono esprimere relazioni così 
come sono, demandando a runtime la scelta di usarla in un verso o nell'altro.

\subsection{Basi di Prolog}
Un programma Prolog è un inseme di regole espresse secondo la seguente 
notazione:
\begin{center}
  \verb|testa :- corpo.|
\end{center}
L'operatore \verb|:-| esprime l'implicazione logica $\leftarrow$, ovvero se 
il \textbf{corpo è vero}, allora anche la \textbf{testa è vera} (non 
viceversa). Se il corpo manca esso viene assunto come vero, dunque la testa di 
tali istruzioni risulta sempre vera. In tal caso l'istruzione viene chiamata 
\textbf{fatto} e l'operatore \verb|:-| viene omesso. La testa ha la forma 
\begin{center}
  \verb|funtore(lista_argomenti)|  
\end{center}
nel caso la lista degli argomenti sia assente le parentesi possono essere 
omesse. Il corpo invece è una congiunzione di termini separati da virgole, dove 
ogni termine ha anch'esso la forma 
\begin{center}
  \verb|funtore(lista_argomenti)|
\end{center}
Inoltre sia testa che corpo possono contenere variabili, le quali devono 
necessariamente iniziare con la maiuscola.

\begin{esempio}{}{}
  \begin{lstlisting}[language=Prolog]
  p(a,12X) :- q(a), r(13), s(X,1).
  s(Y,X) :- q(X), r(Y).
  \end{lstlisting}
  Si noti come in questo caso la testa sia vera solo se tutti i termini del 
  corpo lo sono. Inoltre lo scope di una variabile è la singola regola, perciò 
  la variabile \verb|X| nella prima regola non ha nulla a che vedere con la 
  variabile \verb|X| della seconda regola
\end{esempio}

Infine se occorrono termini che iniziano per la maiuscola (ma che non siano 
variabili) o che contengano caratteri non standard, occorre racchiuderli tra 
apici.

\begin{esempio}{genitori e figli}{}
  Fatti (assiomi):
  \begin{lstlisting}[language=Prolog]
  uomo(adam).
  uomo(peter).
  donna(mary).
  donna(eve).
  genitore(adam, peter). 
  genitore(eve, peter).
  genitore(adam, paul).
  genitore(mary, paul).
  \end{lstlisting}
  Regole:
  \begin{lstlisting}[language=Prolog]
  padre(Y,C) :- uomo(Y), genitore(Y,C).
  madre(X,C) :- donna(X), genitore(X,C).
  \end{lstlisting}
  
  Innanzitutto stabiliamo che i fatti \verb|genitore(a,b)| si leggano come 
  "\verb|a| genitore di \verb|b|". Conseguentemente le due regole sono di 
  facile interpretazione, ovvero "\verb|Y| è padre di C se \verb|Y| è un uomo 
  ed è anche genitore di \verb|C|". Analogamente "\verb|X| è madre di \verb|C| 
  se \verb|X| è una donna ed è anche genitore di \verb|C|".
  Quindi come in un database si ha una conoscenza di base (fatti), ma non solo 
  poiché si hanno anche regole che permettono di dedurre nuove conoscenze. 
  Infatti le possibili query possono:
  
  \begin{itemize}
    \item 
      Recuperare tuple dal "database":
      \begin{lstlisting}[language=Prolog]
  ?- donna(X)
  > X=mary ? ;
  > X=eve ? ;
  > no
      \end{lstlisting}
      Il motore Prolog fornisce un nome di donna alla volta e si ferma in 
      attesa di un OK o di uno skip. In quest'ultimo caso ne elenca un altro e 
      così via. Se non vi sono più risultati restituisce \verb|no|.

    \item 
      Recuperare tuple da sintetizzare al volo:
      \begin{lstlisting}[language=Prolog]
  ?- padre(X,paul)
  > X=adam
  > yes

  ?- padre(eve, paul)
  > no
      \end{lstlisting}
  \end{itemize}
\end{esempio}

\begin{esempio}{equazioni}{}
  Per i numeri naturali non è opportuno adottare la rappresentazione 
  classica\footnote{ovvero $1$, $2$, $3$, \dots} poiché i simboli da essa 
  utilizzati hanno quel significato solo nella nostra mente, ma non sono una 
  \emph{rappresentazione esplicita}. Possiamo quindi indicare con \verb|s(N)| 
  il successore di un numero naturale \verb|N| e con \verb|eq(X,Y)| la 
  relazione fra \verb|X| e \verb|Y|. Dunque l'equazione \verb|x+2=y| si può 
  esprimere tramite la relazione:
  \begin{lstlisting}[language=Prolog]
  eq(X, s(s(X))).
  \end{lstlisting}
  Ora si può interrogare il sistema Prolog fornendo coppie di elementi 
  (ottenendo risposte del tipo \verb|yes|/\verb|no| in base al fatto che 
  soddisfino o meno l'equazione) o uno solo elemento ottenendo l'altro.

  Si può sfruttare la relazione anche per far generare tutte le possibili 
  soluzioni fornendo all'equazione numeri naturali via via diversi. Si può 
  utilizzare a tal proposito un generatore di numeri naturali, ad esempio:
  \begin{lstlisting}[language=Prolog]
  num(1).
  num(s(N)) :- num(N).
  \end{lstlisting}
  Ovvero 1 è un naturale e se \verb|N| è un naturale anche il suo successore 
  lo è. Invocando \verb|num(X)| si otterranno tutte le soluzioni, ovvero 
  \verb|1|, \verb|s(1)|, \verb|s(s(1))|, \dots

  Tuttavia nella pratica esprimere i numeri con la relazione \verb|s(N)| 
  risulta molto scomodo. Il linguaggio Prolog accetta quindi anche numeri 
  reali nell'usuale notazione decimale. Per calcolare il valore di 
  un'espressione numerica tuttavia non si può utilizzare il classico \verb|=| 
  poiché in Prolog esso indica unificazione sintattica e non semantica. 
  \verb|Value = 13-4| dà come risultato la struttura \verb|13-4| ossia 
  \verb|'-'(13,4|) e non 9. Per ottenere 9 bisogna usare il predicato (non 
  invertibile) \verb|is|, ovvero \verb|Value is 13-4|. In questo modo prima di 
  assegnare un valore alla variabile \verb|Value| viene fatta una valutazione 
  semantica del lato destro nel dominio dei numeri reali
\end{esempio}

\begin{esempio}{append su liste}{}
  Siano \verb|l1| e \verb|l2| due liste di cui si vuole fare l'append. 
  Imperativamente occorre stabilire \emph{a priori} gli argomenti di input e 
  di output oppure distinguere i tre casi. Ad esempio in Java si può 
  utilizzare il metodo \verb|addAll|.
  \begin{lstlisting}[language=java]
  l3 = l1.addAll(l2)
  \end{lstlisting}
  Andando però ad esaminare il codice della \verb|addAll| ci si accorge di 
  come esso sia relativamente lungo, error prone, difficile da debuggare e con 
  una "ossessione del controllo" che tratta la macchina come un mero esecutore 
  incapace di autonomia.

  Con l'approccio dichiarativo di Prolog, invece, basta esprimere solo due 
  regole:
  \begin{lstlisting}[language=Prolog]
  append([],L,L).
  append([T|C],L,[T|Cr]) :- append(C,L,Cr).
  \end{lstlisting}
  La prima regola stabilisce che appendendo una lista \verb|L| ad una lista 
  vuota \verb|[]| si riottiene sempre \verb|L|. La seconda stabilisce che 
  appendendo \verb|L| a una lista la cui testa sia \verb|T| e la cui coda sia 
  \verb|C|, si ottiene una nuova lista avente per testa \verb|T| e per coda il 
  concatenamento \verb|Cr| di \verb|C| e \verb|L|.

  Un esempio di utilizzo è trovare i valori delle variabili tali che:
  \begin{lstlisting}[language=Prolog]
  ?- append([a,b], [c,4,d], R).
  > Solution: R / [a,b,c,4,d]

  ?- append([a,b], X, [a,b,c,d]).
  > Solution: X / [c,d]
  \end{lstlisting}
\end{esempio}

\section{Processi computazionali iterativo e ricorsivo}
È bene non confondere un processo computazionale con il costrutto che lo 
rappresenta all'interno di un linguaggio di programmazione, pertanto occorre 
chiedersi cosa distingua un processo computazionale iterativo da uno ricorsivo.

Nei linguaggi imperativi, il costrutto linguistico che esprime un processo 
computazionale iterativo è tipicamente il ciclo. Al di là della sintassi 
specifica di ogni linguaggio, ogni ciclo possiede una variabile che funge da 
\emph{accumulatore} (pertanto l'assegnamento è necessariamente distruttivo) 
che deve essere inizializzata prima del ciclo e che deve essere modificata 
durante il ciclo. Tale variabile, al termine del ciclo, contiene il risultato 
finale dell'intero processo. Di conseguenza, immaginando di congelare 
l'esecuzione al passo $k$ del ciclo, l'accumulatore conterrebbe il risultato 
parziale $k$-esimo, in questo senso un processo iterativo computa \textbf{in 
avanti}.

\begin{esempio}{calcolo iterativo del fattoriale}{}

  \begin{lstlisting}[language=java]
  int fact = 1;
  
  for (int i = 1; i <= n; i++) {
    fact = fact * i;
  }
  
  printf(fact)
  \end{lstlisting}

  Come si può notare nel caso ci si fermi a metà del processo la variabile
  \verb|fact| conterrebbe un valore parziale
  
  \medskip Nel caso in cui \verb|n=3| si avrebbe la seguente immagine a runtime
  \begin{center}
    \includegraphics[width=0.4\textwidth]{fattoriale_iterativo_runtime.png}
  \end{center}
\end{esempio}

Poiché i processi iterativi dispongono dell'assegnamento distruttivo sono 
estremamente efficienti nell'uso della memoria, tuttavia non permettono un 
debug semplice in quanto per capire cosa sia successo è necessario rieseguire 
linea per linea il codice.

Al contrario un processo ricorsivo è tipicamente espresso tramite una \textbf{funzione ricorsiva}. Al di là della sintassi specifica tale schema non prevede alcun accumulatore, infatti ogni chiamata si preoccupa di ottenere il risultato $(k-1)$-esimo e di sintetizzare il risultato $k$-esimo a partire da esso. Durante le chiamate non vi è alcun risultato parziale in quanto solamente una volta raggiunto il caso base del processo è possibile sintetizzare il primo risultato, poi utilizzato per sintetizzare via via gli altri risultati, pertanto il processo in questo caso computa \textbf{all'indietro}.

\begin{esempio}{calcolo ricorsivo del fattoriale}{}

  \begin{lstlisting}[language=java]
  int fact (int n) {
    return (n == 0) ? 1 : fact(n - 1) * n; 
  }
  
  printf(fact(3))
  \end{lstlisting}

  Il primo risultato ottenibile è quello della chiamata \texttt{fact(0)},
  ogni chiamata si occupa di sintetizzare poi nuovi valori aumentando il
  consumo di memoria ma permettendo un debug più agevole.
  
  \medskip Nel caso in cui \verb|n=3| si avrebbe la seguente immagine a runtime
  \begin{center}
    \includegraphics[width=0.6\textwidth]{fattoriale_ricorsivo_runtime.png}
  \end{center}
  Si nota in questo caso una fase ascendente in cui il problema viene 
  sgranato, ma non vi è alcuna computazione ed una fase discendente in cui 
  viene sintetizzato ogni risultato a partire dal precedente
\end{esempio}

In alcuni casi è possibile esprimere un processo computazionale
iterativo mediante un costrutto sintatticamente ricorsivo, ciò accade
se l'istruzione ricorsiva è l'ultima della funzione e il risultato 
parziale k-esimo viene portato avanti passandolo come argomento alla 
chiamata. Si parla in tal caso di \emph{funzione ricorsiva in coda}.

\begin{esempio}{calcolo ricorsivo in coda del fattoriale}{}

  \begin{lstlisting}[language=java]
  int factIt(int acc, int i, int n) {
    return i > n ? acc : factIt(i * acc, i + 1, n - 1);
  }

  printf(factIt(1,n))
  \end{lstlisting}
  Si noti come la variabile \verb|acc| porti in avanti il risultato parziale.
  Come nel caso del ciclo la computazione avviene in avanti, infatti
  interrompendo l'iterazione al passo $k$ si otterrebbe il risultato
  $k$-esimo. Questo processo però si basa sulla sintesi di nuovi valori
  che però possono sovrascrivere i precedenti perché computando in avanti
  questi ultimi non sono più necessari quando si fa una nuova chiamata.

  In questo caso non sono necessari tutti i record di attivazione, ma
  ne basta solamente uno, facendo sì che mano a mano i nuovi record vadano 
  a sovrascrivere il precedente. In questo modo si riesce a risparmiare memoria,
  ma si perde la traccia dell'esecuzione
  
  \medskip Nel caso in cui \verb|n=3| si avrebbe la seguente immagine a runtime
  \begin{center}
    \includegraphics[width=0.6\textwidth]{fattoriale_tail_ricorsivo_runtime.png}
  \end{center}
  Si noti che l'immagine a runtime è identica a quella di un processo 
  iterativo.
\end{esempio}

Poiché la ricorsione in coda permette di esprimere un processo
computazionale iterativo mediante costrutti ricorsivi può essere usata
in alternativa ai cicli per esprimere il concetto di iterazione.
Tipicamente i linguaggi funzionali ed i linguaggi logici tendono a
seguire questo approccio ottimizzando il runtime, mentre i linguaggi
imperativi, possedendo i cicli e l'assegnamento distruttivo, non
ottimizzano la ricorsione in coda. \\
Questa ottimizzazione può essere effettuata allocando il nuovo record di 
attivazione al di sopra del record precedente, in tal modo l'occupazione delle 
risorse è identica a quella del caso ciclico. Di conseguenza ricorsione in 
coda e iterazione mediante cicli sono solo forme diverse dello stesso processo 
computazionale.

\begin{esempio}{}{}
  Questa caratteristica può essere ottenuta in linguaggi come Scala o Kotlin 
  su richiesta. \\
  In Scala è ottimizzata di default (disattivabile tramite un'opzione di 
  compilazione).
  
  \begin{lstlisting}[language=scala]
  def factIt(acc:Int, i:Int, n:Int):Int = {
    if (i>n) acc else factIt(i * acc, i + 1, n)
  }
  \end{lstlisting}
  
  In Kotlin invece viene ottimizzata su richiesta mediante l'utilizzo della 
  keyword \verb|tailrec|
  \begin{lstlisting}[language=kotlin]
  tailrec fun factIt(acc:Int, i:Int, n:Int):Int = {
    return if (i > n) {
      acc 
    } else {
      factIt(i * acc, i + 1, n)
    }
  }
  \end{lstlisting}
  Empiricamente si può dimostrare che con l'attivazione della Tail Recurtion 
  Optimization si ha un risparmio all'incirca del 60\% delle risorse stack 
  utilizzate per realizzare la funzione fattoriale
\end{esempio}
