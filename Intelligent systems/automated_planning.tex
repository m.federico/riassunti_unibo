La pianificazione automatica è un'importante branca dell'intelligenza artificiale che ha avuto una grande diffusione e sviluppo all'inizio degli anni novanta grazie ai fondi dati dall'esercito statunitense. In questo periodo lo scopo principale riguardava la logistica. Questa disciplina poi si è espansa per coprire anche la robotica e la diagnostica.

\begin{definizione}{Pianificazione}{}
    La pianificazione è un esercizio di problem solving che permette di trovare una sequenza di azioni che, se eseguite da un agente, consente di andare da uno stato iniziale ad uno stato finale (goal)
\end{definizione}

In generale questo problema è estremamente difficile in quanto è semidecidibile, ovvero è possibile trovare una soluzione se esiste, ma in generale non si può dimostrare l'assenza di una soluzione. Un pianificatore deve essere inoltre completo e corretto, ovvero deve essere in grado di trovare una soluzione al problema (se questa esiste) e la soluzione trovata deve essere valida.

Un algoritmo di planning necessita di tre dati in input principali: uno stato iniziale, ovvero la condizione da cui partire, un insieme di azioni che può effettuare (i cosiddetti \textit{operatori}), ed uno stato finale, ovvero l'obiettivo da perseguire. Dati questi elementi l'algoritmo deve essere in grado di trovare una sequenza di azioni parzialmente o totalmente ordinata in grado di produrre il risultato atteso a partire dallo stato iniziale. Se l'insieme di azioni è totalmente ordinato viene indicata la sequenza precisa di azioni da intraprendere (ad esempio infilare la calza destra, infilare la calza sinistra, indossare la scarpa destra, indossare la scarpa sinistra), mentre se le azioni sono parzialmente ordinate viene lasciata una certa libertà di scelta all'esecutore, che dovrà comunque rispettare i vincoli di precedenza necessari (ad esempio la calza va infilata prima della scarpa). In generale vi sono vari tipi di pianificazione:
\begin{itemize}
    \item Lineare: la sequenza delle azioni è predeterminata
    \item Non lineare: le azioni sono legate tramite vincoli di precedenza, ma in generale non esiste un criterio di ordinamento totale
    \item Gerarchica: in questo caso si effettua una pianificazione ad alto livello procedendo poi a pianificare più nel dettagli le varie fasi
    \item Condizionale: in questo caso non tutte le condizioni dell'ambiente sono note perciò vengono inserite all'interno del piano una serie di condizioni che specificano cosa fare nel caso si verifichi una certa condizione e nel caso non si verifichi. Di conseguenza si pianifica per tutte le possibili combinazioni di eventi incrementando notevolmente il tempo di calcolo necessario
    \item A grafi
    \item Intelligenza a sciami
\end{itemize}

La ricerca può essere fatta in avanti (forward) o all'indietro (backward). Nel primo caso si parte dallo stato iniziale e si prosegue fino al raggiungimento dello stato finale. Nel secondo caso si parte dallo stato finale fino a giungere a quello iniziale, applicando la tecnica della \textit{goal regression}.

\begin{definizione}{Pianificatore automatico}{}
    Un pianificatore automatico è un agente intelligente in grado di operare all'interno delle condizioni di uno specifico dominio applicativo descritto mediante stati ed operatori
\end{definizione}

Con questa definizione si tocca con mano la differenza tra \textit{narrow AI} e \textit{general AI}. Nel primo caso l'agente è in grado di operare solamente all'interno delle condizioni del dominio applicativo per cui è stato programmato, nel secondo caso è in grado di adattarsi ed operare in vari contesti (eventualmente anche nuovi).

In generale un pianificatore può costruire un piano ed eseguirlo in un secondo momento, oppure pianificare durante l'esecuzione. Questa caratteristica distingue i pianificatori proattivi da quelli reattivi.

\medskip
Un planner si basa su una descrizione formale delle azioni eseguibili (la \textbf{domain theory}). Ogni azione è modellata tramite due insiemi: le precondizioni, ovvero le condizioni che devono essere verificate affinché l'azione possa essere compiuta, e le postcondizioni, ovvero gli effetti che l'azione ha sull'ambiente. Le postcondizioni possono essere rappresentate come un'unica lista di predicati oppure con due liste separate, una per i risultati positivi, una per quelli negati. Spesso le azioni possono essere parametrizzate tramite delle variabili in modo da descrivere delle classi di azioni più generali che possono essere riutilizzate in momenti differenti.

\begin{esempio}{Descrizione azioni e mondo a blocchi}{}
    \begin{algorithm}[H]
        \Fn{stack (X,Y)}{
            \Precond{holding(X) and clear(Y)}\;
            \Postcond{handempty and clear(X) and on(X,Y)}\;
        }
    \end{algorithm}
    \begin{algorithm}[H]
        \Fn{unstack (X,Y)}{
            \Precond{handempty and clear(X) and on(X,Y)}\;
            \Postcond{holding(X) and clear(Y)}\;
        }
    \end{algorithm}

    Possiamo descrivere due azioni eseguibili all'interno del mondo a blocchi. Esse descrivono il caso in cui si voglia sollevare un blocco ed il caso in cui si voglia posare un blocco. Nel primo caso la mano deve tenere il blocco indicato con $X$ e non deve esserci nulla sopra al blocco $Y$. In caso queste due condizioni siano soddisfatte è possibile appoggiare $X$ sopra ad $Y$. Come conseguenza si avrà che $X$ si trova sopra $Y$, sopra $X$ non c'è nulla e la mano è vuota. Per la seconda azione vale lo stesso ragionamento
\end{esempio}
Il mondo a blocchi rappresenta una semplificazione del mondo reale, tuttavia risulta particolarmente comodo per comprendere e studiare il comportamento degli algoritmi in un ambiente semplificato e completamente noto.

\section{Pianificazione non lineare}
La pianificazione vista durante il corso di Fondamenti di Intelligenza Artificiale era di tipo lineare, ovvero era eseguita come una ricerca nello spazio degli stati. In questo tipo di pianificazione l'ordine delle azioni è fissato e predeterminato. Nel momento in cui il pianificatore si accorge che la sequenza trovata è errata (per una qualsiasi ragione) esso deve effettuare uno o più passi di backtracking in modo da disfare quanto già eseguito.

Nel caso della pianificazione non lineare invece il pianificatore effettua una ricerca all'interno dello spazio dei piani. L'algoritmo non genera più il piano come una successione lineare di azioni, ma come una serie di azioni da intraprendere legate da vincoli di precedenza. Costruito poi un piano di questo tipo diviene possibile linearizzarlo ponendo in fila le azioni secondo i vincoli che le legano e risolvendo i possibili conflitti che possono generarsi.

I pianificatori non lineari basano le loro azioni sulla \textbf{closed world assumption}, e quindi non contemplano l'eventualità che qualcosa non sia completamente noto.

\begin{definizione}{Closed world assumption}{}
    Si assume che tutti ciò che non è esplicitamente affermato come vero sia falso
\end{definizione}
Questi pianificatori sono anche chiamati \textit{pianificatori generativi}, essi raccolgono i dati prima della pianificazione ed assumono che lo stato dell'ambiente in cui si trovano sia completamente noto, non venga modificato da nessun'altra entità e rimanga valido fino alla fine della pianificazione, in altre parole assumono che il mondo resti congelato fintanto che la pianificazione non è terminata.

\begin{definizione}{Least commitment planning}{}
    Il pianificatore non impone mai condizioni e restrizioni non strettamente necessarie
\end{definizione}

Questo principio risulta particolarmente comodo in quanto evita di prendere decisioni vincolanti quando non ve ne è reale bisogno ed evita dunque l'utilizzo del backtracking (come invece avviene nel caso dei pianificatori lineari).

\medskip
Un piano non lineare può essere rappresentato come:
\begin{itemize}
    \item Un insieme di azioni (istanze degli operatori), ovvero operatori le cui variabili sono state legate a dei valori
    \item Un insieme di relazioni d'ordine (non esaustivo) tra le azioni
    \item Un insieme di link causali
\end{itemize}
Come anticipato la pianificazione non lineare procede effettuando una ricerca all'interno dello spazio dei piani partendo da un piano vuoto e terminando nel momento in cui viene trovato un piano in grado di soddisfare lo stato finale. Tutti i piani intermedi della ricerca sono piani parziali, ovvero piani in cui almeno un'azione possiede ancora una variabile non legata.

Il piano iniziale contiene unicamente due azioni fittizie: $start$ e $end$. $start$ non ha precondizioni ed ha come conseguenza le condizioni iniziali del problema, $end$, d'altro canto, non ha effetti, ma ha come precondizioni lo stato finale. Queste due azioni sono rigidamente ordinate facendo precedere $start$ a $end$.

\begin{esempio}{}{}
    Lo scopo di questa pianificazione è infilare le scarpe in entrambi i piedi.\\
    \begin{algorithm}[H]
        \Goal{shoeOn(right), shoeOn(left)}\;
        \Fn{WearShoe(Foot)}{
            \Precond{socksOn(Foot)}\;
            \Postcond{shoeOn(Foot)}\;
        }
    \end{algorithm}

    \begin{algorithm}[H]
        \Fn{PutSocks (Foot)}{
            \Precond{not socksOn(Foot)}\;
            \Postcond{socksOn(Foot)}\;
        }
    \end{algorithm}
    Date queste azioni si avranno i seguenti stati iniziale e finale:
    \begin{center}
        \includegraphics[width=\textwidth]{shoes.png}
    \end{center}
    Ottenuto questo piano la linearizzazione può essere fatta in vari modi fintato che le precedenze vengono rispettate.
\end{esempio}

Durante ogni passo della pianificazione viene raffinato un piano aggiungendo delle azioni. La modalità tipica di risoluzione è quella backward, che prevede di partire da uno dei \textit{goal aperti} e si sceglie una fra le azioni possibili che abbia fra i suoi effetti il goal. È importante inoltre capire se le catene di azioni generate sono in conflitto fra loro, ovvero se le azioni di una disfano le condizioni richieste da un'altra. Infine, il piano reale viene generato come linearizzazione delle azioni presenti nel piano.
\begin{definizione}{Goal aperto}{}
    Una condizione non ancora soddisfatta da alcuna azione
\end{definizione}

\begin{definizione}{Link causale}{}
    Si definisce link causale una tripla $<S_i,S_j,C>$ dove $C$ è un predicato precondizione di $S_j$ ed effetto di $S_i$. Un link causale esprime la causalità di un'azione nei confronti dell'altra. I link causali sono indicati nei diagrammi tramite frecce più spesse rispetto a quelle usate per le relazioni d'ordine
\end{definizione}
Tramite l'utilizzo dei link causali è possibile tenere traccia delle relazioni di causalità tra le azioni, consentendo di capire per quale motivo un'azione sia stata inserita in un certo punto del piano. Non è detto che ad un link di ordinamento corrisponda un casual link, ma è vero il viceversa. Essi sono utili inoltre per tracciare e risolvere i problemi relativi alle interazioni tra gli obiettivi. A questo punto l'algoritmo può essere descritto nel seguente modo:
\begin{algoritmo}{Partial Order Planning}{}
    \begin{algorithm}[H]
        \While{il piano non è completo}{
            seleziona un'azione $SN$ che ha una precondizione $C$ non soddisfatta\;
            seleziona un'azione $S$ che abbia $C$ tra gli effetti\;
            aggiungi il vincolo d'ordine $S< SN$\;
            \If{$S$ è una nuova azione}{
                aggiungi il vincolo $start < S< end$\;
            }
            aggiungi il link causale $<S,SN,C>$\;
            risolvi ogni conflitto sul link creato\;
        }
    \end{algorithm}
\end{algoritmo}
Nel caso esista un punto di scelta e si verifichi un errore l'algoritmo può sempre effettuare backtracking e tornare sui propri passi per esplorare delle alternative.

\begin{esempio}{Link causali e minacce}{}
    \begin{center}
        \includegraphics[width=0.5\textwidth]{link_causali_1.png}
    \end{center}
    In questo caso un'azione $S3$ è una minaccia per il link causale $<S1,S2,c>$ dato che ha un effetto negativo su di esso. Nel caso in cui il piano venisse linearizzato come
    \[start\rightarrow S1\rightarrow S3\rightarrow S2\rightarrow end\]
    $S3$ distruggerebbe la condizione $c$ necessaria come precondizione per l'esecuzione di $S2$. Le uniche linearizzazioni possibili sono dunque
    \[start\rightarrow S3\rightarrow S1\rightarrow S2\rightarrow end\]
    \[start\rightarrow S1\rightarrow S2\rightarrow S3\rightarrow end\]
    ottenibili tramite l'aggiunta di una condizione d'ordine. Nel primo caso si parla di \textit{demotion}, nel secondo invece di \textit{promotion}. Sebbene queste soluzioni siano estremamente semplici in molti casi portano alla risoluzione della minaccia, nei casi in cui ciò non accade o si usano altre tecniche oppure è necessario effettuare uno o più passi di backtracking per cercare un altro piano.
\end{esempio}

\begin{esempio}{}{}
    In questo caso si vuole preparare un frullato alla banana, per cui sono necessari latte e una banana come ingredienti ed un frullatore per miscelare i quali sono venduti in negozi differenti. Si supponga di avere le seguenti azioni:\\
    \begin{algorithm}[H]
        \Init{at(home), sells(hws, drill), sells(sm, milk), sells(sm, banana)}\;
        \Goal{at(home), have(milk), have(banana)}\;
        \Fn{Go(X,Y)}{
            \Precond{at(X)}\;
            \Postcond{at(Y), not at(X)}\;
        }

        \Fn{buy(S,Y)}{
            \Precond{at(S), sells(S,Y)}\;
            \Postcond{have(Y)};
        }
    \end{algorithm}
    Si ha il seguente stato iniziale:

    \begin{center}
        \includegraphics[width=0.7\textwidth]{es_minaccia_1_1.png}
    \end{center}
    Procediamo con l'algoritmo sopra descritto. Tra i goal da soddisfare $at(home)$ è già soddisfatto, perciò possiamo concentrarci sugli altri. Si noti che nel caso in cui tutti i goal fossero già soddisfatti non sarebbe necessario inserire alcuna azione. Lavorando backwards cerchiamo di soddisfare il goal $have(drill)$. Tra le azioni a disposizione l'unica che permette di soddisfarlo è $buy$. Inserendo questa azione, ed aggiungendo il necessario link causale, possiamo legare $Y$ a $drill$, tuttavia $S$ rimane aperto. Effettuando il medesimo ragionamento per i restanti due goal si ottiene:
    \begin{center}
        \includegraphics[width=0.7\textwidth]{es_minaccia_1_2.png}
    \end{center}
    A questo punto occorre risolvere i nuovi goal aperti. Per risolvere $sells(S, drill)$ è possibile usare $sells(hws, drill)$ che si trova tra le condizioni iniziali, legando in questo modo $S$ a $hws$. Lo stesso ragionamento può essere applicato anche agli altri casi analoghi.
    \begin{center}
        \includegraphics[width=0.7\textwidth]{es_minaccia_1_3.png}
    \end{center}
    Occorre ricordare a questo punto che il planner opera solamente manipolazioni simboliche, pertanto non ha idea di cosa i simboli possano significare in una certa lingua. Risolviamo ora i vari $at$. Per fare ciò è possibile utilizzare l'azione $go$.
    \begin{center}
        \includegraphics[width=0.7\textwidth]{es_minaccia_1_4.png}
    \end{center}
    A questo punto c'è un problema. L'azione $go$ tra i suoi effetti ha $not at(home)$, andando quindi ad invalidare le precondizioni dell'altra azione $go$. Questo è un \textit{crossed threat}, dato che qualsiasi ordine di esecuzione risulta nell'annullamento di una condizione necessaria. Non è quindi possibile usare né promotion né demotion. La soluzione è effettuare un passo di backtracking in modo da non usare $at(home)$ come precondizione nel secondo $go$ ed usare invece $at(hws)$. Si genera ora una nuova minaccia, in quanto il secondo $go$ annulla la condizione $at(hws)$ necessaria a comprare il frullatore. La soluzione consiste nell'usare la promotion per inserire una condizione d'ordine tra le azioni.

    \begin{center}
        \includegraphics[width=0.7\textwidth]{es_minaccia_1_5.png}
    \end{center}
    Infine è necessario tornare a casa dopo aver effettuato tutti gli acquisti. Si può quindi aggiungere un'ultima azione $go$ per tornare a casa dal supermercato. Questa azione però minaccia tutte e tre le azioni $buy$, quindi occorre nuovamente inserire delle relazioni d'ordine in modo da risolverla.

    \begin{center}
        \includegraphics[width=0.7\textwidth]{es_minaccia_1_6.png}
    \end{center}
\end{esempio}

\begin{algoritmo}{Partial Order Planning (POP) II}{}
    \begin{algorithm}[H]
        \Fn{POP(initialGoal, operators) -> plan}{
            plan := InitialPlan(start, stop, initialGoal)\;
            \While{not Solution(plan)}{
                $SN$, $C$ := SelectSubgoal(plan)\;
                ChooseOperator(plan, operators, $SN$, $C$)
                ResolveThreats(plan)\;
            }\;
            \Return{plan}\;
        }
    \end{algorithm}
    \begin{algorithm}[H]
        \Fn{SelectSubgoal(plan) -> ($SN$, $C$)}{
            scegli $SN$ da Steps(plan) con un requisito non soddisfatto $C$\;
            \Return{$SN$, $C$}\;
        }
    \end{algorithm}
    \begin{algorithm}[H]
        \Fn{ChooseOperator(plan, ops, $SN$, $C$)}{
            scegli $S$ con effetto $C$ da ops o Steps(plan)\;
            \eIf{S non esiste}{
                fail\;
            }{
                aggiungi il link causale $<S,SN,C>$\;
                aggiungi l'ordinamento $S<SN$\;
                \If{S è una nuova azione}{
                    aggiungi $S$ a Steps(plan)\;
                    aggiungi l'ordinamento $start < S < end$\;
                }
            }
        }
    \end{algorithm}
    \begin{algorithm}[H]
        \Fn{SolveThreats(plan)}{
            \ForEach{azione $S$ che minaccia il link tra $S_i$ e $S_j$}{
                scelgi tra promotion e demotion\;
                \If{not Consistent(plan)}{
                    fail\;
                }
            }
        }
    \end{algorithm}
\end{algoritmo}

\section{Modal Truth Criterion (MTC)}
I due metodi, promotion e demotion da soli non bastano a garantire la completezza del pianificatore ovvero a garantire la soluzione di qualunque problema risolubile di pianificazione non lineare. Il Modal Truth Criterion rappresenta un procedimento di costruzione del piano che garantisce la completezza del pianificatore. Un algoritmo POP alterna passi di soddisfacimento di precondizioni con passi di risoluzione di minacce. MTC fornisce cinque metodi di correzione del piano: una per il soddisfacimento delle precondizioni e quattro per la risoluzione delle minacce:
\begin{enumerate}
    \item Establishment: cioè il soddisfacimento di una precondizione attraverso l'inserimento di una nuova azione, oppure attraverso l'inserimento di un vincolo di ordinamento con un'azione già nel piano, o semplicemente mediante un assegnamento di variabili
    \item Promotion
    \item Demotion
    \item White knight: se $S3$, per qualche motivo, non può essere messa nè dopo $S2$ nè prima di $S1$, allora dovrà per forza stare in mezzo. In tal caso però la precondizione $c$ di $S2$ non sarà più soddisfatta, perciò è necessario inserire una nuova azione $S4$ che ha tra i suoi effetti $c$
    \item Separation: inserire vincoli che impongono che due variabili non vengano unicamente tra loro in determinate situazioni che possono creare delle minacce. Questo è possibile solo se queste variabili non sono ancora state istanziate
\end{enumerate}

\begin{definizione}{Anomalia di Sussman}{}
    L'anomalia di Sussman è un problema posto da Sussman per dimostrare i limiti di alcuni pianificatori nel momento in cui si abbiano dei goal che interagiscono tra di loro. Il problema consiste nel spostare i blocchi uno alla volta dalla configurazione di sinistra alla configurazione di destra.\\
    \begin{algorithm}[H]
        \Goal{on(a, b), on(b, c)}\;
        \Init{clear(b), clear(c), on(c, a), ontable(a), ontable(b), handempty}\;
    \end{algorithm}
    \begin{center}
        \includegraphics[width=0.5\textwidth]{sussman.png}
    \end{center}
    Le uniche mosse disponibili sono le seguenti:\\
    \begin{algorithm}[H]
        \Fn{pickup(X)}{
            \Precond{ontable(X), clear(X), handempty}\;
            \Postcond{holding(X), not ontable(X), not clear(X), not handempty}\;
        }
    \end{algorithm}
    \begin{algorithm}[H]
        \Fn{putdown(X)}{
            \Precond{holding(X)}\;
            \Postcond{ontable(X), clear(X), handempty}\;
        }
    \end{algorithm}
    \begin{algorithm}[H]
        \Fn{stack(X, Y)}{
            \Precond{holding(X), clear(Y)}\;
            \Postcond{not holding(X), not clear(Y), handempty, on(X, Y), clear(X)}\;
        }
    \end{algorithm}
    \begin{algorithm}[H]
        \Fn{unstack(X, Y)}{
            \Precond{handempty, on(X, Y), clear(X)}\;
            \Postcond{holding(X), clear(Y), not handempty, not on(X, Y), not clear(X)}\;
        }
    \end{algorithm}
    Usando queste funzioni nel momento in cui si va ad effettuare la pianificazione un problema in cui si incorre è:
    \begin{itemize}
        \item Se si affronta prima $on(a, b)$ occorre spostare $c$ per liberare $a$ e metterlo sopra $b$. A questo punto però $b$ è bloccato e non può essere messo sopra $c$
        \item Se viene affrontato prima $on(b,c)$ si rischia che $b$ venga messo immediatamente sopra $c$ rendendo impossibile poi prelevare $a$ senza disfare il lavoro già svolto
    \end{itemize}

    Una volta risolto il problema si ottiene la seguente linearizzazione:
    \begin{enumerate}
        \item $unstack(c,a)$
        \item $putdown(c)$
        \item $pickup(b)$
        \item $stack(b,c)$
        \item $pickup(a)$
        \item $stack(a,b)$
    \end{enumerate}

    Questo esercizio può essere risolto tramite anche con pianificatori lineari, tuttavia la risoluzione richiede molto più tempo di quanto ne occorra con l'uso di pianificatori non lineari, i quali sono in grado di gestire eventuali interazione tra goal per mezzo di link causali e del MTC
\end{definizione}

\section{Pianificazione gerarchica}
Un approccio che riesce a migliorare l'inefficienza intrinseca nei pianificatori non lineari è la pianificazione gerarchica. I pianificatori gerarchici sono algoritmi di ricerca che gestiscono la redazione di piani complessi a diversi livelli di astrazione, considerando i dettagli più semplici solo dopo aver trovato una soluzione per i più difficili. Anche in questo caso gli operatori sono definiti in termini di precondizioni ed effetti.

Gli approcci usati per definire diversi livelli di astrazione sono:
\begin{itemize}
    \item L'uso di valori di criticità assegnati alle precondizioni. Tra tutti i vari goal ve ne sono alcuni più semplici da realizzare ed alcuni più difficili (un primo metro potrebbe essere il numero di operazioni necessarie a realizzarlo)\footnote{Un $handempty$ richiede sempre una sola operazione quindi risulta più semplice ad esempio di $on(a,b)$}. Questi pianificatori si occupano di risolvere prima i goal più difficili, creando quindi uno schema iniziale da cui partire, e si occupano in un secondo tempo dei goal più semplici
    \item L'uso di operatori atomici e di macro-operatori. I secondi si occupano di risolvere problemi ad un alto livello di astrazione, mentre i primi si occupano di gestire i dettagli finali. Questo tipo di pianificazione risulta molto intuitivo per un essere umano in quanto è il tipo di ragionamento effettuato ad esempio per pianificare un viaggio tra due città
\end{itemize}

In generale è possibile costruire un pianificatore gerarchico sulla base di uno o più pianificatori lineari o pianificatori non lineari. Dato un goal il pianificatore gerarchico effettua una ricerca di meta-livello per generare un piano anch'esso detto di meta livello che porta da uno stato \textit{molto vicino allo stato iniziale} ad uno stato \textit{molto vicino al goal}. Questo piano viene poi rifinito con una ricerca di più basso livello che tiene conto dei dettagli fin qui tralasciati. Quindi un algoritmo gerarchico deve essere in grado di:
\begin{itemize}
    \item Pianificare a livello alto
    \item Espandere piani astratti in piani concreti pianificando parti astratte in termini di azioni più specifiche (pianificazione di livello base) ed espandendo piani già precostruiti
\end{itemize}

La decomposizione dei macro-livelli può essere fatta a posteriori da un esperto del settore, o può essere usata la pianificazione stessa.