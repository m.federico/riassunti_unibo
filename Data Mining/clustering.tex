Consideriamo ora alcune tecniche di apprendimento non supervisionato. Con il clustering si parte dai dati grezzi, senza sapere cosa questi rappresentino. Nel seguito si considereranno solamente cluster disgiunti perché si è interessati a considerare il caso in cui l'unione dei cluster non copre l'intero database, in modo da mantenere il rumore al di fuori dei cluster stessi, dove con rumore si intendono dati non desiderati che hanno errori intrinsechi o letture sbagliate.

Il clustering può essere riassunto nel suo complesso come segue. Dati \(N\) oggetti, ognuno descritto da \(D\) valori (dove \(D\) è il numero delle dimensioni dello spazio che contiene i dati), lo scopo è quello di suddividere lo spazio in \(K\) regioni che raggruppino gli oggetti secondo un qualche criterio di somiglianza possibilmente riuscendo ad individuare anche i dati che causano rumore. Come risultato si ottiene uno \textit{schema di clustering},ovvero una funzione che mappa ogni oggetto nella sequenza \([1, \ldots , k]\) (oppure nell'insieme degli oggetti rumore). Nell'effettuare questa operazione si desidera che gli oggetti facenti parte del medesimo cluster siano il più simile (intuitivamente con simile si intende vicini in senso euclideo) possibile ed allo stesso tempo che gli oggetti contenuti in cluster differenti siano il più diversi possibile (vedi paragrafo~\ref{sec:somiglianza_diversita}).

\medskip
Esistono diverse tipologie di algoritmi di clustering:
\begin{itemize}
    \item A partizioni: prevedono semplicemente di assegnare un'etichetta ad un oggetto, computare qualcosa e cambiare, se necessario, gli assegnamenti per migliorarli. Esempi sono K-Means, Expectation Maximization, Clarens
    \item Gerarchici: prevedono di effettuare una computazione ad un certo livello e poi di specializzarla al livello inferiore
    \item Basati su collegamenti
    \item Basati sulla densità. Ad esempio DBSCAN
    \item Basa sulla statistica
\end{itemize}

\section{Algoritmo K-Means}
Per spiegare il funzionamento dell'algoritmo K-Means prendiamo in considerazione un semplice esempio.

\begin{esempio}{}{}
    \begin{center}
        \includegraphics[width=0.5\textwidth]{esempio_k_means_1.png}
    \end{center}
    Si può notare come vi siano cinque nuvole di punti (\textit{clouds}). Lo scopo è quello di rappresentare il più fedelmente i dati senza però doverli elencare tutti. Si immagini ad esempio di doverli trasmettere su una linea avente poca banda, non è pensabile inviare tutti i dati, ma si possono inviare solamente \(n\) bit, che dunque devono essere rappresentativi dell'intero dataset mantenendo l'errore al minimo. Dunque è necessario un meccanismo di codifica/decodifica in grado da un lato di mappare ogni punto del dataset in un punto che lo rappresenti e dall'altra parte di ottenere i punti originali da quelli ``trasmessi''.

    Una prima soluzione consiste nel dividere il piano in quattro quadranti ognuno individuato da una coppia di bit.
    \begin{center}
        \includegraphics[width=0.5\textwidth]{esempio_k_means_2.png}
    \end{center}
    In questo modo codifichiamo ogni punto con il quadrante di appartenenza, e decodifichiamo ogni quadrante con il suo centro. Come si può vedere questa opzione introduce un errore non trascurabile, soprattutto per il quadrante \(00\).
    Possiamo migliorare questa idea iniziale ipotizzando di decodificare ogni quadrante con il centroide (\textit{centroid}, centro di massa) dei punti che contiene.
    \begin{center}
        \includegraphics[width=0.5\textwidth]{esempio_k_means_3.png}
    \end{center}
\end{esempio}

K-Means parte dell'ultima intuizione iterandola fino a trovare i centri dei cluster.

\begin{algoritmo}{K-Means}{}
    Dato un numero \(K\) di cluster si procede come segue:
    \begin{enumerate}
        \item Associare ogni punto del piano al centro più vicino tra i \(K\) possibili
        \item Per ogni raggruppamento di punti trovare il centroide
        \item Muovere il centro di ogni partizione nel corrispondente centroide
        \item Se almeno un centro si è spostato ripetere dal punto 1, altrimenti terminare l'algoritmo
    \end{enumerate}

    Può accadere che ad un certo punto un cluster risulti vuoto (nessun punto appartiene ad un certo centroide). Nel caso in cui ciò accada è possibile usare spostare il centro vuoto in una nuova posizione. O un punto casuale lontano da quello attuale, oppure un punto scelto tra quelli appartenenti al cluster avente distorsione maggiore (vedi definizione~\ref{def:def:distorsione})
\end{algoritmo}

\begin{esempio}{}{}
    Riprendendo l'esempio precedente possiamo applicare l'algoritmo K-Means in modo da trovare i centroidi di ognuna delle nuvole di punti.
    \begin{center}
        \includegraphics[width=0.5\textwidth]{esempio_k_means_4.png}
    \end{center}
\end{esempio}

\subsection{Minimizzazione della distorsione}
\begin{definizione}{Distorsione}{def:distorsione}
    Data una funzione di codifica \(E\) ed una funzione di decodifica \(D\)
    \[E:\mathbb{R}^D \rightarrow [1..K]\quad D:[1..K] \rightarrow \mathbb{R}^D\]

    possiamo definire la distorsione come
    \[Distortion =\sum_{i=1}^{N}{(e_i- D(E(e_i)))^2}\]

    È possibile inoltre usare una scorciatoia per chiamare la funzione \(D\), ovvero \(D(k)=c_k\). Perciò possiamo ridefinire la distorsione come:
    \[Distortion =\sum_{i=1}^{N}{(e_i- c_{E(e_i)})^2} = \sum_{j=1}^{K}{\sum_{i\in OwnedBy(c_j)}{(e_i-c_j)^2}}\]

    Il nome ufficiale della distorsione è \textit{Sum of Squared Errors} ovvero:
    \begin{align*}
        SSE & =\sum_{j=1}^{K}{\sum_{i\in OwnedBy(c_j)}{(e_i-c_j)^2}} \\
            & =\sum_{j=1}^{K}{SSE_j}
    \end{align*}
    Sulla base di questa ultima formula è possibile effettuare alcune considerazioni:
    \begin{itemize}
        \item Un cluster avente \(SSE\) elevato è di bassa qualità
        \item \(SSE_j = 0\) se e solo se ogni punto coincide con il proprio centroide
        \item \(SSE\) è una funzione monotona il cui valore decresce all'aumentare di \(K\)
    \end{itemize}

\end{definizione}

La distorsione rappresenta la somma dei quadrati tra le differenze tra i punti reali ed i punti trasmessi.  Per fare in modo che la distorsione sia minima occorre che:

\begin{enumerate}
    \item \(e_i\) sia codificato con il centro più vicino. Questo accade perché altrimenti la distorsione potrebbe essere semplicemente ridotta sostituendo \(E(e_i)\) con il centro più vicino
          \[c_{E(e_i)} = \argmin_{c_j \in \{c_1..c_k\}}(e_i-c_j)^2\]

    \item La derivata parziale della distorsione rispetto alla posizione di ogni centro deve essere zero perché in questo caso la funzione ha un massimo o un minimo in tale punto
          \begin{align*}
              \frac{\partial Distortion}{\partial c_j} & =\frac{\partial}{\partial c_j}{\sum_{i\in OwnedBy(c_j)}{(e_i-c_j)^2}} \\
                                                       & =-2\sum_{i\in OwnedBy(c_j)}{(e_i-c_j)}=0
          \end{align*}
          Affinché ciò sia vero deve accadere che
          \[c_j = \frac{1}{|OwnedBy(c_j)|}\sum_{i\in OwnedBy(c_j)}{e_i}\]
          ovvero che ogni centro sia il centroide della propria nuvola di punti
\end{enumerate}

Tenendo a mente queste considerazioni ed il fatto che stiamo cercando iterativamente il minimo di una funzione convessa si può dimostrare che l'algoritmo termina. Per quanto sia alto il numero di modi in cui è possibile partizionare un gruppo di \(N\) oggetti in \(K\) gruppi è finito. Gli stati dell'algoritmo sono dati dai centroidi delle partizioni, pertanto anche il numero di stati possibili è finito. Ogni cambiamento di stato produce una riduzione della distorsione pertanto ogni cambiamento di stato porta ad uno stato non ancora visitato. Questo accade fino a che non vi sono più stati visitabili, ovvero nel momento in cui è stato raggiunto un minimo della funzione che descrive la distorsione.

Va notato che l'algoritmo non permette di comprendere se il minimo raggiunto sia locale o globale (vedi Figura~\ref{fig:kmeans_minimi_locali}).
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.5\textwidth]{kmeans_minimi_locali.png}
    \caption{Esempio del raggiungimento di un minimo locale}\label{fig:kmeans_minimi_locali}
\end{figure}
Al fine di trovare un minimo globale si possono operare alcuni accorgimenti. Ad esempio è possibile scegliere casualmente i centri iniziali lontani il più possibile gli uni dagli altri ed eseguire l'algoritmo. Una seconda possibile consiste nell'eseguire l'algoritmo più volte scegliendo ogni volta i centri iniziali in maniera diversa e scegliendo alla fine la configurazione dei centroidi avente distorsione minore, così da ridurre la possibilità di incappare in un minimo locale.

\medskip
\paragraph{Numero di cluster}
Infine è importante scegliere il giusto numero iniziale di cluster. Una soluzione è quella di provare un certo numero di valori differenti e, mediante una valutazione quantitativa, scegliere il valore corretto (o comunque il migliore) di cluster. In generale il miglior valore rappresenta il miglior compromesso tra la minimizzazione delle distanze tra i punti interni al medesimo cluster e la massimizzazione della distanza tra i punti di cluster diversi. Tuttavia per quanto notato nella definizione~\ref{def:def:distorsione} non è possibile utilizzare la minimizzazione di \(SSE\) in quanto usando \(K=N\) si avrebbe una distorsione minima ma anche una classificazione pessima.

La soluzione più ovvia al problema è l'utilizzo della distanza euclidea, la quale rappresenta generalmente una buona soluzione negli spazi vettoriali. Tuttavia va tenuto a mente che esistono varie alternative più specifiche per alcuni tipi di dato e dataset (vedi capitolo~\ref{sec:somiglianza_diversita}).

\subsection{Considerazioni generali}
\paragraph{Otulier}
Gli outlier punti aventi un'elevata distanza dai relativi centroidi e che quindi portano un alto contributo al valore di della distorsione del relativo cluster. Per questo motivo hanno una cattiva influenza sul risultato finale. A tal proposito a volte può essere una buona idea rimuovere temporaneamente gli outlier prima dell'esecuzione dell'algoritmo, in modo da ottenere il risultato ottimale, e infine riaggiungerli allo schema.

\paragraph{Complessità}
La complessità dell'algoritmo è data da \[O(TKND)\] dove
\begin{itemize}
    \item \(T\) è il numero di iterazioni
    \item \(K\) è il numero di cluster
    \item \(N\) è il numero di elementi nel dataset
    \item \(D\) è il numero di dimensioni
\end{itemize}

\paragraph{Casi d'uso}
È un algoritmo piuttosto efficiente, la cui complessità è quasi linear nel caso in cui \(T,K,D << N\). Tuttavia l'uso è limitato ai casi in cui possono essere calcolati i centroidi delle varie nuvole di punti, ovvero i casi in cui i dati sono solamente numerici. Inoltre è piuttosto sensibile al rumore ed alla presenza degli outlier. Infine non è in grado di processare cluster non convessi.

Nonostante abbia questi difetti è un buon algoritmo per l'esplorazione iniziale dei dati e per la discretizzazione (in spazi unidimensionali) di valori in gruppi aventi dimensione non uniforme (vedi figura~\ref{fig:discretizzazione_1}).

\section{Valutazione dello schema di clustering}
In questo paragrafo verranno descritti degli schemi per la valutazione del clustering su un insieme di dati generico. Va notato che questa operazione è unicamente collegata al risultato finale e non alla tecnica specifica per effettuare il clustering.

\medskip
Il clustering non è una tecnica supervisionata, per cui la valutazione dei risultati è cruciale. Tuttavia nel caso in cui vi siano dei dati supervisionati (\textit{supervised}) essi possono essere usati per affinare la valutazione del clustering. Ad esempio è possibile valutare quanto il clustering risulta vicini alla classificazione reale, ed usare questa informazione per aggiornare la valutazione complessiva.

In generale in spazi bidimensionali una visualizzazione grafica può essere utile ad esaminare i risultati tuttavia al di sopra delle tre dimensioni diventa difficile se non impossibile usare visualizzazioni grafiche, pertanto in questi casi si può ricorrere a delle proiezioni bidimensionali, sebbene sia meglio utilizzare metodo più formali.

Esistono diverse criticità da risolvere nella valutazione di uno schema di clustering:
\begin{itemize}
    \item Distinguere pattern da regolarità apparenti e casuali
    \item Trovare il miglior numero di cluster (\(K\))
    \item Valutazione non supervisionata
    \item Valutazione supervisionata
    \item Comparazione tra schemi di clustering
\end{itemize}

\subsection{Coesione e Separazione}
Coesione e separazione sono due criteri per la valutazione di uno schema di clustering:
\begin{definizione}{Coesione}{}
    Valore di prossimità degli oggetti appartenenti allo stesso cluster. Più è alta migliore risulta la valutazione del cluster.

    Viene calcolata come la somma delle distanze tra gli elementi del cluster e il centro geometrico
    \[Coh(k_i)=\sum_{x\in k_i} Prox(x,c_i)\]

    Dove \(c_i\) può essere o il centroide o il medoide del cluster
\end{definizione}

\begin{definizione}{Centroide}{}
    Punto dello spazio le cui coordinate sono la media di quelle di un insieme di punti del dataset. Concetto analogo al centro di massa
\end{definizione}

\begin{definizione}{Medoide}{}
    Elemento del dataset la cui diversità media rispetto agli altri punti del cluster è minima. Non è necessariamente unico per un cluster, tipicamente viene utilizzato in contesti in cui non è possibile calcolare la media
\end{definizione}

\begin{definizione}{Separazione}{}
    Rappresenta il grado di separazione tra due cluster ed è data (in maniera esclusiva):
    \begin{itemize}
        \item dalla prossimità tra i due prototipi (centroide o medoide)
        \item dalla prossimità tra i due oggetti più vicini dei due cluster
        \item dalla prossimità tra i due oggetti più lontani dei due cluster
    \end{itemize}
\end{definizione}

\begin{definizione}{Separazione globale}{}
    Detto \(c\) il centroide globale del dataset si definisce il grado di separazione globale di uno schema di clustering come:
    \[SSB = \sum_{i=1}^{K}{N_i Dist(c_i,c)^2}\]
\end{definizione}

\begin{definizione}{Total Sum of Squares}{}
    Questo valore rappresenta il collegamento tra coesione e separazione ed è è una proprietà del dataset indipendente dallo schema di clustering. Essa è esprimibile come la somma dei quadrati delle distanze di ogni punto del dataset dal centroide globale

    \begin{align*}
        TSS & =\sum_{i=1}^{K}{\sum_{x\in k_i}{(x-c)^2}}=\sum_{i=1}^{K}{\sum_{x\in k_i}{{((x -c_i) -(c-c_i))}^2}}                                            \\
            & =\sum_{i=1}^{K}{\sum_{x\in k_i}{(x-c_i)^2}} - 2\sum_{i=1}^{K}{\sum_{x\in k_i}{(x-c_i)(c-c_i)}} + \sum_{i=1}^{K}{\sum_{x\in k_i}{{(c-c_i)}^2}} \\
            & =\sum_{i=1}^{K}{\sum_{x\in k_i}{(x-c_i)^2}} + \sum_{i=1}^{K}{\sum_{x\in k_i}{{(c-c_i)}^2}}                                                    \\
            & =\sum_{i=1}^{K}{\sum_{x\in k_i}{(x-c_i)^2}} + \sum_{i=1}^{K}{|k_i|{(c-c_i)}^2} = SSE + SSB
    \end{align*}

    Si ricordi che \(\sum_{x\in k_i}{(x-c_i)} = 0\) per definizione di centroide.
\end{definizione}

Ogni cluster può avere la propria valutazione, inoltre si può considerare un ulteriore divisione per quello peggiore in termini di valutazione. Si possono anche riunire cluster precedentemente divisi nel caso in cui la loro separazione sia debole.

\subsection{Silhouette}
Un altro indice di valutazione dello schema di clustering è la \textbf{Silhouette}

\begin{definizione}{Silhouette}{}
    Dati i valori \(a_i\) e \(b_i\)
    \begin{itemize}
        \item Si calcola per l'i-esimo oggetto la distanza media rispetto gli altri oggetti appartenenti allo stesso cluster. Chiamiamo questo valore \(a_i\)
        \item Si calcola per l'i-esimo oggetto e per ogni cluster diverso da quello a cui appartiene l'oggetto la distanza media rispetto a tutti gli oggetti di quel cluster. Il minimo tra questi valori è \(b_i\)
    \end{itemize} si calcola la silhouette dell'i-esimo oggetto come
    \[s_i =\frac{b_i-a_i}{\max(a_i,b_i)} \in [-1,1]\]
\end{definizione}

Intuitivamente se il valore di Silhouette è positivo l'oggetto considerato appartiene perfettamente al cluster (con uno si indica un'appartenenza perfetta), se invece è negativo significa che oggetti di altri cluster sono tendenzialmente gli sono più vicini rispetto ad oggetti del medesimo cluster.

Questo indice può essere calcolato per ogni oggetto oppure per l'intero dataset (o per un singolo cluster), in questo caso esso è la media degli indici di ogni oggetto.

\begin{esempio}{calcolo silhouette}{calcolo_silhouette}
    \begin{center}
        \includegraphics[width=0.7\textwidth]{esempio_silhouette_1.png}
    \end{center}
    In figura sono mostrati dieci cluster in uno spazio bidimensionale. I punti più scuri hanno un valore di Silhouette più basso. Si può dire che la maggior parte di questi punti scuri si trovi sul bordo del cluster o comunque vicini a punti di altri cluster. Al contrario, al centro dei cluster il colore è più sbiadito (indica un valore di Silhouette più alto), questo perché l'influenza dei punti di cluster diversi è minore e quindi la certezza che il punto appartenga effettivamente al cluster è maggiore
\end{esempio}

Per quando riguarda la complessità, il calcolo dell'indice di Silhouette può essere approssimato a \(O(N^2)\) mentre il calcolo del valore \(SSE\) può essere approssimato a \(O(N)\). Ne deriva che il calcolo del valore di Silhouette è computazionalmente più oneroso.

\subsection{Scelta del numero di cluster}
Alcuni algoritmi, come anche K-means, richiedono il numero di cluster come iperparametro. Per questo motivo è necessario fare diversi tentativi per capire quale sia il miglior valore di \(K\). Gli indici \(SSE\) e Silhouette sono influenzati dal numero di cluster, pertanto possono essere usati per calcolare questo valore.

\(SSE\) decresce monotonicamente (vedi figura~\ref{fig:numero_cluster_sse}) all'aumentare del numero di cluster e diventa nullo nel momento in cui si ha \(K=N\), pertanto non può essere utilizzato.
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.5\textwidth]{numero_cluster_sse.png}
    \caption{Variazione dell'indice \(SSE\) al variare di \(K\) in riferimento all'esempio~\ref{es:calcolo_silhouette}}\label{fig:numero_cluster_sse}
\end{figure}

Per quando riguarda il valore di Silhouette il comportamento è diverso (vedi figura~\ref{fig:numero_cluster_silhouette}). Al variare del numero di cluster si osserva un massimo in corrispondenza del miglior valore. Questo perché questo è il valore che consente di massimizzare la coesione dei valori interni ai cluster.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.4\textwidth]{numero_cluster_silhouette.png}
    \caption{Variazione dell'indice silhouette al variare di \(K\) in riferimento all'esempio~\ref{es:calcolo_silhouette}}\label{fig:numero_cluster_silhouette}
\end{figure}


\subsection{Valutazione supervisionata}
Ci sono casi in cui si hanno a disposizione alcuni dati supervisionati, questi dati sono chiamati “gold standard”. Si possono usare questi dati per allenare uno schema di clustering in grado di fare labeling anche sui dati non supervisionati. Anche in questo caso c'è interesse nel valutare lo schema di clustering, si vuole però generalizzare quanto appreso da un piccolo insieme di dati supervisionati all'intero dataset.

In questo caso si vuole usare un classificatore non uno schema di clustering. Per questo motivo si considera uno schema di clustering \(K = \{k_1, \ldots, k_K\}\), in cui si usano i dati gold standard per validare lo schema di clustering. Si possono usare diverse tecniche:
\begin{itemize}
    \item metodi orientati alla classificazione: misurano come le classi siano distribuite tra i cluster utilizzando misure come la \textit{confusion matrix}, \textit{precision}, \textit{recall}, \textit{f-measure}
    \item metodi orientati alla somiglianza: sono simili per certi versi ai confronti effettuati tra dati binari. Data una partizione \(P=\{P_1, \ldots,P_L\}\) (che chiamiamo golden standard) consideriamo lo schema di clustering \(K=\{k_1,\ldots,k_K\}\). Ogni coppia di oggetti può essere classificata come:
          \begin{enumerate}[label=\alph*]
              \item SS se appartengono allo stesso insieme in \(P\) e \(K\)
              \item SD se appartengono allo stesso insieme in \(k_t\) ma non in \(P\)
              \item DS se appartengono allo stesso insieme in \(P\) ma non in \(P\)
              \item DD se appartengono a insiemi diversi sia in \(K\) che in \(P\)
          \end{enumerate}
          Dati questi valori possiamo definire gli indici
          \begin{itemize}
              \item Indice rand \(R = \frac{a+d}{a+b+c+d}\)
              \item Coefficiente di Jaccard \(J = \frac{a}{a+b+c}\)
          \end{itemize}
\end{itemize}

La complessità di calcolo di questi due indici in entrambi i casi può essere approssimata con \(O(N^2)\)

\section{Clustering gerarchico}
Questo tipo di clustering genera strutture innestate di cluster. Si procede quindi in modo diverso rispetto ad altri algoritmi come K-Means che dato un insieme di punti ed un valore di \(K\) divide i punti in \(K\) insiemi offre un risultato migliorabile modificando il valore di \(K\) e rieseguendo il clustering.

In questo caso invece la suddivisione può essere fatta o top-down o bottom-up:
\begin{itemize}
    \item Modalità agglomerativa (bottom-up): all'inizio della procedura ogni punto è un cluster a sé. Ad ogni passi si individua la coppia di punti più vicini e li si inserisce nel medesimo cluster. Questa operazione potenzialmente unisce ad ogni passo un punto ad un cluster esistente oppure unisce du cluster esistenti. Per poter operare questo tipo di clustering è necessaria una misura del grado di separazione tra due cluster
    \item Modalità divisiva (top-down): all'inizio l'intero dataset è un unico cluster. Ad ogni passo il cluster avente il grado di coesione minore viene diviso in due in modo da ottenere un massimo di coesione per entrambi i nuovi sottocluster. Per poter operare questo tipo di clustering è necessaria una misura del grado di coesione dei cluster. Tipicamente questa tecnica è più difficile da applicare rispetto alla precedente, pertanto è meno usata
\end{itemize}

Il risultato dell'operazione di clustering è o un dendrogramma (figura~\ref{fig:schema_clustering_gerarchico} sinistra) o un diagramma dei cluster innestati (figura~\ref{fig:schema_clustering_gerarchico} destra) sia per i metodi agglomerativi che per i metodi divisivi.
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.6\textwidth]{schema_clustering_gerarchico.png}
    \caption{Risultato del clustering gerarchico}\label{fig:schema_clustering_gerarchico}
\end{figure}

\subsection{Separazione tra cluster}
Ci sono almeno tre modi per calcolare la separazione tra cluster usando grafi dei punti (figura~\ref{fig:separazione_cluster}):
\begin{itemize}
    \item Single link: \(Sep(k_i, k_j) = \min_{x\in k_i, y\in k_j} Dist(x,y)\)
    \item Complete link: \(Sep(k_i, k_j) = \max_{x\in k_i, y\in k_j} Dist(x,y)\)
    \item Link medio: \(Sep(k_i, k_j) = \frac{1}{|k_i||k_j|}\sum_{x\in k_i, y\in k_j} Dist(x,y)\)
\end{itemize}

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.9\textwidth]{separazione_cluster.png}
    \caption{Separazione tra cluster basata su grafi}\label{fig:separazione_cluster}
\end{figure}
Ci sono inoltre metodi per calcolare il grado di separazione tra cluster a partire dalla posizione dei centroidi:
\begin{itemize}
    \item Distanza tra i centroidi
    \item Metodo di Ward: dati due insiemi con i rispettivi \(SSE\), la separazione tra di essi è data dalla differenza dell'\(SSE\) totale risultante in caso di fusione dei sue cluster stessi e la somma degli \(SSE\) iniziali. In questo modo un grado di separazione minore implica un minore incremento di \(SSE\) a seguito della fusione
\end{itemize}

\subsection{Clustering single linkage}
\begin{algoritmo}{Clustering gerarchico single linkage}{}
    Rappresenta un esempio di modello di clustering agglomerativo.

    \begin{lstlisting}
    inizializza i cluster, uno per ogni oggetto
    calcola la matrice della distanza tra tutti i cluster
    while (numero di cluster > 1)
        trova i due cluster meno distanti
        uniscili in un unico cluster
        aggiorna la matrice rimuovendo i due cluster uniti ed aggiornando le distanze
    \end{lstlisting}

    La matrice delle distanze è una matrice quadrata simmetrica con \(N\) righe e diagonale nulla. Nel momento in cui avviene la fusione tra due cluster la nuova distanza computata sarà:
    \[Dist(k_k, k_{(r+s)}) = \min{(Dist(k_k, k_{(r)}), Dist(k_k, k_{(s)}))} \forall k \in [1,K] \]

    La complessità temporale è complessivamente \(O(N^3)\) che può essere ridotto a \(O(N^2\log(N))\) nel caso in cui si usino strutture indicizzate
\end{algoritmo}

\subsection{Ottenere uno schema di clustering}
Utilizzando un dendrogramma per rappresentare il risultato degli algoritmi consente infine di ottenere lo schema di clustering desiderato. Per fare ciò è sufficiente tagliare verticalmente il diagramma nel punto desiderato (vedi figura~\ref{fig:clustering_dendrogramma}), in quanto il numero di cluster dipende dal caso specifico (è possibile aiutarsi con degli indici per scoprire quale sia il valore corretto, come accade con K-Means).
\begin{figure*}[h!b]
    \centering
    \includegraphics[width=0.4\textwidth]{clustering_dendrogramma.png}
    \caption{Sezionamento di un dendrogramma al fine di ottenere uno schema di clustering}\label{fig:clustering_dendrogramma}
\end{figure*}
L'asse orizzontale del dendrogramma rappresenta la diversità totale interna ai cluster, la quale diminuisce all'aumentare del numero di cluster ed al diminuire contemporaneo della loro dimensione. Il diametro di un cluster invece è dato dalla distanza tra gli oggetti più distanti interni al cluster. La metodologia \textit{single linkage} tende a generare cluster con diametri maggiori anche ai livelli più bassi, mentre la metodologia \textit{complete linkage} tende a generare cluster più compatti.

\medskip
Complessivamente però queste soluzioni sono poco scalabili a causa dell'alta complessità computazionale, inoltre non essendoci una funzione obiettivo globale ogni decisione viene presa in termini locali e non può essere annullata. Nonostante ciò la struttura a dendrogramma è particolarmente utili a fini di visualizzazione e di interpretazione dei risultati e porta tipicamente a buoni risultati.

\section{Clustering basato su densità}
Questo tipo di clustering parte da una semplice osservazione: i cluster non sono altro che regioni ad alta densità circondate da regioni a bassa densità. Questa osservazione non tiene conto di concavità e convessità delle nuvole di punti, né della forma specifica e quindi consente in teoria di individuare cluster di ogni forma e dimensione.

Per calcolare la densità di una regione di spazio si possono adottare due soluzioni di base. La prima è basata sull'utilizzo di una griglia che divide l'iperspazio in un certo numero di celle regolari che contengono oggetti.  Questa soluzione è piuttosto semplice da realizzare e misura la densità come numero di oggetti all'interno della singola cella, tuttavia tutto dipende dalle dimensioni della cella stessa. Infatti se la cella è troppo piccola siamo in grado di osservare moltissimi dettagli e piccole variazioni in densità, tuttavia il calcolo diventa estremamente esoso nel suo complesso; se invece le celle sono troppo grandi si perde la cognizione dei dettagli e si tende a confondere il rumore con i dati facenti parte dei cluster. La seconda soluzione consiste nell'utilizzare delle ipersfere a partire da ognuno dei punti. Ogni oggetto viene associato agli altri oggetti che rientrano nella sua ipersfera. In questo caso il raggio della sfera è un iperparametro dell'algoritmo e consente di regolare il livello di dettaglio desiderato.

\subsection{DBSCAN}
DBSCAN sta per \textit{Density Based Spatial Clustering of Applications with Noise}. Intuitivamente consente di costruire cluster aventi un centro (\textit{core}) ed un bordo (\textit{border}) sulla base del concetto di vicinato (\textit{neighborhood}). Intuitivamente il punto \(p\) è un punto ``border'', mentre \(q\) è un punto ``core'' (vedi figura~\ref{fig:dbscan_1})
\begin{figure}[h!bt]
    \centering
    \includegraphics[width=0.4\textwidth]{dbscan_1.png}
    \caption{}\label{fig:dbscan_1}
\end{figure}
A questo punto per ogni punto dello spazio viene definito il concetto di ``neighborhood'' di un punto.
\begin{definizione}{Neighborhood}{}
    Dato un punto \(p\) definiamo neighborhood del punto \(p\)
    come l'insieme dei punti all'interno dell'ipersfera avente raggio \(\epsilon\) centrata in \(p\)
    \begin{center}
        \includegraphics[width=0.4\textwidth]{dbscan_2.png}
    \end{center}
    Come si può vedere dalla figura la relazione di vicinato è simmetrica
\end{definizione}

\begin{definizione}{Punto core, punto border}{}
    Data una soglia \(minPoints\), un punto \(q\) è definito core se il suo vicinato comprende almeno \(minPoints\) oggetti, mentre un punto \(p\) è border se il suo vicinato comprende meno di \(minPoints\) oggetti
    \begin{center}
        \includegraphics[width=0.4\textwidth]{dbscan_2.png}
    \end{center}
\end{definizione}

\begin{definizione}{Direct Density Reachability}{}
    Un punto \(p\) è direttamente raggiungibile per densità da un punto \(q\) se e solo se:
    \begin{itemize}
        \item \(q\) è un punto core
        \item \(q\) è all'interno del vicinato di \(p\)
    \end{itemize}
    \begin{center}
        \includegraphics[width=0.4\textwidth]{dbscan_3.png}
    \end{center}
    La raggiungibilità diretta non è simmetrica
\end{definizione}

\begin{definizione}{Density Reachability}{}
    Un punto \(p\) è raggiungibile per densità da un punto \(q\) se e solo se:
    \begin{itemize}
        \item \(q\) è un punto core
        \item esiste una sequenza di punti \((q, q_1,\ldots, q_{nq})\) tale per cui \(q_1\) è raggiungibile direttamente da \(q\), ogni punto \(q_{n+1}\) è direttamente raggiungibile da \(q_n\) e \(p\) è direttamente raggiungibile da \(q_{nq}\)
    \end{itemize}
    \begin{center}
        \includegraphics[width=0.4\textwidth]{dbscan_4.png}
    \end{center}
    La raggiungibilità non è simmetrica
\end{definizione}

\begin{definizione}{Density Connection}{}
    Un punto \(p\) è connesso per densità ad un punto \(q\) se e solo se esiste un punto \(s\) tale per cui sia \(p\) che \(q\) sono raggiungibili per densità a partire da \(s\)
    \begin{center}
        \includegraphics[width=0.4\textwidth]{dbscan_5.png}
    \end{center}
    La connessione per densità è simmetrica
\end{definizione}

\begin{definizione}{Clustering con DBSCAN}{}
    Un cluster è l'insieme massimo di punti connessi per densità dati un valore di \(\epsilon\) e \(minPoints\) iniziali. I punti ``border'' che non appartengono ad alcun cluster sono etichettati come rumore
\end{definizione}

Usando questa definizione siamo in grado di distinguere le arre ad alta densità dalle aree a bassa densità.

\subsubsection{Proprietà}
L'algoritmo gode delle seguenti proprietà:
\begin{itemize}
    \item DBSCAN è in grado di trovare cluster di ogni forma. Infatti usando il concetto di punti ``border'' l'algoritmo è in grado di seguire facilmente la forma di qualsiasi cluster. Questo non accade ad esempio con K-Means il quale invece considera le distanze dal centroide della nuvola di punti e quindi è in grado di trovare unicamente cluster convessi
    \item In questo algoritmo è inclusa l'idea di rumore, rendendo l'algoritmo particolarmente robusto anche in caso di rumore ed in presenza di outlier
    \item I parametri \(\epsilon\) e \(minPoints\) sono molto sensibili, anche piccole variazioni producono grandi mutamenti nei risultati del clustering. I due parametri lavorano in sensi opposti. All'aumentare di \(\epsilon\) aumenta il grado di inclusione, quindi tendenzialmente si avrà un numero minore di cluster i quali avranno maggiori dimensioni, al contrario al calare di questo parametro si avrà un numero maggiore di cluster aventi però dimensioni minori. All'aumentare di \(minPoints\) invece diminuisce il grado di aggregazione in quanto questo parametro rappresenta la soglia oltre la quale un punto è considerato ``core'', quindi si avranno cluster più piccoli
    \item Non ha un buon comportamento nel caso in cui vi sino grandi differenze di densità tra i vari cluster. Questa proprietà è in qualche modo collegata alla precedente, in quanto i parametri sono i medesimi per la divisione dell'intero dataset. Nel caso in cui i parametri favoriscano l'aggregazione saranno riconosciuti come cluster anche le nuvole di punti meno dense, tuttavia potrebbero essere riconosciute com un unico cluster nuvole molto dense che in realtà sono nettamente separate. Allo stesso modo se i parametri sono tarati per riconoscere nuvole dense aumenterà il grado di rumore e di conseguenza le nuvole meno dense verranno riconosciute come rumore e non come cluster
    \item La complessità computazionale è pari a \(O(N^2)\), che si riduce a \(O(Nlog(N))\) se sono presenti strutture indicizzate
\end{itemize}

\section{Clustering model based}
Uno dei primi approcci al clustering è quello del clustering model based. Esso è un approccio parametrico che parte dall'ipotesi che i dati siano generati da un insieme di distribuzioni di cui è ignoto però il numero. Tale numero rappresenta il numero di cluster. Si suppone in questo caso che gli attributi siano indipendenti tra loro.

Poiché le varie distribuzioni si trovano all'interno di un spazio n-dimensionale ognuna di esse avrà un insieme di parametri per ognuna delle dimensioni dello spazio. Nel caso in cui i dati possono essere approssimati con un'unica distribuzione ricavarne i parametri è triviale, nel caso in cui vi siano più distribuzioni (anche diverse tra loro) viene utilizzato l'algoritmo \textbf{Expectation Maximization}

Supponiamo ora che la distribuzione seguita dai dati sia gaussiana.

\subsection{Algoritmo EM}
\begin{algoritmo}{Expectation Maximization}{}
    \begin{lstlisting}
        seleziona un insieme iniziale di parametri
        while (i parametri non cambiano)
            per ogni oggetto calcola la probabilita' che appartenga ad ogni distribuzione
            date le probabilita' stimate trova una nuova stima dei parametri che massimizzi la verosimiglianza attesa
    \end{lstlisting}

    Il primo passo all'interno del ciclo è il cosiddetto passo ``Expectation'', il secondo invece è il passo ``Maximization''
\end{algoritmo}

\begin{esempio}{}{}
    In figura è rappresentato un possibile risultato dell'algoritmo EM
    \begin{center}
        \includegraphics[width=0.5\textwidth]{expectation_maximization_1.png}
    \end{center}

    In questo caso occorre calcolare cinque parametri (media e deviazione standard per \(A\), media e deviazione standard per \(B\), probabilità per \(A\)). Diciamo:
    \[P(A|x) = \frac{P(x|A)P(A)}{P(x)} = \frac{f(x;\mu_A, \sigma_A)p_A}{P(x)}\]
    dove \[f(x;\mu_A, \sigma_A) = \frac{1}{\sqrt{2\pi}\sigma}e^{\frac{(x-\mu)^2}{2\sigma^2}}\]

    Per il calcolo della media e della deviazione standard si procede come segue:
    \begin{align*}
        \mu_A    & = \frac{w_1e_1+w_2e_2 + \cdots + w_{N} e_{N}}{w_1+w_2+\cdots+w_N}                           \\
        \sigma_A & = \frac{w_1(e_1-\mu_A)^2+w_2(e_2-\mu_A)^2+\cdots+w_N(e_N-\mu_A)^2}{w_1+w_2+\cdots+w_N}
    \end{align*}
\end{esempio}

\section{Considerazioni finali}
In generale l'efficacia di un algoritmo di clustering diminuisce all'aumentare del numero di dimensioni \(D\) dello spazio in cui si trovano i dati e con l'aumentare del rumore di fondo. Il costo computazionale invece cresce all'aumentare delle dimensioni \(N\) del dataset ed all'aumentare del numero delle dimensioni \(D\) in cui si trovano i dati.