I data warehouse sono il punto di inizio per i Big Data. Sono importanti perché i Big Data non possono esistere senza un data warehouse e viceversa. All'interno di un data warehouse si ha l'unione di diversi tipi di dato perché unendo più sorgenti diversi si hanno consigli sulle azioni da compiere più precisi.

\section{Business intelligence}
In generale la Business Intelligence comincia con i dati grezzi, li aggrega e li pulisce per poter dare supporto alle scelte strategiche aziendali, comunicando le giuste informazioni alle giuste persone nel momento giusto attraverso il giusto canale. Oltre a questo, possono essere usate tecniche di Machine Learning e Data Mining per avere una visione ancora più profonda di quale strategia sia meglio adottare.

\begin{figure}[h!bt]
    \centering
    \includegraphics[width=0.5\textwidth]{bi_piramide.png}
    \caption{Struttura piramidale della business intelligence}\label{fig:bi_piramide}
\end{figure}

Per supportare la business intelligence occorrono vari componenti come hardware ad hoc, infrastrutture di rete, database, data warehouse e software frontend adatto. Il tutto può essere organizzato secondo una chiara struttura.

\begin{figure}[h!bt]
    \centering
    \includegraphics[width=0.7\textwidth]{business_intelligence_2.png}
    \caption{Un data warehouse è uno dei support principali alla business intelligence}\label{fig:business_intelligence_2}
\end{figure}

Un passaggio fondamentale della business intelligence è il \textit{Data Staging} (o ETL). In questo strato i dati sono copiati e messi nel data warehouse in modo da non interferire con i dati originali e riuscire a lavorare in real-time.

Sul data warehouse si possono fare diverse importanti operazioni:
\begin{itemize}
    \item OLAP Analysis
    \item Reporting: stato zero del Data Mining, semplice aggregazione di dati (SELECT, GROUP BY)
    \item Data Mining
    \item Reconciliation: nel mondo reale i dati sono spesso sporchi. Quando vengono posti tutti insieme spesso si hanno condizioni di inconsistenza, è necessario risolvere queste situazioni
\end{itemize}


\section{Data Warehouse}
Il data warehouse è lo strumento più importante per il supporto alle operazioni di business intelligence, in quanto l'incremento delle informazioni che un compagnia deve esaminare per formulare una strategia efficace implica la necessità di soluzioni più complesse di un semplice database relazionale. Questo componente non è altro che un repository di dati estremamente ottimizzato alle operazioni di decision-making ed ha diversi vantaggi:
\begin{itemize}
    \item La possibilità di gestire lo storico dei dati
    \item La possibilità di formulare delle analisi multidimensionali. Le dimensioni non sono altro che le colonne di una tabella relazionale. Fare un'analisi multidimensionale significa vedere come le colonne interagiscono tra loro
    \item È basato su un modello semplificato che può essere facilmente appreso dagli utenti
\end{itemize}
Quindi un data warehouse è una collezione di dati al supporto dei processi di decision-making. Questo componente ha le seguenti caratteristiche:
\begin{itemize}
    \item È subject-oriented, ovvero è focalizzato sugli aspetti di interesse dell'azienda come: clienti, fornitori, prodotti, vendite, \dots
    \item È integrato e consistente: data warehouse integra i dati da diverse sorgenti eterogenee e fornisce una forma uniforme di questi
    \item Fornisce una visione dell'evoluzione dei dati e non è volatile. Viene tenuto traccia dei cambiamenti dei dati avvenuti all'interno del database, producendo un resoconto dei cambiamenti in senso temporale. Una volta inseriti, o dati all'interno di un DWH non sono mai modificati o sovrascritti, in quanto vi è la sola possibilità di lettura
\end{itemize}

\subsection{Modello multidimensionale}
Si ipotizzi di avere delle transazioni commerciali (data, prodotto e negozio). Ciò significa che ogni transazione è l'intersezione dei tre attributi. Sui dati raccolti poi possono essere svolte operazioni di raggruppamento come ad esempio i prodotti di una determinata categoria o il profitto dato da un singolo cliente con le sue transazioni. Questi aspetti possono essere visualizzati in un grafico tridimensionale. La figura che si denota è un cubo formato da tanti piccoli cubetti: ogni cubetto è un raggruppamento di dati transazionali (vedi figura~\ref{fig:cubo_multidimensionale}).

\begin{figure}[h!bt]
    \centering
    \includegraphics[width=0.7\textwidth]{cubo_multidimensionale.png}
    \caption{Ogni cubetto contiene gruppi di quattro prodotti}\label{fig:cubo_multidimensionale}
\end{figure}

\subsection{OLTP vs OLAP}
Possiamo riassumere qui le differenze tra la metodologia OLTP e OLAP:\@
\begin{itemize}
    \item OLTP
          \begin{itemize}
              \item Sistema interattivo per il l'elaborazione dei dati basato su transazioni
              \item Ogni transazione legge e scrive un piccolo numero di record da tabelle caratterizzate da semplici relazioni
              \item È dotato di un core congelato all'interno dei programmi applicativi. Non vi sono necessità di cambiare le query effettuate
          \end{itemize}
    \item OLAP
          \begin{itemize}
              \item Sistema interattivo per l'elaborazione dei dati utile in caso di analisi multidimensionali
              \item Ogni query coinvolge un enorme numero di record necessarie ad elaborare un insieme di valori numerici per ottenere un'indicazione circa le performance del business
              \item Il carico di lavoro cambia col tempo
          \end{itemize}
\end{itemize}

\subsection{Database relazionali vs data warehouse}
Riassumiamo qui le differenze principali tra un database relazionale ed un data warehouse:

\begin{table}[h!bt]
    \begin{tabularx}{\textwidth}{lXX}
        \toprule
        \textbf{Caratteristiche} & \textbf{Database}                      & \textbf{Data Warehouse}                   \\
        \midrule
        Utenti                   & Migliaia                               & Centinaia                                 \\
        Tipo di lavoro           & Transazioni prestabilite               & Analisi specifiche                        \\
        Accesso                  & Centinaia di record (rw)               & Milioni di record (r)                     \\
        Scopo                    & Dipende dall'applicazione              & Supporto alle decisioni                   \\
        Dati                     & Dettagliati, sia numerici che testuali & Dati riassuntivi, principalmente numerici \\
        Qualità                  & In termini di integrabilità            & In termini di coerenza                    \\
        Temporalità              & Dati correnti                          & Dati correnti e visione storica           \\
        Aggiornamenti            & Continui                               & Periodici                                 \\
        Modello                  & Normalizzato                           & Non normalizzato, multidimensionale       \\
        Ottimizzazioni           & Per l'accesso OLTP                     & Per l'accesso OLAP                        \\
        \bottomrule
    \end{tabularx}
\end{table}

\subsection{Data Mart}
Il concetto di Data Mart è simile a quello di view nei database relazionali. In sintesi, è una vista dal data warehouse per uno specifico scopo, tralasciando i dati non di interesse. Esistono tuttavia delle differenze tra Data Mart e una vista del modello relazionale:
\begin{itemize}
    \item Nel modello relazionale la vista viene creata e archiviata. All'atto della query, la query è combinata con la definizione della vista ed eseguita
    \item Nel Data Mart viene copiato ogni sottoinsieme di dati utili per una singola attività. Quando viene progettato il data mart occorre definire anche la frequenza di aggiornamento dei dati
\end{itemize}

\section{OLAP}
L'analisi OLAP consente agli utenti di navigare interattivamente le informazioni contenute all'interno di un data warehouse (o data mart). Tipicamente i dati vengono analizzati a diversi livelli di aggregazione utilizzando una serie di operatori OLAP (ognuno dei quali può lanciare una o più query).

Durante una sessione OLAP l'utente può esplorare il modello multidimensionale e scegliere il prossimo operatore in base ai risultati ottenuti dall'applicazione del precedente. In questo modo, l'utente genera un percorso di navigazione che corrisponde ad un'analisi del processo.

L'idea chiave di OLAP è la flessibilità e la capacità di cambiare vista molto velocemente. Il punto di partenza per ogni operatore è il data cube, che contiene i dati raggruppati inizialmente. Gli operatori OLAP principali sono:
\begin{itemize}
    \item Roll-up: consente di raggruppare ulteriormente i dati contenuti all'interno del cubo (idealmente diminuendo il numero di cubetto che lo compongono, ma aumentandone la dimensione)
    \item Drill-down: è l'operazione duale rispetto al roll-up. Riduce il livello di aggregazione aggiungendo dettagli
    \item Slice-and-dice: riduce il numero di dimensioni del cubo dopo aver impostato una delle dimensioni ad uno specifico valore. L'operazione di dicing riduce il set di dati che sono stati analizzati da una criterio di selezione
    \item Pivot: comporta un cambio di layout, rappresenta una visione differente dell'insieme dei dati
    \item Drill-across: permette di creare collegamenti tra i concetti tra cubi correlati, per confrontarli
    \item Drill-through: trasforma la vista aggregata multidimensionale in una tabella operazionale
\end{itemize}

\section{Extraction, Transformation and Loading (ETL)}
La lunga pipeline nella figura sotto, che permette di passare dai dati ai Data Cube, inizia con il processo di ETL.\@ Il processo di ETL estrae, integra e pulisce i dati dai dati operazionali per alimentare lo strato data warehouse.
\begin{figure}[h!bt]
    \centering
    \includegraphics[width=0.7\textwidth]{ETL_1.png}
    \caption{Processo ETL nel suo completo}\label{fig:ETL}
\end{figure}
Si distinguono le seguenti fasi:
\begin{itemize}
    \item Estrazione: è il processo di estrazione dei dati. Si possono avere dati strutturati (ad es.\ dei csv o dei database relazionali) e dati non strutturati (ad.es provenienti da social network). Il processo di estrazione può essere statico, usato per popolare la DWH per la prima volta, o incrementale, usato per aggiornarne il contenuto con regolarità. Nel secondo caso ogni aggiornamento è associato ad un timestamp che lo identifica ed è attivato da dei trigger specifici
    \item Cleansing: è il processo che permette di pulire i dati da eventuali errori o incongruenze, specialmente se si tratta di dati inseriti a mano dall'uomo. Questo processo in alcuni casi può rivelarsi molto complicato. I principali motivi di errore sono:
          \begin{itemize}
              \item Dati duplicati
              \item Dati mancanti
              \item Uso dei campi di compilazione errato
              \item Valori impossibili o palesemente errati
              \item Valori inconsistenti per un singola entità a causa di errori di battitura o dell'utilizzo di convenzioni diverse nella nomenclatura
          \end{itemize}
          Per risolvere i casi di inconsistenza si possono usare diverse tecniche:
          \begin{itemize}
              \item Tecniche basate su dizionari: sono utilizzate per verificare la correttezza dell'attributo basandosi su tabelle di ricerca e dizionari per cercare abbreviazioni e sinonimi. Si possono applicare se il dominio è noto e limitato. Queste tecniche sono adatte per risolvere problemi come errori di battitura e discrepanze di formato
              \item Unione approssimata: questa tecnica è usata quando è necessario deve unire i dati provenienti da fonti diverse, ma non si ha una chiave comune per identificare le tuple corrispondenti. Si possono usare quindi:
                    \begin{itemize}
                        \item Join approssimato: l'unione verrà eseguita sulla base degli attributi comuni. Questi attributi non sono identificatori per il cliente, quindi non sono soggetti a procedure di controllo per garantire vincoli di integrità e assenza di errori di immissione
                        \item Funzioni di similitudine: viene usato il concetto di similitudine per identificare diverse istanze delle stesse informazioni. Gli strumenti ETL hanno al loro interno molti algoritmi che ispezionano i dati e cercano di capire quale trasformazione e strategia per migliorare i dati sia la migliore per un dato caso specifico. Quindi si può usare le funzioni di affinità per calcolare la somiglianza tra due parole. Se la somiglianza è maggiore / minore di una determinata soglia, le due parole sono le stesse e le righe possono essere unite
                    \end{itemize}
              \item Algoritmi ad-hoc: algoritmi personalizzati basati su regole aziendali specifiche (es.\ riguardo al contesto finanziario deve sempre essere verificata la seguente regola, \(profitto = entrate-spese\))
          \end{itemize}
    \item Trasformazione: durante questa fase i dati vengono trasformati affinché rispettino il formato desiderato (ad es.\ tutte le spese nella medesima valuta ecc.). Per risolvere questi problemi si possono usare le seguenti tecniche:
          \begin{itemize}
              \item Conversione: si cambia il dato in accordo con le regole correnti
              \item Arricchimento: combinazione di più attributi per creare nuove informazioni
              \item Separazione/concatenazione
          \end{itemize}
    \item Caricamento: è il processo finale che serve a riempire il data warehouse con dati corretti e puliti. Per farlo ci sono due modi differenti:
          \begin{itemize}
              \item Refresh: ovvero il data warehouse è completamente riscritto
              \item Update: vengono modificati solo i campi che realmente cambiano lasciando invariati gli altri
          \end{itemize}
\end{itemize}

\section{Architettura di un data warehouse}
L'architettura di un data warehouse ha determinati requisiti:
\begin{itemize}
    \item Separazione
    \item Scalabilità
    \item Estensibilità
    \item Sicurezza
    \item Facilità di amministrazione
\end{itemize}
Per raggiungere tutti questi punti si possono usare architetture differenti, ognuna con i suoi pro e contro.

\paragraph{Single-Layer Architecture}
L'obbiettivo di questa architettura è minimizzare la quantità di dati immagazzinati, rimuovendo i dati ridondanti. Lo strato sorgente è l'unico strato fisicamente disponibile. Il data warehouse è implementato come una view multidimensionale dei dati operazionali creata da uno specifico middleware.
\begin{figure}[h!bt]
    \centering
    \includegraphics[width=0.4\textwidth]{single_layer_architecture.png}
    \caption{Single Layer Architecture}\label{fig:single_layer_architecture}
\end{figure}

\paragraph{Two-Layer Architetture}
In questo caso c'è separazione fisica fra sorgente dei dati e data warehouse. Lo strato sorgente dei dati include un insieme eterogeneo di sorgenti. Lo strato di Data Staging immagazzina i dati estratti dalla sorgente per rimuovere inconsistenze ed errori e integra i dati provenienti dalle sorgenti eterogenee in un unico schema comune. Questo strato include le procedure ETL.\@ Lo stato di data warehouse è dove sono immagazzinate le informazioni e sono distribuite secondo un modello logico centralizzato, una repository. Questo strato fornisce un punto di accesso comune per interrogare i dati o creare Data Mart. Lo stato di analisi infine è uno strato che permette un approccio più user-friendly per interrogare i dati, creare report, dashboard, etc \ldots
\begin{figure}[h!bt]
    \centering
    \includegraphics[width=0.4\textwidth]{two_layer_architecture.png}
    \caption{Two Layer Architecture}\label{fig:two_layer_architecture}
\end{figure}

\paragraph{Tree-Layer Architetture}
Nella base questa architettura condivide lo schema dell'architettura a due strati, ma aggiunge uno strato chiamato Reconciled Layer. Questo strato materializza i dati operazionali ottenuti dopo le fasi di integrazione e pulizia. Il risultato è un dato integro, consistenti, corretti, aggiornati e dettagliati. Lo strato aggiunto crea un riferimento comune al modello dei dati per l'intera azienda.
\begin{figure}[h!bt]
    \centering
    \includegraphics[width=0.4\textwidth]{three_layer_architecture.png}
    \caption{Three Layer Architecture}\label{fig:three_layer_architecture}
\end{figure}

\section{Modellazione concettuale DWH: The Dimensional Fact Model (DFM)}
Con The Dimensional Fact Model (DFM) si intende un modello concettuale creato a supporto della progettazione del data mart. Questo modello concettuale ha come obbiettivi:
\begin{itemize}
    \item Fornire supporto al design concettuale
    \item Creare un ambiente in cui gli utenti possano interagire in maniera comoda con la DWH
    \item Favorire la comunicazione tra utenti e designer per formalizzare i requisiti richiesti
    \item Costruire una piattaforma stabile per il design logico
    \item Fornire una chiara e efficace documentazione del design della DWH
\end{itemize}



Introduciamo un esempio di DFM
\begin{esempio}{}{}
    \begin{center}
        \includegraphics[width=0.9\textwidth]{DFM.png}
    \end{center}
\end{esempio}
Il Dimensional Fact Model (DFM) ha i seguenti concetti base:
\begin{itemize}
    \item Fatto: i concetti rilevanti nel processo decisionale. Tipicamente modella un insieme di eventi in avvenimento all'interno dell'azienda. Alcuni esempi sono le vendite, gli acquisti o gli ordini
    \item Misura: è una proprietà numerica che descrive un fatto in maniera quantitativa relativamente ad un qualche aspetto analizzabile. Alcuni esempi sono la quantità venduta e lo sconto applicato
    \item Dimensione: è una proprietà di un fatto dotata di un dominio finito e descrive un punto di coordinazione per l'analisi. Alcuni esempi sono le date, i prodotti, i negozi
    \item Attributi dimensionali: sono altre dimensioni o attributi in grado di descrivere una dimensione. Alcuni esempi sono le categorie di prodotti, i mesi
    \item Gerarchia: è un albero i cui nodi sono attributi dimensionali ed i cui archi modellano relazioni molti ad uno tra gli elementi di una coppia di attributi dimensionali. La radice dell'albero è sempre una dimensione
    \item Evento primario: è una particolare occorrenza di un fatto, identificata da una ennupla contenente un valore per ognuna delle dimensioni. Ad ogni evento primario è associato un valore per ognuna delle misure
          \begin{esempio}{}{}
              In riferimento all'esempio precedente si ha il seguente evento primario
              \begin{tabularx}{\textwidth}{XXXXXX}
                  \toprule
                  Data       & Negozio          & Prodotto & Qta venduta & Guadagno & Prezzo unitario \\
                  \midrule
                  01/03/2015 & Negozio Centrale & Caffe    & 54          & 100      & 5               \\
                  \bottomrule
              \end{tabularx}
          \end{esempio}
    \item Evento secondario: dato un insieme di attributi dimensionali, ogni ennupla di valori identifica un evento secondario che aggrega tutti gli eventi primari corrispondenti. Ogni evento secondario è associato ad un valore per ogni misura che riassume tutti i valori della misura stessa in corrispondenza dell'evento primario
          \begin{esempio}{}{}
              In riferimento all'esempio precedente si hanno i seguenti eventi primari:\\
              \begin{tabularx}{\textwidth}{XXXXXX}
                  \toprule
                  Data       & Negozio          & Prodotto & Qta venduta & Guadagno \\
                  \midrule
                  01/03/2015 & Negozio Centrale & Caffe    & 20          & 60       \\
                  01/03/2015 & Negozio Centrale & Latte    & 25          & 50       \\
                  02/03/2015 & Negozio Centrale & Pane     & 40          & 70       \\
                  10/03/2015 & Negozio Centrale & Vino     & 15          & 150      \\
                  \bottomrule
              \end{tabularx}
              \medskip
              Da cui deriva il seguente evento secondario:

              \begin{tabularx}{\textwidth}{XXXXXX}
                  \toprule
                  Data       & Negozio          & Prodotto       & Qta venduta & Guadagno \\
                  \midrule
                  Marzo 2015 & Negozio Centrale & Cibo e bevande & 100         & 330      \\
                  \bottomrule
              \end{tabularx}
          \end{esempio}
\end{itemize}

L'aggregazione richiede di definire un operatore adatto a comporre i valori delle misure che caratterizzano gli eventi primari in valori da abbinare a ciascun evento secondario.
\begin{itemize}
    \item Misure di flusso: fanno riferimento ad un intervallo temporale al termine del quale esse sono valutate cumulativamente (ad es.\ quantità di beni venduta)
    \item Misure di livello: sono valutate in momenti particolari (ad es.\ numero di prodotti in inventario)
    \item Misure unitarie: sono valutate in momenti particolari ma sono espresse in termini relativi (ad es.\ il prezzo unitario)
\end{itemize}

Una misura viene chiamata additiva lungo una dimensione quando è possibile utilizzare l'operatore SOMMA per aggregare i suoi valori lungo la gerarchia delle dimensioni. Se questo non è il caso, viene chiamata non additivo. Una misura non additiva non è aggregabile quando non è possibile utilizzare nessun operatore di aggregazione.

\begin{table}[h!bt]
    \begin{center}
        \begin{tabularx}{0.6\textwidth}{XXX}
            \toprule
                              & Gerarchie temporali           & Gerarchie non temporali       \\
            \midrule
            Misure di flusso  & Somma, Media, Minimo, Massimo & Somma, Media, Minimo, Massimo \\
            Misure di livello & Media, Minimo, Massimo        & Somma, Media, Minimo, Massimo \\
            Misure unitarie   & Media, Minimo, Massimo        & Media, Minimo, Massimo        \\
            \bottomrule
        \end{tabularx}
    \end{center}
\end{table}

Gli operatori di aggregazione seguono una precisa classificazione:
\begin{itemize}
    \item Distributivi: calcolano aggregando aggregati parziali (somma, min, max)
    \item Algebrici: necessitano l'utilizzo di informazioni addizionali sotto forma di un numero finito di misure di supporto  per calcolare correttamente gli aggregati da aggregati parziali (es. Media)
    \item Olistici: calcolano aggregando aggregati parziali solamente con un numero infinito di misure di supporto (es. Rango)
\end{itemize}

Queste proprietà sono utili quando si utilizza gli operatori Roll-up e Drill-down perché se si pre-calcola la SOMMA nel cubo, quando si applica l'operatore Roll-up poi semplicemente si calcola la somma delle somme.

\subsection{DFM avanzate}
All'interno dei grafici è poi possibile usare vari altri tipi di connessioni:
\begin{itemize}
    \item Attributi descrittivi, usati per fornire ulteriori informazioni ma non usati per le fasi di aggregazione
    \item Attributi inter-dimensionali, ovvero attributi dimensionali il valore è definito dalla combinazione di due o più attributi dimensionali (che possono anche appartenere a gerarchie differenti)
    \item Convergenza, caso in cui due o più archi della stessa gerarchia terminano nel medesimo attributo dimensionale
    \item Gerarchie condivise
    \item Archi multipli, usati per rappresentare relazioni molti a molti
    \item Archi opzionali, usati per modellare scenari che non sono la norma (ad es.\ la taglia di un prodotto è valida solamente per i vestiti e non per il tonno)
    \item Gerarchie incomplete, ovvero quelle gerarchie all'interno delle quali uno o più attributi sono mancanti in alcune istanze
    \item Gerarchie ricorsive, usate per rappresentare relazioni genitore-figlio tra i livelli
\end{itemize}

\subsection{Progettazione logica}
La fase di progettazione logica definisce le strutture dati, ovvero l'insieme di tabelle e le relazioni tra tabelle. Questo perché si vogliono rappresentare i Data Mart secondo il modello logico selezionato in modo da ottimizzare le prestazioni mettendo a punto queste strutture. Quindi, partendo dallo schema concettuale si vuole definire lo schema logico di un Data Mart. Esistono tre passaggi principali per implementare uno schema logico in un DBMS relazionale:

\begin{itemize}
    \item Tradurre i fatti in schemi logici: star o snowflake
    \item Viste materializzanti: insieme di viste secondarie che aggregano i dati della vista primari per migliorarne le prestazioni durante le query
    \item Frammentazione dei fatti: sia orizzontale che verticale
\end{itemize}

Lo schema Star è caratterizzato da fact tables e dimension tables. Una fact table contiene tutte le misure e gli attributi descrittivi legati a un fatto. Per ogni dimensione viene creata una tabella dimensionale che include tutti gli attributi della gerarchia:

\begin{figure}[h!bt]
    \centering
    \includegraphics[width=0.7\textwidth]{schema_stella.png}
    \caption{Schema a stella}
\end{figure}

Una schema snowflake è uno schema Star che varia con la dimensione delle tabelle parzialmente normalizzate.
\begin{figure}[h!bt]
    \centering
    \includegraphics[width=0.7\textwidth]{schema_fiocco_neve.png}
    \caption{Schema a fiocco di neve}
\end{figure}