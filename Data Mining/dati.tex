Prima di poter imparare dai dati occorre dire cosa è un dato e quali tipi di dati esistono. Per prima cosa occorre considerare che esistono vari tipi di dato ognuno dei quali riflette una diversa tipologia di informazioni e consente di effettuare un diverso insieme di operazioni. Inoltre occorre tener conto del fatto che i dati sono di qualità differente, infatti è possibile che vi siano dati mancanti o errati a causa di errori in fase di raccolta. Vi è inoltre il problema riguardante i valori anomali, ovvero piccole quantità di dati che sono radicalmente diversi dagli altri, ci sono casi in cui questi sono rilevanti in quanto rappresentano condizioni fuori dalla norma ma comunque reali e altri in cui possono essere addirittura distruttivi in quanto le deviazioni sono causate da errori o da rumore.

\section{Tipi di dato}

\begin{figure}[h!bt]
    \centering
    \includegraphics[width=\textwidth]{tipi_dato.png}
    \caption{Tipi di dato}\label{fig:tipi_dato}
\end{figure}
Come si può vedere dalla tabella~\ref{fig:tipi_dato}
i dati sono inizialmente distinti inizialmente tra numerici e categorici. A loro volta queste due categorie sono divise in nominali e ordinali per i tipi categorici e in intervalli e rapporti per i dati numerici.
I dati categorici nominali sono costituiti da un insieme di etichette il cui confronto consente solo di distinguerle. Con questo tipo di  dati si possono effettuare statistiche descrittive. I dati categorici ordinali invece rappresentano un insieme di valori dotati di una relazione d'ordine (ad esempio lo soddisfazione in un servizio di assistenza clienti: pessima, buona, ottima, eccellente). L'ulteriore operazione consentita su questo tipo di dati è il confronto (ad esempio ``l'utente x ha avuto una migliore esperienza dell'utente y'').\\
I dati numerici invece sono costituiti da numeri. Gli intervalli (\textit{interval}) indicano dati per cui non è data una definizione univoca di ``0'' (ad esempio lo 0 dei gradi celsius e quello dei gradi Fahrenheit è diverso), tra dati di questo tipo hanno senso solo operazioni di somma e differenza. I dati numerici di tipo rapporto (\textit{ratio}) invece sono dotati di una definizione univoca di ``0'' e quindi consentono ogni tipo di operazione matematica. La differenza fra intervalli e rapporti sta quindi nel fatto che gli intervalli hanno un valore dello 0 arbitrario mentre i rapporti hanno un valore dello 0 assoluto. Si veda la seguente tabella con le possibili trasformazioni accettate da ogni tipo di dato:

\begin{figure}[h!bt]
    \centering
    \includegraphics[width=\textwidth]{trasformazioni.png}
    \caption{Trasformazioni consentite}\label{fig:trasformazioni}
\end{figure}

Il numero di valori caratterizza il tipo di dominio:
\begin{itemize}
    \item In domini discreti è accettato un numero di valori finito
    \item In domini continui è in teoria accettato un numero infinito di valori, nella pratica è accettato il numero di valori consentito della rappresentazione (es.\ float e double)
\end{itemize}
Tipicamente i dati nominali e ordinali sono discreti, preferibilmente binari, mentre intervalli e rapporti sono espressi tramite valori continui.

I metodi per memorizzare i dati possono essere di differente tipologia:
\begin{itemize}
    \item \textbf{Tabelle}: si tratta di tabelle relazionali in cui il numero delle colonne è fisso, per questo motivo l'insieme degli attributi è fisso per ogni tupla
    \item \textbf{Transazioni}: ogni riga è caratterizzata da un TID (\textit{tuple identifier}) e da una lista di items che cambia per ogni tupla. Un esempio di transazione è la relazione tra un identificativo di un cliente di un supermercato e l'insieme dei prodotti acquistati
    \item \textbf{Matrice di dati}: simili è una tabella in cui ogni valore è esclusivamente di tipo numerico
    \item \textbf{Matrice sparsa di dati}: matrice di dati in cui molti campi sono vuoti o nulli
    \item \textbf{Grafi di dati}
\end{itemize}

\section{Qualità dei dati}
Uno dei problemi più rilevanti nell'apprendimento dai dati è la loro qualità. Infatti, per costruire algoritmi di apprendimento in grado di produrre conoscenza corretta è necessario che i dati siano il più possibile completi e corretti.

\subsection{Rumore}
Esistono due definizione di rumore dei dati:

\begin{itemize}
    \item Una modifica di un valore originale
    \item Dati senza interesse che sono mischiati a dati di interesse
          \begin{esempio}{}{}
              Nell'analisi degli accessi ad un sito web potremmo essere interessati ai soli accessi da parte di esseri umani. In questo caso gli accessi dovuti ai web crawler rappresentano rumore
          \end{esempio}
\end{itemize}

\subsection{Valori anomali (outliers)}
Gli outliers sono valori anomali, le cui caratteristiche sono significativamente diverse da quelle dei restanti dati. Va notato che gli outlier non sono necessariamente errori, ma possono anche rappresentare eventi estremamente rari che dunque si discostano dagli altri dati.
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.3\textwidth]{outliers.png}
    \caption{Outlier}\label{fig:outliers}
\end{figure}
Occorre però capire se gli outlier sono di interesse o meno, perché potrebbero rendere più complesso il processo di apprendimento oppure addirittura deviarlo.

\subsection{Valori mancanti}
Non è pensabile che un dataset sia perfetto, soprattutto perché spesso essi sono generati automaticamente, ad esempio, grazie a sensori che raccolgono e memorizzano i dati in maniera automatica. Per questo motivo, è lecito pensare che possano esservi valori mancanti. Questi possono essere dati che non sono stati raccolti oppure informazioni non utilizzabili.

Per risolvere questo problema si può:
\begin{itemize}
    \item Ignorare ogni record contenente un valore mancante. Tipicamente questo riduce di molto la popolazione di dati rendendone disponibili molti meno rispetto a quelli che sarebbero in realtà disponibili
    \item Provare a sostituirli con una stima in base a tutti gli altri valori del data set (ad esempio con il valore medio, o con la mediana)
    \item Ignorare solo i valori mancanti e non gli interi record
    \item Inserire un valore possibile ragionando in termini di probabilità e di distribuzioni di probabilità. Occorre però che tali distribuzioni siano note prima dell'analisi dei dati
\end{itemize}

\subsection{Dati duplicati}
Così come possono esserci dati mancanti è anche possibile che all'interno di un dataset ci siano dati duplicati. Questo può verificarsi nel momento in cui si uniscono più fonti di dati diverse oppure a causa di errori nel momento in cui vengono raccolti i dati. La presenza di dati duplicati modifica le reali distribuzioni dei dati, attribuendo più peso ai record duplicati causando ad esempio overfitting.

In generale il processo riguardante la gestione dei dati duplicati è complesso, soprattutto se i dati sono duplicati soltanto in parte.

\section{Pre-trattamento dei dati}
Una parte rilevante del machine learning riguarda il pre trattamento dei dati. Con questo termine si intende una elaborazione preliminare dei dati affinché siano più adatti ad essere usati per l'apprendimento automatico.

Esistono varie tecniche di trattamento:
\begin{itemize}
    \item{Aggregazione}
    \item{Campiionamento}
    \item{Riduzione delle dimensionalità}
    \item{Creazione di feature}
    \item{Discretizzazione e binarizzazione}
\end{itemize}.
\subsection{Aggregazione}
Per \textbf{aggregazione} si intende la combinazione di due o più attributi a formare un nuovo singolo attributo. L'aggregazione si pone più obiettivi tra cui:
\begin{itemize}
    \item \textbf{Riduzione dei dati}. Questo consente di ridurre il numero di attributi di un oggetto
    \item \textbf{Cambiare la scala} di visualizzazione al fine di modificare il livello di dettaglio. Ad esempio è possibile aggregare le città in province, regioni o stati
    \item \textbf{Aumentare la stabilità dei dati}. Aggregando i dati si tende a ridurne la variabilità, usando questa tecnica in maniera oculata diventa possibile ridurre anche controbilanciare i possibile effetti causati dal rumore all'atto della raccolta dei dati
\end{itemize}

\subsection{Campionamento}
Il \textbf{campionamento} è il più comune strumento di pre-trattamento dei dati, perché in agli albori dell'analisi dei dati gli statistici non erano dotati di strumenti automatici per il calcolo, per questo motivo avevano a che fare con un enorme mole di dati da dover raccogliere e processare a mano. \\
Anche oggi i processi di analisi hanno a che con un enorme mole di dati, per questo motivo vengono considerati dei sottoinsiemi dell'insieme iniziale dato che l'utilizzo dell'intero dataset potrebbe essere impossibile o troppo costoso da maneggiare. L'utilizzo di un campione dei dati può offrire quasi gli stessi risultati rispetto all'utilizzo del dataset completo, questo a patto che il campione sia rappresentativo della popolazione, ovvero che goda nel complesso delle medesime proprietà e possegga le medesime caratteristiche.

Il campionamento viene fatto per:
\begin{itemize}
    \item Investigare in via preliminare i dati
    \item Analizzare i risultati finali
    \item Abbattere i costi
          \begin{itemize}
              \item Ottenere l'intero dataset potrebbe essere impossibile o troppo costoso, quindi è utile ricorrere ad una prospettiva statistica sui dati stessi
              \item Processare l'intero dataset potrebbe essere troppo costoso oppure potrebbe richiedere troppo tempo
          \end{itemize}
\end{itemize}

Esistono diversi tipi di sampling:

\begin{itemize}
    \item Campionamento casuale semplice: scelta casuale di un singolo oggetto data una certa distribuzione di probabilità
    \item Con sostituzione: ripetizione di \(N\) estrazioni indipendenti. Questa tecnica è decisamente più semplice da implementare rispetto al campionamento senza sostituzione e più semplice da interpretare da un punto di vista statistico dato ch le estrazioni sono tra loro indipendenti
    \item Senza sostituzione: ripetizione di \(N\) estrazioni, ma ogni volta l'elemento estratto viene rimosso dalla popolazione iniziale in modo che non possa venir estratto nuovamente
    \item Stratificazione: in questo caso i dati vengono divisi in varie partizioni secondo un qualche criterio e successivamente da ogni partizione vengono estratti casualmente dei campioni. Questo tipo di campionamento viene usato tipicamente quando il dataset iniziale è diviso in sottoinsiemi diversi con caratteristiche eterogenee. La rappresentatività viene però garantita solamente all'interno della singola partizione e non relativamente all'intera popolazione
\end{itemize}

A questo punto occorrere chiedersi quale sia la dimensione corretta per un campione, in quanto è la dimensione minima del campione a garantirne la rappresentatività e il campionamento di per sé è un procedimento \textit{lossy}. A tal proposito la statistica offre tecniche per comprendere quale sia la dimensione ottimale del campione e quale sia la significatività del campione in modo da poter trovare il miglior compromesso tra riduzione della mole di dati da analizzare e precisione.

\begin{esempio}{Campionamento e classi mancanti}{}
    Il grafico in figura mostra la probabilità di campionare almeno un elemento di ognuna delle dieci classi presenti nel dataset. È interessante notare inoltre che questo valore è indipendente dalla dimensione della popolazione stessa.

    \begin{center}
        \includegraphics[width=0.6\textwidth]{prob_campionamento.png}
    \end{center}
\end{esempio}

\subsection{Riduzione della dimensionalità}
\subsubsection{Maledizione della dimensionalità} Nei casi in cui i dati siano rappresentati su molte dimensioni l'occupazione dello spazio diventa relativamente sparsa e la discriminazione dei dati sulla base delle distanze relative diventa complessa in quanto diminuisce la distanza minima media tra due punti. È interessante notare come questa maledizione non può essere spezzata mediante normalizzazione dei dati.

\begin{esempio}{}{}
    In questo esempio sono stati generati 500 punti casuali in spazi aventi via via sempre più dimensioni. Il grafico mostra come all'aumentare del numero di dimensioni dello spazio decresce (molto rapidamente) la distanza relativa minima tra due punti.

    \begin{center}
        \includegraphics[width=0.6\textwidth]{curse_dimensionality.png}
    \end{center}
\end{esempio}

\subsubsection{Riduzione della dimensionalità}
La riduzione della dimensionalità può essere effettuata per varie ragione oltre a scongiurare la maledizione prima esposta.

\begin{itemize}
    \item Riduzione del rumore
    \item Riduzione della complessità spaziale e temporale degli algoritmi di mining
    \item Semplicità di visualizzazione. Per ovvi motivi gli unici spazi visualizzabili sono quelli a una, due e tre dimensioni
    \item Miglioramento della precisione del modello
    \item Riduzione della probabilità di overfitting
\end{itemize}

Vi sono varie tecniche atte a ridurre le dimensioni dello spazio in cui risiedono i dati:
\begin{itemize}
    \item \textbf{\textit{Principal component analysis}}
    \item Singular values decomposition
    \item Tecniche supervisionate
    \item Tecniche non lineari
\end{itemize}

\paragraph{Principal component analisys}
L'idea alla base di questa tecnica è quella di trovare un nuovo spazio su cui proiettare in grado di catturare la massima variazione dei dati. Questo nuovo spazio è definito dagli autovettori della matrice di covarianza. Una volta effettuata la trasformazione il nuovo dataset conterrà solo gli attributi che catturano la variazione massima dei dati.

La prima dimensione selezionata è quella che cattura la maggior parte di variabilità, la seconda cattura la maggior parte della restante e così via, fino a che la variabilità rimanente è al di sotto della soglia desiderata. Da questo deriva che un piccolo numero di nuove variabili (ottenuto come combinazione lineare degli autovettori) cattura la maggior parte di variabilità dei dati.

\begin{esempio}
    Si consideri il classico Iris dataset, si rappresentano in un grafico gli attributi sepalwidth e sepallength.
    \begin{center}
        \includegraphics[width=0.8\textwidth]{pca_2.png}
    \end{center}

    Nella figura di sinistra si possono vere i tre gruppi di fiori distinti mediante il colore dei punti. Si notano tre classi che tuttavia sono difficilmente divisibili. Questo è un set di dati numerici. Applicando PCA si nota che le prime due componenti ora catturano il \(95\%\) della varianza. Se si traccia in un grafico frazione di varianza per ogni componente principale, si nota che la prima componente cattura il \(92\%\) e la seconda una porzione molto esigua (\(3-4\%\))
\end{esempio}

\subparagraph{Scaling multidimensionale}
Questa tecnica non viene utilizzato per la selezione delle caratteristiche, ma piuttosto per la presentazione. Essa è in grado di ragionare bene in 2 dimensioni, quindi se si ha  un'immagine 2-D possiamo facilmente affermare ``questo è qui, questo è qui\ldots''.

Se abbiamo più di due dimensioni, possiamo scegliere le coppie in modo casuale o esaustivo oppure possiamo usare un metodo matematico per rimappare ogni punto in un nuovo spazio 2-D. Partendo dalla distanza tra gli elementi del dataset adatta la proiezione degli elementi in uno spazio ad \(m\) dimensioni in modo tale che le distanze tra gli elementi siano preservate. Esistono varianti di questo algoritmo sia per spazi metric che non metrici.

\subsubsection{Selezione delle feature}
La selezione di un sottoinsieme delle feature rappresenta una tecnica locale per ridurre la dimensione dello spazio in cui si trovano i dati ed allo stesso tempo per selezionare solo quegli attributi che ci consentano di effettuare il processo di apprendimento al meglio.

Può essere utilizzata per rimuovere gli attributi ridondanti, che dunque contengono informazioni già contenute in altri attributi, oppure per rimuovere attributi irrilevanti ai fini dell'apprendimento. Inoltre alcuni attributi possono essere fuorvianti e quindi confondere il processo di apprendimento (ad esempio nello studio della relazione tra salute, esercizio fisico, età e sesso se le età di uomini e donne appartengono a range diversi i risultati saranno lontani da quelli veri). Infine può accadere che un attributo sia fortemente correlato con un altro nella maggior parte dei casi ma non nei restanti in quanto i dati raccolti possono non essere omogenei.
\begin{figure}[h!bt]
    \centering
    \includegraphics[width=0.7\textwidth]{feature_selection.png}
    \caption[Architettura generale]{Architettura generale per la selezione delle feature da includere}
\end{figure}
Si nota sempre quindi che i dati contengono informazioni da estrarre ma che essi non devono essere utilizzati senza prima ragionare sul loro significato.

Per ridurre la dimensione dello spazio in cui si trovano gli attributi sono possibili più tecniche a seconda che ci si trovi in presenza di dati supervisionati o meno:
\begin{itemize}
    \item Se i dati sono non supervisionati è possibile utilizzare solamente un approccio a \textbf{forza bruta}. Esso consiste nel provare tutti i possibili sottoinsiemi di feature come input degli algoritmi di data mining e misurare l'efficacia di ogni ogni tentativo con il dataset ridotto al fine di trovare il sottoinsieme più adatto all'apprendimento in relazione alle dimensioni dello spazio che lo contiene
    \item Se siamo in presenza di dati etichettati, oltre a precedente approccio (comunque utilizzabile, anche se con pessime performance), è possibile utilizzare altre tecniche più efficaci:
          \begin{itemize}
              \item \textbf{Approccio filtrante} (ovvero approcci indipendenti dagli schemi di esecuzione). Le feature di interesse vengono selezionate prima che gli algoritmi vengano fatti girare
              \item Approcci dipendenti dallo schema di esecuzione
                    \begin{itemize}
                        \item \textbf{Approccio implicito} (\textit{embedded}). La selezione avviene naturalmente come parte dell'algoritmo stesso (ad esempio succede negli alberi decisionali dove vengono selezionati per primi gli attributi a cui è associato il maggior guadagno in termini di informazione, in questo caso se viene raggiunto il criterio di terminazione alcuni attributi non vengono considerati)
                        \item \textbf{Approccio wrapper}. È l'algoritmo stesso a scegliere il miglior sottoinsieme delle feature da considerare, similmente a come succede con l'approccio a forza bruta ma senza effettuare una ricerca esaustiva
                    \end{itemize}
          \end{itemize}
\end{itemize}

\paragraph{Metodi filtranti}
Questi metodi si basano sulle caratteristiche generali dei dati. L'idea di base consiste nel selezionare un insieme di attributi indipendentemente dal modello di mining da utilizzare per apprendere pattern (per esempio attributi tra loro indipendenti che però correlano bene con la classe). Alcuni metodi sono:
\begin{itemize}
    \item Correlazione di Pearson: una misura quantitativa (i cui valori vanno tra \([-1,1]\)) della dipendenza (o indipendenza) tra due variabili continue
    \item Liner Discrimination Analysis (LDA): usata per trovare combinazioni lineari di feature che caratterizzano o separano due o più classi
    \item ANOVA (Analysis Of Variance): simile a LDA se non per il fatto che usa feature categoriche ed una sola feature continua
    \item Chi-quadro: test statistico applicato ad un insieme di feature categoriche to valutare la verosimiglianza di correlazione o associazione tra di esse usando le distribuzioni di frequenza
\end{itemize}

\begin{figure}[h!bt]
    \centering
    \includegraphics[width=0.7\textwidth]{filter_methods_1.png}
    \caption{Schema riassuntivo dei metodi}\label{fig:filter_methods_1}
\end{figure}
In generale lo schema da seguire è nel suo complesso:
\begin{enumerate}
    \item Insieme di tutte le feature
    \item Selezione delle feature migliori
    \item Utilizzo degli algoritmi di apprendimento
    \item Tuning e valutazione delle performance
\end{enumerate}

\paragraph{Metodi wrapper}
In generale questi metodi iterano i precedenti passi 2 e 3 più volte al fine di trovare il miglior sottoinsieme di feature. Questo fa sì che la complessità del problema non sia banale soprattutto in termini temporali, in quanto occorre allenare ripetutamente l'algoritmo di apprendimento. Tuttavia utilizzando questo schema il problema si riduce ad una ricerca all'interno di uno spazio degli stati.
\begin{esempio}{}{}
    \begin{center}
        \includegraphics[width=0.8\textwidth]{wrapper_method_1.png}
    \end{center}
    Usando un dataset per le previsioni meteo si può creare un grafo contenente tutte le possibili combinazioni di attributi e poi usare algoritmi di ricerca greedy per trovare il miglior insieme di feature
\end{esempio}

\paragraph{Differenze tra metodi wrapper e filtering}
Di seguito le principali differenze tra metodi di filtering e di wrapping:
\begin{itemize}
    \item I metodi di filtering misurano la rilevanza delle caratteristiche in base alla loro correlazione con la variabile dipendente mentre i metodi di wrapping misurano l'utilità di un sottoinsieme di funzionalità addestrando effettivamente un modello
    \item I metodi di filtering sono molto più veloci rispetto ai metodi di wrapping in quanto non implicano l'addestramento di modelli. I metodi di wrapping sono molto costosi dal punto di vista computazionale
    \item I metodi di filtering utilizzano metodi statistici per la valutazione di un sottoinsieme di caratteristiche mentre i metodi di wrapping utilizzano la cross validation
    \item I metodi di filtering potrebbero non riuscire a trovare il miglior sottoinsieme di funzionalità in molte occasioni, mentre i metodi di  wrapping possono sempre fornire il miglior sottoinsieme di features (quindi sono più affidabili)
    \item L'utilizzo del sottoinsieme di funzioni dai metodi di wrapping rende il modello più incline overfitting (saremo molto ben adattati a quello specifico set di dati) rispetto all'utilizzo di subset di feature date da metodi di filtering
\end{itemize}

\subsection{Creazione di attributi}
Aggiungere nuovi attributi aumenta la dimensione dello spazio in cui sono rappresentati di dati, ma permette anche di catturare più efficacemente ed efficientemente alcune caratteristiche dei dati. A tal proposito è possibile:

\begin{itemize}
    \item Estrarre caratteristiche a partire dai dati già esistenti (ad esempio mappare strutture di pixel in tratti facciali come la distanza fra gli occhi)
    \item Mappare feature in un nuovo spazio (ad esempio nell'analisi audio spesso risulta utile passare al dominio delle frequenze da quello del tempo)
    \item Creazione di nuove feature (ad esempio mappare volume e peso nella densità)
\end{itemize}

\subsection{Discretizzazione}
Dietro alla necessità di discretizzazione risiedono varie motivazioni. Alcuni algoritmi, ad esempio, sono in grado di fornire risultati migliori in presenza di dati categorici dunque la discretizzazione consente di raggruppare dati originariamente appartenenti ad un dominio continuo. Inoltre avere un numero relativamente piccolo di valori distinti consente da un lato l'emergere di schemi in maniera più netta e dall'altro di diminuire l'impatto che il rumore ha sui risultati finali ottenuti.
\begin{esempio}{}{}
    Nel considerare i salari dei dipendenti di un'azienda può convenire raggrupparli in classi anziché considerare ogni singolo valore
\end{esempio}

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.7\textwidth]{discretizzazione_1.png}
    \caption{Trasformazione di dati da un dominio continuo ad un dominio discreto}\label{fig:discretizzazione_1}
\end{figure}
La discretizzazione può essere utilizzata per trasformare dati appartenenti ad un dominio continuo in dati appartenenti ad un dominio discreto (vedi Figura~\ref{fig:discretizzazione_1}) oppure per ridurre il numero di valori di valori discreti all'interno di uno stesso dominio discreto. In entrambi i casi occorre utilizzare delle soglie (nel caso limite una sola soglia produce una binarizzazione dei dati) per dividere lo spazio in varie aree ognuna corrispondente ad un attributo discreto. In particolare nel secondo caso, ma anche nel primo, questa procedura deve essere guidata da una profonda conoscenza del dominio in modo da discretizzare i dati senza perdere troppe informazioni.

Come si vede in figura~\ref{fig:discretizzazione_1} sono possibili varie tecniche per disporre le soglie ognuna con pro e contro. La prima soluzione è la più semplice tuttavia non è la migliore in quanto taglia a metà alcune nuvole. La seconda soluzione è migliore rispetto alla precedente in quanto considera la distribuzione dei dati secondo frequenza, tuttavia taglia comunque alcune nuvole. La terza ed ultima soluzione invece è una tecnica adottata nel clustering che consente di isolare le varie nuvole in maniera ottimale.

\medskip
Talvolta queste tecniche non sono sufficienti. Ad esempio \texttt{sklearn} implementa i metodi di classificazione solo per attributi numerici, pertanto non è in grado di elaborare attributi categorici in generale. In questi casi si può propendere per la binarizzazione di attributi discreti. Supponendo di avere un attributo con \(V\) valori discreti si possono generare \(V\) attributi binari mutuamente esclusivi (vedi Figura~\ref{fig:binarizzazione_1}), ognuno dei quali è associato ad uno dei valori originali (può essere pensata come una stringa di \(V\) bit in cui un solo bit può essere \(1\)).
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.7\textwidth]{binarizzazione_1.png}
    \caption{Binarizzazione di un attributo discreto}\label{fig:binarizzazione_1}
\end{figure}

\subsubsection{Trasformazione degli attributi}
In questo caso vogliamo mappare l'intero insieme di valori in un nuovo dominio secondo una certa funzione (ad esempio \(x^k\), \(\log(x)\), \(|x|\)). In generale questa procedura modifica la distribuzione dei dati. Questo effetto potrebbe essere desiderato in modo da far emergere caratteristiche desiderate, in altri casi potrebbe essere deleterio in quanto rende più complesso notare certi schemi nei dati.

Se i valori originali seguono però una distribuzione normale è possibile trasformarli in modo che seguano una distribuzione normale standard (con media nulla e varianza unitaria). In questo caso la funzione specifica mappa \[x\rightarrow\frac{x-\mu}{\sigma}\]

Un'ulteriore alternativa è data dalla normalizzazione. In questo caso i dati vengono modificati in modo che il range a cui appartengono sia \([0,1]\) oppure \([-1,1]\) senza però modificarne la distribuzione. Tipicamente viene effettuata questa scelta affinché le scelte effettuata dall'algoritmo non siano influenzate dalla scala dei dati. In questo caso la funzione specifica mappa \[x\rightarrow\frac{x-x_{min}}{x_{max}-x_{min}}\] oppure \[x\rightarrow\frac{x- \frac{x_{max} + x_{min}}{2}}{\frac{x_{max} - x_{min}}{2}}\]

\section{Somiglianza e diversità}\label{sec:somiglianza_diversita}
Si dice \textbf{somiglianza} una misura numerica di quanto due oggetti siano simili. Tipicamente essa ricade all'interno dell'intervallo \([0,1]\), dove \(1\) indica la massima somiglianza.\\
La \textbf{diversità} indica invece quanto due oggetti siano differenti. In questo caso tipicamente essa ricade nell'intervallo \([0,k]\), dove \(k\) è un valore dipendente dall'applicazione che indica il massimo grado di dissomiglianza.\\
Il termine \textbf{prossimità} può riferirsi, a seconda del contesto, sia alla somiglianza che alla diversità.

Dati due valori \(p\) e \(q\) a seconda del tipo di dato si possono definire somiglianza e diversità in vari modi:
\begin{itemize}
    \item Per gli attributi nominali si ha:
          \begin{itemize}
              \item \(s=\begin{cases} 1 & \mbox{se } p = q    \\
                        0 & \mbox{if } p \neq q\end{cases}\)
              \item \(d=\begin{cases} 0 & \mbox{se } p = q    \\
                        1 & \mbox{if } p \neq q\end{cases}\)
          \end{itemize}
    \item Per gli attributi ordinali (mappati su valori numerici nell'intervallo \([0,V-1]\)) si ha:
          \begin{itemize}
              \item \(s=1-\frac{|p-q|}{V-1}\)
              \item \(d=\frac{|p-q|}{V-1}\)
          \end{itemize}
    \item Per gli intervalli o i rapporti si ha:
          \begin{itemize}
              \item \(s=\frac{1}{1+d}\)
              \item \(s=1-\frac{d-\min(d)}{\max(d)-\min(d)}\)
              \item \(d=|p-q|\)
          \end{itemize}

\end{itemize}

\subsection{Distanza}
Qualsiasi sia il tipo di distanza scelto, vi sono delle proprietà comuni:
\begin{enumerate}
    \item \(dist(p,q)\geq 0 \ \forall p,q\)
    \item \(dist(p,q)=dist(q,p)\)
    \item \(dist(p,q)\leq dist(p,r)+ dist(q,r)\ \forall p,q,r\)
\end{enumerate}
\subsubsection{Distanza euclidea}
Quando ci si riferisce alla diversità spesso si fa riferimento alla distanza euclidea (\(L_2\))
\[L_2 = \sqrt{\sum_{d=1}^{D}{{(p_d-q_d)}^2}}\]
dove \(D\) è il numero di dimensioni che caratterizza lo spazio in cui sono espressi i dati, mentre \(p_d\) e \(q_d\) sono i \(d\)-esimi attributi degli oggetti \(p\) e \(q\).

Tipicamente se le scale dei dati differiscono occorre effettuare un passo di standardizzazione (se i dati seguono entrambi una gaussiana) o di normalizzazione.

\subsubsection{Distanza di Minkowski}
Un caso più generale è dato dalla distanza di Minkowski
\[dist={(\sum_{d=1}^{D}{|p_d-q_d|^r})}^{\frac{1}{r}}\]
In questo caso \(r\) è un parametro scelto dipendentemente dal dataset e dall'applicazione finale.

Nel caso in cui si abbia \(r=1\) si ha la distanza di Manhattan (\(L_1\)). Essa è il modo migliore per distinguere gli oggetti con distanza pari a \(0\) dagli oggetti con distanza circa \(0\) in quanto qualsiasi variazione nella distanza tra i due oggetti si riflette direttamente sulla distanza complessiva. Per questo fatto e per la sua semplicità è migliore della distanza euclidea nei casi in cui si abbia a che fare con spazi aventi molte dimensioni.

Nel caso in cui si abbia \(r=2\) si ricade nella distanza euclidea.

Nel caso in cui si abbia \(r=\infty\) si ha la distanza di Chebyshev (\(L_\infty\)) la quale considera solamente la dimensione in cui la differenza è massima, dando una valutazione semplificata scartando tutte le altre dimensioni.
\[dist_\infty = \lim_{r\rightarrow +\infty}{{(\sum_{d=1}^{D}{|p_d-q_d|^r})}^{\frac{1}{r}}} = \max_{d}{|p_d-q_d|}\]

\subsubsection{Distanza di Mahalanobis}
Questa distanza considera la distribuzione dei dati. La distanza di Mahalanobis tra due punti \(p\) e \(q\) decresce se mantenendo la medesima distanza euclidea il segmento che connette i due punti è parallelo alla direzione in cui è massima la variazione dei dati.

La distribuzione è descritta usando la matrice di covarianza dei dati \(\Sigma\):
\[dist_m=\sqrt{(p-q)\Sigma^{-1}{(p-q)}^T}\]

\begin{esempio}{}{}
    \begin{center}
        \includegraphics[width=0.5\textwidth]{mahalanobis.png}
    \end{center}
    Si hanno:
    \[A=(0.5,0.5)\quad B=(0,1)\quad C=(1,1)\]
    \[\Sigma = \begin{bmatrix}
            0.3 & 0.2 \\
            0.2 & 0.3 \\
        \end{bmatrix}\]
    Si ha:
    \[L_2(A,B) = 0.707\quad dist_m(A,B)=2.236\]
    \[L_2(A,C) = 0.707\quad dist_m(A,C)=1\]
\end{esempio}

\paragraph{Matrice delle covarianze}
Tale matrice è una generalizzazione della covarianza al caso di dimensione dello spazio maggiore di due. Rappresenta la variazione di ogni variabile rispetto alle altre (inclusa se stessa). È una matrice simmetrica.
\[\Sigma_{ij}=\frac{1}{N-1}\sum_{k=1}^{N}{(e_{ki}- \bar{e_i})(e_{kj}-\bar{e_j})}\]

La diagonale della matrice contiene la varianza di ogni variabile. In generale una cella è positiva se le due variabili crescono assieme, mentre è negativa se mentre una cresce l'altra decresce. Nel caso in cui le variabili siano rappresentate da una gaussiana standard nella diagonale tutte le varianze sono unitarie. Se la matrice è diagonale allora le variabili sono indipendenti, mentre le nel caso di variabili indipendenti distribuite secondo una gaussiana standard si ha una matrice identità e la distanza di Mahalanobis coincide con la distanza euclidea.

\subsection{Somiglianza}
La somiglianza gode di alcune proprietà generali:
\begin{enumerate}
    \item \(sim(p,q)=1 \mbox{se e solo se } p=q\)
    \item \(sim(p,q)=sim(q,p)\)
\end{enumerate}

\subsubsection{Somigliaza tra vettori binari}
Nel caso di attributi binari si può procedere come segue. Detti:
\begin{itemize}
    \item \(M_{00}\) il numero di attributi per cui \(p=0\) e \(q=0\)
    \item \(M_{01}\) il numero di attributi per cui \(p=0\) e \(q=1\)
    \item \(M_{10}\) il numero di attributi per cui \(p=1\) e \(q=0\)
    \item \(M_{11}\) il numero di attributi per cui \(p=1\) e \(q=1\)
\end{itemize}
si possono descrivere i coefficienti:
\begin{itemize}
    \item Simple Matching coefficient (SMC): \(SMC=\frac{M_{00}+M_{11}}{M_{00}+M_{01}+M{10}+M_{11}}\)
    \item Coefficiente di Jaccard (JC): \(JC=\frac{M_{11}}{M_{01}+M{10}+M_{11}}\)
\end{itemize}
\subsubsection{Caso generale}
Nel caso più generale è possibile utilizzare altre metriche:
\begin{itemize}
    \item Somiglianza cosinusoidale: \(cod(p,q)=\frac{p\cdot q}{\left\|p\right\|\left\|q\right\|}\). Rappresenta il coseno dell'angolo compreso tra i due vettori
    \item Coefficiente di Jaccard esteso (Tanimoto): \(T(p,q)=\frac{p\cdot q}{\left\|p\right\|+\left\|q\right\|-p\cdot q}\)
\end{itemize}

\subsection{Correlazione}
In generale una correlazione è una relazione tra due variabili tale per cui a ciascun valore della prima corrisponda un valore della seconda, seguendo una certa regolarità. Nel caso in cui due variabili siano indipendenti il coefficiente di correlazione è nullo, mentre il fatto che il coefficiente di correlazione tra due variabili sia nullo non implica che esse siano indipendenti.
Inoltre in generale valori positivi implicano una correlazione lineare positiva (Figura~\ref{fig:correlazione}.)
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{correlazione.png}
    \caption{Esempio di correlazione di una nuvola di punti}\label{fig:correlazione}
\end{figure}

Il calcolo della correlazione per dati numerici può essere eseguito in questo modo:
\begin{enumerate}
    \item Per due attributi \(p\) e \(q\) considerare come vettori la lista ordinata dei valori per tutti i record
    \item Standardizzazione dei valori dei vettori ottenendo i vettori \(p'\) e \(q'\)
    \item La correlazione è data dal prodotto scalare tra i due vettori, ovvero
          \[corr(p,q)=p'\cdot q'\]
\end{enumerate}

Per quanto riguarda i dati nominali è possibile calcolare la correlazione attraverso il calcolo dell'incertezza simmetrica (symmetric uncertainty):
\[U(p,q)=2\frac{H(p)+H(q)-H(p,q)}{H(p)+H(q)}\quad 0\leq U(p,q)\geq 1 \forall p,q\]
dove \(H()\) è l'entropia del singolo attributo mentre \(H(,)\) è l'entropia comune dei due attributi.