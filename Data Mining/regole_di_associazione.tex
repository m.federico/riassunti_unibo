Le regole di associazione sono un'altra metodologia di apprendimento non supervisionata che mira ad imparare relazioni tra i dati a partire dai dati stessi. In questi casi diventa fondamentale una misura della qualità dei risultati
\section{Introduzione alla ``analisi del carrello''}
Questo problema è stato il capostipite dell'analisi delle regole di associazione. Dato un insieme di transazioni commerciali, questa analisi si pone il problema di trovare regole che possano predire la presenza di elementi in carrelli futuri basate sul contenuto dei carrelli passati.

In questo caso possiamo considerare l'intero inventario del supermercato come se fosse l'inventario (spesso chiamato \textit{universo}) ed una transazione riguarda un piccolo sottoinsieme degli elementi facenti parte dell'insieme universo. Come semplificazione si considera solamente la presenza degli elementi all'interno del carrello e non la loro quantità.

Possiamo descrivere una transazione come un vettore di bit la cui cardinalità coincide con quella dell'insieme universo. Solamente i bit associati agli elementi presenti nel carrello saranno posti ad 1, mentre gli altri sono posti a 0. Un'altra modalità per rappresentare una transazione è quella che vede l'uso di una tabella delle transazioni (vedi figura~\ref{fig:market_basket_1}). In questo caso ogni riga è associata ad una transazione avente un proprio \(TID\). Si noti che questa tabella non è una tabella relazionale, in quanto la seconda colonna (quella contenente la lista degli elementi) non è in forma normale.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.5\textwidth]{market_basket_1.png}
    \caption{Insieme di transazioni}\label{fig:market_basket_1}
\end{figure}

Dalle transazioni in figura~\ref{fig:market_basket_1} si possono derivare alcune regole di associazione:
\begin{itemize}
    \item \(\{Diaper\} \rightarrow \{Beer\}\)
    \item \(\{Bread, Milk\}\rightarrow \{Coke, Eggs\}\)
    \item \(\{Beer, Bread\}\rightarrow \{Milk\}\)
\end{itemize}
Queste regole possono essere interpretate come ``se nel carrello sono presenti gli elementi sulla sinistra allora è probabile che siano presenti anche quelli sulla destra''. Si noti l'espressione ``\textit{è probabile che}'', infatti queste regole sono diverse dall'implicazione logica, sono regole vere entro un certo livello di tolleranza. Infine sono regole che indicano la co-occorrenza di due eventi e non implicano in alcun modo causalità di alcun tipo.

\begin{definizione}{Itemset}{}
    Definiamo itemset un insieme di uno o più elementi
\end{definizione}

\begin{esempio}{itemset}{}
    \[\{Milk, Bread, Diaper\}\]
\end{esempio}

\begin{definizione}{k-Itemset}{}
    Definiamo itemset un insieme di k elementi
\end{definizione}

\begin{definizione}{Regola di associazione}{}
    Definiamo regola di associazione una regola della forma \(A\Rightarrow C\) dove \(A\) e \(C\) sono itemset detti rispettivamente antecedente e conseguente
\end{definizione}

\begin{esempio}{regola di associazione}{}
    Riprendendo l'esempio in figura~\ref{fig:market_basket_1} si ha:
    \[\{Milk,Diaper\} \Rightarrow \{Beer\}\]
\end{esempio}

\begin{definizione}{Support count}{}
    Definiamo support count (\(\sigma\)) come la frequenza di occorrenza di un itemset all'interno dell'intero insieme delle transazioni
\end{definizione}

\begin{esempio}{support count}{}
    Riprendendo l'esempio in figura~\ref{fig:market_basket_1} si ha:
    \(\sigma(\{Milk, Bread, Diaper\}) = 2\)

    Questo indicatore è antimonotono, ovvero può solo diminuire all'aumentare della cardinalità dell'insieme valutato
\end{esempio}

\begin{definizione}{Support}{}
    Definiamo support (\(sup\)) come la frequenza relativa di un itemset all'interno dell'intero insieme delle transazioni
\end{definizione}

\begin{esempio}{support}{}
    Riprendendo l'esempio in figura~\ref{fig:market_basket_1} si ha:
    \(sup(\{Milk, Bread, Diaper\}) = 2/5\)
\end{esempio}

\begin{definizione}{Confidence}{}
    Definiamo confidence (\(conf\)) di una regola di associazione come
    \[\frac{\sigma(A\cup C)}{\sigma(A)}\]
\end{definizione}

\begin{esempio}{confidence}{}
    Riprendendo l'esempio in figura~\ref{fig:market_basket_1} si ha:
    \[conf(\{Milk,Diaper\} \Rightarrow \{Beer\})= \frac{\sigma(\{Milk,Diaper,Beer\})}{\sigma(\{Milk,Diaper\})}\]
\end{esempio}

\begin{definizione}{Itemset frequente}{}
    Definiamo itemset frequente un itemset il cui support è superiore ad un certo valore \(minsup\)
\end{definizione}

I valori di \(sup\) e \(conf\) sono importanti indicatori di interesse nei confronti di regole. Nel caso in cui una regola abbia un basso livello di support potrebbe essere stata prodotta da un'associazione casuale e quindi poco interessante ai fini della previsione di comportamenti futuri. Regole aventi un baso livello di confidence invece sono poco affidabili in quanto è vero che sono situazioni successe e reali, ma sono probabili. Un caso diverso riguarda quelle regole aventi un basso valore di support, ma al contempo un elevato livello di confidence. Queste regole rappresentano eventi rari ma piuttosto interessanti.

Si può notare infine che le regole aventi origine nel medesimo itemset hanno il medesimo livello di support ma differiscono per il valore di confidence.

\begin{esempio}{}{}
    Riprendendo l'esempio in figura~\ref{fig:market_basket_1} si ha:
    \begin{align*}
        \{Diaper, Milk\} & \Rightarrow \{Beer\}         & sup=0.4\quad & conf=0.67 \\
        \{Beer, Milk\}   & \Rightarrow \{Diaper\}       & sup=0.4\quad & conf=1    \\
        \{Beer, Diaper\} & \Rightarrow \{Milk\}         & sup=0.4\quad & conf=0.67 \\
        \{Beer\}         & \Rightarrow \{Diaper, Milk\} & sup=0.4\quad & conf=0.67 \\
        \{Diaper\}       & \Rightarrow \{Beer, Milk\}   & sup=0.4\quad & conf=0.5  \\
        \{Milk\}         & \Rightarrow \{Beer, Diaper\} & sup=0.4\quad & conf=0.5  \\
    \end{align*}
\end{esempio}

\medskip
Scopo di un algoritmo è quindi trovare tutte quelle regole che sono interessanti, ovvero aventi dei valori minimi di support e confidence di almeno \(minsup\) e \(minconf\) rispettivamente. Questi sono iperparametri dell'algoritmo, pertanto il risultato dipende dalla loro corretta valutazione. In generale regole aventi support troppo alto non sono di particolare interesse in quanto sono talmente tanto frequenti da essere quasi scontate.

Per prima cosa si potrebbe pensare ad un approccio a forza bruta, ovvero generare tutte le possibili regole di associazione e calcolare i valori di support e confidence per ognuna di esse eliminando tutte quelle che non soddisfano i criteri imposti. In realtà questo approccio è proibitivo in quanto il numero di regole possibile cresce esponenzialmente con \(O(3^D)\) (in realtà il numero esatto è \(3^D-2^{D+1} + 1\)) dove \(D\) è la cardinalità dell'inventario.

È evidente la necessità di dotarsi di un approccio a due fasi
\begin{enumerate}
    \item \textbf{Frequent Itemset Generation} genera tutti gli itemset aventi support maggiore di \(minsup\)
    \item \textbf{Rule Generation} genera regole aventi alti livelli di confidence a partire da itemset particolarmente frequenti mediante partizionamento binario dell'itemset stesso
\end{enumerate}

\section{Generazione del frequent itemset}
Introduciamo un esempio che accompagnerà il resto di questo sotto paragrafo.
\begin{esempio}{}{frequent_itemset_generation_1}
    Supponiamo di avere un inventario contenente \(D\) oggetti. Da ciò si evince che vi sono \(2^D\) possibili itemset differenti.
    \begin{center}
        \includegraphics[width=0.95\textwidth]{frequent_itemset_generation_1.png}
    \end{center}
\end{esempio}

L'approccio a forza bruta procede a generare ogni itemset nel grafo nell'esempio~\ref{es:frequent_itemset_generation_1} e quindi a calcolare il valore degli indici \(sup\) e \(conf\) per ognuna delle regole di associazione derivanti da ognuno degli itemset. La complessità computazionale diviene così dell'ordine di \(O(NWM)\) dove \(N\) è il numero complessivo di transazioni, \(W\) è il numero medio di elementi all'interno di una transazione e \(M\) è il numero di candidati. È possibile calcolare il numero complessivo di regole di associazione come
\[
    R=\sum_{k=1}^{D-1}  {(\binom{D}{k}\times \sum_{j=1}^{D-k}{\binom{D-k}{j}})} = 3^D -2^{D+1}+1
\]

Per quanto riguarda la generazione degli itemset frequenti è possibile rifarsi ad alcune strategie di base. In primo luogo è possibile diminuire il numero di candidati complessivo \(M\) dal valore iniziale \(2^M\) mediante tecniche di pruning. In secondo luogo è possibile ridurre il numero di confronti dall'originario valore \(NM\) usando strutture dati efficienti per memorizzare i candidati e le transazioni in modo tale de rendere non necessario confrontare ogni candidato possibile con ogni transazione.

\subsection{Algoritmo Apriori}
Per ridurre il numero di candidati è possibile usare il principio \textbf{Apriori}. Esso sfrutta il fatto che la funzione \(sup\) è antimonotona, quindi dato un itemset frequente tutti i suoi sottoinsiemi devono essere di conseguenza frequenti a loro volta (questo perché al diminuire degli elementi all'interno dell'itemset si ottiene un insieme avente meno condizioni da soddisfare la cui frequenza è quindi maggiore). Formalmente può essere descritto come:
\[
    \forall X,Y: (X\subseteq Y ) \Rightarrow sup(X) \ge sup(Y)
\]
Risulta vera anche l'affermazione opposta, ovvero dato un itemset se esso risulta poco frequente allora anche tutti gli insiemi di cui esso risulta un sottoinsieme saranno poco frequenti a loro volta. Questo fatto consente di potare parte del grafo precedente.
\begin{esempio}{}{}
    Supponiamo di aver capito che l'itemset \(\{AB\}\) sia poco frequente, ne consegue che tutti gli itemset racchiusi nel cerchio rosso (ovvero itemset aventi tra i sottoinsiemi proprio \(\{AB\}\)) sono poco frequenti e quindi possono essere potati.
    \begin{center}
        \includegraphics[width=0.8\textwidth]{frequent_itemset_generation_pruning.png}
    \end{center}
\end{esempio}

\begin{algoritmo}{Apriori}{apriori}
    Definiamo \(C_k\) come l'insieme dei candidati di dimensione \(k\),  \(L_k\) l'insieme degli itemset frequenti di dimensione \(k\) e \(subset_k(c)\) l'insieme dei sottoinsiemi di \(c\) aventi \(k\) elementi.

    L'algoritmo è diviso in due fasi
    \begin{enumerate}
        \item \textbf{Join}. Rappresentiamo \(L_k\) come una tabella avente \(k\) colonne ed in cui ogni riga contiene un itemset frequente. Supponiamo che gli elementi in ogni riga siano ordinati in ordine lessicografico. Il valore di \(C_{k+1}\) è generato come un self join su \(L_k\) del tipo:
              \begin{lstlisting}
            insert into C_{k+1}
            select item p.item_1, p.item_2, ..., p.item_k, q.item_k
            from L_k p, L_k q
            where p.item_1 = q.item_1 and ... and p.item_{k-1} = q.item_{k-1} and p.item_k < q.item_k
        \end{lstlisting}
        \item \textbf{Pruning}. Ogni itemset \(k+1\)-esimo che include un itemset che non è incluso in \(L_k\) è rimosso da \(C_{k+1 }\)
    \end{enumerate}

    L'algoritmo può quindi essere riassunto come:
    \begin{lstlisting}
        L_1 <- itemset di dimensione k frequenti
        k <- 1

        while L_k != {} do
            C_{k+1} = candidati generati da L_k
            calcola support per ogni elemento di C_{k+1}
            L_{k+1} <- {c} in C_{k+1}: sup(c) >= minsup
            k <- k + 1 

        return k, L_k
    \end{lstlisting}

    Il nome dell'algoritmo deriva dal fatto che ogni calcolo è fatto sulla base della conoscenza pregressa (a priori) acquisita dalla valutazione del livello precedente.

    La complessità dell'algoritmo è influenzata
    \begin{itemize}
        \item dal valore di \(minsup\). Al diminuire di questo valore aumenta il numero di itemset frequenti e diminuisce l'efficacia del pruning
        \item dalle dimensioni dell'inventario che si riflette nella necessità di spazio per memorizzare il conteggio di ognuno degli item e nell'incremento del numero di item frequenti
        \item dalle dimensioni del database ovvero dal numero di transazioni
        \item dalla dimensione media di una transazione che si traduce nell'incremento della lunghezza media degli itemset frequenti e quindi nel numero di sotto insiemi frequenti
    \end{itemize}
\end{algoritmo}

\begin{esempio}{pruning}{}
    \begin{center}
        \includegraphics[width=0.7\textwidth]{frequent_itemset_generation_apriori.png}
    \end{center}
    Utilizzando il pruning si nota che il numero di itemset da valutare passa da \(41\)  a \(13\) dato che vengono eliminati tutti quelli discendenti da un itemset non abbastanza frequente.
\end{esempio}

Anche dopo aver filtrato il dataset sulla base del valore di \(sup\) il numero di itemset frequenti può essere imponente, per questo motivo risulta utile identificare un piccolo numero di itemset frequenti e rappresentativi:
\begin{itemize}
    \item \textbf{Maximal frequent itemset} ovvero il più piccolo insieme di itemset da cui è possibile derivare per intero gli itemset frequenti. Un MFI non ha alcun sovra insieme immediato frequente (vedi figura~\ref{fig:frequent_itemset_generation_mfi}) e si trova dunque vicino al bordo divisivo tra itemset frequenti e non frequenti. Questo insieme da una rappresentazione compatta di tutti gli itemset frequenti. Esistono algoritmi in grado di trovarli senza effettuare l'enumerazione dei relativi sottoinsiemi
    \item \textbf{Closed itemset} ovvero la minima rappresentazione degli itemset senza perdete informazioni riguardo al valore di \(sup\). Un itemset è \textbf{chiuso} se nessuno dei suoi sovra insiemi ha il medesimo valore di support.
\end{itemize}

\begin{figure}[h!b]
    \centering
    \includegraphics[width=0.8\textwidth]{frequent_itemset_generation_mfi.png}
    \caption{MFI relativamente all'esempio~\ref{es:frequent_itemset_generation_1}}\label{fig:frequent_itemset_generation_mfi}
\end{figure}

Si può dimostrare che i \textbf{maximal frequent itemset} sono un sottoinsieme dei \textbf{closed frequent itemset} che a loro volta sono un sottoinsieme dei \textbf{frequent itemset}.

\subsection{Algoritmo FP-Growth}
Il grosso problema dell'algoritmo apriori è il fatto che ha la necessità di generare gli insiemi dei candidati (\(C_k\)), il cui numero può essere anche elevatissimo, e per far ciò ha la necessità di scorrere il database numerose volte per effettuare i calcoli relativi al valore di support per ogni candidato.

L'algoritmo FP-Growth invece trasforma il problema da trovare pattern lunghi e frequenti a trovare pattern più brevi e concatenare questi con dei suffissi.
L'algoritmo usa una rappresentazione compatta del database mediante un FP-Tree, sul quale una volta costruito viene usato un approccio ricorsivo del tipo divide et impera per estrarre gli itemset frequenti.

\begin{algoritmo}{FP-Growth}
    La fase di costruzione dell'albero può essere riassunta come una serie di passi:
    \begin{enumerate}
        \item Scansiona il database per calcolare il valore di support di ogni itemset contenente un singolo elemento
        \item Crea la radice dell'albero
        \item Scansiona il database e per ogni transazione:
              \begin{enumerate}
                  \item Riordina gli elementi secondo valori di support discendenti
                  \item Spostati alla radice dell'albero
                  \item Per ogni item della transazione ordinata
                        \begin{itemize}
                            \item Se coincide con uno dei figli del nodo attuale spostati su di esso ed aumentane il contatore associato di 1
                            \item Altrimenti crea un nuovo nodo figlio avente contatore inizializzato ad 1
                        \end{itemize}
              \end{enumerate}
    \end{enumerate}

    Al contempo occorre tener traccia delle posizioni di ogni item mediante una lista linkata

    \begin{center}
        \includegraphics[width=0.8\textwidth]{frequent_itemset_generation_fp_growth.png}
    \end{center}

    Un fatto interessante è che la struttura dell'albero dipende unicamente dall'ordine in cui vengono valutate le transazioni. Di conseguenza nel caso in cui le transazioni vengano valutate secondo l'ordine dato dal valore di support si ottiene la rappresentazione più compatta possibile dell'albero.

    \medskip
    Dato un valore minimo per il valore di support e un albero FP è possibile generare l'insieme degli itemset frequenti. Utilizzando le liste linkate è possibile trovare tutte le catene che terminano con un dato item. Per ognuna di esse occorre risalire alla radice a partire dal nodo trovato seguendo la lista e nel fare questo calcolare il minimo valore del contatore. Se il valore finale ottenuto è superiore alla soglia prestabilita allora la catena rappresenta un itemset frequente
\end{algoritmo}

Se con l'algoritmo apriori si ha la necessità di fare la scansione di tutto il database contando il support e il numero di volte che compare la dimensione del più grande Itemsets + 1. Con FP-tree c'è bisogno solo di scansionare una sola volta perché basta un solo passaggio per costruire l'intero albero. Ovviamente sono interessanti le performance di questo algoritmo: cambiano in base a come cambia la soglia, più la soglia di support è piccola più pattern si genereranno (vedi~\ref{fig:frequent_itemset_generation_fp_growth_perf}).

\begin{figure}[h!bt]
    \centering
    \includegraphics[width=0.4\textwidth]{frequent_itemset_generation_fp_growth_perf.png}
    \caption{Confronto dei tempi di esecuzione al variare della soglia su un dataset sparso}\label{fig:frequent_itemset_generation_fp_growth_perf}
\end{figure}

\section{Generazione delle regole}
Trovati i gli itemset frequenti bisogna estrarre le regole. Gli itemset frequenti sono generatori di regole perché le regole sono interessanti solo se sono generate dal Frequent itemset. Per generare le regole ci si appoggia ai valori di support e confidence.
\[
    conf(A\rightarrow C)=\frac{sup(A\rightarrow C)}{sup(A)}
\]

Per il pruning basato sulla confidenza è sufficiente sapere il valore di support degli Itemset frequenti. Dato un itemset frequente \(L\), il problema è trovare tutti i subset non vuoti \(f\) per cui il valore di \(conf(f\Rightarrow L - f)\) non è minore della confidenza minima. Per questo motivo dato \(\{Beer, Diaper, Milk\}\) le possibili regole sono:
\begin{align*}
    \{Beer, Diaper\} \rightarrow \{Milk\} \\
    \{Beer, Milk\} \rightarrow \{Diaper\} \\
    \{Milk, Diaper\} \rightarrow \{Beer\} \\
    \{Beer\} \rightarrow \{Milk, Diaper\} \\
    \{Diaper\} \rightarrow \{Milk, Beer\} \\
    \{Milk\} \rightarrow \{Beer, Diaper\} \\
\end{align*}

Se la dimensione di \(|L| = k\) allora ci sono \(2^k - 2\) regole possibili (ogni elemento può trovarsi a destra o a sinistra della freccia ed escludiamo il cado in cui siano tutti a destra o tutti a sinistra).

Ci si pone ora il problema circa la generazione efficiente delle regole a partire da un itemset frequente. In generale però la confidenza non è antimonotona, ma gode di una proprietà simile. Se si considerano le regole generate dallo stesso itemset \(i = \{A, B, C, D\} appartenenti a L\) si ha:
\[
    conf(ABC \rightarrow D) \ge conf(AB \rightarrow CD) \ge conf(A \rightarrow BCD)
\]
quando si sposta un oggetto da sinistra a destra si cambia il denominatore della formula per il calcolo della confidenza, si incrementa il support e si diminuisce il valore della confidenza. Questo rende possibile la potature del grafo contenente tutte le possibili regole. Osservando la figura~\ref{fig:rule_generation_1}, si parte dal grafo che contiene tutte le regole generate partendo da un itemset. Se una regola ha un valore di confidenza sotto la soglia allora anche i suoi discendenti, che sono ottenuti spostando un oggetto da sinistra a destra, avranno indice inferiore alla soglia.
\begin{figure}[h!bt]
    \centering
    \includegraphics[width=0.8\textwidth]{rule_generation_1.png}
    \caption{Pruning del grafo contenente tutte le regole ottenibili dall'itemset \(\{A,B,C,D\}\)}\label{fig:rule_generation_1}
\end{figure}

Anche utilizzando l'algoritmo apriori si possono generare regole in modo equivalente mediante join. La regola candidata è generata dal merging di due regole che condividono lo stesso prefisso nella regola conseguente.
\begin{itemize}
    \item Il join \((CD \rightarrow AB,\ BD \rightarrow AC)\) produrrà la regola \(D \rightarrow ABC\)
    \item La regola \(D \rightarrow ABC\) sarà potata se il subset \(AD \rightarrow BC\) non ha valore di confidenza alta
\end{itemize}

Ovviamente è necessario impostare una soglia per il supporto \(minsup\) e una soglia per la confidenza \(minconf\) in maniera sensata. In generale la distribuzione del support del singolo oggetto è un indice asimmetrico. Osservando la figura sotto se si ordinano gli oggetti in ordine decrescente rispetto al support e si rappresenta sull'asse y il support count è molto comune avere una distribuzione come quella in figura che implica che ci sono molti item con valore di support molto alto e molti con valore molto basso. La scelta delle soglie è cruciale perché se minsup è troppo bassa, il calcolo dell'algoritmo diventa computazionalmente costoso e la dimensione degli itemset è molto grande. Per questo motivo si vuole essere in grado di usare diversi valori di soglia minsup.

\begin{figure}[h!bt]
    \centering
    \includegraphics[width=0.4\textwidth]{distribuzione_support.png}
    \caption{Molti dataset reali hanno una distribuzione dell'indice support asimmetrica}\label{fig:distribuzione_support}
\end{figure}

\subsubsection{Multiple Minimum Support}
Ci si può chiedere come scegliere il valore corretto di \(minsup\) o se occorra sceglierne per forza solamente uno. In alcuni casi potrebbe non essere sufficiente usare un singolo valore in quanto la situazione da modellare potrebbe essere complessa, in questo caso è possibile definire più valore minimi di support, anche uno per ogni item in modo da poter aggiustare la soglia di support minimo basandosi sulla conoscenza pregressa. In questo ultimo caso tale valore può essere relazionato all'oggetto stesso.

Come maneggiare però quelle transazioni che includono oggetti con soglie differenti? Si può certamente scegliere il minimo delle soglie \(minsup\) tra tutti gli oggetti, ma in questo modo il support non è più antimonotono. Si può progettare una variante dell'algoritmo per generare gli itemset frequenti che tenga in conto questo support minimo. In questo caso l'idea di base è quella di ordinare gli oggetti secondo il loro minimo support (in ordine ascendente) e quindi modificare l'algoritmo Apriori nel seguente modo:
\begin{algoritmo}{Apriori con support variabile}{}
    Detti \(L_1\) l'insieme degli item frequenti, \(F_1\) l'insieme degli item il cui valore di support è \(MS(1)=\min_i{(MS(i))}\) e \(C_2\) l'insieme degli itemset candidati di dimensione 2 generato da \(F_1\) e \(L_1\)

    Nell'algoritmo Apriori tradizionale, un elemento candidato \((k + 1)\)-esimo viene generato unendo due itemset frequenti di dimensione \(k\) e il candidato viene eliminato se contiene sottoinsiemi poco frequenti di dimensione k. In questo caso va modificato la fase di pruning, nella quale il pruning avviene solo se si trova un sottoinsieme non frequente che contiene il primo elemento dell'itemset stesso.
\end{algoritmo}

\subsubsection{Valutazione dei pattern}
Vediamo ora come valutare i pattern ottenuti a partire dagli algoritmi precedenti. Essi infatti generano un numero elevato di regole, delle quali spesso molte sono ridondanti o poco interessanti.
\begin{esempio}{}{}
    Le regole
    \[
        \{A,B,C\} \rightarrow \{D\}\quad \{A,B\} \rightarrow \{D\}
    \]
    sono ridonandoti nel caso abbiano i medesimo valori di support e confidence
\end{esempio}
L'obiettivo è, tramite parametri specifici, di potare/valutare i pattern derivati. Le misure di interesse sono applicate in diversi passaggi della derivazione della conoscenza a partire dai dati grezzi e non solamente in fase finale. Infatti dopo ogni fase è possibile valutare il risultato intermedio ottenuto per ottenere la miglior valutazione finale.

Per ottenere queste misure di interesse è necessario costruire una \textit{contingency table} basata sulle regole ottenute dall'esecuzione degli algoritmi. \\
Dalla regola \(A \rightarrow C\) si ottiene la contingency table in figura~\ref{fig:contingency_table} i cui elementi sono le basi per la maggior parte delle misure di interesse.
\begin{figure}[h!bt]
    \centering
    \includegraphics[width=0.4\textwidth]{contingency_table.png}
    \caption{Contingency table}\label{fig:contingency_table}
\end{figure}

\begin{esempio}{}{}
    Supponiamo di avere la seguente contingency table
    \begin{center}
        \includegraphics[width=0.4\textwidth]{esempio_contingency_table.png}
    \end{center}
    Potrebbe sembrare che la regola \(\{Tea\}\rightarrow\{Coffee\}\) abbia un valore piuttosto alto di confidence
    \[
        conf(\{Tea\}\rightarrow\{Coffee\}) = \frac{sup(Tea, Coffee)}{sup(Tea)}=\frac{15}{20}=0.75
    \]
    Ma dalla tabella si nota che la probabilità di trovare caffè nel carrello ammonta a \(0.9\) e che la probabilità di trovare caffè ma non tè ammonta a circa il \(0.94\)
    \[
        P(Coffee) = 0.9,\quad P(Coffee | \bar{Tea}) = \frac{75}{80}=0.9375
    \]

    Nonostante l'alto valore di \(conf\) l'alta probabilità dell'assenza di tè incrementa la probabilità del caffè, dunque il valore iniziale di confidence è fuorviante
\end{esempio}

Sono necessarie nuove misure perché a volte la confidenza potrebbe essere ingannevole. Per questo motivo si usa il concetto di \textbf{indipendenza statistica}. Il problema insito nel considerare la probabilità invece della confidenza è che la nozione di indipendenza statistica è molto forte e comporta l'assunzione forte che il valore trovato sia la probabilità vera e non la frequenza. Considerando la nozione di intervallo di confidenza, si può esprimere la probabilità come la frequenza più o meno qualcosa ma in questo modo quando il numero diventa piccolo questa probabilità ha un'alta incertezza e se si moltiplicano le probabilità con l'incertezza si ha un'amplificazione della stessa. \\
Per questo motivo, l'utilizzo della probabilità solo per trovare regole interessanti non è una soluzione valida. È più utile filtrare le regole usando support e confidenza, quindi con una fase di post-elaborazione possiamo usare la nozione di indipendenza statistica (o alcune nuove misure relative a questa) per fare un ulteriore filtraggio mediante:
\begin{itemize}
    \item \textbf{Lift}: è la confidenza di una regola diviso il support del conseguente. Potrebbe anche essere espressa come la probabilità degli eventi congiunti diviso per il prodotto della probabilità
          \[
              lift(A\rightarrow C) = \frac{conf(A\rightarrow C)}{sup(C)}=\frac{P(A \cap C)}{P(A)P(C)}
          \]
          Questo indice ha valore \([0,1]\) dove \(1\) si raggiunge solo in caso di completa indipendenza. È un valore che non tiene conto della direzione delle regole. Maggiore è il valore maggiore è la probabilità che la regola \textbf{non} sia una coincidenza
    \item \textbf{Laverage}: vale 0 nel caso di completa indipendenza, è positiva per correlazione positiva e negativa per correlation correlazione. Non è sensibile alla direzione della regola. Rappresenta il numero di casi in relazione alla indipendenza.
          \[
              leve(A\rightarrow C)=sup(A\cup C) - sup(A)sup(C)=P(A\cap C) - P(A)P(C)
          \]
    \item \textbf{Conviction}: rappresenta il rapporto della frequenza aspettata che \(A\) si presenti senza \(C\), è chiamata anche \textit{novelty}, è sensibile alla direzione della regola e risulta infinita se la regola è sempre vera
          \[
              conv(A\rightarrow C) =\frac{1-sup(C)}{1-conf(A\rightarrow C)} = \frac{P(A)(1-P(C))}{P(A)- P(A\cap C)}
          \]

          Maggiore è il valore meno la regola viene violata rispetto ai casi in cui antecedente e conseguente sono scorrelati
\end{itemize}

\begin{esempio}{}{}
    Riprendendo l'esempio precedente si ottengono i seguenti valori:
    \begin{itemize}
        \item \(conf(\{Tea\}\rightarrow\{Coffee\}) =\frac{15}{20}=0.75\) questo valore è apparentemente alto
        \item \(lift(\{Tea\}\rightarrow\{Coffee\}) =\frac{0.15}{0.9\cdot0.2}=0.83\) il valore è minore di \(1\) quindi non è interessante
        \item \(leve(\{Tea\}\rightarrow\{Coffee\}) =0.15-0.9\cdot0.2=-0.03\) il valore è minore di zero quindi la regola non è interessante
        \item \(conv(\{Tea\}\rightarrow\{Coffee\}) =\frac{1-0.9}{1-0.75}=0.4\) questo valore è basso (il valore massimo è \(+\infty\))
    \end{itemize}
\end{esempio}

Per concludere, le proprietà che una buona misura \(M\) deve soddisfare sono:
\begin{itemize}
    \item \(M(A, B)\) = 0 (or 1) se \(A\) e \(B\) sono statisticamente indipendenti
    \item \(M(A, B)\) cresce monotonamente con \(P(A, B)\) quando \(P(A)\) e \(P(B)\) rimangono invariate
    \item \(M(A, B)\) decrescono monotonamente con \(P(A)\) (o \(P(B)\)) quando \(P(A, B)\) e \(P(B)\) (o \(P(A)\)) rimangono invariate
\end{itemize}

Ci sono varie misure proposte in letteratura oltre a quelle presentate. La confidence è solitamente la misura di base, le altre misure possono essere usate per testare il risultato dato dalla confidenza e per filtraggi ulteriori.

\subsubsection{Regole di associazione multidimensionali}
Nei paragrafi precedenti è stato descritto come ottenere regole relazionali da database relazionali. Espandiamo ora questi concetti anche alle tabelle relazionali in generale. Questo significa non avere più regole binarie, ovvero che coinvolgono solo due attributi per volta.

\begin{esempio}{}{}
    \begin{center}
        \includegraphics[width = 0.4\textwidth]{regole_multidimensionali_1.png}
    \end{center}
    In tabella sono presenti tre attributi. Ogni componente può essere interpretato come un evento, per questo le possibili regole di associazione tra valori e attributi possono essere:
    \begin{align*}
        nationality=French&\rightarrow income=high  &[&sup=0.5,  &conf&=1]    \\
        income=high &\rightarrow nationality=French &[&sup=0.5,  &conf&=0.75] \\
        age=50&\rightarrow nationality=Italian      &[&sup=0.33, &conf&=1]    \\
    \end{align*}
\end{esempio}



Da ciò si deduce che, anche una tabella relazionale standard, può avere rappresentate informazioni che possono essere usate per ricavare regole di associazione.  A tal proposito si può ragionare in due modi:
\begin{itemize}
    \item \textbf{Mono-Dimensionale}: gli attributi sono gli oggetti nella transazione. L'evento è la transazione che è descritta come: ”gli oggetti \(A\), \(B\) e \(C\) sono insieme in una transazione”
    \item \textbf{Multi-Dimensionale}: l'evento è la tupla ed è descritto come: “l'attributo \(A\) ha valore \(a\), l'attributo \(B\) ha valore \(b\) e l'attributo \(C\) ha valore \(c\)”
\end{itemize}

In generale è possibile passare da una rappresentazione monodimensionale ad una multidimensionale tramite regole di trasformazione.

\medskip
L'approccio multidimensionale però soffre in caso di attributi quantitativi (vedi figura~\ref{fig:regole_multidimensionali_2}). Per esempio, si considerino gli attributi della tabella sotto, ci son troppi valori distinti per trasformazione (multi/mono). Molti software per le regole di associazione non riescono a maneggiare attributi quantitativi. Per risolvere questo problema si procede con la tecnica della discretizzazione perdendo però la possibilità di fare confronti e ordinare gli attributi.
\begin{figure*}[h!bt]
    \centering
    \includegraphics[width=0.4\textwidth]{regole_multidimensionali_2.png}
    \caption{Transazione con dati numerici}\label{fig:regole_multidimensionali_2}
\end{figure*}

\subsubsection{Regole di associazione multilivello}
Uno step ulteriore è quello di includere all'interno delle regole di associazione della conoscenza pregressa. Ciò permette di fare confronti tra gli oggetti del dataset a su più livelli (vedi figura~\ref{fig:regole_mutilivello}), permettendo di organizzare gli oggetti in strutture gerarchiche.
\begin{figure}[h!bt]
    \centering
    \includegraphics[width=0.8\textwidth]{regole_mutilivello.png}
    \caption{Gerarchia concettuale dei prodotti di un supermercato}\label{fig:regole_mutilivello}
\end{figure}

Questo permette di osservare le regole su più livelli. Spostandosi da regole specifiche a regole generali si guadagna una visione d'insieme che generalmente consente di avere un'idea più precisa di come le cose stiano andando globalmente. In questo caso in generale il valore di support delle regole cresce in quanto le regole includono categorie più ampie.
Al contrario, spostandosi da regole generali a regole specifiche, il valore di support diminuisce (scendendo potenzialmente sotto la soglia minima).

Per quanto riguarda il valore di confidence non è possibile trovare una regola specifica, in quanto in generale cambia sia il numeratore che il denominatore delle regole. Tuttavia è possibile dire che se una regola più specifica ha circa lo stesso valore di confidenza della sua corrispettiva più generale allora essa è ridondante.