\chapter{Introduzione}

\section{Introduzione alla sicurezza informatica}
Il mondo moderno, estremamente interconnesso e orientato ai dati, comprende anche il fatto che molte infrastrutture critiche ed irrinunciabili siano totalmente dipendenti dalla tecnologia e dall'informatica in generale. In questo caso eventuali danni a tali infrastrutture non solo comportano perdite economiche ma hanno anche ripercussioni sulla vita quotidiana di tutti noi\footnote{Basti pensare ad un blackout generale ed alle sue conseguenze sulle case e gli ospedali}.

La sicurezza informatica (\textit{Information Security}) si occupa di contrastare tutto ciò che riguarda azioni deliberate volte a provocare danni. Essa è diversa dalla \textit{safety}, pur traducibile in italiano con sicurezza, in quanto quest'ultima ha come scopo quello di contrastare eventi accidentali.\\
Esistono varie sfumature della sicurezza informatica come: sicurezza dell'informazione, IT security, cybersecurity. Quella più strategicamente critica è la cybersecurity in quanto mira a proteggere tutti quegli ambiti in cui un attacco informatico potrebbe causare un danno reale alla popolazione, anche un danno fisico.

Stime hanno dimostrato che il giro di affari dietro al cybercrimine si attesta attorno ad una cifra simile al PIL della Cina. Esso è particolarmente lucrativo in quanto consente di sfruttare aree ancora grigie nella legislazione dei paesi e problemi eventuali di coordinamento tra enti e paesi diversi per colpire i bersagli più vari, essendo l'informatica una disciplina estremamente trasversale. Risulta evidente che con il passare degli anni, le attività di crimine informatico facciano sempre più gola a diverse entità, ciò porta ad una aumento del numero, del profilo e della varietà degli attacchi stessi. Allo stesso tempo però, con evidente ritardo rispetto alla nascita degli attacchi, si sta diffondendo una maggior sensibilità in materia e stanno aumentando anche gli interventi in grado di reagire in qualche modo a queste situazioni. Ad esempio, un problema che si sta cercando di affrontare riguarda la sovranità dei dati. Attualmente la quasi totalità dei dati generati in UE ad esempio si trova in server americani oppure in server gestiti da aziende americane, e le istituzioni (lentamente) stanno cercando di spingere affinché i dati europei vengano gestiti da enti europei.

\medskip
Affrontare i problemi di sicurezza informatica è sostanzialmente un esercizio di gestione del rischio, in cui occorre conoscere a fondo il possibili impatti e costi di un attacco riuscito e la probabilità che questo possa riuscire. Semplificando questo processo in maniera estrema, esso può essere riassunto nella formula:
\[Rischio = Probabilit\grave{a} \times Impatto\]
In sostanza occorre valutare economicamente i costi associati alle contromisure da adottare affinché le probabilità associate ad un certo evento si riducano. Nel caso in cui costi di meno adottare contromisure allora il processo di difesa diventa favorevole e fattibile.

La sicurezza \textbf{non è un prodotto} che può essere acquistato, bensì un \textbf{processo \href{https://www.protectivesecurity.govt.nz/information-security/lifecycle/}{iterativo}} continuo volto a tener traccia delle continue evoluzioni delle minacce e delle vulnerabilità ed allo stesso tempo delle misure utilizzabili per contrastarle.

\section{Proprietà della sicurezza}\label{sec:cia}
La sicurezza di un sistema può essere scomposta in tre proprietà chiave, riassunte dalla sigla CIA (\textit{Confidentility, Integrity, Availability}):
\begin{itemize}
    \item \textbf{Riservatezza}: consiste nel mantenere inaccessibili dati o proprietà di un sistema a chi non è autorizzato a conoscerli
    \item \textbf{Integrità}: consiste nel garantire che il contenuto di un dato corrisponda a quanto si ritiene corretto. Si può aggiungere a questa proprietà anche l'\textbf{autenticità}, la quale consiste nel garantire che l'origine di un dato sia quella ritenuta corretta
    \item \textbf{Disponibilità}: consiste nel garantire la possibilità effettiva di accedere a dati e servizi quando necessario
\end{itemize}
Generalmente queste tre proprietà sono contrapposte l'un l'altra.

\begin{esempio}{Proprietà in conflitto}{}
    La riservatezza e la disponibilità sono proprietà fortemente in conflitto. Per massimizzare la riservatezza di un dato si potrebbe pensare di scriverlo su un foglio, di chiuderlo in una cassaforte e poi di affondare quest'ultima nel mare. A questo punto nessuno potrà accedere ai dati, compresi i legittimi proprietari
\end{esempio}

\section{Progettazione di un sistema sicuro}
Nel momento in cui si va a progettare un sistema di protezione della sicurezza bisogna separare, seguendo sempre principi ingegneristici, la dimensione del ``cosa'' dalla dimensione del ``come'', ossia distinguere tra:
\begin{itemize}
    \item \textbf{Politiche di sicurezza}, ovvero la dichiarazione di ciò che è consentito (approccio a whitelist) o proibito (approccio a blacklist) fare, dichiarano qual è il fine della sicurezza. Entrambi gli approcci hanno lati positivi e negativi. Nel primo caso il sistema è maggiormente sicuro in quanto per definire la politica è necessario concentrarsi unicamente su cosa sia consentito fare, escludendo quindi in automatico tutte le possibili azioni scorrette (anche quelle non immaginate o contemplate), tuttavia allo stesso tempo è problematico descrivere ed enumerare tutti i casi validi senza dimenticarne alcuno, potenzialmente minando quindi la disponibilità del sistema. Nel secondo caso c'è il problema opposto, ovvero bisogna enumerare correttamente tutte le azioni proibite facendo attenzione a non dimenticarne alcuna affinché questa non possa essere eseguita. In ogni caso queste liste devono essere mantenute e gestite affinché continuino ad essere efficaci
    \item \textbf{Meccanismi di sicurezza}, ovvero i metodi, gli strumenti e le procedure usate per far rispettare le politiche di sicurezza. La scelta di un meccanismo dipende dalle singole condizioni (ad es. per entrare su un sito web si useranno username e password, per entrare in una stanza un badge) e dalla sicurezza desiderata. Ovviamente anche i meccanismi vanno gestiti debitamente in modo tale che rimangano efficaci nel tempo. I meccanismi possono combinare diverse strategie:
          \begin{itemize}
              \item Prevenzione: mira a far fallire l'attacco. I meccanismi di questo tipo solitamente possono essere piuttosto invasivi (ad es. per cifrare dei dati occorre dotare tutti gli utenti autorizzati della chiave necessaria, gestire in maniera consona le chiavi, eventualmente effettuare un backup dei dati del sistema nel caso qualcosa vada storto, ecc\ldots) e minare la disponibilità. Questi meccanismi sono efficaci soltanto se l'implementazione è inalterabile e non aggirabile
              \item Rilevazione: mira a rilevare un attacco che potrebbe però avere successo. Questi meccanismi sono efficaci soltanto contro certi tipi di minacce come la deception, in quanto una modifica ai dati può essere rilevata facilmente ma non prevenuta, e non contro altri come la disclosure, dato che ormai i dati sono stati violati e una pura rilevazione non può far altro che attestare questo fatto
              \item Reazione: mira a indicare come comportarsi quando l'attacco è in corso oppure è appena terminato e riguarda sia comportamenti automatici che eseguibili da esseri umani. Sebbene possa non sembrare molto, il possesso di un elenco di passi da seguire nel momento del bisogno può fare una enorme differenza nella protezione di un sistema e dei dati che contiene
              \item Ripristino: mira al ripristino totale o parziale del sistema dopo la terminazione di un attacco, in modo da garantire il ritorno in produzione
          \end{itemize}
\end{itemize}

Politiche e meccanismi non sono necessariamente strumenti tecnici, anzi molto spesso, tra i più importanti, ci sono le procedure, i comportamenti e regole di interazione tra persone ed il sistema informatico. Inoltre essi si applicano a ogni interazione del sistema col mondo esterno e ad ogni interazione che avviene all'interno del sistema stesso.

\section{Attacchi e minacce}
Possiamo dare ora alcune definizioni:
\begin{definizione}{Minaccia}{}
    Una condizione che potenzialmente può compromettere una o più proprietà della sicurezza
\end{definizione}
Tra le minacce si hanno la \textbf{disclosure}, ovvero la violazione della riservatezza, la \textbf{deception}, ovvero una violazione dell'integrità che porta ad accettare dati falsi, la \textbf{disruption}, ovvero una violazione della disponibilità, l'\textbf{usurpation}, una violazione che porta all'uso non autorizzato di un sistema.

\begin{definizione}{Attacco}{}
    Un'azione che porta al concretizzarsi di una minaccia
\end{definizione}

\begin{definizione}{Attaccante}{}
    L'entità che sferra l'attacco
\end{definizione}

Il concetto di minaccia è indissolubilmente legata alle intenzioni dell'attaccante ed alla sua posizione. Script kiddies, criminali comuni, ricercatori, reporter, spie e governi hanno tutti intenzioni, capacità e obiettivi diversi.

\medskip
Possiamo ora associare alcuni tipi di attacchi alle relative minacce:
\begin{itemize}
    \item Snooping, eavesdropping: sono attacchi passivi che rispettivamente mirano a leggere senza autorizzazione dati da un disco o mentre sono in transito su una rete. Questi attacchi provocano una violazione della riservatezza
    \item Modification, alteration: sono attacchi attivi che mirano a modificare dei dati (memorizzati o in transito). Possono avere come effetto deception ed usurpation, in quanto la modifica può essere associata poi alla capacità di ottenere un utilizzo non autorizzato del sistema (ad es. modificando la password di un utente)
    \item Masquerading, spoofing: sono attacchi attivi che puntano a modificare le proprietà dei dati in transito in modo da alterare la propria identità. Questa è una violazione dell'integrità dei dati in transito che porta il destinatario dei dati ad accettare dei dati falsi (deception). Inoltre si tratta di usurpation in quanto questo può portare ad accessi non autorizzati
    \item Repudiation of origin, denial of receipt: sono attacchi attivi. Il primo consiste nel dimostrare falsamente di non aver prodotto un certo dato, la seconda consiste invece nel negare di aver ricevuto un certo dato durante una transazione. Questi attacchi possono portare a usurpation e deception (ad esempio perché il ripudio interrompe a metà un processo minacciandone la disponibilità)
    \item Delay, destruction, denial of service: sono attacchi passivi che prevalentemente vanno a negare la disponibilità di risorse (disruption), deception e usurpation
\end{itemize}

\begin{esempio}{Attacco di delay}{}
    Supponiamo di avere un sistema composto da un sistema primario e da un sistema di backup. Tramite un accesso secondario l'attaccante riesce a prendere il controllo del sistema di backup (magari perché è meno protetto). Si può introdurre a questo punto un forte ritardo nelle comunicazioni con il primario, il sistema rendendosene conto innesca la procedura di failover scambiando così i due sistemi. A questo punto il sistema attivo è quello di backup controllato dall'attaccante
\end{esempio}

\begin{definizione}{Vettore d'attacco}{}
    Ogni modo reso accessibile ad un attaccante per stimolare un'interazione con il sistema
\end{definizione}
Ogni vettore può essere realizzato combinando uno o più canali di accesso tra fisico (accesso fisico al sistema), cyber (accesso remoto al sistema), umano\footnote{Una delle tecniche più usate è \href{https://it.wikipedia.org/wiki/Ingegneria_sociale}{l'ingegneria sociale}, la quale consiste nell'applicare diverse metodologie più o meno ingannevoli al fine di convincere un utente a rivelare dati che non dovrebbe rivelare}.

\begin{definizione}{Superficie d'attacco}{}
    Insieme dei vettori d'attacco relativi ad un sistema
\end{definizione}

Se le politiche ed i meccanismi di protezione di un sistema fossero perfetti le minacce non potrebbero concretizzarsi, in quanto ogni vettore sarebbe bloccato. Gli attacchi hanno successo se esistono errori nell'individuazione della superficie d'attacco\footnote{Si parla di porosità quando un vettore esiste là dove non dovrebbe}, nella definizione delle politiche o nell'implementazione dei meccanismi.

\begin{definizione}{Vulnerabilità}{vuln}
    Errore nella definizione di una politica o nell'implementazione di un meccanismo, nel qual caso può essere strutturale (e dipendere da hardware o software), oppure può dipendere da un uso o configurazione errata. Essa esiste in quanto tale, a prescindere dal fatto che venga sfruttata
\end{definizione}

Alcuni esempi di vulnerabilità sono:
\begin{itemize}
    \item Uno switch propaga pacchetti a destinatari non designati se la tabella di switching è satura (vincolo hardware)
    \item Un router accetta qualsiasi annuncio gli pervenga riguardante la topologia della rete (caratteristica intrinseca del protocollo)
    \item Un utente clicca un link all'interno di un messaggio non verificando la fonte (errore umano di applicazione di una procedura)
    \item Un processo non controlla prima di sovrascrivere un'area di memoria che non gli appartiene (errore di implementazione del software)
    \item Un processo interpreta sequenze di byte come comandi anche se dovrebbero essere considerate puri dati (errore di progetto del software)
    \item Un computer che gestisce dati riservati può avere le porte USB abilitate (errore di definizione della politica di sicurezza)
\end{itemize}

\begin{definizione}{Exploit}{exploit}
    Uno strumento (tecnico o umano) utilizzato per trarre vantaggio da una vulnerabilità
\end{definizione}

I vettori umani, fisici e software che permettono di accedere a un computer sono normalmente usati per installare malware.

\medskip
Un modello molto utilizzato per descrivere le fasi di attacco a un sistema informatico è la Cybersecurity Kill Chain, che rappresenta tutte le fasi che un attaccante deve compiere per effettuare un attacco. Lo scopo principale della difesa è quello di interrompere questa catena per evitare che l'attacco possa essere portato a compimento.
\begin{enumerate}\label{enum:kill_chain}
    \item Recon: l'attaccante si fa un'idea di quale sia la superficie d'attacco e quali siano le vulnerabilità
    \item Weaponisation: l'attaccante prepara gli strumenti che possono essere necessari per sferrare l'attacco
    \item Delivery: l'attaccante trova il modo di sfruttare una vulnerabilità per guadagnare un accesso al sistema
    \item Exploitation: l'attaccante esegue l'attacco vero e proprio. Difficilmente l'attacco si ferma qua, spesso infatti viene lasciato sul sistema qualcosa che serva per entrare nuovamente nel sistema con maggior facilità, oppure per esfiltrare dati in automatico
    \item Installation: fase in cui viene installato un sistema per rendere più semplice un secondo accesso oppure un sistema per esfiltrare dati
    \item Command and control: tale sistema può anche essere usato per controllare da remoto il sistema
    \item Exfiltration: esfiltrazione dei dati vera e propria
\end{enumerate}

\section{Difesa}
La messa in sicurezza è un processo metodico che può essere effettuato con l'ausilio di framework e le metodologie standard. Come tutti i processi ingegneristici, politiche e meccanismi derivano dalla sequenza di analisi di requisiti, progetto, implementazione, test. Questi strumenti inoltre devono essere applicati a processi organizzativi, contesto fisico, sistemi, reti, applicazioni.

La sfida maggiore nel processo di progettazione di sistemi di sicurezza è quella di non trascurare nessun dettaglio. Come tutti i processi ingegneristici, infatti, è richiesta una metodicità quasi maniacale spesso difficile da perseguire nell'ambiente reale.

Per quanto riguarda la raccolta dei requisiti, questa è molto diversa da quella tradizionale in quanto è focalizzata sul ``non deve accadere'' invece che sul ``deve funzionare''. Essa studia i ``misuse case'' per verificare se una minaccia possa concretizzarsi o meno in un attacco e i ``security use case'' per distillare i requisiti dei meccanismi da applicare. Finita l'analisi può essere definito un ``attack tree'' per modellare come si combinino diversi possibili eventi e condizioni al contorno nell'ottica dell'esecuzione di un attacco. Questo porta a definire infine i punti da difendere ed i vettori da bloccare.

Nel caso della progettazione ingegneristica definire i test risulta relativamente semplice, nel senso che è sufficiente verificare che il sistema abbia il comportamento desiderato. Nel caso dei test della sicurezza ci si trova di fronte ad un problema semidefinito\footnote{Un problema per cui è possibile dimostrare la verità ma non la falsità. Ad esempio dato un generatore di numeri pari è possibile sapere se un numero scelto a caso sia pari in quanto prima o poi verrà generato dal sistema, ma non è possibile sapere se sia dispari in quanto, dato che i numeri pari sono infiniti, il generatore continuerà a generare numeri all'infinito senza la possibilità di sapere se il numero successivo nella sequenza sia quello scelto oppure no}, ovvero è possibile dimostrare la presenza di una vulnerabilità, ma non è possibile dimostrare l'assenza in generale di vulnerabilità. Oltre a controlli e test formali è possibile usare altre modalità quali \textit{vulnerability assessment}\footnote{Testare tutte le vulnerabilità contenute in una lista condivisa e affermata per controllare che nessuna di queste sia presente. Il problema è che la lista potrebbe non essere esaustiva e che controllando solamente la presenza non si verificano l'effettiva profondità della vulnerabilità ed le possibilità che questa può dare una volta sfruttata}, \textit{penetration testing}\footnote{Un attaccante etico autorizzato dal proprietario del sistema attacca il sistema per verificarne la sicurezza}, \textit{red team operations}\footnote{È un'estensione del penetration testing che prevede di testare non solo i sistemi ma anche tutte le condizioni al contorno} (vedi capitolo~\ref{cap:offsec}).

\medskip
Il mondo professionale della cyber security soffre di due grossi problemi incombenti. Infatti, attualmente vi è una forte mancanza di personale qualificato e di piattaforme adatte a favorire lo sviluppo delle capacità necessarie a svolgere questo tipo di professione. È possibile certificare il possesso di queste qualità mediante l'uso di certificazioni, inoltre si sta cercando, anche con piani comunitari, di sopperire a questi problemi.
