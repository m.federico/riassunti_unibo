\chapter{Generazione di numeri casuali}\label{cap:num_casuali}
Un generatore di numeri casuali (RNG) è spesso richiesto per la realizzazione di meccanismi e servizi per la sicurezza. Questo tipo di generatori può essere progettato per generare un singolo bit alla volta oppure una sequenza di lunghezza maggiore e discrezionale. Per garantire l'effettiva casualità dei valori generati è necessario che un RNG rispetti le seguenti proprietà:
\begin{itemize}
    \item Ogni valore generabile deve avere la medesima probabilità di essere generato, ovvero la distribuzione dei valori deve essere uniforme
    \item I valori generati devono essere statisticamente indipendenti gli uni dagli altri
\end{itemize}

Per valutare l'efficacia degli algoritmi RNG sono stati definiti 15 diversi test standard, alcuni dei quali sono:
\begin{itemize}
    \item Monobit: valuta il numero di 1 e di 0 presenti all'interno della stringa verificando che abbiano circa la stessa frequenza
    \item Poker: suddivide la sequenza in blocchi di 4 bit consecutivi e valuta se il numero di ciascuna delle configurazioni è approssimativamente lo stesso
    \item Run: considera la stringa di bit consecutivi tutti uno o zero, e valuta, per le varie lunghezze, se il loro numero approssima il valore atteso per una vera sequenza casuale
    \item Long run: considera sequenze di uno (e di zero) presente nella sequenza in esame e valuta se la loro lunghezza è compatibile con quella attesa per una vera sequenza casuale
\end{itemize}

Sono possibili varie tipologie di generatori di numeri casuali, tra cui:
\begin{itemize}
    \item True random number generator (TRNG): questa implementazione genera il valore casuale tramite la digitalizzazione di un fenomeno fisico casuale (tempo medio di emissione di particelle durante il decadimento radioattivo, rumore termico ai morsetti di un diodo o di un resistore, turbolenza di fluidi, immagini di lavalamp, ecc) garantendo quindi una reale casualità dei valori. Tuttavia questo tipo di generatore:
          \begin{itemize}
              \item ha una bassa frequenza di generazione dei valori
              \item non è in grado di riprodurre un valore (ad esempio dell'altro lato della comunicazione)
              \item produce valori non polarizzati
          \end{itemize}
          Di conseguenza non è adatto a tutti gli scopi applicativi
      \item Pseudo random number generator (PRNG): questa implementazione utilizza algoritmi deterministici per generare il valore casuale, di conseguenza non garantisce la reale casualità dei valori. Infatti i valori generati si ripetono ciclicamente con un periodo di lunghezza variabile a seconda delle implementazioni, maggiore è la lunghezza del periodo maggiore è l'apparente casualità dei valori. Per molti PRNG la generazione dei valori parte da un valore iniziale detto seme (\textit{seed}). Se il seme risulta noto è possibile riprodurre la medesima sequenza di valori ogni volta, pertanto è importante che esso sia scelto in maniera realmente casuale, ad esempio mediante un TRNG. Questo tipo di generatori garantisce un'alta frequenza di generazione dei valori e, a fronte della conoscenza del seme, anche la riproducibilità dei valori. I PRNG sono modellati sulla base di un automa a stati di cui almeno una delle funzioni $F$ (funzione di uscita) e $G$ (funzione di stato) deve essere unidirezionale. Questo tipo di generatore è valutato dal test Next-bit: dati $N$ bit della stringa generata non deve esistere un algoritmo polinomiale in grado di predire il bit $N + 1$-esimo con una probabilità maggiore di $0,5$. Nel caso in cui il generatore superi il test next-bit e una delle due funzioni sia anche unidirezionale tale generatore viene detto sicuro
\end{itemize}

Questi generatori hanno sia pro che contro. I PRNG consentono un'alta frequenza di generazione e la ripetibilità dei valori, tuttavia le sequenze generate si ripetono ed i valori non risultano realmente imprevedibili (a meno di condizioni particolari). I TRNG invece sono in grado di generare valori realmente casuali, tuttavia la frequenza di generazione è bassa ed i valori non risultano riproducibili né polarizzati.

\chapter{Scambio di chiave Diffie-Hellman}\label{appendix:diffie_hellman}
Questo consente di scambiare una chiave in maniera sicura su di un canale insicuro per mezzo di funzioni pseudo unidirezionali. Per questo motivo lo scambio Diffie-Hellman è poco efficiente e va utilizzato con parsimonia. Sebbene la forma più essenziale di tale algoritmo sia anonima (ovvero non garantisce l'identità dell'attore con cui si sta scambiando una chiave) essa è alla base di alcuni protocolli autenticati come TLS. Nell'implementazione originale (che è anche la più semplice) si considera un valore $g$ generatore del gruppo moltiplicativo degli interi modulo $p$, dove $p$ è un numero primo.
\begin{definizione*}{Generatore di un gruppo moltiplicativo}{}
    Il numero $g$ è detto generatore del gruppo moltiplicativo modulo $n$ se per ogni intero $a$ coprimo con $n$ esiste un intero $k$ tale che $g k \equiv  a\ mod\ n$. Il valore $k$ è chiamato logaritmo discreto di $a$ in base $g$ modulo $n$
\end{definizione*}

Per effettuare lo scambio si procede come segue. Uno dei due interlocutori, Alice ad esempio, sceglie un valore casuale $a$, un numero primo $p$ grande ed un generatore $g$ per $p$ e calcola il valore
\[V_A = g^a\ mod\ p\]
e lo invia a Bob sul canale insicuro assieme a $p$ ed a $g$. Bob sceglie a sua volta un valore casuale $b$ e
calcola
\[V_B = g^b\ mod\ p\]
e lo invia ad Alice. A questo punto Alice può calcolare
\[K_A = (V_B)^a\ mod\ p\]
e Bob può calcolare
\[K_B = (V_A)^b\ mod\ p\]
Grazie alle proprietà dell'aritmetica modulare si dimostra che tali valori sono uguali, ovvero che
\[K_B = (V_A)^b\ mod\ p = (g a\ mod\ p)^b\ mod\ p \equiv (gb\ mod\ p)^a\ mod\ p = (V_B)^a\ mod\ p = K_A\]
Di conseguenza Alice e Bob sono in possesso della medesima chiave sebbene non l'abbiano mai condivisa esplicitamente.
Data la difficoltà computazionale associata all'inversione del logaritmo discreto, un intruso Eva che si metta in ascolto sul canale non riuscirà a risalire ai valori $a$ e $b$ mantenuti segreti dalle due parti comunicanti. Questo protocollo tuttavia è vulnerabile in caso di attacchi attivi del tipo ``man in the middle'', in cui un attaccante modifica i valori pubblicati da Alice e Bob in modo da ingannare le due parti.
L'algoritmo infatti è in grado di attuare lo scambio in segreto a patto che le informazioni pubbliche siano valide, pertanto occorre dotarsi di precauzioni atte a garantire la correttezza dei dati pubblici ricevuti. Questo scambio è anonimo e non permette in alcun modo di identificare le parti in causa; esistono tuttavia varianti che prevedono anche l'identificazione. La sua robustezza è legata alla difficoltà di calcolare il logaritmo discreto di un numero; in particolare, tale operazione risulta molto complessa quando la base del logaritmo è un numero primo molto grande.

\chapter{Rainbow table}\label{cap:rainbow}
Le rainbow table sono delle tabelle di associazione che offrono un compromesso tempo-memoria usate per il recupero di chiavi a partire dall'hash. L'idea di fondo è quella di usare un certo quantitativo di memoria costante per memorizzare informazioni calcolate una volta per tutte che possono rendere il tempo di ricerca di un hash lineare. 

Una prima implementazione semplicistica consiste nel salvare coppie password hash all'interno di un file, trasformando il problema dalla ricerca a forza bruta ad una ricerca lineare all'interno di un file. Tuttavia questa implementazione richiede un enorme quantitativo di spazio e quindi non è realizzabile. Un miglioramento di questa prima idea consiste nell'utilizzare due funzioni:
\begin{itemize}
    \item Una funzione di hash
    \item Una funzione di riduzione, la quale prende come ingresso un generico hash e lo trasforma in una password. Questa funzione applica solo una trasformazione e non inverte la funzione di hash, pertanto la password ottenuta non è quella che aveva inizialmente generato l'hash ma una password completamente nuova
\end{itemize}
All'interno delle rainbow table vengono usate varie funzioni di riduzione, ognuna delle quali corrisponde ad uno dei colori che compongono l'arcobaleno, ma viene usata sempre la stessa funzione di hash.

L'algoritmo prevede che a partire da una password venga generato il corrispondente hash. A partire da questo viene ottenuta una nuova password che serve per generare un nuovo hash e così via fino a che non sono state usate tutte le funzioni di riduzione.Una volta generata la catena vengono salvati solamente il testo in chiaro iniziale e l'hash finale (risparmiando spazio). L'algoritmo prevede che vengano generate varie catene.

\begin{esempio}{}{}
    Per semplicità supponiamo di voler usare una rainbow table solo sui valori interi compresi tra $0$ e $100$. La funziona di hash scelta, per semplicità di calcolo sarà
    $$h(k) = \lfloor m \cdot (kA\ mod\ 1)\rfloor$$
    dove $k$ è la password, $m=2000$, $A=0,618$. La funzione di riduzione, nuovamente per semplicità, sarà
    $$r(h) = h\ mod\ 100$$
    ovvero una funzione che restituisce le ultime due cifre del valore ottenuto. Per esempio se la password fosse $78$ l'hash calcolato sarebbe $0408$ (per mantenere il numero di cifre pari a quattro) e la riduzione sarebbe $08$.

    Si comincia così a costruire la tabella, tenendo conto del fatto che più applicazioni della funzione di riduzione vengono effettuate minore sarà lo spazio occupato, ma maggiore sarà lo sforzo computazionale (per brevità sono scritte solo le prime righe):
    \begin{center}
        \begin{tabular}{cccccccc}
            $p1$                    & $h1$                   & $p2$                   & $h2$                   & $p3$                   & $h3$                   & $p4$                   & $h4$                   \\
            $01$                   & $1236$                 & $36$                   & $0496$                 & $96$                   & $0656$                 & $56$                   & $1215$                 \\
            $02$                   & $0472$                 & $72$                   & $0992$                 & $92$                   & $1712$                 & $12$                   & $0832$                 \\
            $03$                   & $1708$                 & $08$                   & $1888$                 & $88$                   & $0768$                 & $68$                   & $0048$                 \\
            \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{}
        \end{tabular}
    \end{center}

    Della tabella ottenuta vengono quindi salvate solamente la prima colonna e l'ultima dato che tutti gli altri valori possono essere ricavati da questi.
    
    \begin{center}
        \begin{tabular}{cc}
            $p$                   & $h4$                   \\
            $01$                  & $1215$                 \\
            $02$                  & $0832$                 \\
            $03$                  & $0048$                 \\
            \multicolumn{1}{l}{}  & \multicolumn{1}{l}{}
        \end{tabular}
    \end{center}
\end{esempio}

Per effettuare la ricerca si controlla per prima cosa se l'hash cercato compare già nella tabella all'interno della colonna $h4$. Nel caso in cui compaia si sa che la password $p1$ è l'inizio della catena che genera quella password, ma non è quella password. Si comincia quindi ad applicare una serie di riduzioni ed hash fino a trovare il valore che si trovava inizialmente nella colonna $p4$. Se invece l'hash cercato non si trova all'interno della colonna $h4$ si sa che questo compare probabilmente all'interno di una delle colonne nascoste, quindi si applicano uno o più passi di riduzione fino a che l'hash ottenuto non è quello cercato, fino a trovare una ripetizione o fino a che il valore ottenuto dalla riduzione non si trova nella colonna $p1$.

\begin{esempio}{}{}
    Supponiamo di voler cercare la password che dà come hash il valore $1888$. Questo valore non si trova nella tabella precedente, pertanto occorre applicare una prima riduzione che dà come risultato $88$. Questo valore non si trova all'interno della tabella ridotta, pertanto si calcola il suo hash ($0768$) e la relativa riduzione ($68$). L'hash corrispondente è $0048$, valore che si trova nella terza riga della tabella. Sappiamo quindi che la catena avente come valore di origine $03$ produce ad un certo punto l'hash $0048$. Ricalcolando la catena si trova che la password cercata è $08$, in quanto tale valore produce l'hash cercato $1888$
\end{esempio}


\chapter{PAM}\label{cap:pam}
PAM (\textit{Pluggable Authentication Modules}) è una suite di librerie che consentono ad un amministratore di sistema di configurare vari metodi per l'autenticazione in sistemi Linux. PAM fornisce un meccanismo centralizzato per gestire e cambiare le politiche di autenticazione per un sistema usando dei file di configurazione anziché modificando il codice applicativo. Esistono ad esempio librerie PAM che consentono l'utenticazione tramite password, impronte digitali o directory LDAP. Oltre a ciò PAM fornisce metodi per la gestione di password, gestione delle sessioni utente, gestione dei diritti degli account, utilizzo di più metodi in cascata per l'autenticazione.

Per rendere un programma C compatibile con PAM è necessario includere nel sorgente la libreria \path{security/pam_appl.h}, la quale fornisce un insieme di funzionalità generiche direttamente invocabili e che possono essere configurate in maniera più o meno specifica tramite file di configurazione, come già detto. Includendo questo header il linker provvede quindi a collegare l'eseguibile alle librerie PAM di sistema (come \path{libpam} e \path{libpam_misc}), le quali usano i moduli che si trovano all'interno di \path{lib/security}.

\medskip
Le configurazioni di PAM possono trovarsi o all'interno di \path{/etc/pam.conf}, i quali includono righe nel formato
\begin{lstlisting}
    program module-type control-flag module-path arguments
\end{lstlisting}
o in maniera specifica per ogni applicativo in \path{/etc/pam.d} (vedi esempio), che contiene righe nel formato
\begin{lstlisting}
    module-type control-flag module-path arguments
\end{lstlisting}

\begin{esempio}{Configurazione di PAM specifica per un applicativo}{}
    \begin{lstlisting}
    auth        sufficient    /lib/security/pam_ldap.so
    account     sufficient    /lib/security/pam_ldap.so
    password    sufficient    /lib/security/pam_ldap.so
    session     optional      /lib/security/pam_ldap.so
    auth        requisite     pam_unix2.so
    auth        required      pam_securetty.so
    auth        required      pam_nologin.so
    \end{lstlisting}
\end{esempio}

\section{File di configurazione}
Nei file di configurazione la sezione \path{module-type} indica il servizio da configurare tra:
\begin{itemize}
    \item \path{auth}: fornisce servizi di autenticazione
    \item \path{account}: fornisce funzionalitò di gestione degli account non associate all'autenticazione, come promemoria relativi alla scadenza delle password o restrizioni varie basate sull'orario o sull'appartenenza ad un gruppo
    \item \path{session}: fornisce strumenti per la gestione delle sessioni, come impostazione di certe proprietà al login
    \item \path{password}: gestione degli aggiornamenti dei token per l'autenticazione
\end{itemize}

La sezione \path{control-flags} invece descive come PAM debba reagire al successo o all'insuccesso di un modulo.
\begin{itemize}
    \item \path{requisite}. In caso di successo permette la prosecuzione dell'esecuzione dello stack, delegando il successo o il fallimento dello stack ad altri moduli. In caso di insuccesso l'esecuzione dello stack termina immediatamente e lo stack fallisce
    \item \path{required}. In caso di successo permette la prosecuzione dell'esecuzione dello stack, delegando il successo o il fallimento dello stack ad altri moduli. In caso di insuccesso l'esecuzione dello stack continua, ma lo stack fallisce nel complesso
    \item \path{sufficient}. In caso di successo l'esecuzione dello stack termina immediatamente, il risultato complessivo dipende dal risultato dei flag \path{required} precedenti (fallisce se anche uno solo di essi ha fallito). In caso di insuccesso l'esecuzione dello stack continua e il risultato finale dello stack dipende dal risultato ottenuto dai moduli successivi
    \item \path{optional}. Sia in caso di successo che di insuccesso l'esecuzione dello stack continua e il risultato finale dipende dal risultato ottenuto da altri moduli. Nel caso in cui altri moduli non siano presenti o diano risultati non conclusivi l'esecuzione dello stack termina con un successo o con un fallimento a seconda dell'esito del modulo stesso
\end{itemize}

La sezione \path{module-path} specifica il percorso assoluto presso cui trovare il modulo, successivamente all'indicazione del modulo è possibile specificare un elenco di parametri da passare, che possono essere specifici del modulo o generali (come \path{debug}, \path{no_warn}, \path{use_first_pass}, che istruisce ad usare la password usata per il modulo precedente provocando un fallimento in caso di errore, o \path{try_first_pass}, che istruisce a tentatre l'utilizzo della password precedente e di usare quella per il modulo attuale in caso di fallimento). Alcuni moduli comuni sono \path{unix}, che implementa i meccanismi tradizionali di autenticazione tramite \path{/etc/passwd}, \path{time}, che implementa regole di restrizione dell'accesso basate sul tempo, \path{env}, che imposta l'ambiante di esecuzione sulla base della sessione per cui si è loggati, \path{deny}, che blocca qualsiasi tentativo di login ed è utile per fornire una politica generale del tipo default deny, oppure \path{warn}, che permette il dump di informazioni nei log di sistema.

\begin{esempio}{Configurazione di PAM}{}
    Consideriamo il seguente estratto:
    \begin{lstlisting}
    auth       required   /lib/security/pam_unix.so
    auth       required   /lib/security/pam_securetty.so
    auth       required   /lib/security/pam_nologin.s
    \end{lstlisting}
    Ogni tentativo di autenticazione deve essere approvato da tutti e tre i moduli, dato che sono tutti required. Il primo modulo richiede la password dell'utente (classico metodo di autenticazione Unix), il secondo fa fallire qualsiasi accesso come \path{root}, a meno che non venga eseguito tramite uno dei terminali contenuti in \path{/etc/securetty}, il terzo fa fallire tutti i login, tranne quelli di \path{root} nel caso in cui esista il file \path{/etc/nologin}.

    L'ordine dei moduli è importante. Se esiste il file \path{/etc/nologin} sono autorizzati solo i login di \path{root} attraverso console sicure. Se invece il primo modulo fosse \path{sufficient} e non \path{required} sarebbero consentiti anche i login di \path{root} da terminali non sicuri.
\end{esempio}

\begin{esempio}{Configurazione di PAM}{}
    Consideriamo la seguente configurazione:
    \begin{lstlisting}
    auth required   pam_unix.so try_first_pass
    auth sufficient pam_krb5.so try_first_pass
    auth required   pam_env.so
    \end{lstlisting}
    I primi due moduli autenticano gli utenti mentre l'ultimo prepara l'ambiente. In generale questo stack ha successo solo se il primo modulo ha successo. Nel caso in cui le prime due regole fossero invertite:
    \begin{lstlisting}
    auth sufficient pam_krb5.so try_first_pass
    auth required   pam_unix.so try_first_pass
    auth required   pam_env.so
    \end{lstlisting}
    l'accesso invece sarebbe bloccato solo se entrambi i primi due moduli fallissero.

    In entrambi i casi però il successo dei primi due moduli bypassa l'ultimo modulo.
\end{esempio}

\begin{esempio}{Configurazione default deny}{}
    La seguente configurazione applica una politica del tipo default deny
    \begin{lstlisting}
    auth          required        pam_warn.so
    auth          required        pam_deny.so
    account       required        pam_warn.so
    account       required        pam_deny.so
    password      required        pam_warn.so
    password      required        pam_deny.so
    session       required        pam_warn.so
    session       required        pam_deny.so
    \end{lstlisting}
\end{esempio}
\chapter{SELinux}\label{cap:selinux}
Security-Enhanced Linux (SELinux) è un'architettura di sicurezza per i sistemi Linux, che offre agli amministratori un livello di controllo superiore sugli utenti autorizzati ad accedere al sistema. Originariamente sviluppato dall'Agenzia di sicurezza nazionale statunitense (NSA) come serie di patch per il kernel Linux sotto il nome di Flask, tale sistema di controllo utilizza i moduli di sicurezza Linux (LSM, Linux Security Modules). È stato poi espanso e condiviso con la community open source nel 2000 e integrato nel kernel Linux upstream nel 2003 con il nuovo nome SELinux.

SELinux definisce i controlli di accesso per le applicazioni, i processi e i file su un sistema basandosi su criteri di sicurezza. Si tratta di una serie di regole che indicano a SELinux gli elementi a cui è possibile accedere o meno, in modo da applicare il criterio di accesso consentito.

Quando un'applicazione o un processo, che prende il nome di soggetto, effettua una richiesta di accesso a un oggetto, ad esempio un file, SELinux consulta una cache in cui sono memorizzate le autorizzazioni per soggetti e oggetti.

Se, basandosi sulle autorizzazioni nella cache, non è possibile determinare l'accesso, SELinux invia la richiesta al server di sicurezza che verifica il contesto di sicurezza dell'applicazione o del processo e quello del file. Il contesto di sicurezza applicato è quello disponibile nel database dei criteri di SELinux e, a questo punto, l'autorizzazione viene concessa o negata.

\section{Funzionamento}
SELinux supporta tre modelli di sicurezza:
\begin{itemize}
    \item Type enforcement (TE) obbligatoriamente
    \item User identity (UI)
    \item Role based access control (RBAC) obbligatoriamente
    \item Multi level security (MLS) attivabile opzionalmente
\end{itemize}

In SELinux ogni soggetto ed ogni oggetto è identificato internamente per mezzo di un identificativo numerico detto security identifier (SID), mentre è identificato esternamente al sistema per mezzo di una terna chiamata ``security context'', la quale contiene attributi immediatamente identificabili. Le due modalità di identificazione sono collegate tramite una relazione biunivoca per mezzo di una tabella di mapping (security context table).

L'utilizzo interno di SID è estremamente vantaggioso in quanto essi consentono di prendere efficientemente decisioni di sicurezza senza svelare i dettagli della terna e svincola allo stesso tempo il gestore della sicurezza dalla terna di attributi scelta per identificare il medesimo soggetto o oggetto.

Un contesto di sicurezza è invece dotato di una semantica definita per mezzo dei tre attributi che lo compongono:
\begin{itemize}
    \item User identity, ovvero l'account SELinux associato al soggetto (o oggetto). Occorre notare che gli account SELinux sono svincolati da quelli di sistema, tanto che più account di sistema possono essere mappati su un unico account SELinux e che modifiche ad un account di sistema non hanno alcun effetto su di un account SELinux
    \item Role, ovvero il ruolo correntemente utilizzato dall'utente (ogni utente può assumere un solo ruolo in un dato momento). Tra i ruoli esistono
          \begin{itemize}
              \item \path{sysadm_r} che può essere rivestito solamente dall'amministratore del sistema
              \item \path{object_r} che è un ruolo ``dummy'' usato nelle terne che non hanno necessità di un ruolo specifico
          \end{itemize}
    \item Type, utilizzato da SELinux per prendere decisioni in merito all'autorizzazione. Tecnicamente si parla di \textit{type} per soggetti e di \textit{domain} per oggetti
\end{itemize}
Per distinguere i tre attributi all'interno della terna si usa una convenzione testuale. I type terminano con suffisso \path{_t}, i ruoli con suffisso \path{_r}, mentre gli utenti non hanno alcun suffisso.

\section{Etichettatura delle applicazioni}
L'assegnazione di etichette e l'applicazione del tipo di etichetta sono i due concetti cardine in SELinux. SELinux opera come sistema di etichettatura, associando un'etichetta SELinux a ciascun file, processo e porta di un sistema. Le etichette costituiscono un metodo logico per raggruppare gli oggetti e vengono gestite dal kernel durante la fase di avvio.

Le etichette sono in formato
\begin{center}
    \path{utente:ruolo:tipo[:livello]}
\end{center}
Utente, ruolo e livello vengono utilizzati nelle implementazioni più avanzate di SELinux, ad esempio quelle che utilizzano la sicurezza multilivello. Per i criteri mirati, il tipo di etichetta è l'elemento imprescindibile.

Le associazioni per gli oggetti persistenti sono contenute all'interno di file di configurazione, i quali per convenzione hanno estensione \path{.fc} la cui colonne contengono un'espressione regolare che identifica l'oggetto, un insieme di trattini che identifica il tipo e infine il contesto (la terna).
\begin{lstlisting}
    /usr/sbin/snort      -- system_u:object_r:snort_exec_t
    /usr/local/bin/snort -- system_u:object_r:snort_exec_t
    /etc/snort(/.*)?        system_u:object_r:snort_etc_t
    /var/log/snort(/.*)?    system_u:object_r:snort_log_t
\end{lstlisting}
Dato che i file però sono tra gli oggetti più comuni, al fine di velocizzare l'associazione tra un file ed il proprio contesto, viene eseguita un'operazione di etichettatura che consente di memorizzare il contesto tra gli attributi del file stesso (nei file system che li supportano).

Il security server è poi chiamato a prendere decisioni in merito alla sicurezza, decisioni che possono riguardare l'accesso ad un oggetto da parte di un soggetto, o che possono riguardare le transizioni di tipo degli oggetti. In generale tuttavia SELinux applica politiche del tipo default deny, impedendo qualsiasi azione non esplicitamente consentita.

Le decisioni relative all'accesso sono prese sulla base di un \textit{vettore di accesso}, di cui esiste un'istanza per ogni \textbf{classe} di oggetti. Il vettore di accesso contiene un bit per ogni azione definita per la classe. Nel momento in cui il server viene interrogato restituisce tre istanze del vettore di accesso calcolate sulla base del dominio del soggetto, del tipo dell'oggetto e della sua classe. La possibilità di effettuare l'azione dipende dai bit impostati nei tre vettori:
\begin{itemize}
    \item nessun bit impostato: l'azione non è consentita e il risultato viene loggato
    \item allow impostato: l'azione è permessa, ma viene loggata solo esiste un'altra regola del tipo audit-allow
    \item audit-allow impostato: l'azione non è consentita a meno che non sia impostato un bit allow, ma viene sempre loggata
    \item dont-audit: l'azione è impedita, ma non viene effettuato alcun log
\end{itemize}

Le decisioni relative alle transizioni. Per quanto riguarda le nuove risorse, esse ereditano il contesto di sicurezza del loro contenitore padre. Le risorse già esistenti possono essere rietichettate da un soggetto. I nuovi soggetti ereditano il contesto di sicurezza di chi li ha creati di default, ma possono anche nascere con un contesto diverso in caso o la politica SELinux lo imponga o se il creatore lo ha deciso. I soggetti esistenti possono richiedere una transizione di contesto. In generale una caratteristica fondamentale che distingue SELinux da altre soluzioni è la presenza di un'API che consente di scrivere programmi in grado di interagire con il security server, anziché solamente ``subire'' una policy.

\section{Configurazione}
Le policy SELinux sono caricate dal kernel in file in formato binario risultante dalla compilazione di una collezione di sorgenti\footnote{File TE, file RBAC, dichiarazioni degli utenti, specifiche dei contesti di sicurezza, vincoli vari} distinte sia per tipologia che per sottosistema.
\begin{itemize}
    \item Le regole RBAC sono usate essenzialmente per mappare un utente su di un ruolo, consentire l'utilizzo di un dominio ad un ruolo o consentire la transizione di un utente da un ruolo ad un altro
    \item Le regole di type enforcement sono tipicamente numerose e complesse. Permettono di esprimere definizioni di tipo (eventualmente anche con alias) corredato da attributi, i quali possono essere usati per rendere più chiara la scrittura di regole associando un identificatore con una semantica evidente per un dato dominio. Permettono inoltre di definire regole che definiscono cosa sia consentito ai vari tipi (particolarmente utili sono le regole del tipo \textit{never-allow} ai fini della coerenza). Infine regole che consentono transizioni di tipo.
\end{itemize}

\medskip
Un sistema con SELinux può essere avviato in tre modi:
\begin{itemize}
    \item Disabilitato. Non prevede alcun controllo degli accessi, nessun log, e la perdita dei SID associati ai file in caso di modifica
    \item Permissive mode. Il controllo degli accessi genera unicamente un log delle decisioni di sicurezza, ma non le mette in atto. \path{audit2allow} permette inoltre di convertire il log in policy TE che permettono l'attuazione delle operazioni registrate\footnote{attenzione a effettuare i test in ambiente privo di rischi}
    \item Enforcing mode. Prevede la piena funzionalità del sistema di controllo degli accessi
\end{itemize}

Se in un ambiente SELinux è stato disabilitato, questo può essere abilitato modificando \path{/etc/selinux/config} e impostando \path{rb|SELINUX=permissive}. Dal momento che SELinux non è abilitato, non è consigliabile impostare subito la modalità enforcing, perché probabilmente le etichette degli oggetti nel sistema non sono corrette e questo potrebbe impedirne l'avvio.

È possibile imporre al sistema di riapplicare le etichette nel file system automaticamente, creando un file vuoto denominato \path{.autorelabel} nella directory radice e riavviando il sistema. Se nel sistema sono presenti troppi errori, occorre eseguire un riavvio mentre è attiva la modalità \textit{permissive}, al fine di completare correttamente la procedura di avvio. Terminata la riapplicazione di tutte le etichette, è possibile impostare SELinux nella modalità \textit{enforcing} tramite \path{/etc/selinux/config}, quindi riavviando il sistema o eseguendo \path{setenforce 1}.

SELinux garantisce un ulteriore livello di sicurezza del sistema, integrato nelle distribuzioni di Linux, che dovrebbe rimanere sempre attivo per proteggere il sistema qualora venga compromesso.

