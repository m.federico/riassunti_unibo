\chapter{Controllo dell'accesso}
Quando si parla di controllo dell'accesso in generale si fa riferimento alla regola delle tre A (AAA), ovvero \textbf{autenticazione}, la quale consiste nell'attribuzione certa dell'identità di un soggetto,  \textbf{autorizzazione}, che consiste nella verifica dei diritti che un soggetto ha in relazione ad un insieme di azioni possibili su di un certo oggetto, e \textbf{auditing}, che infine consiste nel tracciamento affidabile di tutte le decisioni di autenticazione e autorizzazione prese da un sistema al fine di verificare l'efficacia delle politiche e dei meccanismi di autenticazione adottati e da capire se le decisioni prese sono valide. 

\section{Autenticazione}
Come anticipato l'autenticazione consiste nell'attribuzione certa dell'identità di un soggetto. È importante però separare il concetto di autenticazione da quello di identificazione, pur incluso nell'autenticazione, in quanto l'identificazione consiste nel chiedere ad un soggetto quale sia la propria identità mentre l'autenticazione verifica l'affermazione effettuata. Collassare questi due momenti può portare ad una diminuzione della sicurezza (vedi esempio~\ref{es:separazione_aut_id}).
\begin{esempio}{Separazione di identificazione e autenticazione}{separazione_aut_id}
    Si pensi ad uno scanner per impronte digitali. Se non viene effettuato il passo di identificazione l'impronta immessa dovrà essere confrontata con tutte quelle in memoria fino a trovare una corrispondenza. Dato che alcune impronte possono essere simili tra loro ed il meccanismo non è perfetto questo sistema potrebbe produrre degli errori. Se invece si introduce un passo di identificazione il sistema dovrà confrontare l'impronta immessa solamente con quella corrispondente nella memoria, riducendo così le possibilità di errore
\end{esempio}
Nella progettazione di un sistema occorre inoltre non confondere elementi identificativi come autenticanti, in modo da non abbassare il livello di sicurezza (vedi esempio~\ref{es:el_id_aut}). 
\begin{esempio}{Elementi identificanti ed elementi autenticanti}{el_id_aut}
    Il SSN (\textit{Social Security Number}) americano è un dato identificate e non autenticante, dato che chiunque potrebbe leggerlo da un documento ed utilizzarlo
\end{esempio}

L'autenticazione infatti è basata sulla conoscenza o sul possesso di informazioni o oggetti che solamente il reale proprietario di un'identità potrebbe conoscere o avere. In questo senso in generale si parla di ``qualcosa che hai'', ovvero provando il possesso di un qualche oggetto (smart card, badge, ecc\ldots), ``qualcosa che sai'', ovvero provando la conoscenza di qualche informazione (password, pin, ecc\ldots), oppure ``qualcosa che sei'', ovvero mediante il possesso di qualche caratteristica fisica (impronta digitale, forma dell'iride, comportamento tipico, ecc\ldots). Ovviamente alcuni di questi metodi non sono applicabili a delle macchine o a dei processi, per cui ci concentriamo sui modi che un attore, detto prover ($P$), ha per dimostrare ad un secondo attore, detto verifier ($V$), di essere a conoscenza di un segreto.

In generale è possibile implementare queste funzionalità per ogni applicativo che ne ha bisogno, ma spesso è più utile affidarsi a soluzioni già pronte che forniscono tutte le funzionalità già implementate e che offrono metodi standard per interfacciarvisi. Una di queste soluzioni, specifica per il mondo Linux, è PAM (vedi appendice~\ref{cap:pam}).

\subsection{Autenticazione passiva}
L'autenticazione passiva si ottiene dimostrando il possesso di un segreto $s$ concordato in precedenza durante la fase di registrazione. Tipicamente quindi $P$ deve inviare $s$ a $V$ così da dimostrare la propria identità. Questa tecnica di base ha vari problemi. In primo luogo $s$ viaggia su canali nella maggior parte dei casi insicuri. Se viene inviato in chiaro un attaccante può sempre intercettarlo, se invece viene inviato offuscato (ma sempre uguale) può essere a termine un attacco di tipo replay. Di conseguenza si nota la necessità di protocolli più sofisticati, che ad esempio utilizzino dei numeri di sequenza per bloccare attacchi di replay.

Quand'anche il canale fosse sicuro, in ogni caso sarebbe possibile il furto del segreto dal database di $V$, rendendo inutile ogni precauzione presa sul canale. Per questo motivo $V$ non deve conoscere mai direttamente il segreto condiviso, ma solamente, ad esempio, la sua impronta\footnote{Qualsiasi sistema che permetta dunque il recupero (non il reset) delle credenziali è intrinsecamente insicuro, in quanto è a conoscenza delle password}, come succede nel caso della memorizzazione delle password degli utenti all'interno dei sistemi operativi. Sui sistemi Linux viene utilizzato il file \path{/etc/passwd}, il quale contiene l'hash delle varie password. In questi casi è possibile, da parte di un attaccante, tentare attacchi a dizionario sugli hash\footnote{Esistono degli elenchi contenenti le password più comuni, è possibile a questo punto calcolare l'hash di ognuna e confrontarlo con quello nel database per vedere se corrisponde} oppure usare delle rainbow table (vedi appendice~\ref{cap:rainbow}).

Per mitigare il rischio di questi rischi occorre utilizzare delle variazioni casuali nelle password tramite i cosiddetti salt\footnote{È importante che per ogni password e per ogni nuovo cambiamento di password il salt sia diverso}, ovvero stringhe di bit casuale che vengono concatenate alle password prima di calcolarne l'hash da salvare e che vengono memorizzate assieme all'hash stesso. In questo modo viene completante reso inutile l'utilizzo delle rainbow table, dato che per ogni password esistono diverse variazioni, e l'attacco più efficace rimane la forza bruta. Al momento della verifica il sistema prende dal database il salt, lo concatena alla password inserita e verifica che l'hash ottenuto sia quello atteso.

Un ultimo fattore importante da tenere in considerazione nel caso delle password è la robustezza, misurata tramite il concetto di \textbf{entropia}, calcolata come
$$E=\log_{2}{N}$$
dove $N$ è il numero di possibili combinazioni. Tale valore solitamente può essere calcolato come $S^L$, dove $S$ è la dimensione dell'insieme di caratteri tra cui è possibile scegliere ed $L$ è la lunghezza della password. Maggiore è il livello di entropia maggiore è la sicurezza della password stessa. Va comunque notato che un attacco a forza bruta difficilmente richiede la verifica di tutte le password, ma mediamente richiede la verifica della metà di esse.

\medskip
Nei contesti moderni ogni utente è proprietario di decine di account ad ognuno dei quali corrisponde una password diversa che deve essere memorizzata e diventa necessario tenere fortemente in considerazione la reale possibilità che ci sia un leak di password che comprometta l'autenticazione e la sicurezza degli utenti su uno o più siti. Una prima contromisura consiste nell'utilizzare password diverse per ogni sito, facendosi eventualmente aiutare nella memorizzazione da strumenti come password manager. Una seconda contromisura consiste nell'utilizzo di tecniche di autenticazione a più fattori, che hanno come scopo l'utilizzo di almeno due tecniche (distinte) di autenticazione per verificare l'identità di un attore.
\begin{itemize}
    \item MFA (\textit{Multi Factor Authentication}): consiste nel far ripetere più volte la fase di autenticazione all'utente per evitare il problema dell'intercettazione della fase di autenticazione
    \item 2SA (\textit{Two Steps Authentication}): richiede la prova di identità ed il possesso di due segreti differenti. Tuttavia questi sono sempre due segreti che possono essere indovinati o trovati da un attaccante
    \item 2FA (\textit{Two Factor Authentication}): autenticazione a due fattori diversi, questo spesso consiste nel conoscere un segreto e nel possedere un token fisico crittograficamente sicuro (spesso una specie di chiavetta USB, come le yubikey\footnote{Sono dei componenti crittografici che implementano dei protocolli sfida risposta e che consentono di effettuare uno dei passi di autenticazione}). Il livello aggiuntivo di autenticazione fa sì che un attaccante abbia maggior difficoltà nello spacciarsi per un utente
\end{itemize}

Queste tecniche permettono di aumentare la sicurezza anche al fronte del leak del segreto. Ovviamente non sono totalmente sicure e possono essere soggette ad attacchi che coinvolgono direttamente i proprietari degli account, come attacchi di social engineering.

\begin{esempio}{One Time Password}{}
    Nel caso dell'OTP il fattore aggiuntivo è la conoscenza di un codice utilizzabile una singola volta inviato per SMS
\end{esempio}

\begin{esempio}{Timed One Time Password}{}
    Il TOTP si comporta come il OTP, tuttavia il token inviato rimane valido solamente per un periodo limitato di tempo
\end{esempio}

Esistono inoltre degli standard come quello proposto dalla FIDO (\textit{Fast IDentity Online}) alliance composta da aziende come Google e Microsoft e che permettono un'esperienza di autenticazione più semplice e sicura su siti web e servizi mobili. Vi sono proposte come UAF (\textit{Universal Authentication Framework}) e U2F (\textit{Universal Second Authentication}). FIDO UAF, progettato come sostituto dell'autenticazione di base, supporta la possibilità di autenticazione senza password.  In questo standard, un utente che si autentica su un'applicazione o un servizio sfrutterà uno o più fattori di sicurezza sul proprio dispositivo digitale (solitamente un telefono cellulare) per rilasciare una chiave privata che viene utilizzata per firmare una sfida emessa dal server FIDO UAF. Il meccanismo di verifica dell'utente sul dispositivo stesso può essere biometrico, basato sulla conoscenza o sul possesso per sbloccare la chiave privata per le funzioni di firma. FIDO U2F invece rafforza e semplifica la 2FA utilizzando dispositivi USB o bluetooth ed ha come ambito la protezione da phishing e dirottamento delle sessioni. È uno standard di autenticazione aperto che consente agli utenti di accedere in modo sicuro a qualsiasi servizio online con una singola chiave di sicurezza, istantaneamente e senza bisogno di driver o software client.

\subsection{Autenticazione attiva}
In questo caso $P$ prende un ruolo attivo nella fase di autenticazione calcolando di volta in volta un segreto sempre diverso, mentre $V$ memorizza solamente un dato utile a verificare la correttezza di quanto inviato da $P$, rendendo quindi inutili eventuali furti. Occorre tuttavia fare attenzione agli attacchi man in the middle.

\paragraph{S-Key One-Time Password}
Questo protocollo prevede che solo $P$ conosca il segreto $s$. $P$ applica quindi $k$ volte un algoritmo di hash ad $s$ ottenendo $H^k(s)$, che comunica a $V$. Ad ogni autenticazione $P$ invia $H^{k-n}(s)$ a $V$, il quale calcola l'hash del valore ricevuto e lo confronta con $H^{k-n+1}(s)$. Se coincidono l'autenticazione è avvenuta con successo, $V$ memorizza il valore ricevuto sostituendo $H^{k-n+1}(s)$ e viene incrementato il valore di $n$. Una volta che viene utilizzato l'ultimo valore disponibile (ovvero $n=k$), il protocollo deve essere reinizializzato.

L'utilizzo di funzioni di hash ha vari vantaggi tra cui elevata efficienza e unidirezionalità del processo. Inoltre utilizzando un meccanismo simmetrico quale l'hash, questo protocollo è resistente anche nel caso della crittografia quantistica.

\paragraph{Protocolli sfida-risposta}
Il meccanismo di autenticazione attiva basata su sfida e risposta si basa sul proporre sfide alla parte da autenticare la cui soluzione richiede la conoscenza del segreto. Per esempio utilizzando la crittografia asimmetrica $V$ può generare un nonce casuale $k$ e cifrarlo con la chiave pubblica della parte da verificare. Se $P$ è veramente chi afferma di essere sarà in possesso della chiave privata corrispondente e dunque sarà in grado anche di decifrarlo e di inviarlo in chiaro a $V$.

\section{Autorizzazione}
Una volta eseguita l'autenticazione di un soggetto occorre definire le operazioni che questo è autorizzato a svolgere. In generale questo viene fatto in tre passaggi. In primo luogo occorre definire un modello del sistema controllato, successivamente occorre definire quale sia la politica d'accesso alle risorse, ovvero le regole in base alle quale l'accesso viene regolamentato. Infine occorre attuale e mettere in pratica tale politica tramite opportuni meccanismi hardware e software. Questo schema risulta particolarmente utile inoltre anche perché permette di confrontare diverse politiche senza essere sommessi da eventuali dettagli implementativi, per modellare i comportamenti ad un livello di astrazione maggiore o in generale più appropriato oppure per progettare meccanismi modulari e riutilizzabili per diverse politiche.

In generale per quanto riguarda le politiche occorre tener fede al \textbf{principio del minimo privilegio}, ovvero attribuire ad ogni soggetto solo le autorizzazioni necessarie a svolgere il proprio compito. Inoltre occorre tener conto di tre ulteriori fattori:
\begin{itemize}
    \item Coerenza (consistency): lo schema delle autorizzazioni non deve essere ambiguo e deve essere coerente con se stesso. In generale non esiste una soluzione unica, ma occorre studiarne una adatta ad ogni situazione diversa, in particolare in caso di conflitti si potrebbe privilegiare una regola più specifica rispetto ad una più generale, oppure applicare politiche del tipo default deny
    \item Completezza (completeness): qualsiasi richiesta deve poter essere inquadrata all'interno del sistema di regole, pertanto non devono esistere casi non coperti dal sistema stesso (eventualmente anche utilizzando regole di default)
    \item Correttezza (correctness)
\end{itemize}

In generale invece un meccanismo deve essere resistente alle manomissioni, sufficientemente piccolo ed autonomo (ovvero facile da testare e mantenere) e ragionevolmente economico (non ha senso utilizzare un meccanismo che costi più dell'eventuale danno che è in grado di prevenire). Inoltre occorre tenere a mente il \textbf{principio di mediazione completa}, il quale afferma che ogni richiesta debba essere mediata da almeno uno dei meccanismi, il quale dovrà vagliare e validare la richiesta.

Politiche e meccanismi si basano su alcuni parametri generali. In primo luogo occorre conoscere ed essere certi dell'identità del soggetto. In secondo luogo occorre attribuire ad ogni soggetto uno o più ruoli, sulla base dei quali verranno poi prese le decisioni. La decisione riguardante la possibilità di accedere ad un sistema inoltre può essere basata sulle caratteristiche dell'operazione che il soggetto vuole effettuare, del luogo e del momento in cui viene effettuata l'operazione o anche sullo storico delle attività svolte in passato.

\medskip

Di base sono possibili più paradigmi per il controllo dell'accesso:
\begin{itemize}
    \item Discretionary Access Control (DAC): ogni oggetto ha un proprietario che ne decide i permessi. Un esempio sono i permessi dei filesystem Unix e NTFS
    \item Mandatory Access Control (MAC): esiste una policy centralizzata decisa da un security manager che regola gli accessi alle risorse. In questo caso la proprietà di un oggetto non consente di modificarne i permessi. Alcuni esempio sono Apparmor e SELinux (vedi appendice~\ref{cap:selinux})
    \item Role Based Access Control (RBAC): vengono identificati dei ruoli all'interno del sistema a cui vengono assegnati i permessi. Permette di specificare i permessi per una risorsa in maniera molto precisa e contestuale, tuttavia viene solamente spostato il problema riguardante la decisione di quali permessi dare ad un certo soggetto
\end{itemize}

A livello estremamente generale controllare l'accesso ad una risorsa significa controllare chi possa eseguire quale operazione su una certa risorsa. Molto banalmente questo può essere espresso tramite una matrice dei permessi in cui ogni riga si riferisce ad un soggetto ed ogni colonna ad un oggetto. Ogni cella invece rappresenta le operazioni consentite ad un particolare soggetto su di un particolare oggetto. Tipicamente questa matrice è tuttavia sparsa, in quanto ogni soggetto avrà dei permessi di default per la maggior parte delle risorse\footnote{Basti pensare che in una installazione fresca di Linux vi sono una cinquantina di soggetti e milioni di oggetti, non è pensabile che tutti i soggetti possano fare tutto su tutti gli oggetti}. Per questo motivo la matrice tipicamente non viene memorizzata né utilizzata nella sua forma completa, ma viene sempre utilizzata scomponendola.

È possibile avere una scomposizione per righe (ovvero per soggetto), ottenendo un insieme di liste contenenti i permessi per oggetti specifici, ovvero le \textbf{capability list}. Ogni soggetto possiede quindi la propria lista contenente i vari permessi, questo schema risulta efficiente nel caso in cui si impersoni un soggetto e si voglia conoscere immediatamente cosa questo possa fare o non fare. Tipicamente risiede su un file apposito.
È possibile scomporre la matrice per oggetto, ottenendo le \textbf{access control list}, ovvero una lista di soggetti con i relativi permessi per ogni risorsa. In questo caso risulta maggiormente efficiente controllare tutti i permessi relativi ad un singolo file. Tipicamente può essere associata alla risorsa stessa.
È inoltre possibile scomporre la matrice in senso relazionale, costruendo una tabella in cui ogni riga è individuata dalla coppia $(S_i, R_i)$ e contiene i permessi per uno specifico utente relativamente ad una specifica risorsa. Questo schema viene tipicamente usato all'interno dei DBMS per esprimere i permessi dei vari utenti, ma molto meno usato all'interno dei sistemi operativi.
In ogni caso vengono comunque esplicitati solamente quei permessi che sono diversi dal default in modo da risparmiare spazio.

I sistemi POSIX (quindi anche le varie incarnazioni di Unix) e Microsoft implementano entrambi le ACL DAC all'interno dei rispettivi filesystem.

\subsection{DAC nei sistemi Unix}
I sistemi Unix tradizionalmente adottano uno schema ACL rigido in cui i soggetti presenti sono l'utente proprietario (U), il gruppo proprietario (G) ed un gruppo fittizio (O) contenente tutti gli utenti diversi da U e non facenti parte di G. Nei sistemi Unix, inoltre, ogni utente deve appartenere almeno ad un gruppo tanto che, negli ultimi decenni, i sistemi ne creano uno apposito contenente solamente il nuovo utente. Ovviamente però ogni utente può appartenere anche ad un numero variabile di gruppi. Inoltre gli utenti, al fine di garantire maggior protezione, possono esistere in uno stato \textit{locked}, ovvero che impedisce il login, o in uno stato \textit{unlocked}.

Ogni file è descritto da un i-node, all'interno del quale sono indicati utente e gruppo proprietari e le autorizzazioni espresse tramite 12 bit (vedi figura~\ref{fig:permessi_unix}).
\begin{figure}[!htp]
    \begin{center}
        \includegraphics[width=0.6\textwidth]{permessi_unix.png}
        \caption{Permessi standard su sistemi Unix}\label{fig:permessi_unix}
    \end{center}
\end{figure}
Lo sticky bit, ora deprecato, serve per suggerire al sistema di mantenere in cache un eseguibile in modo da non doverlo ricaricare nel momento del bisogno. Nelle directory questo bit è chiamato temp ed impone che i file contenuti all'interno della directory stessa siano eliminabili solamente dal proprietario del file\footnote{Risulta molto utile nelle directory temporanee come /tmp, dove tutti gli utenti possono scrivere e potenzialmente rimuovere file}. I bit SUID e SGID (set user ID e set group ID) servono per richiedere al sistema operativo di impostare come utente e gruppo del processo creato all'esecuzione non utente e gruppo che hanno lanciato l'eseguibile, ma quelli proprietari del file stesso\footnote{Questo viene usato ad esempio da alcuni programmi di sistema che devono però essere eseguibili con particolari privilegi come passwd}. Nel caso delle directory il bit SUID non viene utilizzato, ma SGID permette ad un utente di assumerne temporaneamente come gruppo attivo quello associato alla directory (se ne fa parte), consentendo nelle aree condivise di condividere automaticamente dei file. Il comportamento degli altri nove bit può essere espresso nel seguente modo, ricordando che anche le directory sono file. I bit R esprimono in generale la capacità di leggere un file, quindi per un file regolare si intende la lettura classica, per una directory l'elencazione dei file contenuti all'interno. I bit W consentono la modifica del contenuto di un file, quindi per un file regolare si intende la possibilità di scrivervi all'interno, mentre per una directory la possibilità di creare, rinominare ed eliminare file (anche file su cui l'utente attuale non ha alcun permesso). I bit X consentono l'esecuzione di un file regolare e consentono l'attraversamento di una directory, ovvero di eseguire il lookup completo dell'inode.

Il sistemi Unix applicano questi permessi in un ordine strettamente definito. Per prima cosa viene controllato se l'utente attuale coincide con l'utente proprietario del file, nel qual caso vengono applicati i permessi associati al proprietario. Se l'utente non coincide si passa a controllare il gruppo, nel caso in cui l'utente attuale appartenga al gruppo proprietario del file verranno applicati i relativi permessi. Infine in caso nemmeno l'appartenenza al gruppo sia soddisfatta vengono applicati i permessi generali, quelli associati ad O. Questo schema ha alcune implicazioni controintuitive, ma che consento al sistema di essere completo. In particolare se l'utente è sia proprietario del file sia appartenente al gruppo proprietario verranno applicati i permessi associati ad U e non quelli a G, sebbene potenzialmente essi siano più permissivi.

Per garantire inoltre correttezza e completezza servono degli automatismi in grado di attribuire i permessi all'atto della creazione di un file\footnote{Inteso in senso Unix, può essere un file regolare, una socket, una directory, un link\ldots}. Al file viene associato come proprietario l'utente che ha creato il file, e come gruppo il gruppo attivo del proprietario\footnote{Il gruppo predefinito, quello indicato in \path{/etc/passwd}. Questo può essere modificato mediante l'uso di \path{newgrp}}. Nelle directory il cui SGID è impostato verrà invece impostato come gruppo quello proprietario della directory stessa.\\
Relativamente ai permessi vengono attribuiti tutti quelli sensati, ovvero 666 per i file e 777 per le directory. Ad ogni utente viene inoltre associata una \path{umask}\footnote{Può essere modificata con il comando omonimo}, la quale esprime i permessi da rimuovere all'atto della creazione di un file ed è impostata tipicamente a 066 (ovvero toglie i permessi di scrittura e lettura ad O).

Nei filesystem linux esistono inoltre gli attributi, i quali permettono di esprimere caratteristiche aggiuntive e risultano particolarmente utili nel caso di tuning del fs. Alcuni attributi sono ``compressed'' (c), data ``journaling'' (j), ``undeletable'' (u). Alcuni di essi, come ``immutable'' (i) e ``secure deletion'' (s), sono particolarmente rilevanti per la sicurezza. Questi possono essere impostati o visualizzati mediante l'uso di \path{chattr} e \path{lsattr}.

All'interno dei sistemi POSIX inoltre sono presenti le ACL nel senso più generale, le quali permettono maggior flessibilità nella gestione dei permessi\footnote{Si vedano \path{setfacl} e \path{getfacl}}. Queste consentono di specificare una lista arbitraria di utenti e gruppi coi relativi permessi, ereditare la maschera di creazione della directory e limitare tutti i permessi simultaneamente mediante un'ulteriore maschera.

\medskip
Nei sistemi Unix esiste il concetto di superutente (l'utente \path{root}\footnote{In realtà \path{root} è solamente un nome, un utente è il superutente se ha UID pari a 0}). Questo tipicamente ha privilegi illimitati e può scavalcare i meccanismi di protezione disposti all'interno del sistema. Per questo motivo è estremamente importante proteggere questo account. Questo può essere fatto usando account non privilegiati fintanto che è possibile, in quanto per la maggior parte delle operazioni non è necessario possedere il massimo dei privilegi, disabilitando l'accesso a questo account sia da GUI che da console, impedendo quindi di eseguire il login con questo utente, e ottenendone temporaneamente i privilegi usando altre tecniche come \path{sudo} in Linux.

In realtà i poteri di \path{root} non sono monolitici, ma sono costituiti da 41 capability (da non confondere con le capability list). Queste rappresentano tutte le autorizzazioni normalmente negate agli utenti standard e riguardano molteplici aspetti associati al controllo delle risorse di calcolo e fisiche\footnote{Ad esempio consentono di interagire con pacchetti di rete grezzi senza passare dallo stack TCP/IP}. È possibile assegnare alcune di queste capacità anche ad utenti standard in modo che possano eseguire compiti particolari senza però possederle tutte e 41 (vedi principio del minimo privilegio).

\subsection{DAC nei sistemi Windows}
Nel mondo Microsoft il controllo dell'accesso è più complesso. Questo lo rende più flessibile ma anche più complesso da gestire sia per utenti normali sia per amministratori di sistema.

Nell'uso più comune, i sistemi Microsoft sono raggruppati in un dominio, ovvero un insieme di computer comunicanti tra loro e che condividono un directory database comune, all'interno del quale sono memorizzati dati di vario tipo tra cui i computer che fanno parte del dominio, le risorse condivise, gli utenti validi sul dominio ed i gruppi di utenti.

Gli utenti quindi si dividono tra utenti locali, utilizzabili solamente all'interno del computer su cui sono stati creati, e utenti di dominio, ovvero utenti appartenenti ad un dominio il cui profilo è memorizzato su una directory di rete (tipicamente Active Directory, implementazione simile ad LDAP) e che possono accedere anche a risorse non locali.

Ogni soggetto può far parte di uno o più gruppi (del tipo e scope appropriati). Si distingue tra ``distribution group'', i quali possono essere usati da una qualsiasi applicazione che abbia bisogno di una lista di utenti, ed i ``security group'', i quali estendono i DG potendo anche essere usati nelle regole che controllano la sicurezza. Per entrambi i tipi di gruppi vale il concetto di scope, il quale definisce quali oggetti di quali domini possano entrare a far parte del gruppo e in quali domini possa essere usato un gruppo. Una prima distinzione si ha tra gruppi locali e gruppi di dominio.

Questo sistema permette di dividere le responsabilità all'interno di un'organizzazione. Ad esempio separando chi conosce gli utenti e chi conosce le risorse, così facendo è possibile ottenere un mapping tra le due conoscenze per mezzo dei gruppi.
\begin{esempio}{}{}
    L'amministratore di sistema può conoscere tutte le risorse disponibili e creare dei gruppi che consentano di gestirle. I vari direttori del personale o delle varie divisioni poi possono riempire i gruppi con i vari utenti di loro conoscenza
\end{esempio}

\medskip
Nel filesystem NTFS\footnote{FAT e FAT32 non hanno il concetto di permessi} vengono implementate le ACL, sebbene in maniera differente rispetto a quanto accade nei fs Linux. Questo sistema di controllo ha due obiettivi principali. In primo luogo garantire un'elevata precisione nel controllo dell'accesso sia in termini di azioni che è possibile negare o consentire sia in termini di relazioni tra utenti e gruppi titolari delle relazioni. In secondo luogo garantire una certa facilità d'uso sia per il personale tecnico che per il personale non tecnico.

Al posto del semplice RWX di Linux si hanno tredici diversi tipi di autorizzazioni, tra cui ``traverse folder/execute file'', ``list folder/read data'', ``create files/write data'', ``change permissions'', ``read extended attributes''. Per ognuna di queste esistono due flag, ``allow'' e ``deny'', a seconda delle cui combinazioni viene usata una diversa politica\footnote{Windows segue un modello del tipo default deny}:
\begin{itemize}
    \item Allow selezionato: accesso garantito
    \item Deny selezionato: accesso impedito in maniera forte (non scavalcabile)
    \item Sia Allow che Deny selezionati: accesso impedito (Deny vince)
    \item Niente selezionato: accesso impedito in maniera debole (scavalcabile)
\end{itemize}
Sebbene questo sembri inutile in realtà risulta molto utile dato che un utente può appartenere a più gruppi diversi. Nel momento in cui viene effettuato l'accesso ad un file vengono cercate le ACL associate a tutti i gruppi di cui fa parte l'utente e per ogni tipo di autorizzazione vengono messi in OR logico i flag. A seconda del risultato della combinazione vengono determinate le politiche di accesso.

\begin{esempio}{}{}
    Supponiamo che un utente appartenga ai gruppi $G_1$ e $G_2$. Relativamente alla proprietà ``traverse folder/execute file'' $G_1$ è impostato come ``Allow'', mentre $G_2$ non ha nulla impostato. Questi flag vengono messi in OR logico ottenendo $``Allow''=1$ e $``Deny''=0$, di conseguenza l'accesso è consentito
\end{esempio}

\begin{esempio}{}{}
    Supponiamo che un utente appartenga ai gruppi $G_1$ e $G_2$. Relativamente alla proprietà ``traverse folder/execute file'' $G_1$ è impostato come ``Deny'', mentre $G_2$ non ha nulla impostato. Questi flag vengono messi in OR logico ottenendo $``Allow''=0$ e $``Deny''=1$, di conseguenza l'accesso è negato
\end{esempio}

\begin{esempio}{}{}
    Supponiamo che un utente appartenga ai gruppi $G_1$ e $G_2$. Relativamente alla proprietà ``traverse folder/execute file'' $G_1$ è impostato come ``Allow'', mentre $G_2$ è impostato come ``Deny''. Questi flag vengono messi in OR logico ottenendo $``Allow''=1$ e $``Deny''=1$, di conseguenza l'accesso è negato
\end{esempio}

Normalmente gli utenti interagiscono però attraverso un'interfaccia semplificata, la quale non espone tutte e tredici le proprietà, ma espone sei diversi profili (``full control'', ``modify'', ``read \& execute'', ``list folder content'', ``read'', ``write'') ai quali corrispondono diverse combinazioni delle proprietà e non sono mutuamente esclusivi. Esiste infine un profilo speciale chiamato ``no access'', il quale corrisponde a mettere ``deny'' su tutte le proprietà.

\subsection{Mandatory access control}
In questo caso le regole di accesso sono dettate da un'autorità centrale, senza che i soggetti possano apportarvi modifiche in alcun modo. Ad ogni soggetto o risorsa viene assegnata una classe di accesso che tipicamente specifica un \textbf{livello di sicurezza}, ovvero un valore all'interno di un insieme ordinato, ed una \textbf{categoria}, ovvero un valore all'interno di un insieme non ordinato che riflette le aree funzionali del sistema. I livelli di sicurezza associati alle risorse rappresentano la sensibilità delle informazioni, ossia la gravità delle conseguenze associate ad una violazione di policy, mentre quelli associati ai soggetti rappresentano l'affidabilità (clearance) del soggetto stesso.

Una volta associati questi valori ad ogni utente e risorsa è possibile esprimere la dominanza di una classificazione $L_1=<S_1, C_1>$ su di un'altra $L_2=<S_2,C_2>$ (dove $S_i$ è il livello di sicurezza e $C_i$ è un insieme di categorie). $L_1$ domina su $L_2$ se e solo se
\[
    S_1 \ge S_2,\ C_1 \supseteq C_2
\]
Le relazioni di dominanza vanno utilizzate in maniera differente a seconda della proprietà da proteggere, in particolare esiste il modello Bell-La Padula per proteggere la riservatezza ed il modello Biba per proteggere l'integrità. L'applicazione simultanea di due modelli è possibile, ma assegnando classi di accesso diverse, una per ogni proprietà da controllare.

\paragraph{Modello Bell-La Padula}
Progettato per realizzare la sicurezza in ambiente militare, garantendo la confidenzialità delle informazioni, è stato concepito per mantenere i segreti e non per garantire l'integrità dei dati. Questo modello associa a un modello di protezione due regole di sicurezza MAC che stabiliscono la direzione di propagazione delle informazioni nel sistema:
\begin{itemize}
    \item Proprietà di semplice sicurezza (no-read-up): un soggetto al livello di sicurezza $k$ può leggere solo oggetti al suo livello o a quelli inferiori (no-read-up), ovvero se la sua classe di accesso domina la classe di accesso della risorsa, altrimenti leggerebbe
          una risorsa troppo sensibile per il suo livello
    \item Proprietà di integrità * (no-write-down): un processo in esecuzione al livello di sicurezza $k$ può scrivere solo oggetti al suo livello o a quelli superiori, ovvero se la sua classe di accesso è dominata dalla classe di accesso della risorsa, altrimenti potrebbe far trapelare un segreto in luoghi accessibili a soggetti con minor clearance. Questo accade perché se un soggetto è a conoscenza di informazioni sensibili deve avere la possibilità di proteggerle, tuttavia non deve mai avere la possibilità di scrivere informazioni sensibili ad un livello più basso, altrimenti avverrebbe un leak
\end{itemize}

\paragraph{Modello Biba}
Questo modello a differenza di quello Bell-La Padula è stato concepito per garantire l'integrità dei dati, e come l'altro modello si basa su 2 regole di sicurezza:
\begin{itemize}
    \item Proprietà di semplice sicurezza (no-write-up): un soggetto al livello di sicurezza $k$ può scrivere solo oggetti al suo livello o a quelli inferiori, ovvero se la sua classe di accesso domani la classe di accesso della risorsa , altrimenti modificherebbe una risorsa troppo sensibile per il suo livello
    \item Proprietà di integrità * (no-read-down): un soggetto al livello $k$ può leggere solo oggetti al suo livello o a quelli superiori, ovvero se la sua classe di accesso è dominata dalla classe di accesso della risorsa, altrimenti utilizzerebbe informazioni meno attendibili al proprio livello di fiducia più elevato
\end{itemize}

\subsection{Role based access control}
In questo caso le autorizzazioni sono concesse a dei ruoli e non a degli utenti direttamente. Ogni utente può ricoprire più ruoli allo stesso tempo, i quali possono anche cambiare nel tempo o secondo il contesto specifico in cui sta operando. Nel momento in cui la modellazione del sistema è stata fatta correttamente questo tipo di politica risulta molto efficace in quanto permette di modellare un grande numero di situazioni possibili e di esprimere tutti i principi fondamentali per la sicurezza come minimo privilegio, separazione delle responsabilità ed astrazione. Inoltre se le autorizzazioni sono ben modellate queste non hanno necessità di cambiare granché nel tempo, in quanto le autorizzazioni, ad esempio, associate ad un dirigente saranno sempre all'incirca le stesse, a prescindere da chi o cosa ricopre quel ruolo.
