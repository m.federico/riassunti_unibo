﻿const int MAX_VALUE = int.MaxValue;
const int MIN_VALUE = int.MinValue;

// O(1)
int SafeAdd(int a, int b)
{
    if ((a >= 0 && b >= (MAX_VALUE - a)) || (a < 0 && b <= (MIN_VALUE - a)))
    {
        throw new ArgumentException();
    }

    return a + b;
}

int SafeIncrement(int num)
{
    return SafeAdd(num, 1);
}

int SafeDecrement(int num)
{
    return SafeAdd(num, -1);
}

int SafeSubtract(int a, int b)
{
    return SafeAdd(a, -b);
}

// O(1) - facciamo finta che sia così
int SafeMultiply(int a, int b)
{
    if (((a > 0 && b > 0) || (a < 0 && b < 0)) && b >= (MAX_VALUE / a))
    {
        throw new ArgumentException();
    }

    if (((a > 0 && b < 0) || (a < 0 && b > 0)) && b <= (MIN_VALUE / a))
    {
        throw new ArgumentException();
    }

    return a * b;
}

// O(n) -> Sarebbe O(M(n)) dove M è la complessità della moltiplicazione, che in questo caso supponiamo essere lineare
int SafeFactiorial(int n)
{
    int acc = 1;
    for (int idx = 1; idx <= n; idx = SafeIncrement(idx))
    {
        acc = SafeMultiply(acc, idx);
    }

    return acc;
}


// O(n)
int BinomialCoefficient(int n, int k)
{
    if (n <= 0 || k <= 0 || k > n)
    {
        throw new ArgumentException();
    }

    return SafeFactiorial(n) / SafeMultiply(SafeFactiorial(k), SafeSubtract(n, k));
}