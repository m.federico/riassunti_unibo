﻿using System.Globalization;

namespace Es1
{

    public class Rational
    {
        private int _num;
        private int _den;

        public Rational(int num, int den)
        {
            if (den == 0)
            {
                throw new ArgumentException(nameof(den));
            }
            var posNum = num >= 0 ? num : -num;
            var posDen = den >= 0 ? den : -den;
            var gcd = Gcd(posNum, posDen);

            _num = num / gcd;
            _den = den / gcd;
        }

        public Rational Mult(Rational other)
        {
            return new Rational(this._num * other._num, this._den * other._den);
        }

        public Rational Mult(int coeff)
        {
            return new Rational(this._num * coeff, this._den);
        }

        private int Gcd(int a, int b)
        {
            if (b > a)
            {
                var tmp = a;
                a = b;
                b = tmp;
            }

            if (a % b == 0)
            {
                return b;
            }

            return Gcd(b, a % b);
        }

        public override string ToString()
        {
            if (_den == 1)
            {
                return _num.ToString();
            }
            return $"{_num}/{_den}";
        }
    }

    public class Program
    {
        public static void Main()
        {
            var r1 = new Rational(1, 3);
            Console.WriteLine(r1.ToString());

            var r2 = new Rational(2, 3);
            Console.WriteLine(r2.ToString());

            var r3 = new Rational(3, 3);
            Console.WriteLine(r3.ToString());

            Console.WriteLine($"{r1.Mult(r2)}");
        }
    }
}