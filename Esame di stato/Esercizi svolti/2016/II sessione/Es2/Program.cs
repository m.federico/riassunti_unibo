﻿

class Matrix : IEquatable<Matrix>
{
    public const int SIDE_LENGTH = 19;
    private int[,] _inner;

    private Matrix(int[,] inner)
    {
        _inner = inner;
    }

    public static Matrix Empty()
    {
        return new Matrix(new int[SIDE_LENGTH, SIDE_LENGTH]);
    }

    public int this[int r, int c]
    {
        get
        {
            return _inner[r, c];
        }
        set
        {
            _inner[r, c] = value;
        }
    }

    /// <summary>
    /// Scambia le cifre 1 e 2
    /// </summary>
    /// <returns></returns>
    public Matrix SwapDigits()
    {
        Matrix res = Matrix.Empty();

        for (int r = 0; r < SIDE_LENGTH; r++)
        {
            for (int c = 0; c < SIDE_LENGTH; c++)
            {
                if (_inner[r, c] == 1)
                {
                    res[r, c] = 2;
                }
                else if (_inner[r, c] == 2)
                {
                    res[r, c] = 1;
                }
                else
                {
                    res[r, c] = 0;
                }
            }
        }

        return res;
    }

    public Matrix SwapRows()
    {
        Matrix res = Matrix.Empty();

        for (int r = 0; r < SIDE_LENGTH; r++)
        {
            for (int c = 0; c < SIDE_LENGTH; c++)
            {
                res[SIDE_LENGTH - 1 - r, c] = _inner[r, c];
            }
        }

        return res;
    }

    public Matrix SwapColumns()
    {
        Matrix res = Matrix.Empty();

        for (int r = 0; r < SIDE_LENGTH; r++)
        {
            for (int c = 0; c < SIDE_LENGTH; c++)
            {
                res[r, SIDE_LENGTH - 1 - c] = _inner[r, c];
            }
        }

        return res;
    }

    public override bool Equals(object? obj)
    {
        return Equals(obj as Matrix);
    }

    public bool Equals(Matrix? other)
    {
        if (other == null)
        {
            return false;
        }

        var Check = (Matrix theOther) => InnerEquals(theOther) ||
                InnerEquals(theOther.SwapRows()) ||
                InnerEquals(theOther.SwapRows().SwapColumns()) ||
                InnerEquals(theOther.SwapColumns());

        return Check(other) || Check(other.SwapDigits());
    }

    public override int GetHashCode()
    {
        var results = new int[8];

        results[0] = this._inner.GetHashCode();
        results[1] = this.SwapRows()._inner.GetHashCode();
        results[2] = this.SwapRows().SwapColumns()._inner.GetHashCode();
        results[3] = this.SwapColumns()._inner.GetHashCode();
        results[4] = this.SwapDigits()._inner.GetHashCode();
        results[5] = this.SwapDigits().SwapRows()._inner.GetHashCode();
        results[6] = this.SwapDigits().SwapRows().SwapColumns()._inner.GetHashCode();
        results[7] = this.SwapDigits().SwapColumns()._inner.GetHashCode();

        return results.Order().ToArray().GetHashCode();
    }

    private bool InnerEquals(Matrix m)
    {
        for (int r = 0; r < SIDE_LENGTH; r++)
        {
            for (int c = 0; c < SIDE_LENGTH; c++)
            {
                if (m[r, c] != _inner[r, c])
                {
                    return false;
                }
            }
        }

        return true;
    }
}
