﻿uint[] GetOrderedArray(uint n)
{
    var tmp = n;
    if (n == 0)
    {
        return new uint[] { 0 };
    }

    var res = new uint[n + 1];
    for (int idx = 0; idx <= n; idx++)
    {
        res[idx] = tmp;
        tmp--;
    }

    return res;
}

