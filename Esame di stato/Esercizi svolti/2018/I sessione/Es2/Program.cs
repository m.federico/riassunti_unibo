﻿int[,]? Process(int[,] A, int[,] B, uint D)
{
    if (A.GetLength(0) != B.GetLength(0) || A.GetLength(0) == 0 || B.GetLength(0) == 0)
    {
        return null;
    }

    if (A.GetLength(1) != B.GetLength(1))
    {
        return null;
    }

    int[,] C = new int[A.GetLength(0), A.GetLength(1)];

    for (int r = 0; r < A.GetLength(0); r++)
    {
        for (int c = 0; c < A.GetLength(1); c++)
        {
            var d = Enumerable
                        .Range(0, (int)D + 1)
                        .Select(d => new { k = d, V = r - d >= 0 ? Math.Abs(A[r - d, c] - B[r - d, c]) : int.MaxValue })
                        .OrderBy(kvp => kvp.V)
                        .FirstOrDefault()?.k ?? 0;
            C[r, c] = d;
        }
    }

    return C;
}