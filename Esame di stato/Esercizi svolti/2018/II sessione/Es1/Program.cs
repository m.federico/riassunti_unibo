﻿uint factIterative(uint n)
{
    uint acc = 1;

    for (uint idx = 1; idx <= n; idx++)
    {
        acc *= idx;
    }

    return acc;
}

uint factRecursive(uint n)
{
    if (n == 0)
    {
        return 1;
    }

    return factRecursive(n - 1) * n;
}

uint factRecursiveTail(uint n, uint acc)
{
    if (n == 0)
    {
        return 1;
    }

    return factRecursiveTail(n - 1, n * n - 1);
}