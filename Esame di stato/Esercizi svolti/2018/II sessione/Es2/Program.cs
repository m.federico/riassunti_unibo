﻿using System.Security.Cryptography;

class Program
{
    bool TryNaiveSmooth(in ushort[,] inputImage, out ushort[,] outputImage, in int k)
    {
        var w = inputImage.GetLength(0);
        var h = inputImage.GetLength(1);

        outputImage = new ushort[w, h];

        if (k > w || k > h)
        {
            return false;
        }

        if (k % 2 == 0)
        {

            return false;
        }

        for (var r = 0; r < w; r++)
        {
            for (var c = 0; c < h; c++)
            {
                ushort sum = 0;
                ushort count = 0;
                for (var i = Math.Max(0, r - (k - 1) / 2); i < Math.Min(w, r + (k - 1) / 2); i++)
                {
                    for (var j = Math.Max(0, c - (k - 1) / 2); j < Math.Min(w, c + (k - 1) / 2); j++)
                    {
                        sum += inputImage[i, j];
                        count++;
                    }
                }

                outputImage[r, c] = (ushort)(sum / count);
            }
        }

        return true;
    }


    bool TryIntegralSmooth(in ushort[,] inputImage, out ushort[,] outputImage, in int k)
    {
        var w = inputImage.GetLength(0);
        var h = inputImage.GetLength(1);
        var radius = (k - 1) / 2;

        outputImage = new ushort[w, h];

        var integral = new ushort[w, h];

        if (k > w || k > h)
        {
            return false;
        }

        if (k % 2 == 0)
        {

            return false;
        }

        for (var r = 0; r < h; r++)
        {
            for (var c = 0; c < w; c++)
            {
                ushort sum = inputImage[r, c];
                var rPos = r - 1 >= 0;
                var cPos = c - 1 >= 0;

                if (rPos)
                {
                    sum += integral[r - 1, c];
                }

                if (cPos)
                {
                    sum += integral[r, c - 1];
                }

                if (rPos && cPos)
                {
                    sum -= integral[r - 1, c - 1];
                }

                integral[r, c] = sum;
            }
        }

        for (var r = 0; r < h; r++)
        {
            for (var c = 0; c < w; c++)
            {
                var count = (Math.Min(h - 1, r + radius) + 1 - Math.Max(0, r - radius)) * (Math.Min(w - 1, c + radius) + 1 - Math.Max(0, c - radius));

                var cR = Math.Min(h - 1, r + radius);
                var cC = Math.Min(w - 1, c + radius);

                // C - B - D + A
                var sum = integral[cR, cC];

                if (r - radius >= 0 && c - radius >= 0)
                {
                    // A è contenuto nella tabella
                    var aR = Math.Max(0, r - radius);
                    var aC = Math.Max(0, c - radius);

                    sum += integral[aR, aC];
                }

                if (r + radius < h && c - radius >= 0)
                {
                    // B è contenuto nella tabella
                    var bR = Math.Min(h - 1, r + radius);
                    var bC = Math.Max(0, c - radius);

                    sum += integral[bR, bC];
                }

                if (r + radius < h && c - radius >= 0)
                {
                    // D è contenuto nella tabella
                    var dR = Math.Min(h - 1, r + radius);
                    var dC = Math.Max(0, c - radius);

                    sum += integral[dR, dC];
                }

                outputImage[r, c] = (ushort)(sum / count);
            }
        }
        return true;
    }
}