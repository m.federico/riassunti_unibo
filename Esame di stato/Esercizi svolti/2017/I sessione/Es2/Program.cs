﻿using System.Runtime.CompilerServices;

abstract class Function<T, R>
{
    public abstract R Apply(T input);
}

class Add : Function<float, float>
{
    private float _n;

    public Add(float n)
    {
        _n = n;
    }

    public override float Apply(float input)
    {
        return _n + input;
    }
}

class Mult : Function<float, float>
{
    private float _n;

    public Mult(float n)
    {
        _n = n;
    }

    public override float Apply(float input)
    {
        return _n * input;
    }
}

class Composite<T1, T2, R> : Function<T1, R>
{
    private Function<T1, T2> _first;
    private Function<T2, R> _second;

    public Composite(Function<T1, T2> firstApplication, Function<T2, R> secondApplication)
    {
        _first = firstApplication;
        _second = secondApplication;
    }

    public override R Apply(T1 input)
    {
        return _second.Apply(_first.Apply(input));
    }
}

class Linear : Function<float, float>
{
    private Composite<float, float, float> _comp;

    public Linear(float m, float q)
    {
        _comp = new Composite<float, float, float>(new Mult(m), new Add(q));
    }

    public override float Apply(float input)
    {
        return _comp.Apply(input);
    }
}

class Greater : Function<float, bool>
{
    private float _par;

    public Greater(float par)
    {
        _par = par;
    }

    public override bool Apply(float input)
    {
        return input > _par;
    }
}

class IfFunction<T, R> : Function<T, R>
{
    private Function<T, bool> _condition;
    private Function<T, R> _thenFunction;
    private Function<T, R> _elseFunction;

    public IfFunction(Function<T, bool> condition, Function<T, R> thenFunction, Function<T, R> elseFunction)
    {
        _condition = condition;
        _thenFunction = thenFunction;
        _elseFunction = elseFunction;
    }

    public override R Apply(T input)
    {
        return _condition.Apply(input) ? _thenFunction.Apply(input) : _elseFunction.Apply(input);
    }
}

class Test
{
    public static void Main()
    {
        var requestFunction = new IfFunction<float, float>(new Greater(0), new Add(-1), new Linear(0.1f, -1));

        Console.WriteLine(requestFunction.Apply(5));
    }
}