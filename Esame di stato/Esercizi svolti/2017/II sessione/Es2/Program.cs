﻿interface ILogicalExpression
{
    bool? Evaluate();
}

class TrueLE : ILogicalExpression
{
    public bool? Evaluate() => true;
}

class FalseLE : ILogicalExpression
{
    public bool? Evaluate() => false;
}

class NotLE : ILogicalExpression
{
    private ILogicalExpression? _argument = null;

    public void SetArgument(ILogicalExpression expr)
    {
        _argument = expr;
    }

    public bool? Evaluate() => !_argument?.Evaluate();
}

class IsLE : ILogicalExpression
{
    private ILogicalExpression? _argumentA = null;
    private ILogicalExpression? _argumentB = null;

    public void SetArgumentA(ILogicalExpression expr)
    {
        _argumentA = expr;
    }

    public void SetArgumentB(ILogicalExpression expr)
    {
        _argumentB = expr;
    }

    public bool? Evaluate()
    {
        if (_argumentA is null || _argumentB is null)
        {
            return null;
        }
        return (_argumentA.Evaluate() == _argumentB.Evaluate()) || (!_argumentA.Evaluate() == !_argumentB.Evaluate());
    }
}

class XorLE : ILogicalExpression
{
    private ILogicalExpression? _argumentA = null;
    private ILogicalExpression? _argumentB = null;

    public void SetArgumentA(ILogicalExpression expr)
    {
        _argumentA = expr;
    }

    public void SetArgumentB(ILogicalExpression expr)
    {
        _argumentB = expr;
    }

    public bool? Evaluate()
    {
        if (_argumentA is null || _argumentB is null)
        {
            return null;
        }

        var isle = new IsLE();
        isle.SetArgumentA(_argumentA);
        isle.SetArgumentB(_argumentB);

        var not = new NotLE();
        not.SetArgument(isle);

        return not.Evaluate();
    }
}

class Program
{
    public static void Main()
    {
        var n = new NotLE();
        var x = new IsLE();
        x.SetArgumentA(x);
        x.SetArgumentB(n);
        n.SetArgument(x);

        x.Evaluate();
    }
}